/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

/* struct to keep log entries resulting from iteration over an existing log
 * block during rewrite
 *
 * Used for two purposes:
 *
 * - For objects, because we iterate backwards and need to collect all _REG_
 *   entries before an _OBJ_ entry, we keep a todo pointer and a number of
 *   entries which still need to be copied, IFF we decide to keep the object.
 *
 * - A ban export can be a single DLE (_REG) or multiple (_IMM) which we
 *   hold until the end of the iteration in order to write them last
 *
 * On the logmtx:
 *
 * As soon as we resurrect an object, the exp thread or festash_work_fes() could
 * free it via Deref, which obviously must not happen before we have even added
 * the object to the log. So whenever we call the resurrect function, we take
 * the logmtx and unlock after the log submission.
 */

struct iter_out {
	unsigned magic;
#define ITER_OUT_MAGIC		0x1ab1ede0
	// variable
	uint16_t nused;
	uint16_t ntodo;
	struct fellow_dle entry[FELLOW_DISK_LOG_BLOCK_ENTRIES];
	pthread_mutex_t *mtx;
	const struct fellow_dle *todo;
	// fixed
	const struct flics *flics;
};

//lint -e{454} mutex
static inline void
iter_out_lock(struct iter_out *it)
{

	if (it->flics->ffd->phase != FP_INIT || it->mtx != NULL)
		return;

	it->mtx = &it->flics->ffd->logmtx;
	AZ(pthread_mutex_lock(it->mtx));
}

//lint -e{455} mutex
static inline void
iter_out_unlock(struct iter_out *it)
{

	// must be submitted
	AZ(it->nused);
	if (it->mtx == NULL)
		return;
	AZ(pthread_mutex_unlock(it->mtx));
	it->mtx = NULL;
}

static inline void
iter_out_init(struct iter_out *it, const struct flics *flics)
{
	INIT_OBJ(it, ITER_OUT_MAGIC);
	it->flics = flics;
}

static inline void
iter_out_fini(struct iter_out *it)
{

	CHECK_OBJ_NOTNULL(it, ITER_OUT_MAGIC);

	// must be cleared or committed
	AZ(it->ntodo);
	AZ(it->todo);

	iter_out_unlock(it);
	iter_out_init(it, it->flics);
}

static inline void
iter_out_clear(struct iter_out *it)
{
	CHECK_OBJ_NOTNULL(it, ITER_OUT_MAGIC);
	it->ntodo = 0;
	it->todo = NULL;
}

static inline int
iter_out_resurrect(const struct iter_out *it, const struct fellow_dle *e)
{
	CHECK_OBJ_NOTNULL(it, ITER_OUT_MAGIC);
	AN(it->flics);
	AN(it->flics->resur_f);

	switch (e->type) {
	case DLE_OBJ_ADD:
		if (! it->flics->resur_f(it->flics->resur_priv, e))
			return (0);
		break;
	case DLE_OBJ_CHG:
		WRONG("DLE_OBJ_CHG in iter_out_resurrect");
		break;
	default:
		break;
	}
	return (1);
}

static void
iter_out_take(buddy_t *dskbuddy, const struct fellow_dle *e, unsigned n)
{
	unsigned u = 0, max = n * DLE_REG_NREGION;
	struct buddy_off_extent totake[max];
	off_t off;
	size_t sz;

	memset(totake, 0, sizeof totake);

	for (; n > 0; n--, e++) {
		switch (e->type) {
		case DLE_OBJ_ADD:
			assert(u < max);
			totake[u++] = BUDDY_OFF_EXTENT(
			    fdb_off(e->u.obj.start),
			    fdb_size(e->u.obj.start));
			break;
		case DLE_REG_ADD:
			DLE_REG_FOREACH(e, off, sz) {
				assert(u < max);
				totake[u++] = BUDDY_OFF_EXTENT(off, sz);
			}
			break;
		case DLE_OBJ_CHG:
			WRONG("DLE_OBJ_CHG in iter_out_take");
			break;
		default:
			break;
		}
	}
	buddy_take_off_extent(dskbuddy, totake, u);
}

static inline void
iter_out_commit(struct iter_out *it)
{
	const struct fellow_dle *e;

	CHECK_OBJ_NOTNULL(it, ITER_OUT_MAGIC);
	AN(it->flics);
	AN(it->flics->ffd);

	if (it->ntodo == 0 || it->todo == NULL) {
		AZ(it->ntodo);
		AZ(it->todo);
		return;
	}

	e = it->todo + (it->ntodo - 1);

	assert(DLE_TYPE(e->type) == DLE_T_OBJ);
	if (it->flics->ffd->phase == FP_INIT) {
		iter_out_lock(it);

		if (! iter_out_resurrect(it, e)) {
			iter_out_clear(it);
			return;
		}
		iter_out_take(it->flics->ffd->dskbuddy, it->todo, it->ntodo);
	}

	assert(it->nused + it->ntodo <= FELLOW_DISK_LOG_BLOCK_ENTRIES);
	memcpy(&it->entry[it->nused], it->todo,
	    it->ntodo * sizeof *it->todo);
	it->nused += it->ntodo;
	iter_out_clear(it);
}

/* submit commited to an lbuf */
static inline void
iter_out_submit(struct iter_out *it, struct fellow_logbuffer *lbuf)
{
	struct fellow_dle *entry;
	uint16_t nused;

	CHECK_OBJ_NOTNULL(it, ITER_OUT_MAGIC);

	if (it->nused > 0 && it->flics->ffd->phase != FP_FINI) {
		iter_out_lock(it);
		// local variables for flexelint value tracking
		entry = it->entry;
		nused = it->nused;
		assert(nused <= FELLOW_DISK_LOG_BLOCK_ENTRIES);
		fellow_privatelog_submit(it->flics->ffd, lbuf, entry, nused);
		it->nused = 0;
	}

	iter_out_fini(it);
}

// start a new _OBJ_ entry
static inline void
iter_out_start(struct iter_out *it, const struct fellow_dle *e)
{
	CHECK_OBJ_NOTNULL(it, ITER_OUT_MAGIC);

	AZ(it->ntodo);
	AZ(it->todo);

	it->todo = e;
	it->ntodo = 1;
}

// add a _REG_ entry, but only if started
static inline void
iter_out_add_reg(struct iter_out *it, const struct fellow_dle *e)
{
	CHECK_OBJ_NOTNULL(it, ITER_OUT_MAGIC);

	assert(DLE_TYPE(e->type) == DLE_T_REG);

	AN(it->todo);
	assert(e + 1 == it->todo);
	it->todo = e;
	it->ntodo++;
}

/*
 * (try to) save a number of entries - start/add_reg/commit in one go
 *
 * returns 1 if successful, 0 otherwise (in which case can submit)
 */

static inline int
iter_out_save(struct iter_out *it, const struct fellow_dle *e, uint16_t n)
{
	CHECK_OBJ_NOTNULL(it, ITER_OUT_MAGIC);

	if (n + it->nused > FELLOW_DISK_LOG_BLOCK_ENTRIES)
		return (0);

	// can not be used with obj/reg
	AZ(it->todo);
	AZ(it->ntodo);

	memcpy(&it->entry[it->nused], e, sizeof *e * n);
	it->nused += n;
	return (1);
}
