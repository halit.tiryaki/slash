/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <assert.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>

#include "miniobj.h"

#include "buddy_tune.h"
#include "buddy_util.h"

/* initialize with sane defaults */
const char *
stvbu_tune_init(struct stvbu_tune *tune, size_t sz)
{
	INIT_OBJ(tune, STVBU_TUNE_MAGIC);

//lint -e773,
#define TUNE(t, n, d, min, max)			\
	tune->n = (d)
#include "tbl/buddy_tunables.h"
//lint +e773,
	tune->sz = sz;
	return (stvbu_tune_check(tune));
}

// XXX VSL for warnigs

const char *
stvbu_tune_check(struct stvbu_tune *tune)
{
	unsigned l;
	size_t sz;

	l = log2down(tune->sz) - 4;
	if (tune->chunk_exponent > l) {
		fprintf(stderr,"buddy: chunk_exponent limited to %u "
		    "(less than 1/16 of memory size)\n", l);

		tune->reserve_chunks <<= (tune->chunk_exponent - l);
		tune->chunk_exponent = l;
	}

	sz = tune->sz >> (tune->chunk_exponent - 1);
	assert(sz <= UINT_MAX);
	l = (unsigned)sz;
	if (tune->reserve_chunks > l) {
		fprintf(stderr,"buddy: reserve_chunks limited to %u "
		    "(less than 1/8 of memory size)\n", l);
		tune->reserve_chunks = l;
	}

//lint --e{685,568} misc const comparisons
#define TUNE(t, n, d, min, max)						\
	if (tune->n < (min))						\
		return ("Value of " #n " is too small, minimum is " #min); \
	if (tune->n > (max))						\
		return ("Value of " #n " is too big, maximum is " #max); \

#include "tbl/buddy_tunables.h"
	return (NULL);
}
