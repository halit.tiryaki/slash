/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#ifdef FCERR_INJECT

#ifdef HAVE_STDATOMIC_H
#include <stdatomic.h>
#elif HAVE_ATOMIC_H
#include <atomic.h>
#define atomic_int volatile uint_t
#define atomic_load(x) (*(x))
#define atomic_store(x, i) *(x) = i
#define atomic_fetch_add(x, i) (atomic_add_int_nv(x, i) - i)
// XXX TODO actually not related to stdatomic
#define _Thread_local
#else
#error Need some atomic implementation
#endif

_Thread_local char errinjbuf[256];

struct errinject {
	atomic_int count;
	int        fail;
};

/* XXX static limits testing to a single translation
 * unit - fine for now, but to be revisited.
 */
static struct errinject errinject;

static inline int
fc_inj_injected(const char *func, int line)
{
	int v = atomic_fetch_add(&errinject.count, 1) + 1;

	int r = v == errinject.fail;

	fprintf(stderr, "%s %d = %s:%d\n", r ? "HIT" : "inj",
	    v, func, line);
	return (r);
}

static inline int
fc_inj_count(void)
{
	return (atomic_load(&errinject.count));
}

static inline void
fc_inj_set(int fail)
{
	atomic_store(&errinject.count, 0);
	errinject.fail = fail;
}

static inline void
fc_inj_reset(void)
{
	fc_inj_set(0);
}

static inline const char *
fc_inj_err(const char *err)
{
	const char * const append = " (injected)";
	size_t l;

	if (fc_inj_count() != errinject.fail)
		return (err);

	l = strlen(err);
	if (l + strlen(append) + 1 > sizeof errinjbuf)
		l = sizeof errinjbuf - strlen(append) - 1;
	l++;
	(void) strncpy(errinjbuf, err, l);
	(void) strcat(errinjbuf, append);
	return (errinjbuf);
}

#define FC_INJ fc_inj_injected(__func__, __LINE__)
#define FC_ERRSTR(x) fc_inj_err(x)

_Thread_local size_t size_limit = SIZE_MAX;

#define FC_INJ_SZLIM(x) do {						\
		if (x > size_limit) {					\
			DBG("%s limited from %zu to %zu",		\
			    #x, x, size_limit);				\
			x = size_limit;					\
		}							\
		size_limit = SIZE_MAX;					\
	} while (0)
#define FC_INJ_SZLIM_SET(x) size_limit = x

#else  // FCERR_INJECT

#define FC_INJ 0
#define FC_ERRSTR(x) x
#define fc_inj_reset() (void)0
#define fc_inj_set(x) (void)0
#define fc_inj_count() 0
#define FC_INJ_SZLIM(x) (void)0
#define FC_INJ_SZLIM_SET(x) (void)0

#endif // FCERR_INJECT
