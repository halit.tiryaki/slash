/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "cache/cache_varnishd.h"

#include "debug.h"
#include "compiler.h"
#include "buddy.h"
#include "fellow_pri.h"
#include "fellow_io.h"
#include "fellow_io_backend.h"
#include "fellow_log_storage.h"
#include "fellow_log.h"
#include "fellow_hash.h"
#include "fellow_cache.h"
#include "fellow_cache_storage.h"
#include "fellow_tune.h"
#include "fellow_task.h"

#include "foreign/vend.h"

#ifdef TEST_DRIVER
#define FCERR_INJECT
#endif
#include "fellow_inject.h"

// pretty useless, but also harmless to help Flexelint
static pthread_mutex_t wrong_mtx = PTHREAD_MUTEX_INITIALIZER;
static char wrongbuf[1024];

#define FC_WRONG(...) do {						\
		PTOK(pthread_mutex_lock(&wrong_mtx));			\
		bprintf(wrongbuf, __VA_ARGS__);			\
		WRONG(wrongbuf);					\
		PTOK(pthread_mutex_unlock(&wrong_mtx));			\
	} while (0)

static pthread_mutexattr_t fc_mtxattr_errorcheck;
static void __attribute__((constructor))
init_mutexattr(void)
{
	PTOK(pthread_mutexattr_init(&fc_mtxattr_errorcheck));
	PTOK(pthread_mutexattr_settype(&fc_mtxattr_errorcheck,
		PTHREAD_MUTEX_ERRORCHECK));
}

struct fellow_cache_obj;
// look up objects by fdb
VRBT_HEAD(fellow_cache_fdb_head, fellow_cache_obj);

struct fellow_cache_seg;
VTAILQ_HEAD(fellow_cache_lru_head, fellow_cache_seg);

/* ============================================================
 * LRU
 */

struct fellow_cache_lru {
	unsigned			magic;
#define FELLOW_CACHE_LRU_MAGIC		0x5fd80809
	// informational: how many entries on list
	unsigned			n;
	struct fellow_cache		*fc;
	pthread_mutex_t			lru_mtx;
	struct fellow_cache_lru_head	lru_head;
	pthread_t			lru_thread;
	struct fellow_cache_seg		*resume;
};

static struct fellow_cache_lru *
fellow_cache_lru_new(struct fellow_cache *fc)
{
	struct fellow_cache_lru *lru;

	AN(fc);

	ALLOC_OBJ(lru, FELLOW_CACHE_LRU_MAGIC);
	AN(lru);
	lru->fc = fc;
	AZ(pthread_mutex_init(&lru->lru_mtx, &fc_mtxattr_errorcheck));
	VTAILQ_INIT(&lru->lru_head);

	return (lru);
}

static void
fellow_cache_lru_fini(struct fellow_cache_lru **lrup)
{
	struct fellow_cache_lru *lru;
	void *r;

	TAKE_OBJ_NOTNULL(lru, lrup, FELLOW_CACHE_LRU_MAGIC);
	if (lru->lru_thread != 0) {
		AZ(pthread_join(lru->lru_thread, &r));
		AZ(r);
	}
	assert(VTAILQ_EMPTY(&lru->lru_head));
	AZ(lru->n);
	AZ(pthread_mutex_destroy(&lru->lru_mtx));
}

/*
 * lifetime of a fellow_obj in cache
 *
 * presence in fdb tree is reliable
 * priv PTR is hint
 *
 * 1 create fellow_cache_obj and ref in fdb tree
 * 2 fetch obj
 *
 *
 * remove from cache:
 * - nuke  : varnish-cache forgets about the object
 * - evict : object still exists, but removing from memory
 *
 * eviction:
 * - remove stobj priv ptr
 * - remove from fdb tree
 * - remove from lru
 * when refcnt == 0:
 * - free
 *
 * refcnt'ing obj (fco) and body (fcs)
 *
 * IF body is needed:
 * - for fco INCORE, grab ref on all body fcs
 *   for fco FETCHING -> INCORE this happens with the transition
 *
 * - fco ref must be held if any body fcs ref is held
 */

enum fcos {
	FCOS_INIT = 1,
	FCOS_USABLE,
	FCOS_BUSY,
	FCOS_WRITING,
	FCOS_DISK,
	FCOS_MEM,
	FCOS_READING,
	FCOS_CHECK,
	FCOS_REDUNDANT,
	FCOS_INCORE,
	FCOS_READFAIL,
	FCOS_EVICT,
	FCOS_MAX
} __attribute__ ((__packed__));

#define FCOS_SHIFT 4
#define FCOS_MASK ((1 << FCOS_SHIFT) - 1)
#define FCO_HIGH (2 << FCOS_SHIFT)
#define FCS_HIGH (3 << FCOS_SHIFT)
#define FCOS_HIGH(x) (x & FCS_HIGH)

#define FCO(fcos) (FCO_HIGH | FCOS_ ## fcos)
#define FCS(fcos) (FCS_HIGH | FCOS_ ## fcos)
#define FCOS(x) (x & FCOS_MASK)

#define FCOSD(x, y, c, lru, fdb, mem, dsk) x ## _ ## y = x(y),

//lint -e{123} macro FCO
enum fcos_state {
	FCOS_INVAL = 0,
#include "tbl/fco_states.h"
	FCO_MAX,
#include "tbl/fcs_states.h"
	FCS_MAX,
	FCOS_STATE_MAX = FCS_MAX
} __attribute__ ((__packed__));
#undef FCOSD
//lint -e{786} string concat
//lint -e{785} too few
static const char * const fcos_state_s[FCOS_STATE_MAX] = {
#define FCOSD(x, y, c, lru, fdb, mem, dsk)  [x ## _ ## y] = #x "_" #y,
#include "tbl/fco_states.h"
#include "tbl/fcs_states.h"
};
#undef FCOSD

#define FCOS_IS(state, fcos) (FCOS(state) == FCOS_ ## fcos)

static void __attribute__((constructor))
assert_fcos(void)
{
	//lint --e{506} constant boolean
	assert(FCOS_MAX <= FCOS_MASK);
	assert(FCO_READFAIL == FCO(READFAIL));
	assert(FCS_READFAIL == FCS(READFAIL));
	assert(FCOS_IS(FCS_INCORE, INCORE));
	assert(FCOS_IS(FCO_INCORE, INCORE));
	assert(! FCOS_IS(FCO_INCORE, DISK));
}

#define FCOS_BIT(fcos) ((uint16_t)1 << (fcos - 1))

// this is enum fcos_state -> FCOS_BIT(enum fcos)
static uint16_t fcos_transition[FCOS_STATE_MAX];

static void __attribute__((constructor))
init_fcos_transitions(void)
{

	memset(fcos_transition, 0, sizeof fcos_transition);
#define FCOT(f, t, c)						\
	fcos_transition[FCO_ ## f] |= FCOS_BIT(FCOS_ ## t);
#include "tbl/fco_transition.h"
#define FCST(f, t, c)						\
	fcos_transition[FCS_ ## f] |= FCOS_BIT(FCOS_ ## t);
#include "tbl/fcs_transition.h"
}

static inline void
assert_fcos_transition(enum fcos_state f, enum fcos_state t)
{

	assert(FCOS_HIGH(f) == FCOS_HIGH(t));
	if (! (fcos_transition[f] & FCOS_BIT(FCOS(t))))
		FC_WRONG("transition %s -> %s",
		    fcos_state_s[f], fcos_state_s[t]);
}

/* batch of changes to be applied on fcses */
struct fellow_lru_chgbatch {
	unsigned			magic;
#define FELLOW_LRU_CHGBATCH_MAGIC	0xaab452d9
	unsigned			n_add;
	unsigned			l_rem, n_rem;
	struct fellow_cache_obj		*fco;
	struct fellow_cache_lru_head	add;
	struct fellow_cache_seg		**fcs;
};

#define FELLOW_LRU_CHGBATCH_INIT(name, fcoa, size) {{		\
	.magic = FELLOW_LRU_CHGBATCH_MAGIC,			\
	.n_add = 0,						\
	.l_rem = (size),					\
	.n_rem = 0,						\
	.fco = (fcoa),						\
	.add = VTAILQ_HEAD_INITIALIZER((name)->add),		\
	.fcs = (struct fellow_cache_seg*[size + 1]){0}		\
}}

static inline void
fellow_cache_seg_transition_locked_notincore(struct fellow_cache_seg *fcs,
    enum fcos_state to);

static inline void
fellow_cache_seg_transition_locked(
    struct fellow_lru_chgbatch *lcb, struct fellow_cache_seg *fcs,
    enum fcos_state from, enum fcos_state to);

struct fellow_disk_seg {
	uint16_t			magic;
#define FELLOW_DISK_SEG_MAGIC		0xf93d
	uint8_t				_reserved;
	uint8_t				fht;
	uint32_t			segnum;
	struct buddy_off_extent	seg;

	// checksum of data referred to by seg
	// EXCEPT for fellow_disk_obj where it is only
	// the bottom of fellow_disk_obj + va_data
	union fh			fh[1];
};

struct fellow_disk_seglist {
	uint32_t			magic;
#define FELLOW_DISK_SEGLIST_MAGIC	0x06bbf521
	uint8_t				version;
	uint8_t				_reserved[2];
	uint8_t				fht;

	// off = 8 - checksum of data below
	union fh			fh[1];

	// vvv CHECKSUMMED

#define FELLOW_DISK_SEGLIST_CHK_START			\
	offsetof(struct fellow_disk_seglist, next)
#define FELLOW_DISK_SEGLIST_LEN(fdsl)				\
	(sizeof *fdsl + (fdsl)->nsegs * sizeof *(fdsl)->segs)
#define FELLOW_DISK_SEGLIST_CHK_LEN(fdsl)			\
	(FELLOW_DISK_SEGLIST_LEN(fdsl) - FELLOW_DISK_SEGLIST_CHK_START)

	struct buddy_off_extent	next;
	uint16_t			nsegs;
	uint16_t			lsegs;
	uint8_t				_reserved2[4];
	struct fellow_disk_seg		segs[];
};

/* A good maximum number of segments per seglist is _just_ under
 * 1<<16
 *
 * formula:
 * size of 1<<16 seg plus the seglist
 * rounded down to a fellow block
 * minus the seglist
 * devided by size of the seg
 *
 * see t_seglist_sizes() output
 */

#define FELLOW_DISK_SEGLIST_MAX_SEGS	/* 65534 */			\
	(unsigned)(((((sizeof(struct fellow_disk_seg) << 16) +		\
	sizeof(struct fellow_disk_seglist)) & ~FELLOW_BLOCK_ALIGN) -	\
	sizeof(struct fellow_disk_seglist)) /				\
	sizeof(struct fellow_disk_seg))

#define SEGLIST_SIZE(xdsl, lsegs)		\
	(sizeof *xdsl + lsegs * sizeof *xdsl->segs)

#define SEGLIST_FIT_FUNC(what)						\
static inline uint16_t							\
	fellow_ ## what ## _seglist_fit(size_t sz)			\
{									\
	assert(sz >= sizeof(struct fellow_ ## what ## _seglist));	\
	sz -= sizeof(struct fellow_ ## what ## _seglist);		\
	sz /= sizeof(struct fellow_ ## what ## _seg);			\
	if (sz > UINT16_MAX)						\
		sz = UINT16_MAX;					\
	return (uint16_t)sz;						\
}

SEGLIST_FIT_FUNC(disk)

struct fellow_cache_obj;
struct fellow_cache_seg {
	uint16_t			magic;
#define FELLOW_CACHE_SEG_MAGIC		0x6278
	enum fcos_state			state;
	unsigned			fcs_onlru:1;
	unsigned			fco_infdb:1;
	unsigned			lcb_add:1;
	unsigned			lcb_remove:1;
	/*
	 * during LRU (fco mutate path): the FCO is already
	 * removed from the LRU, but fcs_onlru is still 1
	 * for fcs consistency. If mutation fails, the FCO
	 * is put back on LRU for real
	 */
	unsigned			fco_lru_mutate:1;

	/*
	 * for FCO, protected by fdb_mtx
	 * for FCS, protected by fco mtx
	 */
	unsigned			refcnt;

	VTAILQ_ENTRY(fellow_cache_seg)	lru_list;
	struct fellow_cache_obj		*fco;
	struct fellow_disk_seg		*disk_seg;
	struct buddy_ptr_extent		alloc;
	union {
		size_t			fcs_len;	// FCS_* used size
		struct fellow_disk_obj	*fco_fdo;	// FCO_* ptr to fdo
	} u;
};

/*
 * marks an fcsl which is being created. iterators over fcsls need to
 * wait on fco->cond while this value is being read
 */
static struct fellow_cache_seglist * const fcsl_pending =
    (void *)(uintptr_t)0x3ead;

struct fellow_cache_seglist {
	uint32_t			magic;
#define FELLOW_CACHE_SEGLIST_MAGIC	0xcad6e9db
	uint16_t			lsegs;		// how many av.

	struct fellow_disk_seglist	*fdsl;		// to free
	size_t				fdsl_sz;	// 0 for embedded
	size_t				fcsl_sz;	// 0 for embedded

	struct fellow_cache_seglist	*next;
	struct fellow_cache_seg	segs[];
};

SEGLIST_FIT_FUNC(cache)

#define fellow_cache_seglist_size(n) (sizeof(struct fellow_cache_seglist) + \
	(n) * sizeof(struct fellow_cache_seg))

struct fellow_disk_obj_attr {
	uint32_t	aoff;	// from struct fellow_disk_obj
	uint32_t	alen;
};

static void
fellow_cache_seglist_wait_avail_locked(struct fellow_cache_obj *fco,
    struct fellow_cache_seglist * const * const fcslp);
static void
fellow_cache_seglist_wait_avail(struct fellow_cache_obj *fco,
    struct fellow_cache_seglist * const * const fcslp);


struct fcscursor {
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	// points to next
	unsigned u;
};

static inline void
fcsc_init(struct fcscursor *c, struct fellow_cache_seglist *fcsl)
{

	assert(fcsl != fcsl_pending);
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	c->fcsl = fcsl;
	c->fdsl = fcsl->fdsl;
	CHECK_OBJ_NOTNULL(c->fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	c->u = 0;
}

static inline struct fellow_cache_seg *
fcsc_next(struct fcscursor *c)
{
	struct fcscursor next[1];

	if (c->u == c->fdsl->nsegs) {
		assert(c->fcsl->next != fcsl_pending);
		if (c->fcsl->next == NULL)
			return (NULL);
		fcsc_init(next, c->fcsl->next);
		if (next->fdsl->nsegs == 0)
			return (NULL);
		*c = *next;
	}
	assert(c->u < c->fdsl->nsegs);
	return (&c->fcsl->segs[c->u++]);
}

static inline struct fellow_cache_seg *
fcsc_next_ignore_pending(struct fcscursor *c)
{

	if (c->u == c->fdsl->nsegs && c->fcsl->next == fcsl_pending)
		return (NULL);
	return (fcsc_next(c));
}

static inline struct fellow_cache_seg *
fcsc_next_wait_locked(struct fellow_cache_obj *fco, struct fcscursor *c)
{
	if (c->u == c->fdsl->nsegs)
		fellow_cache_seglist_wait_avail_locked(fco, &c->fcsl->next);
	return (fcsc_next(c));
}

static inline struct fellow_cache_seg *
fcsc_next_wait(struct fellow_cache_obj *fco, struct fcscursor *c)
{
	if (c->u == c->fdsl->nsegs)
		fellow_cache_seglist_wait_avail(fco, &c->fcsl->next);
	return (fcsc_next(c));
}

// look at the next element, but do not change the cursor
static inline struct fellow_cache_seg *
fcsc_peek_wait(struct fellow_cache_obj *fco, const struct fcscursor *ca)
{
	struct fcscursor c;

	AN(ca);
	c = *ca;
	return (fcsc_next_wait(fco, &c));
}

/*
 * fdo attribues we want to keep our own enum and, consequently, bitfield
 * stable.  so we define the enum manually and then generate code from the table
 * files to ensure that all values in the tables are defined
 */

enum fdo_attr {
	FDOA_FLAGS = 0,
	FDOA_LEN,
	FDOA_VXID,
	FDOA_LASTMODIFIED,
	FDOA_GZIPBITS,
	FDOA_VARY,
	FDOA_HEADERS,
	FDOA_ESIDATA,
	FDOA__MAX
};

/*
 * the sl and sr functions are to avoid shift left/right by negative amount
 * errors. the compiler emits these before eliminating dead code
 */

static inline uint16_t
sl(uint16_t v, uint8_t b) {
	//lint -e{701} shift left of signed -- WEIRD
	//lint -e{734} loss of precision
	return (v << b);
}

static inline uint16_t
sr(uint16_t v, uint8_t b) {
	return (v >> b);
}

static uint16_t
fdoa2oa_present(const uint16_t fdoa) {
	uint16_t oa = 0;

	//lint --e{506} constant value boolean
	//lint --e{774} boolean always false
	//lint --e{570} loss if sign
	//lint --e{778} constant 0 in -
#define HANDLE_FDOA(x) do {						\
		assert((int)OA_##x < UINT8_MAX);			\
		assert((int)FDOA_##x < UINT8_MAX);			\
		if ((uint8_t)OA_##x == (uint8_t)FDOA_##x)		\
			oa |= fdoa & (1 << (uint8_t)FDOA_##x);		\
		else if ((uint8_t)OA_##x > (uint8_t)FDOA_##x) {		\
			oa |= sl(fdoa & (1 << (uint8_t)FDOA_##x),	\
			    (uint8_t)OA_##x - (uint8_t)FDOA_##x);	\
		}							\
		else {							\
			oa |= sr(fdoa & (1 << (uint8_t)FDOA_##x),	\
			    (uint8_t)FDOA_##x - (uint8_t)OA_##x);	\
		}							\
	} while(0)

#define FDO_FIXATTR(U, l, s, ss) HANDLE_FDOA(U);
#define FDO_VARATTR(U, l)        HANDLE_FDOA(U);
#define FDO_AUXATTR(U, l)        HANDLE_FDOA(U);
#include "tbl/fellow_obj_attr.h"
#undef HANDLE_FDOA

	return (oa);
}

static uint16_t
oa2fdoa_present(const uint16_t oa) {
	uint16_t fdoa = 0;

#define HANDLE_OA(x) do {						\
	    assert((int)OA_##x < UINT8_MAX);				\
	    assert((int)FDOA_##x < UINT8_MAX);				\
	    if ((uint8_t)OA_##x == (uint8_t)FDOA_##x)			\
		    fdoa |= oa & (1 << (uint8_t)OA_##x);		\
	    else if ((uint8_t)OA_##x > (uint8_t)FDOA_##x) {		\
		    fdoa |= sr(oa & (1 << (uint8_t)OA_##x),		\
			(uint8_t)OA_##x - (uint8_t)FDOA_##x);		\
	    }								\
	    else {							\
		    fdoa |= sl(oa & (1 << (uint8_t)OA_##x),		\
			(uint8_t)FDOA_##x - (uint8_t)OA_##x);		\
	    }								\
} while(0)

#define OBJ_FIXATTR(U, l, s)    HANDLE_OA(U);
#define OBJ_VARATTR(U, l)       HANDLE_OA(U);
#define OBJ_AUXATTR(U, l)       HANDLE_OA(U);
#include "tbl/obj_attr.h"
#undef HANDLE_OA

	return (fdoa);
}

static void __attribute__((constructor))
assert_oa_fdoa(void)
{
	assert(oa2fdoa_present(1 << (uint8_t)OA_LEN | 1 << (uint8_t)OA_VXID) ==
	    (1 << (uint8_t)FDOA_LEN | 1 << (uint8_t)FDOA_VXID));
#define HANDLE_OA(x)							\
	assert(oa2fdoa_present(1 << (uint8_t)OA_##x) == (1 << (uint8_t)FDOA_##x));	\
	assert(fdoa2oa_present(1 << (uint8_t)FDOA_##x) == (1 << (uint8_t)OA_##x));	\

#define OBJ_FIXATTR(U, l, s)    HANDLE_OA(U);
#define OBJ_VARATTR(U, l)       HANDLE_OA(U);
#define OBJ_AUXATTR(U, l)       HANDLE_OA(U);
#include "tbl/obj_attr.h"
#undef HANDLE_OA
}

// fdo_flags
#define FDO_F_INLOG	1
#define FDO_F_VXID64	2	// vxid uses 8 bytes

/*
 * disk layout
 *
 * struct fellow_disk_obj	disk_obj
 * uint8_t			varattr_data[variable]
 * fellow_disk_seglist		seglist
 *
 * new memory allocation layout IF FITS IN POW2
 *
 * struct fellow_disk_obj	disk_obj
 * uint8_t			varattr_data[variable]
 * fellow_disk_seglist		seglist
 * ... variable length disk segs
 * fellow_cache_seglist
 * ... variable length cache segs
 * fellow_cache_obj
 */


struct fellow_disk_obj {
	uint32_t			magic;
#define FELLOW_DISK_OBJ_MAGIC		0x50728fbd
	uint8_t			version;
	uint8_t			_reserved[3];

	// segment off/sz is for fellow_disk_obj + seglist
	// checksum is only for data below
	struct fellow_disk_seg		fdo_fds;
#define FELLOW_DISK_OBJ_CHK_START		\
	offsetof(struct fellow_disk_obj, va_data_len)
#define FELLOW_DISK_OBJ_CHK_LEN(fdo)					\
	((sizeof(struct fellow_disk_obj) - FELLOW_DISK_OBJ_CHK_START) +	\
	(fdo)->va_data_len)

	/* FDO_FIXATTR is 2, 8, 8, 8, 32
	 * so we use 4, 1, 1 to pad to 2x 32 */
	uint32_t			va_data_len;
	uint8_t			fdo_flags;
	uint8_t			fdo_reserve;

	/* attributes */
#define FDO_FIXATTR(U, l, s, vs)			\
	uint8_t			fa_##l[s];
#include "tbl/fellow_obj_attr.h"
	uint8_t			fa_reserve[62];

	uint16_t			fdoa_present;
#define FDO_VARATTR(U, l)			\
	struct fellow_disk_obj_attr	va_##l;
#include "tbl/fellow_obj_attr.h"
	struct fellow_disk_obj_attr	va_reserve[4];

#define FDO_AUXATTR(U, l)			\
	struct fellow_disk_seg		aa_##l##_seg;
#include "tbl/fellow_obj_attr.h"
	// no need for aa_reserve, va offset is variable

	uint8_t			va_data[];
};

// pointer to the embedded fdsl
#define fellow_disk_obj_fdsl(fdo)					\
	(void *)((uint8_t *)fdo + sizeof *fdo + fdo->va_data_len)

#define fellow_disk_obj_size(fdo, fdsl)					\
	(sizeof *fdo + fdo->va_data_len +				\
	 sizeof *fdsl + fdsl->lsegs * sizeof *fdsl->segs)

// shaved off one pointer...
#define FCO_FCS(fco)	(&(fco)->fdo_fcs)
#define FCO_FDO(fco)	fellow_disk_obj(FCO_FCS(fco))
#define FCO_REFCNT(fco) (fco)->fdo_fcs.refcnt
#define FCO_STATE(fco)	(fco)->fdo_fcs.state

/*
 * dunno -> wantlog
 * dunno -> nolog
 * dunno -> toolate
 * dunno -> inlog	// read from disk
 * wantlog -> toolate
 * wantlog -> inlog
 * inlog -> deleted
 */
enum fcol_state {
	FCOL_DUNNO = 0,	// not yet decided
	FCOL_WANTLOG,
	FCOL_NOLOG,	// private object
	FCOL_TOOLATE,	// already dead
	FCOL_INLOG,
	FCOL_DELETED,
	FCOL__MAX
} __attribute__ ((__packed__));

enum seglistload_state {
	SEGL_INVAL = 0,
	SEGL_NEED,
	SEGL_LOADING,
	SEGL_DONE
	//SEGL_SLIMMED	// segments are gone - TODO
} __attribute__ ((__packed__));

struct fellow_cache_obj {
	unsigned			magic;
#define FELLOW_CACHE_OBJ_MAGIC		0x837d555f
	enum fcol_state			logstate;
	enum seglistload_state		seglstate;

	// ^^^ protected by mtx - do not add <64bit values

	struct fellow_cache_lru		*lru;
	struct fellow_cache_res		fcr;
	uint8_t				ntouched;

	struct buddy_ptr_extent		fco_mem;
	struct buddy_ptr_page		fco_dowry;

	pthread_mutex_t			mtx;
	pthread_cond_t			cond;	// signal ready

	struct objcore			*oc;

	fellow_disk_block		fdb;	// *1) below
	VRBT_ENTRY(fellow_cache_obj)	fdb_entry;

	struct fellow_cache_seg		fdo_fcs;
#define FDO_AUXATTR(U, l)				\
	struct fellow_cache_seg		aa_##l##_seg;
#include "tbl/fellow_obj_attr.h"

	struct fellow_cache_seglist	*fcsl;
	// *2) below
};

/* comments on the above:
 *
 * *1) this is redundant to region_fdb(&fdo->fdo_fds.seg) for efficiency of the
 *     fdb lookup (see fco_cmp())
 *
 * *2) fcsl can be a separate allocation or immediately follow the fco. Also,
 *     the fellow_disk_obj may be copied into the same allocation. So the
 *     structure is
 *
 *     fellow_cache_obj
 *     [fellow_cache_seglist + n * fellow_cache_seg]
 *     [fellow_disk_obj]
 *
 *     we write fellow_disk_object(s) with the embedded fellow_disk_seglist such
 *     that, when reading the object, the fco allocation can also hold all of
 *     the fellow_cache_seglist required for the fellow_disk_seglist.
 *
 *     the numbers on linux:
 *
 *     (gdb) p ((1<<12) - sizeof(struct fellow_cache_obj) - sizeof(struct fellow_cache_seglist)) / sizeof(struct fellow_cache_seg)
 *     $1 = 58
 *     (gdb) p sizeof(struct fellow_cache_obj) + sizeof(struct fellow_cache_seglist) + 58 * sizeof(struct fellow_cache_seg)
 *     $2 = 4072
 *
 *     -> 24 bytes left
 *
 *     mutex_t and cond_t can be bigger on other platforms and might change in
 *     size, so this optimization may always fail when transporting storage to
 *     other platforms. no problem if it does, we might just waste some space
 *     when reading back objects
 */

#define fellow_cache_obj_with_seglist_size(n)				\
	(sizeof(struct fellow_cache_obj) + fellow_cache_seglist_size(n))

#define FDO_MAX_EMBED_SEGS \
	(unsigned)(((MIN_FELLOW_BLOCK - sizeof(struct fellow_cache_obj)) \
	   - sizeof(struct fellow_cache_seglist))			\
	 / sizeof(struct fellow_cache_seg))

#define FCR_OK(p)\
	(struct fellow_cache_res){.r.ptr = (p), .status = fcr_ok}
#define FCR_ALLOCERR(str)\
	(struct fellow_cache_res){.r.err = (str), .status = fcr_allocerr}
#define FCR_IOERR(str)\
	(struct fellow_cache_res){.r.err = (str), .status = fcr_ioerr}
#define FCR_ALLOCFAIL(str) FCR_ALLOCERR(FC_ERRSTR(str))
#define FCR_IOFAIL(str) FCR_IOERR(FC_ERRSTR(str))

const char * const fellow_cache_res_s[FCR_LIM] = {
	[fcr_ok] = "ok",
	[fcr_allocerr] = "allocation",
	[fcr_ioerr] = "io"
};

struct fellow_body_region {
	struct buddy_off_extent	*reg;
	// used space
	size_t				len;
	// counting allocations from the same region
	unsigned			segnum;
};

/* info struct for busy writes */
enum fellow_busy_io_e {
	FBIO_INVAL = 0,
	FBIO_SEG,
	FBIO_SEGLIST
} __attribute__ ((__packed__));

enum fellow_busy_iosync_e {
	FBIOS_INVAL = 0,
	FBIOS_SYNC,
	FBIOS_ASYNC
} __attribute__ ((__packed__));

// XXX tunable?
#define FBIO_MAX_RETRIES 3

struct fellow_busy_io_seglist {
	struct fellow_disk_seglist *fdsl;
	struct buddy_off_extent reg;
};

// pool for segment memory
BUDDY_POOL(fbo_segmem, 1);
BUDDY_POOL_GET_FUNC(fbo_segmem, static)
//BUDDY_POOL_AVAIL_FUNC(fbo_segmem, static)

struct fellow_busy_io {
	uint16_t				magic;
#define FELLOW_BUSY_IO_MAGIC			0x0bcb
	uint16_t				retries;
	enum fellow_busy_io_e			type;
	enum fellow_busy_iosync_e		sync;
	struct fellow_busy			*fbo;
	union {
		// for FBIO_SEG
		struct fellow_cache_seg		*fcs;
		// for FBIO_SEGLIST
		struct fellow_busy_io_seglist	seglist;
	} u;
};

// Busy Body Region Requests
BUDDY_REQS(fellow_bbrr, 1);

// stored in stevedore priv
struct fellow_busy {
	unsigned			magic;
#define FELLOW_BUSY_MAGIC		0x8504a132

	// for fellow_busy_setattr()
	uint32_t			va_data_len;
	uint8_t				*va_data;

	struct buddy_ptr_extent	fbo_mem;
	size_t				sz_estimate;

	// sum of getspace returned
	size_t				sz_returned;
	// we add more whenever we exceed estimate
	size_t				sz_increment;
	// how much we already alloced on dsk
	size_t				sz_dskalloc;

	struct fellow_cache		*fc;
	struct fellow_cache_obj	*fco;
	struct fellow_cache_seglist	*body_seglist;
	struct fellow_cache_seg	*body_seg;
	struct fellow_body_region	body_region;
	struct fellow_bbrr		bbrr;
	struct buddy_off_extent	segdskdowry;

	struct buddy_ptr_page		segdowry;
	struct fbo_segmem		segmem[1];

#define FCO_MAX_REGIONS ((FELLOW_DISK_LOG_BLOCK_ENTRIES - 1) * DLE_REG_NREGION)
// for fdsl and busy region last chance
#define FCO_REGIONS_RESERVE 3
	struct buddy_off_extent	region[FCO_MAX_REGIONS];
	unsigned			nregion;
	uint8_t				grown;
#define FBO_NIO 91
	/* protected by fco mtx */
	uint8_t				io_idx;
	uint8_t				io_outstanding;
	struct fellow_busy_io		io[FBO_NIO];
};

#define MAX_NLRU_EXPONENT 6

struct fellow_cache_lrus {
	unsigned			magic;
#define FELLOW_CACHE_LRUS_MAGIC	0xadad56fb
	pthread_mutex_t			mtx;
	struct fellow_cache_lru		*lru[1 << MAX_NLRU_EXPONENT];
};

struct fellow_cache {
	unsigned			magic;
#define FELLOW_CACHE_MAGIC		0xe2f2243e
	unsigned			running;

	buddy_t				*membuddy;
	struct fellow_fd		*ffd;
	struct stvfe_tune		*tune;
	fellow_task_run_t		*taskrun;

	pthread_mutex_t			fdb_mtx;
	struct fellow_cache_fdb_head	fdb_head;
	uint64_t			*g_mem_obj;	// stats

	struct fellow_cache_lrus	lrus[1];

	pthread_mutex_t			async_mtx;
	pthread_cond_t			async_cond;
	void				*async_ioctx;
	pthread_t			async_thread;
	unsigned			async_idle;
};

/* ============================================================
 * multi-LRU
 */

static void * fellow_cache_lru_thread(struct worker *wrk, void *priv);

static void
fellow_cache_lrus_init(struct fellow_cache_lrus *lrus)
{

	INIT_OBJ(lrus, FELLOW_CACHE_LRUS_MAGIC);
	AZ(pthread_mutex_init(&lrus->mtx, &fc_mtxattr_errorcheck));
}

static void
fellow_cache_lrus_fini(struct fellow_cache_lrus *lrus)
{
	unsigned u;

	for (u = 0; u < 1 << MAX_NLRU_EXPONENT; u++) {
		if (lrus->lru[u] == NULL)
			continue;
		fellow_cache_lru_fini(&lrus->lru[u]);
	}
	AZ(pthread_mutex_destroy(&lrus->mtx));
}

static struct fellow_cache_lru *
fellow_cache_get_lru(struct fellow_cache *fc, uint64_t n)
{
	struct fellow_cache_lrus *lrus;
	struct fellow_cache_lru *lru;
	struct stvfe_tune *tune;
	uint8_t exponent;
	pthread_t thr;
	size_t i;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	lrus = fc->lrus;
	CHECK_OBJ_NOTNULL(lrus, FELLOW_CACHE_LRUS_MAGIC);
	tune = fc->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	exponent = tune->lru_exponent;
	assert(exponent <= MAX_NLRU_EXPONENT);
	i = exponent ? fib(n, exponent) : 0;

	lru = lrus->lru[i];
	if (lru != NULL && lru->lru_thread != 0)
		return (lru);

	AZ(pthread_mutex_lock(&lrus->mtx));
	lru = lrus->lru[i];
	if (lru == NULL) {
		lru = lrus->lru[i] = fellow_cache_lru_new(fc);
		WRK_BgThread(&thr, "sfe-mem-lru", fellow_cache_lru_thread, lru);
		AN(thr);
	}
	AZ(pthread_mutex_unlock(&lrus->mtx));
	AN(lru);
	return (lru);
}

/* ============================================================
 * util
 */

static void
fellow_cache_res_check(const struct fellow_cache *fc,
    const struct fellow_cache_res fcr)
{
	struct stvfe_tune *tune;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	tune = fc->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	switch (fcr.status) {
	case fcr_allocerr:
		if (tune->allocerr_obj)
			return;
		break;
	case fcr_ioerr:
		if (tune->ioerr_obj)
			return;
		break;
	case fcr_ok:
		return;
	default:
		FC_WRONG("fcr.status %d", fcr.status);
	}

	FC_WRONG("fcr.status = %d, fcr.r.err = %s",
	    fcr.status, fcr.r.err);
}

// check with latch
static struct fellow_cache_res
fellow_cache_obj_res(const struct fellow_cache *fc,
    struct fellow_cache_obj *fco, struct fellow_cache_res fcr)
{

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	fellow_cache_res_check(fc, fcr);
	if (fco->fcr.status != fcr_ok)
		return (fco->fcr);
	/*
	 * the latched ok value must be the fco, but we do return other values,
	 * depending on context
	 */
	assert(fco->fcr.status == fcr_ok);
	if (fcr.status != fcr_ok)
		fco->fcr = fcr;
	else if (fco->fcr.r.ptr == NULL)
		fco->fcr.r.ptr = fco;
	else
		assert(fco->fcr.r.ptr == fco);
	return (fcr);
}

/* ============================================================
 * fwd decl
 */

static void
fellow_cache_obj_fini(const struct fellow_cache_obj *fco);
static void
fellow_cache_obj_redundant(const struct fellow_cache *fc,
    struct fellow_cache_obj **fcop);

static inline unsigned
fellow_cache_seg_ref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs);
static inline unsigned
fellow_cache_seg_deref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs);
static unsigned
fellow_cache_obj_deref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache *fc, struct fellow_cache_obj *fco);
static void
fellow_cache_async_write_complete(struct fellow_cache *fc,
    void *fbio, int32_t result);
static void
fellow_cache_seglists_need(const struct fellow_cache *fc,
    struct fellow_cache_obj *fco);

static void
fellow_busy_log_submit(const struct fellow_busy *);
static void
fellow_busy_free(struct fellow_busy **fbop);

/* ============================================================
 * busy io
 */

/* under fco lock */
static struct fellow_busy_io *
fellow_busy_io_get(struct fellow_busy *fbo, struct fellow_busy_io *stk)
{
	struct fellow_busy_io *fbio;
	uint8_t u;

	AN(stk);

	for (u = 0; u < FBO_NIO; u++) {
		fbio = &fbo->io[fbo->io_idx];
		fbo->io_idx++;
		fbo->io_idx %= FBO_NIO;
		if (fbio->magic == 0 && fbio->type == FBIO_INVAL) {
			INIT_OBJ(fbio, FELLOW_BUSY_IO_MAGIC);
			fbio->fbo = fbo;
			fbio->sync = FBIOS_ASYNC;
			return (fbio);
		}
	}

	fbio = stk;
	INIT_OBJ(fbio, FELLOW_BUSY_IO_MAGIC);
	fbio->fbo = fbo;
	fbio->sync = FBIOS_SYNC;
	return (fbio);
}

static void
fellow_busy_io_submit(struct fellow_busy_io *fbio)
{
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;
	struct fellow_busy *fbo;
	struct fellow_cache *fc;
	uint64_t info;
	void *ptr;
	size_t size;
	off_t off;
	int r;

	CHECK_OBJ_NOTNULL(fbio, FELLOW_BUSY_IO_MAGIC);
	fbo = fbio->fbo;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	switch (fbio->type) {
	case FBIO_SEG:
		fcs = fbio->u.fcs;
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		fds = fcs->disk_seg;
		CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

		ptr  = fcs->alloc.ptr;
		size = fcs->alloc.size;
		off  = fds->seg.off;
		break;
	case FBIO_SEGLIST:
		CHECK_OBJ_NOTNULL(fbio->u.seglist.fdsl,
		    FELLOW_DISK_SEGLIST_MAGIC);
		ptr  = fbio->u.seglist.fdsl;
		size = fbio->u.seglist.reg.size;
		off  = fbio->u.seglist.reg.off;
		break;
	default:
		WRONG("fbio->type");
	}

	AN(ptr);
	AN(size);
	AN(off);	// first block is header

	switch (fbio->sync) {
	case FBIOS_ASYNC:
		info = faio_ptr_info(FAIOT_CACHE_WRITE, fbio);

		AZ(pthread_mutex_lock(&fc->async_mtx));
		r = fellow_io_write_async_enq(fc->async_ioctx,
		    info, ptr, size, off);
		if (fc->async_idle)
			AZ(pthread_cond_signal(&fc->async_cond));
		AZ(pthread_mutex_unlock(&fc->async_mtx));
		if (r)
			return;
		/* FALLTHROUGH */
	case FBIOS_SYNC:
		fellow_cache_async_write_complete(fc, fbio,
		    fellow_io_pwrite_sync(fc->ffd, ptr, size, off));
		return;
	default:
		WRONG("fbio->sync");
	}
}


/* ============================================================
 * disk obj
 */

#define fdowrong(x) do {					\
		return (FC_ERRSTR("disk object wrong " x));	\
	} while(0)

static inline struct fellow_disk_obj *
fellow_disk_obj(const struct fellow_cache_seg *fcs)
{

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	assert(FCOS_HIGH(fcs->state) == FCO_HIGH);
	if (fcs->alloc.ptr != NULL)
		assert(fcs->alloc.ptr == fcs->u.fco_fdo);
	return (fcs->u.fco_fdo);
}

static const char *
fellow_disk_obj_check(const struct fellow_disk_obj *fdo,
    fellow_disk_block fdba)
{
	const struct fellow_disk_seg *fds;
	size_t off, len;

	AN(fdo);
	/*
	 * FC_INJ: Because the error handling path in the caller is identical we
	 * do not inject at all possible points
	 */
	if (FC_INJ || fdo->magic != FELLOW_DISK_OBJ_MAGIC)
		fdowrong("magic");
	if (fdo->version != 1)
		fdowrong("version");
	if (fdo->va_data_len + sizeof *fdo > fdb_size(fdba))
		fdowrong("va_data_len out of bounds");
	fds = &fdo->fdo_fds;
	if (FC_INJ || fds->magic != FELLOW_DISK_SEG_MAGIC)
		fdowrong("fds magic");
	if (fds->fht >= FH_LIM)
		fdowrong("hash type (>= FH_LIM)");
	if (! fh_name[fds->fht])
		fdowrong("hash type (support missing)");
	if (fds->segnum != 0)
		fdowrong("fds segnum");
	if (fds->seg.off != fdb_off(fdba))
		fdowrong("fds off mismatch");
	if (fds->seg.size != fdb_size(fdba))
		fdowrong("fds size mismatch");
	if (FC_INJ || fhcmp(fds->fht, fds->fh,
		(uint8_t *)fdo + FELLOW_DISK_OBJ_CHK_START,
		FELLOW_DISK_OBJ_CHK_LEN(fdo)))
		fdowrong("chksum");
#define FDO_VARATTR(U, l)						\
	len = fdo->va_##l.alen;						\
	if (len > 0) {							\
		off = fdo->va_##l.aoff;					\
		if (off < offsetof(struct fellow_disk_obj, va_data))	\
			fdowrong(#l " offset");				\
		if (off + len > sizeof(*fdo) + fdo->va_data_len)	\
			fdowrong(#l " length");				\
	}
#include "tbl/fellow_obj_attr.h"

	return (NULL);
}

/*
 * convert old formats to current
 */
static void
fellow_disk_obj_compat(struct fellow_disk_obj *fdo)
{
	uint32_t vxid;

	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	if (fdo->fdo_flags & FDO_F_VXID64)
		return;

	vxid = vbe32dec(fdo->fa_vxid);
	vbe64enc(fdo->fa_vxid, (uint64_t)vxid);
	fdo->fdo_flags |= FDO_F_VXID64;
}

static struct fellow_disk_obj *
fellow_disk_obj_trim(const struct fellow_cache *fc,
    struct fellow_cache_seg *fcs)
{
	struct fellow_disk_seglist *fdsl;
	struct fellow_disk_obj *fdo;
	struct buddy_ptr_extent mem;
	size_t trim_sz;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	fdo = fellow_disk_obj(fcs);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	// not if already relocated
	assert(fdo == fcs->alloc.ptr);

	fdsl = fellow_disk_obj_fdsl(fdo);
	CHECK_OBJ(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

	assert(fdsl->lsegs >= fdsl->nsegs);
	fdsl->lsegs = fdsl->nsegs;

	trim_sz = fellow_disk_obj_size(fdo, fdsl);

	/*
	 * Objects which are already potentially accessed asynchronously need to
	 * keep their address (fellow_cache_async_write_complete() call).
	 *
	 * But objects which we are reading and which thus do not yet have
	 * concurrent access can be relocated. We need to copy, but we gain a
	 * larger page.
	 */

	switch (fcs->state) {
	case FCO_READING:
		if (log2up(trim_sz) < log2up(fcs->alloc.size)) {
			mem = buddy_alloc1_ptr_extent_wait(fc->membuddy,
			    FEP_SPCPRI, trim_sz, 0);
			if (mem.ptr) {
				memcpy(mem.ptr, fdo, trim_sz);
				buddy_return1_ptr_extent(fc->membuddy,
				    &fcs->alloc);
				fcs->alloc = mem;
				fcs->u.fco_fdo = mem.ptr;
				break;
			}
		}
		/* FALLTHROUGH */
	case FCO_WRITING:
		buddy_trim1_ptr_extent(fc->membuddy, &fcs->alloc, trim_sz);
		assert(fdo == fcs->alloc.ptr);
		break;
	default:
		WRONG("fcs->state to call fellow_disk_obj_trim()");
	}
	return (fellow_disk_obj(fcs));
}

/* ============================================================
 * cache obj
 */

static inline int
fco_cmp(const struct fellow_cache_obj *o1, const struct fellow_cache_obj *o2)
{
	uint64_t b1, b2;

	AN(o1);
	AN(o2);
	b1 = o1->fdb.fdb;
	b2 = o2->fdb.fdb;
	// busy objects may not be added to the fdb tree
	AN(b1);
	AN(b2);

	if (b1 < b2)
		return (-1);
	if (b1 > b2)
		return (1);
	return (0);
}

VRBT_GENERATE_INSERT_COLOR(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
//VRBT_GENERATE_FIND(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, fco_cmp, static)
#ifdef VRBT_GENERATE_INSERT_FINISH
VRBT_GENERATE_INSERT_FINISH(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
#endif
VRBT_GENERATE_INSERT(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, fco_cmp, static)
VRBT_GENERATE_REMOVE_COLOR(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
VRBT_GENERATE_REMOVE(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
//VRBT_GENERATE_NEXT(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
//VRBT_GENERATE_MINMAX(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)

static inline int
fellow_cache_shouldlru(enum fcos_state state, const struct objcore *oc,
    unsigned refcnt)
{
	if (FCOS(state) != FCOS_INCORE)
		return (0);
	if (FCOS_HIGH(state) == FCO_HIGH)
		return (oc != NULL && refcnt > 0);
	return (refcnt == 0);
}

static void
fellow_cache_lru_chgbatch_apply(struct fellow_lru_chgbatch *lcb)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_lru *lru;
	unsigned n;

	CHECK_OBJ_NOTNULL(lcb, FELLOW_LRU_CHGBATCH_MAGIC);

	//DBG("%u/%u", lcb->n, !VTAILQ_EMPTY(&lcb->add));

	if (lcb->n_rem == 0 && (VTAILQ_EMPTY(&lcb->add) || lcb->n_add == 0)) {
		AZ(lcb->n_rem);
		AZ(lcb->n_add);
		assert(VTAILQ_EMPTY(&lcb->add));
		return;
	}

	fco = lcb->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	lru = fco->lru;
	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);

	n = lcb->n_rem;
	while (n--) {
		fcs = lcb->fcs[n];
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		assert(fcs->fco == fco);
		AZ(fcs->lcb_add);
		AN(fcs->lcb_remove);
		fcs->lcb_remove = 0;
	}
	n = 0;
	VTAILQ_FOREACH(fcs, &lcb->add, lru_list) {
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		assert(fcs->fco == fco);
		AZ(fcs->lcb_remove);
		AN(fcs->lcb_add);
		fcs->lcb_add = 0;
		n++;
	}
	assert(n == lcb->n_add);

	AZ(pthread_mutex_lock(&lru->lru_mtx));
	lru->n += lcb->n_add;
	lru->n -= lcb->n_rem;
	while (lcb->n_rem) {
		lcb->n_rem--;
		TAKE_OBJ_NOTNULL(fcs, &lcb->fcs[lcb->n_rem],
		    FELLOW_CACHE_SEG_MAGIC);
		assert(fcs->fco == fco);
		VTAILQ_REMOVE(&lru->lru_head, fcs, lru_list);
	}
	VTAILQ_CONCAT(&lru->lru_head, &lcb->add, lru_list);
	AZ(pthread_mutex_unlock(&lru->lru_mtx));
	lcb->n_add = 0;
	assert(VTAILQ_EMPTY(&lcb->add));
	AZ(lcb->n_rem);
}

/* chg is fellow_cache_shouldlru(new) - fellow_cache_shouldlru(old)
 *
 * iow: 0 -> noop, 1 -> add, -1 remove
 *
 * to be called after the change
 *
 * the lcb can be null if the caller knows that always chg == 0
 */
static inline void
fellow_cache_lru_chg(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, int chg)
{
	uint16_t i;

	if (chg == 0)
		return;

	CHECK_OBJ_NOTNULL(lcb, FELLOW_LRU_CHGBATCH_MAGIC);
	AN(lcb->fcs);

	assert(lcb->fco == fcs->fco);

	unsigned add = chg > 0;

#ifdef EXTRA_ASSERTIONS
	assert(add ==
	    fellow_cache_shouldlru(fcs->state, fcs->fco->oc, fcs->refcnt));
#endif
	AZ(fcs->fco_lru_mutate);
	assert(fcs->fcs_onlru == (unsigned)!add);
	fcs->fcs_onlru = add ? 1 : 0;

	if (add && fcs->lcb_remove) {
		AZ(fcs->lcb_add);

		//DBG("%p -rem", fcs);
		// remove the remove
		AN(lcb->n_rem);
		for (i = 0; i < lcb->n_rem; i++) {
			if (lcb->fcs[i] != fcs)
				continue;
			lcb->fcs[i] = NULL;
			break;
		}
		assert(i < lcb->n_rem);
		if (i + 1 < lcb->n_rem) {
			memmove(&lcb->fcs[i], &lcb->fcs[i + 1],
			    sizeof lcb->fcs[0] * (lcb->n_rem - (i + 1)));
		}
		lcb->n_rem--;
		fcs->lcb_remove = 0;
	}
	else if (add) {
		//DBG("%p +add", fcs);
		AZ(fcs->lcb_add);
		AZ(fcs->lcb_remove);
		fcs->lcb_add = 1;
		VTAILQ_INSERT_TAIL(&lcb->add, fcs, lru_list);
		lcb->n_add++;
	}
	else if (fcs->lcb_add) {
		//DBG("%p -add", fcs);
		AZ(fcs->lcb_remove);
		VTAILQ_REMOVE(&lcb->add, fcs, lru_list);
		fcs->lcb_add = 0;
		AN(lcb->n_add);
		lcb->n_add--;
	}
	else {
		//DBG("%p +rem", fcs);
		AZ(fcs->lcb_remove);
		AZ(fcs->lcb_add);
		if (lcb->n_rem == lcb->l_rem) {
			fellow_cache_lru_chgbatch_apply(lcb);
			AZ(lcb->n_rem);
		}
		fcs->lcb_remove = 1;
		lcb->fcs[lcb->n_rem++] = fcs;
	}
}

static inline void
assert_cache_seg_consistency(const struct fellow_cache_seg *fcs)
{
#define FCOSA_NOLRU AZ(fcs->fcs_onlru)
#define FCOSA_MAYLRU assert(fcs->fcs_onlru == \
	fellow_cache_shouldlru(fcs->state, fcs->fco->oc, fcs->refcnt))
#define FCOSA_NOFDB AZ(fcs->fco_infdb)
#define FCOSA_MAYFDB assert(fcs->fco_infdb == (unsigned)(fcs->refcnt > 0))
#define FCOSA_NOMEM do {				\
		AZ(fcs->alloc.ptr);			\
		AZ(fcs->alloc.size);			\
		AZ(fcs->u.fcs_len);				\
	} while(0)
#define FCSA_MEM do {					\
		AN(fcs->alloc.ptr);			\
		AN(fcs->alloc.size);			\
	} while(0)
#define FCOA_MEM do {						\
	    /* can be NULL for embedded fdo */			\
	    if (fcs->alloc.ptr != NULL) {			\
		AN(fcs->alloc.size);				\
		assert(fcs->alloc.ptr == fcs->u.fco_fdo);	\
	    }							\
	} while(0)
#define FDSA_NULL AZ(fcs->disk_seg)
#define FDSA_MAY CHECK_OBJ_ORNULL(fcs->disk_seg, FELLOW_DISK_SEG_MAGIC)
#define FDSA_UN do {							\
	    CHECK_OBJ_NOTNULL(fcs->disk_seg, FELLOW_DISK_SEG_MAGIC);	\
	    AZ(fcs->disk_seg->seg.off);					\
	    AZ(fcs->disk_seg->seg.size);				\
	} while(0)
#define FDSA_ALLOC do {							\
	    CHECK_OBJ_NOTNULL(fcs->disk_seg, FELLOW_DISK_SEG_MAGIC);	\
	    AN(fcs->disk_seg->seg.off);					\
	    AN(fcs->disk_seg->seg.size);				\
	} while(0)

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	switch (fcs->state) {
//lint --e{525} Negative indentation
//lint --e{665} Unparan ... as expression
#define FCOSD(what, state, comment, lru, fdb, mem, fds)	\
		case what ## _ ## state:			\
			lru;					\
			fdb;					\
			mem;					\
			fds;					\
			break;
#include "tbl/fco_states.h"
#include "tbl/fcs_states.h"
	default:
		WRONG("fcs->state");
	}
#undef FCOSA_NOLRU
#undef FCOSA_MAYLRU
#undef FCOSA_NOFDB
#undef FCOSA_FDB
#undef FCOSA_NOMEM
#undef FCOA_MEM
#undef FCSA_MEM
#undef FDSA_NULL
#undef FDSA_UN
#undef FDSA_ALLOC
}

static inline void
fellow_cache_seg_wait_locked(const struct fellow_cache_seg *fcs)
{
	AN(fcs->refcnt);
//	DBG("%p", fcs);
	assert_cache_seg_consistency(fcs);
	AZ(pthread_cond_wait(&fcs->fco->cond, &fcs->fco->mtx));
}

/*
 * free the cached segment, not the fellow_(cache|disk)_seg
 *
 * called holding the fco lock
 */
static void
fellow_cache_seg_free(struct buddy_returns *memret,
    struct fellow_cache_seg *fcs, unsigned deref)
{
	struct buddy_ptr_extent mem;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	DBG("%p deref %u", fcs, deref);

	while (fcs->state == FCS_READING || fcs->state == FCS_WRITING) {
		(void) fellow_cache_seg_ref_locked(NULL, fcs);
		fellow_cache_seg_wait_locked(fcs);
		deref++;
	}

	assert(
	    fcs->state == FCO_EVICT ||
	    fcs->state == FCO_REDUNDANT ||

	    fcs->state == FCS_INIT ||
	    fcs->state == FCS_USABLE ||
	    fcs->state == FCS_INCORE ||
	    fcs->state == FCS_DISK ||
	    fcs->state == FCS_CHECK ||
	    fcs->state == FCS_READFAIL);
	assert_cache_seg_consistency(fcs);
	if (fcs->state == FCS_INCORE) {
		struct fellow_lru_chgbatch lcb[1] =
		    FELLOW_LRU_CHGBATCH_INIT(lcb, fcs->fco, 1);

		fellow_cache_seg_transition_locked(lcb, fcs,
		    FCS_INCORE, FCS_DISK);
		fellow_cache_lru_chgbatch_apply(lcb);
	}
	TAKE(mem, fcs->alloc);
	AZ(fcs->fcs_onlru);
	assert(fcs->refcnt == deref);
	fcs->refcnt = 0;
	fcs->u.fcs_len = 0;
	/*
	 * at this point, the fcs is not consistent in all cases, e.g. FCS_EVICT
	 * has no memory - but this is the point where it does no longer exist
	 */
	if (mem.ptr)
		AN(buddy_return_ptr_extent(memret, &mem));
}

static void
fellow_cache_seglist_wait_avail_locked(struct fellow_cache_obj *fco,
    struct fellow_cache_seglist * const * const fcslp)
{
	while (*fcslp == fcsl_pending)
		AZ(pthread_cond_wait(&fco->cond, &fco->mtx));
}

static void
fellow_cache_seglist_wait_avail(struct fellow_cache_obj *fco,
    struct fellow_cache_seglist * const * const fcslp)
{

	CHECK_OBJ(fco, FELLOW_CACHE_OBJ_MAGIC);
	AN(fcslp);
	if (*fcslp != fcsl_pending)
		return;
	AZ(pthread_mutex_lock(&fco->mtx));
	fellow_cache_seglist_wait_avail_locked(fco, fcslp);
	AZ(pthread_mutex_unlock(&fco->mtx));
}

/*
 * references on AUXATTR are never returned after access
 * through fellow_cache_obj_getattr()
 *
 * fco->mtx held
 */
static void
fellow_cache_seg_auxattr_free(struct buddy_returns *memret,
    struct fellow_cache_seg *fcs)
{

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	assert_cache_seg_consistency(fcs);
	fellow_cache_seg_free(memret, fcs, fcs->refcnt);
}

/* fco->mtx held unless surplus seglist */
static void
fellow_cache_seglist_free(struct buddy_returns *memret,
    struct fellow_cache_seglist *fcsl)
{
	struct fellow_cache_seglist *next;
	struct buddy_ptr_extent e;
	unsigned u, segs;

	while (fcsl != NULL) {
		CHECK_OBJ(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);

		segs = 0;
		if (fcsl->fdsl)
			segs = fcsl->fdsl->nsegs;

		for (u = 0; u < segs; u++)
			fellow_cache_seg_free(memret, &fcsl->segs[u], 0);
		for (; u < fcsl->lsegs; u++) {
			assert(FCOS_IS(fcsl->segs[u].state, INIT) ||
			    FCOS_IS(fcsl->segs[u].state, USABLE));
			AZ(fcsl->segs[u].alloc.ptr);
		}

		if (fcsl->fdsl == NULL)
			AZ(fcsl->fdsl_sz);
		else if (fcsl->fdsl_sz > 0) {
			e = BUDDY_PTR_EXTENT(fcsl->fdsl, fcsl->fdsl_sz);
			fcsl->fdsl = NULL;
			fcsl->fdsl_sz = 0;
			AN(buddy_return_ptr_extent(memret, &e));
		}

		/*
		 * there can not be a seglist loader active, because there are
		 * no references when the object is freed and the seglist reader
		 * holds one
		 */
		next = fcsl->next;
		if (next == fcsl_pending)
			next = NULL;

		if (fcsl->fcsl_sz > 0) {
			e = BUDDY_PTR_EXTENT(fcsl, fcsl->fcsl_sz);
			fcsl->fcsl_sz = 0;
			AN(buddy_return_ptr_extent(memret, &e));
		}

		fcsl = next;
	}
}

/*
 * get back region list from segments
 */

static inline void
fellow_seg_regions(const struct fellow_fd *ffd,
    const struct fellow_cache_seg *fcs,
    struct buddy_off_extent region[FCO_MAX_REGIONS], unsigned *na)
{
	const struct fellow_disk_seg *fds;
	unsigned u, n = *na;
	buddyoff_t off;
	size_t sz;

	fds = fcs->disk_seg;
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	sz = fellow_rndup(ffd, fds->seg.size);
	if (fds->segnum > 0) {
		for (u = 0; u < n; u++) {
			off = region[u].off + (buddyoff_t)region[u].size;
			if (off == fds->seg.off) {
				region[u].size += sz;
				assert (*na == n);
				return;
			}
		}
		WRONG("segnum > 0 needs to follow previous region");
	}
	region[n].off = fds->seg.off;
	region[n].size = sz;
	AN(fds->seg.off);
	AN(sz);
	n++;
	assert(n <= FCO_MAX_REGIONS);
	*na = n;
}

static unsigned
fellow_seglist_regions(const struct fellow_cache *fc,
    const struct fellow_fd *ffd,
    struct fellow_cache_obj *fco,
    struct buddy_off_extent region[FCO_MAX_REGIONS], unsigned n)
{
	struct fellow_cache_seg *fcs;
	struct fcscursor c;

	fellow_cache_seglists_need(fc, fco);
	fcsc_init(&c, fco->fcsl);

	while ((fcs = fcsc_next_wait(fco, &c)) != NULL)
		fellow_seg_regions(ffd, fcs, region, &n);
	return (n);
}

static unsigned
fellow_obj_regions(const struct fellow_cache *fc,
    struct fellow_cache_obj *fco,
    struct buddy_off_extent region[FCO_MAX_REGIONS])
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	const struct fellow_fd *ffd;
	unsigned n;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	ffd = fc->ffd;

	n = fellow_seglist_regions(fc, ffd, fco, region, 0);
	assert(n <= FCO_MAX_REGIONS);
	DBG("seglist_regions %u", n);
#define FDO_AUXATTR(U, l)						\
	if (fco->aa_##l##_seg.state != FCS_USABLE)			\
		fellow_seg_regions(ffd, &fco->aa_##l##_seg, region, &n);
#include "tbl/fellow_obj_attr.h"
	DBG("+auxattr %u", n);
	assert(n <= FCO_MAX_REGIONS);

	fcsl = fco->fcsl;
	fdsl = fcsl->fdsl;

	/* can not be pending because fellow_seglist_regions waited already */
	while ((fcsl = fcsl->next) != NULL) {
		assert(fcsl != fcsl_pending);
		/* fcsl is ahead of fdsl by one (fcsl is next of fdsl) */
		CHECK_OBJ(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
		CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

		assert(n < FCO_MAX_REGIONS);
		region[n] = fdsl->next;
		n++;
		fdsl = fcsl->fdsl;
	}
	DBG("+seglists %u", n);
	AZ(fdsl->next.off);
	return (n);
}

/* fco mtx must be held, will be unlocked */
static void
fellow_cache_obj_free(const struct fellow_cache *fc,
    struct fellow_cache_obj **fcop)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	struct buddy_ptr_extent mem;
	struct buddy_returns *memret;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	TAKE_OBJ_NOTNULL(fco, fcop, FELLOW_CACHE_OBJ_MAGIC);

	memret = BUDDY_RETURNS_STK(fc->membuddy, BUDDY_RETURNS_MAX);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	fcs = FCO_FCS(fco);
	AZ(FCO_REFCNT(fco));
	assert_cache_seg_consistency(fcs);
	fellow_cache_seg_transition_locked(lcb, FCO_FCS(fco),
	    FCO_STATE(fco), FCO_EVICT);
	fellow_cache_lru_chgbatch_apply(lcb);

	DBG("fco %p", fco);
	fellow_cache_seglist_free(memret, fco->fcsl);

#define FDO_AUXATTR(U, l)				\
	fellow_cache_seg_auxattr_free(memret, &fco->aa_##l##_seg);
#include "tbl/fellow_obj_attr.h"

	AZ(fcs->fco_infdb);
	AZ(fcs->fcs_onlru);

	AZ(pthread_mutex_unlock(&fco->mtx));
	AZ(pthread_mutex_destroy(&fco->mtx));
	AZ(pthread_cond_destroy(&fco->cond));

	if (fco->fco_dowry.bits)
		AN(buddy_return_ptr_page(memret, &fco->fco_dowry));
	TAKE(mem, fco->fco_mem);
	// this frees the fdo, which may contain the fco
	fellow_cache_seg_free(memret, fcs, 0);
	if (mem.ptr)
		AN(buddy_return_ptr_extent(memret, &mem));
	buddy_return(memret);
}

/*
 * evict from stvfe_mutate():
 *
 * state must be FCO_INCORE, object is on LRU, must be referenced
 *
 * fco mtx is held
 *
 * similar in structure to fellow_cache_obj_deref(), but
 * - keep reference
 * - remove exp
 */

void
fellow_cache_obj_evict_mutate(const struct fellow_cache_lru *lru,
    struct fellow_cache_obj *fco)
{
	struct fellow_cache_seg *fcs;
	struct fellow_cache *fc;

	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);
	fc = lru->fc;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	assert(lru == fco->lru);

	fcs = FCO_FCS(fco);

	assert_cache_seg_consistency(fcs);
	assert(fco->logstate == FCOL_INLOG);

	// fdb
	AN(fcs->fco_infdb);
	fcs->fco_infdb = 0;
	AN(FCO_REFCNT(fco));
	AZ(pthread_mutex_lock(&fc->fdb_mtx));
	(void) VRBT_REMOVE(fellow_cache_fdb_head, &fc->fdb_head, fco);
	AN(*fc->g_mem_obj);
	(*fc->g_mem_obj)--;
	AZ(pthread_mutex_unlock(&fc->fdb_mtx));

	// state, oc (relevant for lru)
	assert(FCO_STATE(fco) == FCO_INCORE);
	FCO_STATE(fco) = FCO_EVICT;
	AN(fco->oc);
	fco->oc = NULL;

	/* finish lru transaction which has been started in
	 * fellow_cache_lru_work(): the fcs is already removed
	 * from LRU, we just finish the flags
	 */

	AN(fcs->fco_lru_mutate);
	AN(fcs->fcs_onlru);
	fcs->fco_lru_mutate = 0;
	fcs->fcs_onlru = 0;

	assert_cache_seg_consistency(fcs);
}

static inline void
fellow_disk_seg_init(struct fellow_disk_seg *fds, uint8_t fht)
{

	assert(fht > FH_NONE);
	assert(fht < FH_LIM);
	AN(fh_name[fht]);
	INIT_OBJ(fds, FELLOW_DISK_SEG_MAGIC);
	fds->fht = fht;
}

static struct fellow_disk_seglist *
fellow_disk_seglist_init(void *ptr, uint16_t ldsegs, uint8_t fht)
{
	struct fellow_disk_seglist *fdsl = ptr;
	unsigned u;

	AN(ptr);
	assert(PAOK(ptr));
	INIT_OBJ(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	fdsl->version = 1;
	fdsl->lsegs = ldsegs;
	fdsl->fht = fht;
	for (u = 0; u < ldsegs; u++)
		fellow_disk_seg_init(&fdsl->segs[u], fht);
	return (fdsl);
}

static inline void
fellow_cache_seg_init(struct fellow_cache_seg *fcs,
    struct fellow_cache_obj *fco, enum fcos_state state)
{

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	INIT_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);
	fcs->state = state;
	fcs->fco = fco;
}

static inline void
fellow_cache_seg_associate(struct fellow_cache_seg *fcs,
    struct fellow_disk_seg *fds, enum fcos_state state)
{

	assert(fcs->state == FCS_INIT);
	assert_cache_seg_consistency(fcs);
	fcs->disk_seg = fds;
	fellow_cache_seg_transition_locked_notincore(fcs, state);
}

/*
 * cache seglist must be able to hold all disk seglist
 */
static void
fellow_cache_seglist_associate(
    struct fellow_cache_seglist *fcsl,
    struct fellow_disk_seglist *fdsl, enum fcos_state state)
{
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;
	unsigned max, u;

	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

	assert(fcsl->lsegs >= fdsl->nsegs);
	AZ(fcsl->fdsl);
	fcsl->fdsl = fdsl;

	for (u = 0; u < fdsl->nsegs; u++) {
		fcs = &fcsl->segs[u];
		fds = &fdsl->segs[u];

		fellow_cache_seg_associate(fcs, fds, state);
	}
	max = fdsl->lsegs;
	if (fcsl->lsegs < max)
		max = fcsl->lsegs;
	for (; u < max; u++) {
		fcs = &fcsl->segs[u];
		fds = &fdsl->segs[u];

		fellow_cache_seg_associate(fcs, fds, FCS_USABLE);
	}
}

static struct fellow_cache_seglist *
fellow_cache_seglist_init(void *ptr, size_t space, struct fellow_cache_obj *fco)
{
	struct fellow_cache_seglist *fcsl = ptr;
	size_t lsegs;
	uint16_t u;

	AN(ptr);
	assert(PAOK(ptr));
	assert(space >= sizeof *fcsl);
	INIT_OBJ(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	lsegs = space / sizeof *fcsl->segs;
	assert(lsegs <= UINT16_MAX);
	fcsl->lsegs = (uint16_t)lsegs;
	for (u = 0; u < fcsl->lsegs; u++)
		fellow_cache_seg_init(&fcsl->segs[u], fco, FCS_INIT);
	return (fcsl);
}

static void
fellow_disk_seglist_fini(struct fellow_disk_seglist *fdsl)
{
	unsigned u;

	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	assert(fdsl->version == 1);
	// AN(fdsl->nsegs); // not possible for the embedded seglist
	assert(fdsl->nsegs <= fdsl->lsegs);
	// no zero segments active
	for (u = 0; u < fdsl->nsegs; u++) {
		AN(fdsl->segs[u].seg.off);
		AN(fdsl->segs[u].seg.size);
	}
	fh(fdsl->fht, fdsl->fh,
	    (char *)fdsl + FELLOW_DISK_SEGLIST_CHK_START,
	    FELLOW_DISK_SEGLIST_CHK_LEN(fdsl));
}

/*
 * region is from the previous disk seglist
 *
 * the first seglist is embedded
 */
static void
fellow_cache_seglists_write(struct fellow_busy *fbo,
    const struct fellow_cache_seglist *fcsl,
    struct buddy_off_extent reg)
{
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_obj *fco;
	struct fellow_busy_io *fbio, fbiostk[1];

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	while (fcsl) {
		// not for reads
		assert(fcsl != fcsl_pending);
		CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
		AN(reg.off);
		AN(reg.size);
		assert(reg.size <= FIO_MAX);
		fdsl = fcsl->fdsl;

		fellow_disk_seglist_fini(fdsl);

		AZ(pthread_mutex_lock(&fco->mtx));
		fbo->io_outstanding++;
		fbio = fellow_busy_io_get(fbo, fbiostk);
		AZ(pthread_mutex_unlock(&fco->mtx));

		AN(fbio);
		fbio->type = FBIO_SEGLIST;
		fbio->u.seglist.reg = reg;
		fbio->u.seglist.fdsl = fdsl;

		reg = fdsl->next;
		fcsl = fcsl->next;

		fellow_busy_io_submit(fbio);
	}
}

#define fdslwrong(x) return(FC_ERRSTR("disk seglist wrong " x));

static const char *
fellow_disk_seglist_check(const struct fellow_disk_seglist *fdsl)
{

	AN(fdsl);
	if (FC_INJ || fdsl->magic != FELLOW_DISK_SEGLIST_MAGIC)
		fdslwrong("magic");
	if (fdsl->version != 1)
		fdslwrong("version");
	if (fdsl->nsegs > fdsl->lsegs)
		fdslwrong("nsegs");
	if (fdsl->fht >= FH_LIM)
		fdslwrong("hash type (>= FH_LIM)");
	if (! fh_name[fdsl->fht])
		fdslwrong("hash type (support missing)");
	if (FC_INJ || fhcmp(fdsl->fht, fdsl->fh,
		(char *)fdsl + FELLOW_DISK_SEGLIST_CHK_START,
		FELLOW_DISK_SEGLIST_CHK_LEN(fdsl)))
		fdslwrong("chksum");
	return (NULL);
}

// load seglists priv
struct fclsp {
	unsigned			magic;
#define FELLOW_CACHE_LSP_MAGIC		0x93837496
	struct buddy_ptr_extent		mem;
	struct fellow_cache_obj		*fco;
	fellow_task_privstate		state;
};

/*
 * we use a task to load seglists because the alloc/read/alloc combination is
 * not well suited for async IO, in particular because we need to wait for
 * allocations.
 */

static void
fellow_cache_seglists_load(struct worker *wrk, void *priv)
{
	struct buddy_ptr_extent mem, fdsl_mem, fcsl_mem;
	struct fellow_cache_seglist *fcsl, *ofcsl;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_obj *fco;
	struct fellow_cache_lru *lru;
	struct buddy_off_extent next;
	struct fellow_cache *fc;
	struct fclsp *fclsp;
	const char *err;
	unsigned refcnt;
	ssize_t ssz;

	(void)wrk;
	CAST_OBJ_NOTNULL(fclsp, priv, FELLOW_CACHE_LSP_MAGIC);
	fco = fclsp->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	lru = fco->lru;
	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);
	fc = lru->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);
	struct fellow_cache_res fcr = FCR_OK(fco);

	// invalidates fclsp, no use after this point
	TAKE(mem, fclsp->mem);
	buddy_return1_ptr_extent(fc->membuddy, &mem);

	assert(fco->seglstate == SEGL_LOADING);
	AN(FCO_REFCNT(fco));

	fcsl = fco->fcsl;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);

	fdsl = fcsl->fdsl;
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	assert(fdsl->next.off >= 0);
	assert(fdsl->next.size > 0);

	while (1) {
		CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
		CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

		assert(fcsl->next == fcsl_pending);

		next = fdsl->next;
		if (next.off == 0 || next.size == 0) {
			AZ(next.off);
			AZ(next.size);
			fcsl->next = NULL;
			break;
		}

		fcsl_mem = buddy_ptr_extent_nil;
		fdsl_mem = buddy_alloc1_ptr_extent_wait(fc->membuddy, FEP_META,
		    next.size, 0);
		fdsl = fdsl_mem.ptr;
		if (FC_INJ || fdsl == NULL) {
			fcr = FCR_ALLOCFAIL("disk seglist fdsl");
			goto err;
		}
		assert(fdsl_mem.size >= next.size);

		ssz = fellow_io_pread_sync(fc->ffd, fdsl, next.size, next.off);
		if (FC_INJ || ssz < 0 || (size_t)ssz != next.size) {
			fcr = FCR_IOFAIL("disk seglist read");
			goto err;
		}
		err = fellow_disk_seglist_check(fdsl);
		if (err != NULL) {
			fcr = FCR_IOFAIL(err);
			goto err;
		}
		fcsl_mem = buddy_alloc1_ptr_extent_wait(fc->membuddy, FEP_META,
		    SEGLIST_SIZE(fcsl, fdsl->nsegs), 0);

		if (FC_INJ || fcsl_mem.ptr == NULL) {
			fcr = FCR_ALLOCFAIL("disk seglist fcsl");
			goto err;
		}

		ofcsl = fcsl;
		fcsl = fellow_cache_seglist_init(fcsl_mem.ptr,
		    fcsl_mem.size - sizeof *fcsl, fco);
		fcsl->next = fcsl_pending;
		fcsl->fdsl_sz = fdsl_mem.size;
		fcsl->fcsl_sz = fcsl_mem.size;

		fellow_cache_seglist_associate(fcsl, fdsl, FCS_DISK);

		assert(ofcsl->next == fcsl_pending);
		ofcsl->next = fcsl;
		AZ(pthread_mutex_lock(&fco->mtx));
		AZ(pthread_cond_broadcast(&fco->cond));
		AZ(pthread_mutex_unlock(&fco->mtx));
	}

	AZ(pthread_mutex_lock(&fco->mtx));
	assert(fco->seglstate == SEGL_LOADING);
	fco->seglstate = SEGL_DONE;
	refcnt = fellow_cache_obj_deref_locked(lcb, fc, fco);
	AZ(pthread_cond_broadcast(&fco->cond));
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(FCO_FCS(fco));
	if (refcnt == 0)
		fellow_cache_obj_free(fc, &fco);
	else
		AZ(pthread_mutex_unlock(&fco->mtx));

	return;
  err:
	if (fdsl_mem.ptr != NULL)
		buddy_return1_ptr_extent(fc->membuddy, &fdsl_mem);
	if (fcsl_mem.ptr != NULL)
		buddy_return1_ptr_extent(fc->membuddy, &fcsl_mem);

	assert(fcsl->next == fcsl_pending);
	fcsl->next = NULL;

	AZ(pthread_mutex_lock(&fco->mtx));
	assert(fco->seglstate == SEGL_LOADING);
	fco->seglstate = SEGL_DONE;
	if (FCO_STATE(fco) == FCO_INCORE || FCO_STATE(fco) == FCO_READING) {
		fellow_cache_seg_transition_locked(lcb, FCO_FCS(fco),
		    FCO_STATE(fco), FCO_READFAIL);
	}
	(void) fellow_cache_obj_res(fc, fco, fcr);
	refcnt = fellow_cache_obj_deref_locked(lcb, fc, fco);
	AZ(pthread_cond_broadcast(&fco->cond));
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(FCO_FCS(fco));
	if (refcnt == 0)
		fellow_cache_obj_free(fc, &fco);
	else
		AZ(pthread_mutex_unlock(&fco->mtx));
}

static void
fellow_cache_seglists_need(const struct fellow_cache *fc,
    struct fellow_cache_obj *fco)
{
	struct buddy_ptr_extent fclsp_mem;
	enum seglistload_state st;
	struct fclsp *fclsp;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	switch (fco->seglstate) {
	case SEGL_NEED:
		break;
	case SEGL_LOADING:
	case SEGL_DONE:
		return;
	default:
		WRONG("fco->seglstate");
	}

	AZ(pthread_mutex_lock(&fco->mtx));
	st = fco->seglstate;
	if (st == SEGL_NEED) {
		fco->seglstate = SEGL_LOADING;
		FCO_REFCNT(fco)++;
	}
	AZ(pthread_mutex_unlock(&fco->mtx));

	if (st != SEGL_NEED)
		return;


	fclsp_mem = buddy_alloc1_ptr_extent_wait(fc->membuddy, FEP_META,
	    sizeof *fclsp, 0);
	AN(fclsp_mem.ptr);
	fclsp = fclsp_mem.ptr;
	INIT_OBJ(fclsp, FELLOW_CACHE_LSP_MAGIC);
	TAKE(fclsp->mem, fclsp_mem);
	fclsp->fco = fco;

	AZ(fc->taskrun(fellow_cache_seglists_load, fclsp, &fclsp->state));
}

static void
fellow_cache_seglists_wait_locked(struct fellow_cache_obj *fco)
{

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	while (fco->seglstate == SEGL_LOADING)
		AZ(pthread_cond_wait(&fco->cond, &fco->mtx));
	assert(fco->seglstate == SEGL_DONE || fco->seglstate == SEGL_NEED);
}

static void
fellow_cache_seglists_wait(struct fellow_cache_obj *fco)
{

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	if (fco->seglstate != SEGL_LOADING)
		return;
	AZ(pthread_mutex_lock(&fco->mtx));
	fellow_cache_seglists_wait_locked(fco);
	AZ(pthread_mutex_unlock(&fco->mtx));
}

/*
 * MEM LAYOUT
 *
 *     fellow_disk_obj
 * ? x va_data
 *     fellow_disk_seglist
 * ? x fellow_disk_seg
 * dsk_sz = sum(^^)
 *
 * mem_sz = sum(vv)
 *     fellow_cache_obj
 *     [fellow_cache_seglist]	embedded in cache_obj
 * n x fellow_cache_seg
 *
 * for a call from fellow_busy_obj_alloc, we know how many disk_seg
 * for a call from fellow_cache_obj_prepread, we will guess (0 argument)
 *
 * we make one or two allocations, depending on whether the
 * second part fits in the same LOG2UP or not
 *
 * XXX
 * - collapse allocation with fellow_budy_obj_alloc?
 */

static struct fellow_cache_res
fellow_cache_obj_new(
    struct fellow_cache *fc,
    size_t dsk_sz, unsigned nseg_guess,
    struct buddy_ptr_extent *fbo_mem,
    struct buddy_ptr_page *dowry,
    uint8_t pri)
{
	struct fellow_disk_obj *fdo;
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	struct buddy_reqs *reqs;
	struct buddy_ptr_extent fco_mem;
	struct buddy_ptr_extent fdo_mem;
	size_t mem_sz;
	unsigned u;

	DBG("arg dsk_sz %zu nseg_guess %u", dsk_sz, nseg_guess);

	AN(nseg_guess);

	// for busy objects, mem_sz may be less than MIN_FELLOW_BLOCK
	mem_sz = (size_t)1 << log2up(
	    fellow_cache_obj_with_seglist_size(nseg_guess));
	assert(mem_sz <= MIN_FELLOW_BLOCK);

	// dowry is largest, so we allocate it first
	reqs = BUDDY_REQS_STK(fc->membuddy, 4);
	BUDDY_REQS_PRI(reqs, pri);
	if (dowry != NULL)
		AN(buddy_req_page(reqs, fc->tune->chunk_exponent, 0));
	if (fbo_mem != NULL)
		AN(buddy_req_extent(reqs, sizeof(struct fellow_busy), 0));

	assert(dsk_sz == fellow_rndup(fc->ffd, dsk_sz));
	assert(dsk_sz >= mem_sz);
	AN(buddy_req_extent(reqs, dsk_sz, 0));
	AN(buddy_req_extent(reqs, mem_sz, 0));

	u = buddy_alloc_wait(reqs);
	if (FC_INJ || u != 2 + (dowry ? 1 : 0) + (fbo_mem ? 1 : 0)) {
		buddy_alloc_wait_done(reqs);
		return (FCR_ALLOCFAIL("fellow_cache_obj_new alloc failed"));
	}

	if (dowry != NULL) {
		assert(u >= 3);
		*dowry = buddy_get_next_ptr_page(reqs);
	}
	if (fbo_mem != NULL)
		*fbo_mem = buddy_get_next_ptr_extent(reqs);

	fdo_mem = buddy_get_next_ptr_extent(reqs);
	fco_mem = buddy_get_next_ptr_extent(reqs);

	buddy_alloc_wait_done(reqs);

	fdo = fdo_mem.ptr;
	AN(fdo);

	fco = fco_mem.ptr;
	AN(fco);

	INIT_OBJ(fco, FELLOW_CACHE_OBJ_MAGIC);
	DBG("fco %p", fco);
	fco->fco_mem = fco_mem;
	fco->lru = fellow_cache_get_lru(fc, (uintptr_t)fco);

	AZ(pthread_mutex_init(&fco->mtx, &fc_mtxattr_errorcheck));
	AZ(pthread_cond_init(&fco->cond, NULL));

#define FDO_AUXATTR(U, l)						\
	fellow_cache_seg_init(&fco->aa_##l##_seg, fco, FCS_INIT);
#include "tbl/fellow_obj_attr.h"

	fcs = &fco->fdo_fcs;
	fellow_cache_seg_init(fcs, fco, FCO_INIT);

	fcs->alloc = fdo_mem;
	fcs->u.fco_fdo = fdo;
	return (fellow_cache_obj_res(fc, fco, FCR_OK(fco)));
}

static void
fellow_busy_fill_segmem(struct buddy_reqs *reqs, const void *priv)
{
	const struct stvfe_tune *tune;
	unsigned u, bits;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	CAST_OBJ_NOTNULL(tune, priv, STVFE_TUNE_MAGIC);

	bits = tune->chunk_exponent;

	for (u = 0; u < reqs->space; u++)
		AN(buddy_req_page(reqs, bits, 0));
}

struct fellow_cache_res
fellow_busy_obj_alloc(struct fellow_cache *fc,
    struct fellow_cache_obj **fcop, uintptr_t *priv2,
    unsigned wsl)
{
	struct fellow_disk_obj *fdo;
	struct fellow_disk_seg *fds;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_res fcr;
	struct fellow_busy *fbo;
	struct buddy *dskbuddy;
	struct buddy_reqs *reqs;
	struct buddy_ptr_extent fbo_mem;
	struct buddy_ptr_page fbo_dowry;
	struct buddy_off_extent fds_seg, dskdowry;
	size_t sz, asz, dsk_sz;
	uint16_t ldsegs;
	unsigned u;

	// XXX peek for known content-length=

	// round up to disk block such that there
	// is space for at least one disk_seg
	wsl = (unsigned)PRNDUP(wsl);
	sz = asz = sizeof(struct fellow_disk_obj) + wsl;
	assert(PAOK(asz));
	sz += sizeof(struct fellow_disk_seglist);
	sz += sizeof(struct fellow_disk_seg);
	assert(PAOK(sz));

	dsk_sz = fellow_rndup(fc->ffd, sz);
	assert(PAOK(dsk_sz));
	assert(dsk_sz >= sz);

	ldsegs = fellow_disk_seglist_fit(dsk_sz - asz);
	if (ldsegs > FDO_MAX_EMBED_SEGS)
		ldsegs = FDO_MAX_EMBED_SEGS;

//	DBG("-> %u embedded segs", ldsegs);

	// uring has signed 32bit length
	assert(dsk_sz <= FIO_MAX);

	// XXX delay to allow new objects during FP_INIT ?
	dskbuddy = fellow_dskbuddy(fc->ffd);

	/*
	 * cram == 0:
	 * the dskdowry acts as an admission ticket to creating an
	 * expensive busy object: We need to motivate dsk LRU to
	 * start makeing room if it needs to before we allocate
	 * memory which LRU would need
	 */
	reqs = BUDDY_REQS_STK(dskbuddy, 2);
	BUDDY_REQS_PRI(reqs, FEP_NEW);

	sz = (size_t)1 << (fc->tune->chunk_exponent);
	AN(buddy_req_extent(reqs, sz, 0));
	AN(buddy_req_extent(reqs, dsk_sz, 0));

	u = buddy_alloc_wait(reqs);
	if (u == 0) {
		buddy_alloc_wait_done(reqs);
		fcr = FCR_ALLOCFAIL("dsk dowry and fds");
		fellow_cache_res_check(fc, fcr);
		return (fcr);
	}
	assert(u == 2);

	fds_seg = FC_INJ ? buddy_off_extent_nil :
	    buddy_get_off_extent(reqs, 1);

	dskdowry = FC_INJ ? buddy_off_extent_nil :
	    buddy_get_off_extent(reqs, 0);
	buddy_alloc_wait_done(reqs);

	if (fds_seg.off < 0) {
		if (dskdowry.off >= 0)
			buddy_return1_off_extent(dskbuddy, &dskdowry);
		fcr = FCR_ALLOCFAIL("fds region");
		fellow_cache_res_check(fc, fcr);
		return (fcr);
	}

	fcr = fellow_cache_obj_new(fc, dsk_sz, ldsegs, &fbo_mem, &fbo_dowry,
	    FEP_NEW);
	if (fcr.status != fcr_ok) {
		fellow_cache_res_check(fc, fcr);
		buddy_return1_off_extent(dskbuddy, &fds_seg);
		if (dskdowry.off >= 0)
			buddy_return1_off_extent(dskbuddy, &dskdowry);
		return (fcr);
	}
	fco = fcr.r.ptr;

	CHECK_OBJ(fco, FELLOW_CACHE_OBJ_MAGIC);
	fcs = &fco->fdo_fcs;
	CHECK_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);

	fbo = fbo_mem.ptr;
	AN(fbo);
	INIT_OBJ(fbo, FELLOW_BUSY_MAGIC);
	fbo->fbo_mem = fbo_mem;
	fbo->segdowry = fbo_dowry;
	BUDDY_REQS_INIT(&fbo->bbrr, dskbuddy);
	fbo->segdskdowry = dskdowry;

	// disk object in memory
	fdo = fellow_disk_obj(fcs);
	memset(fdo, 0, fcs->alloc.size);
	fdo->magic = FELLOW_DISK_OBJ_MAGIC;
	fdo->version = 1;
	fdo->va_data_len = wsl;
	fdo->fdo_flags = FDO_F_VXID64;

	// disk obj init (allocation happened before)
	fds = &fdo->fdo_fds;
	fellow_disk_seg_init(fds, fc->tune->hash_obj);

	fds->seg = fds_seg;

	fcs->disk_seg = fds;
	fco->fdb = region_fdb(&fds->seg);
	AN(fcop);
	*fcop = fco;
	AN(priv2);
	*priv2 = fco->fdb.fdb;

	fbo->va_data = fdo->va_data;
#define FDO_AUXATTR(U, l)						\
	fellow_disk_seg_init(&fdo->aa_##l##_seg, fc->tune->hash_obj);	\
	fellow_cache_seg_associate(&fco->aa_##l##_seg,			\
	    &fdo->aa_##l##_seg, FCS_USABLE);
#include "tbl/fellow_obj_attr.h"

	// we prepare the state the object will have
	// when inserted -- 2nd ref is for write fellow_cache_async_complete()
	fcs->refcnt = 2;
	fellow_cache_seg_transition_locked_notincore(fcs, FCO_BUSY);

	fdsl = fellow_disk_seglist_init(fellow_disk_obj_fdsl(fdo),
	    ldsegs, fc->tune->hash_obj);

	// FDO_MAX_EMBED_SEGS ensures this
	sz = fellow_cache_seglist_size(ldsegs);
	assert(sizeof *fco + sz <= fco->fco_mem.size);
	fco->fcsl = fellow_cache_seglist_init((void *)(fco + 1), sz, fco);
	AN(fco->fcsl);
	fellow_cache_seglist_associate(fco->fcsl, fdsl, FCS_USABLE);
	fco->seglstate = SEGL_DONE;
//	DBG("fdsl lsegs %u fcsl lsegs %u", fdsl->lsegs, fco->fcsl->lsegs);

	fbo->fc = fc;
	fbo->fco = fco;
	fbo->body_seglist = fco->fcsl;
	AZ(fbo->body_seg);

	AN(fbo->body_seglist);
	AN(fbo->body_seglist->lsegs);
	AN(fbo->body_seglist->fdsl->lsegs);
	AZ(fbo->body_seglist->fdsl->nsegs);

	/* keep one io outstanding until unbusy is called
	 * to prevent the object from completing to FCO_INCORE
	 *
	 * fellow_cache_obj_unbusy does not take an IO, it owns this one
	 */
	fbo->io_outstanding = 1;

	BUDDY_POOL_INIT(fbo->segmem, fc->membuddy, FEP_SPC,
	    fellow_busy_fill_segmem, fc->tune);
	buddy_return1_ptr_page(fc->membuddy, &fbo->segdowry);

	return (FCR_OK(fbo));
}

/* commit a region to the busy object */

static struct buddy_off_extent *
fellow_busy_region_commit(struct fellow_busy *fbo, struct buddy_off_extent reg)
{
	struct buddy_off_extent *fdr;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	if (reg.off < 0) {
		return (NULL);
	}

	AN(reg.size);
	AZ(reg.size & FELLOW_BLOCK_ALIGN);

	assert(fbo->nregion < FCO_MAX_REGIONS);
	fdr = &fbo->region[fbo->nregion++];
	*fdr = reg;

	return (fdr);
}

/* allocate a new region for body, aux attr or seglist
 *
 * aux attr use: We do not yet coalesce allocations because
 * there only is a single aux attr, so the one segment gets
 * its own region
 */
static struct buddy_off_extent *
fellow_busy_region_alloc(struct fellow_busy *fbo, size_t size, int8_t cram)
{
	const struct fellow_cache *fc;
	struct buddy_off_extent *r;
	struct buddy_reqs *reqs;
	struct buddy *dskbuddy;
	unsigned u;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	dskbuddy = fellow_dskbuddy(fc->ffd);
	reqs = BUDDY_REQS_STK(dskbuddy, 1);
	BUDDY_REQS_PRI(reqs, FEP_SPC);

	AN(size);
	AN(buddy_req_extent(reqs, size, cram));

	if (FC_INJ)
		return (NULL);

	u = buddy_alloc_wait(reqs);
	assert(u == 1);

	r = fellow_busy_region_commit(fbo, buddy_get_off_extent(reqs, 0));
	buddy_alloc_wait_done(reqs);

	return (r);
}

/*
 * return an unused region for seglist trim
 */
static void
fellow_busy_region_free(struct fellow_busy *fbo, struct buddy_off_extent *fdr)
{
	struct fellow_cache *fc;
	ssize_t u;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	u = fdr - &fbo->region[0];
	assert(u >= 0);
	assert(u < fbo->nregion);

	// obviously: assert(&fbo->region[u] == fdr);

	buddy_return1_off_extent(fellow_dskbuddy(fc->ffd), fdr);

	if (u == --fbo->nregion)
		return;

	memmove(&fbo->region[u], &fbo->region[u + 1],
	    (fbo->nregion - (size_t)u) * sizeof *fdr);
}

#ifdef TEST_DRIVER
static uint16_t XXX_LIMIT_LDSEGS = 0;
#endif

static uint16_t
fellow_busy_body_seglist_size_strategy(size_t segs, unsigned chunk_exponent)
{
	struct fellow_cache_seglist *fcsl = NULL;
	unsigned bits;

	// size of the fcsl, round down to power2
	bits = log2down(SEGLIST_SIZE(fcsl, segs));
	(void) fcsl;	// flexelint

	if (bits < MIN_FELLOW_BITS)
		bits = MIN_FELLOW_BITS;
	else if (bits > chunk_exponent)
		bits = chunk_exponent;

	segs = fellow_cache_seglist_fit((size_t)1 << bits);
	if (segs > FELLOW_DISK_SEGLIST_MAX_SEGS)
		segs = FELLOW_DISK_SEGLIST_MAX_SEGS;
	assert(segs <= UINT16_MAX);
	return ((uint16_t)segs);
}

static struct fellow_cache_res
fellow_disk_seglist_alloc(struct fellow_busy *fbo,
    struct fellow_cache_seglist *ofcsl, uint16_t ldsegs, uint8_t fht);

static struct fellow_cache_res
fellow_busy_body_seglist_alloc(struct fellow_busy *fbo,
    struct fellow_cache_seglist *ofcsl)
{
	struct fellow_cache *fc;
	unsigned chunk_exponent;
	uint16_t ldsegs;
	size_t sz;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fc->tune, STVFE_TUNE_MAGIC);
	chunk_exponent = fc->tune->chunk_exponent;

	assert(fbo->sz_estimate > fbo->sz_returned);

	/*
	 * XXX for the grown case, we currently try to avoid FCO_MAX_REGIONS
	 *     by always allocating the largest posstible seglist. but that
	 *     leads to a hang for super small memory configurations.
	 *
	 *     the case is likely excotic, but we avoid it for the common
	 *     cases by allocating one extra seglist by strategy
	 *
	 * sz is number of segments
	 */
	if (fbo->grown && fbo->body_seglist != fbo->fco->fcsl) {
		sz = fbo->fc->tune->objsize_max - fbo->sz_dskalloc;
		sz += (((size_t)1 << MIN_FELLOW_BITS) - 1);
		sz >>= MIN_FELLOW_BITS;
		// can not apply strategy because we could hit FCO_MAX_REGIONS
		if (sz > FELLOW_DISK_SEGLIST_MAX_SEGS)
			sz = FELLOW_DISK_SEGLIST_MAX_SEGS;
		ldsegs = (uint16_t)sz;
	} else {
		sz = fbo->sz_estimate - fbo->sz_returned;
		sz += (((size_t)1 << chunk_exponent) - 1);
		sz >>= chunk_exponent;
		ldsegs = fellow_busy_body_seglist_size_strategy(sz,
		    chunk_exponent);
	}


#ifdef TEST_DRIVER
	if (XXX_LIMIT_LDSEGS > 0)
		ldsegs = XXX_LIMIT_LDSEGS;
#endif

	return (
	    fellow_disk_seglist_alloc(fbo, ofcsl,
	      ldsegs, fc->tune->hash_obj));
}

static struct fellow_cache_res
fellow_disk_seglist_alloc(struct fellow_busy *fbo,
    struct fellow_cache_seglist *ofcsl, uint16_t ldsegs, uint8_t fht)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *ofdsl, *fdsl;
	struct buddy_off_extent *fdr;
	struct buddy_ptr_extent fdsl_mem, fcsl_mem;
	struct buddy_reqs *reqs;
	unsigned u;
	size_t sz;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	CHECK_OBJ_NOTNULL(ofcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	AZ(ofcsl->next);

	ofdsl = ofcsl->fdsl;
	AZ(ofdsl->next.off);
	AZ(ofdsl->next.size);

	sz = SEGLIST_SIZE(fdsl, ldsegs);

	fdr = fellow_busy_region_alloc(fbo, sz, 0);
	if (fdr == NULL)
		return (FCR_ALLOCFAIL("seglist disk region"));
	ofdsl->next = *fdr;

	ldsegs = fellow_disk_seglist_fit(fdr->size);

	reqs = BUDDY_REQS_STK(fbo->fc->membuddy, 2);
	BUDDY_REQS_PRI(reqs, FEP_META);
	AN(buddy_req_extent(reqs, fdr->size, 0));
	AN(buddy_req_extent(reqs, SEGLIST_SIZE(fcsl, ldsegs), 0));
	u = buddy_alloc_wait(reqs);
	if (FC_INJ || u != 2) {
		buddy_alloc_wait_done(reqs);
		fellow_busy_region_free(fbo, fdr);
		return (FCR_ALLOCFAIL("seglist memory"));
	}

	fdsl_mem = buddy_get_ptr_extent(reqs, 0);
	fcsl_mem = buddy_get_ptr_extent(reqs, 1);

	buddy_alloc_wait_done(reqs);

	assert(fdsl_mem.size == fdr->size);
	fdsl = fellow_disk_seglist_init(fdsl_mem.ptr, ldsegs, fht);

	AN(fcsl_mem.ptr);
	fcsl = fellow_cache_seglist_init(fcsl_mem.ptr,
	    fcsl_mem.size - sizeof *fcsl, fbo->fco);

	fellow_cache_seglist_associate(fcsl, fdsl, FCS_USABLE);

	fcsl->fcsl_sz = fcsl_mem.size;
	fcsl->fdsl_sz = fdsl_mem.size;
	ofcsl->next = fcsl;

	return (FCR_OK(fcsl));
}

struct szc {
	size_t sz;
	int8_t cram;
};

/*
 * Return a size to allocate next for the object body
 */
static struct szc
fellow_busy_body_size_strategy(const struct fellow_busy *fbo)
{
	unsigned chunkbits;
	struct szc szc = {0,0};

	if (fbo->sz_estimate <= fbo->sz_dskalloc)
		return (szc);

	chunkbits = fbo->fc->tune->chunk_exponent;
	szc.cram = fbo->fc->tune->cram;

	assert(fbo->sz_estimate > fbo->sz_dskalloc);
	szc.sz = fbo->sz_estimate - fbo->sz_dskalloc;

	// out of regions: no cram
	// if also grown: assume worst case size
	if (fbo->nregion >= FCO_MAX_REGIONS - FCO_REGIONS_RESERVE) {
		szc.cram = 0;

		if (fbo->grown) {
			szc.sz = fbo->fc->tune->objsize_max -
			    fbo->sz_dskalloc;
		}
	}
	else if (fbo->grown == 0 && (szc.sz >> chunkbits) == 0)
		// c-l or initial chunked - keep small req
		(void)szc.sz;
	else if (fbo->grown == 0)
		// known size -> round down
		szc.sz = (size_t)1 << log2down(szc.sz);
	else
		// size unknown -> round up
		szc.sz = (size_t)1 << log2up(szc.sz);

	/* for allocation larger than chunk size, always allow cram down to
	 * chunk size, but do split pages (INT8_MIN)
	 */
	if (szc.cram > 0 && szc.sz >= (size_t)1 << chunkbits) {
		assert(chunkbits <= INT8_MAX);
		szc.cram = buddy_cramlimit_bits(szc.sz, INT8_MIN,
		    (int8_t)chunkbits);
	}
	if (szc.cram)
		FC_INJ_SZLIM(szc.sz);
	return (szc);
}

/* allocate a new region for body data */
static size_t
fellow_busy_body_seg_alloc(struct fellow_busy *fbo,
    struct fellow_disk_seg *fds, size_t want)
{
	struct fellow_body_region *fbr;
	struct buddy_off_extent reg, *fdr;
	struct buddy_reqs *reqs;
	struct fellow_cache *fc;
	struct szc szc;
	size_t spc;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fc->tune, STVFE_TUNE_MAGIC);

	fbr = &fbo->body_region;
	AZ(fds->seg.off);
	AZ(fds->seg.size);

	fdr = fbr->reg;
	if (fdr == NULL || fbr->len == fdr->size) {
		if (FC_INJ)
			return (0);

		memset(fbr, 0, sizeof *fbr);

		reqs = &fbo->bbrr.reqs;
		reg = fbo->segdskdowry;
		fbo->segdskdowry = buddy_off_extent_nil;

	  wait_dskalloc:
		BUDDY_REQS_PRI(reqs, FEP_SPCPRI);
		if (reg.off < 0 && buddy_alloc_async_wait(reqs)) {
			reg = buddy_get_next_off_extent(reqs);
			assert(reg.off >= 0);
		}

		buddy_alloc_wait_done(reqs);

		// must return a size, or sz_estimate is wrong
		szc = fellow_busy_body_size_strategy(fbo);
		AN(szc.sz);

		// return and realloc if one power of two too big
		if (reg.off < 0 ||
		    (log2up(reg.size) > MIN_FELLOW_BITS &&
		     log2up(reg.size) > log2up(szc.sz))) {
			AN(buddy_req_extent(reqs, szc.sz, szc.cram));
			BUDDY_REQS_PRI(reqs, FEP_SPCPRI);
			(void) buddy_alloc_async(reqs);
			if (reg.off >= 0)
				buddy_return1_off_extent(reqs->buddy, &reg);
			goto wait_dskalloc;
		}

		// Can only fail if reg.off < 0, which can't happen
		fdr = fellow_busy_region_commit(fbo, reg);
		AN(fdr);

		fbr->reg = fdr;
		AZ(fbr->len);
		fbo->sz_dskalloc += fdr->size;

		// async allocation for remaining known size
		szc = fellow_busy_body_size_strategy(fbo);
		if (szc.sz) {
			AN(buddy_req_extent(reqs, szc.sz, szc.cram));
			BUDDY_REQS_PRI(reqs, FEP_SPC);
			(void) buddy_alloc_async(reqs);
		}
	}

	assert(fbr->len < fdr->size);

	spc = fdr->size - fbr->len;
	AZ(spc & FELLOW_BLOCK_ALIGN);

	want = fellow_rndup(fc->ffd,  want);
	AZ(want & FELLOW_BLOCK_ALIGN);
	if (spc < want)
		want = spc;

	fds->seg.off = fdr->off + (buddyoff_t)fbr->len;
	fds->seg.size = want;
	fds->segnum = fbr->segnum++;

	fbr->len += want;

	return (want);
}

// adjust fds size to fcs size
static void
fellow_busy_body_seg_adjust(struct fellow_busy *fbo,
    const struct fellow_cache_seg *fcs)
{
	struct fellow_body_region *fbr;
	struct buddy_off_extent *fdr;
	struct fellow_disk_seg *fds;
	size_t surplus;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fds = fcs->disk_seg;
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	if (fcs->alloc.size == fds->seg.size)
		return;

	AN(fds->seg.off);
	AN(fds->seg.size);

	assert(fds->seg.size > fcs->alloc.size);
	surplus = fds->seg.size - fcs->alloc.size;
	assert((surplus & FELLOW_BLOCK_ALIGN) == 0);

	fbr = &fbo->body_region;
	fdr = fbr->reg;
	AN(fdr);

	assert(fbr->len >= surplus);
	fbr->len -= surplus;
}

/* return an fds to the body seg (for failed memalloc) */
static void
fellow_busy_body_seg_return(struct fellow_busy *fbo,
    struct fellow_disk_seg *fds)
{
	struct fellow_body_region *fbr;
	struct buddy_off_extent *fdr;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	AN(fds->seg.off);
	AN(fds->seg.size);

	fbr = &fbo->body_region;
	fdr = fbr->reg;
	AN(fdr);

	assert(fbr->len >= fds->seg.size);
	fbr->len -= fds->seg.size;
	assert(fds->seg.off == fdr->off + (buddyoff_t)fbr->len);
	fbr->segnum--;

	fds->seg.off = 0;
	fds->seg.size = 0;
}


static void
fellow_cache_seg_fini(const struct fellow_cache_seg *fcs)
{
	struct fellow_disk_seg	*fds;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fds = fcs->disk_seg;
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	AN(fcs->alloc.ptr);
	AN(fcs->u.fcs_len);
	assert(fcs->u.fcs_len <= fds->seg.size);
	fds->seg.size = fcs->u.fcs_len;
	fh(fds->fht, fds->fh, fcs->alloc.ptr, fcs->u.fcs_len);
}

static inline void
fellow_cache_seg_transition_locked_notincore(struct fellow_cache_seg *fcs,
    enum fcos_state to)
{

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	/*
	 * to/from INCORE requires lru operations
	 * see fellow_cache_seg_transition*
	 */
	assert(! FCOS_IS(fcs->state, INCORE));
	assert(! FCOS_IS(to, INCORE));

	assert(fcs->state != to);
	assert_fcos_transition(fcs->state, to);
	fcs->state = to;
	assert_cache_seg_consistency(fcs);
}

static inline void
fellow_cache_seg_transition_locked(
    struct fellow_lru_chgbatch *lcb, struct fellow_cache_seg *fcs,
    enum fcos_state from, enum fcos_state to)
{
	int o, n;

	DBG("%p %s %s", fcs, fcos_state_s[from], fcos_state_s[to]);
	o = fellow_cache_shouldlru(from, fcs->fco->oc, fcs->refcnt);
	n = fellow_cache_shouldlru(to, fcs->fco->oc, fcs->refcnt);

	assert(fcs->state == from);
	fcs->state = to;

	fellow_cache_lru_chg(lcb, fcs, n - o);
}

static void
fellow_cache_seg_transition(
    struct fellow_cache_seg *fcs,
    enum fcos_state from, enum fcos_state to)
{
	struct fellow_cache_obj *fco;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fco = fcs->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	assert(from != to);

	assert_fcos_transition(from, to);

	AZ(pthread_mutex_lock(&fco->mtx));
	fellow_cache_seg_transition_locked(lcb, fcs, from, to);
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(fcs);
	if (fcs->refcnt)
		AZ(pthread_cond_broadcast(&fco->cond));
	AZ(pthread_mutex_unlock(&fco->mtx));
}

/* return old refcnt */
static inline unsigned
fellow_cache_seg_ref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs)
{
	unsigned refcnt;
	int o, n;

	refcnt = fcs->refcnt++;

	o = fellow_cache_shouldlru(fcs->state, fcs->fco->oc, refcnt);
	n = fellow_cache_shouldlru(fcs->state, fcs->fco->oc, fcs->refcnt);

	fellow_cache_lru_chg(lcb, fcs, n - o);
	return (refcnt);
}

static inline unsigned
fellow_cache_seg_deref_n_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, unsigned nref)
{
	unsigned refcnt;
	int o, n;

	assert(fcs->refcnt >= nref);
	fcs->refcnt -= nref;
	refcnt = fcs->refcnt;

	o = fellow_cache_shouldlru(fcs->state, fcs->fco->oc, refcnt + nref);
	n = fellow_cache_shouldlru(fcs->state, fcs->fco->oc, refcnt);

	fellow_cache_lru_chg(lcb, fcs, n - o);
	return (refcnt);
}

/* return new refcnt */
static inline unsigned
fellow_cache_seg_deref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs)
{

	return (fellow_cache_seg_deref_n_locked(lcb, fcs, 1));
}

static void
fellow_cache_seg_unbusy(struct fellow_busy *fbo, struct fellow_cache_seg *fcs){
	const enum fcos_state from = FCS_BUSY, to = FCS_WRITING;
	struct fellow_cache_obj *fco;
	struct fellow_busy_io *fbio, fbiostk[1];

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	CHECK_OBJ_NOTNULL(fbo->fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fco = fcs->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	assert(fco == fbo->fco);
	AN(fcs->refcnt);
	AN(fcs->u.fcs_len);
	assert_fcos_transition(from, to);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	AZ(pthread_mutex_lock(&fco->mtx));
	fbo->io_outstanding++;
	fbio = fellow_busy_io_get(fbo, fbiostk);
	fellow_cache_seg_transition_locked(lcb, fcs, from, to);
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(fcs);
#ifdef WAIT_FOR_FCS_WRITING
	// we should never have a wait for FCS_WRITING
	if (fcs->refcnt)
		AZ(pthread_cond_broadcast(&fco->cond));
#endif
	AZ(pthread_mutex_unlock(&fco->mtx));

	fellow_cache_seg_fini(fcs);

	AN(fbio);
	fbio->type = FBIO_SEG;
	fbio->u.fcs = fcs;

	fellow_busy_io_submit(fbio);
}

static void
fellow_cache_obj_trim(const struct fellow_cache *fc,
    struct fellow_cache_obj *fco)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	uint16_t n;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	// no trim if fdo is embedded - XXX cleaner way?
	assert(FCO_FDO(fco) == FCO_FCS(fco)->alloc.ptr);

	fcsl = fco->fcsl;
	CHECK_OBJ(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fdsl = fcsl->fdsl;
	CHECK_OBJ_ORNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	if (fdsl == NULL)
		n = 0;
	else
		n = fdsl->nsegs;

	// for a busy object, fcsl and fdsl are embedded in fco/fdo
	AZ(fcsl->fcsl_sz);
	AZ(fcsl->fdsl_sz);

	assert(fcsl->lsegs >= n);
	fcsl->lsegs = n;
	buddy_trim1_ptr_extent(fc->membuddy, &fco->fco_mem,
	    fellow_cache_obj_with_seglist_size(n));
}

/* XXX can save some io for inlog == 0 */

static void
fellow_cache_obj_unbusy(struct fellow_busy *fbo, enum fcol_state wantlog)
{
	const enum fcos_state from = FCO_BUSY, to = FCO_WRITING;
	struct fellow_cache_obj *fco, *ofco;
	struct fellow_cache_seg *fcs;
	struct fellow_cache *fc;
	struct fellow_busy_io *fbio, fbiostk[1];

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	fcs = FCO_FCS(fco);
	assert_fcos_transition(from, to);

	assert(wantlog == FCOL_WANTLOG || wantlog == FCOL_NOLOG);

	AZ(pthread_mutex_lock(&fco->mtx));
	AZ(fcs->fco_infdb);
	fcs->fco_infdb = 1;
	AZ(pthread_mutex_lock(&fc->fdb_mtx));
	ofco = VRBT_INSERT(fellow_cache_fdb_head, &fc->fdb_head, fco);
	AZ(ofco);
	(*fc->g_mem_obj)++;
	AZ(pthread_mutex_unlock(&fc->fdb_mtx));

	if (likely(fco->logstate == FCOL_DUNNO))
		fco->logstate = wantlog;
	else
		assert(fco->logstate == FCOL_TOOLATE);

	/*
	 * fco has its io_outstanding from fellow_busy_obj_alloc()
	 * fbo->io_outstanding++;
	 */
	fbio = fellow_busy_io_get(fbo, fbiostk);
	fellow_cache_seg_transition_locked(lcb, fcs, from, to);
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(fcs);
#ifdef WAIT_FOR_FCS_WRITING
	// we should never have a wait for FCS_WRITING
	if (fcs->refcnt)
		AZ(pthread_cond_broadcast(&fco->cond));
#endif
	AZ(pthread_mutex_unlock(&fco->mtx));

	fellow_cache_obj_fini(fco);
	fellow_cache_obj_trim(fc, fco);

	AN(fbio);
	fbio->type = FBIO_SEG;
	fbio->u.fcs = fcs;

	fellow_busy_io_submit(fbio);
}

/*
 * ============================================================
 * ASYNC
 *
 */

// handle completed io based on info
static void
fellow_cache_read_complete(struct fellow_cache *fc, void *ptr, int32_t result)
{
	struct fellow_cache_seg *fcs;
	struct fellow_cache_obj *fco;
	struct fellow_cache_res fcr;
	enum fcos_state fcos_next;
	unsigned refcount;

	CAST_OBJ_NOTNULL(fcs, ptr, FELLOW_CACHE_SEG_MAGIC);
	assert(FCOS(fcs->state) == FCOS_READING);

	fco = fcs->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	assert(fcs->alloc.size <= INT32_MAX);
	if (FC_INJ || result < (int32_t)fcs->alloc.size) {
		fcr = FCR_IOFAIL("fcs read error");
		fcos_next = (typeof(fcos_next))FCOS_READFAIL;
	}
	else {
		fcr = FCR_OK(fco);
		fcos_next = (typeof(fcos_next))FCOS_CHECK;
	}

	//lint -e{655} bit-wise operation uses (compatible) enum's
	fcos_next |= (typeof(fcos_next))FCOS_HIGH(fcs->state);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	assert_fcos_transition(fcs->state, fcos_next);

	AZ(pthread_mutex_lock(&fco->mtx));
	(void) fellow_cache_obj_res(fc, fco, fcr);
	fellow_cache_seg_transition_locked(lcb, fcs, fcs->state, fcos_next);
	// io holds a ref on the seg and the fco
	if (fellow_cache_seg_deref_locked(lcb, fcs))
		AZ(pthread_cond_broadcast(&fco->cond));
	refcount = fellow_cache_obj_deref_locked(lcb, fc, fco);
	fellow_cache_lru_chgbatch_apply(lcb);
	if (refcount == 0)
		fellow_cache_obj_free(fc, &fco);
	else
		AZ(pthread_mutex_unlock(&fco->mtx));
}

// handle completed io based on info
static void
fellow_cache_async_write_complete(struct fellow_cache *fc,
    void *ptr, int32_t result)
{
	struct fellow_busy_io *fbio;
	struct fellow_busy *fbo;
	struct fellow_cache_seg *fcs = NULL;
	struct fellow_cache_obj *fco;
	struct fellow_cache_res fcr;
	struct fellow_disk_obj *fdo, *fdo2;
	enum fcos_state fcos_next = FCOS_INVAL;
	enum fellow_busy_io_e type;
	uint8_t io_outstanding;
	unsigned refcount;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fc->tune, STVFE_TUNE_MAGIC);

	CAST_OBJ_NOTNULL(fbio, ptr, FELLOW_BUSY_IO_MAGIC);

	if (result == -EAGAIN && fbio->retries++ < FBIO_MAX_RETRIES) {
		fellow_busy_io_submit(fbio);
		return;
	}

	type = fbio->type;

	fbo = fbio->fbo;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	// can potentially change the object and a segment
	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 2);

	if (type == FBIO_SEG) {
		fcs = fbio->u.fcs;
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		assert(fcs->fco == fco);

		assert(fcs->alloc.size <= INT32_MAX);

		if (FC_INJ || result < (int32_t)fcs->alloc.size)
			fcr = FCR_IOFAIL("fcs write error");
		else
			fcr = FCR_OK(fco);

		assert(FCOS(fcs->state) == FCOS_WRITING);
		fcos_next = (typeof(fcos_next))
		    (FCOS_HIGH(fcs->state) | FCOS_INCORE);
		assert_fcos_transition(fcs->state, fcos_next);

		if (fcs->state == FCO_WRITING) {
			fdo = fellow_disk_obj(fcs);
			fdo2 = fellow_disk_obj_trim(fc, fcs);
			assert(fdo == fdo2);
		}
	} else {
		assert(type == FBIO_SEGLIST);
		if (FC_INJ || result < (int32_t)fbio->u.seglist.reg.size)
			fcr = FCR_IOFAIL("seglist write error");
		else
			fcr = FCR_OK(fco);
	}

	/* we keep the FCO writing until all else is written
	 * to avoid a race with evict, which removes the oc, which
	 * we need for writing the log
	 *
	 * the writer acquired a ref, so we deref by one
	 *
	 */
	AZ(pthread_mutex_lock(&fco->mtx));
	memset(fbio, 0, sizeof *fbio);
	AN(fbo->io_outstanding);
	io_outstanding = --fbo->io_outstanding;
	if (type == FBIO_SEG) {
		AN(fcs);
		if (FCOS_HIGH(fcs->state) == FCS_HIGH) {
			/* transition a segment */
			assert(fcos_next != FCOS_INVAL);
			fellow_cache_seg_transition_locked(lcb, fcs,
			    fcs->state, fcos_next);
			if (fellow_cache_seg_deref_locked(lcb, fcs))
				AZ(pthread_cond_broadcast(&fco->cond));
		}
	}

	/*
	 * We latch an error, but _still_ add the object to the log, such that
	 * fellow_cache_obj_delete() can do a thin delete
	 */
	(void) fellow_cache_obj_res(fc, fco, fcr);

	//lint --e{456} Two execution paths are being combined...
	//lint --e{454} A thread mutex has been locked...
	if (io_outstanding == 0) {
		switch (fco->logstate) {
		case FCOL_WANTLOG:
			/* fellow_cache_obj_delete() can not race
			 * us because of wait for FCO_WRITING.
			 *
			 * unlock during busy_log_submit because
			 * of LRU and waiting allocs
			 *
			 * sfe_oc_event DOES race us and we may
			 * lose events between the time we write
			 * the log here and call stvfe_oc_log_submitted()
			 *
			 * the whole event thing is racy anyway, not sure
			 * how relevant...
			 */
			fellow_cache_lru_chgbatch_apply(lcb);
			AZ(pthread_mutex_unlock(&fco->mtx));

			fellow_busy_log_submit(fbo);
			stvfe_oc_log_submitted(fco->oc);

			AZ(pthread_mutex_lock(&fco->mtx));
			assert(fco->logstate == FCOL_WANTLOG);
			fco->logstate = FCOL_INLOG;
			break;
		case FCOL_NOLOG:
		case FCOL_TOOLATE:
			break;
		case FCOL_DUNNO:
		case FCOL_INLOG:
		case FCOL_DELETED:
			FC_WRONG("fellow_cache_async_write_complete "
			    "wrong logstate %d", fco->logstate);
			break;
		default:
			WRONG("fellow_cache_async_write_complete "
			    "invalid logstate");
		}

		fcs = FCO_FCS(fco);
		assert(fcs->state == FCO_WRITING);
		fcos_next = FCO_INCORE;

		/* transition the FCO */
		fellow_cache_seg_transition_locked(lcb, fcs,
		    fcs->state, fcos_next);
		refcount = fellow_cache_obj_deref_locked(lcb, fc, fco);
		fellow_cache_lru_chgbatch_apply(lcb);
		if (refcount == 0) {
			fellow_cache_obj_free(fc, &fco);
		}
		else {
			AZ(pthread_cond_broadcast(&fco->cond));
			AZ(pthread_mutex_unlock(&fco->mtx));
		}
	} else {
		fellow_cache_lru_chgbatch_apply(lcb);
		AZ(pthread_mutex_unlock(&fco->mtx));
	}

	if (io_outstanding)
		return;

	fellow_busy_free(&fbo);
}

static void
fellow_cache_seg_async_compl_cb(void *priv,
    /*lint -e{818}*/ struct fellow_io_status *status, unsigned n)
{
	struct fellow_cache *fc;
	unsigned u;

	CAST_OBJ_NOTNULL(fc, priv, FELLOW_CACHE_MAGIC);
	//lint -e{455} not locked mutex unlocked
	AZ(pthread_mutex_unlock(&fc->async_mtx));

	for (u = 0; u < n; u++) {
		switch (faio_info_type(status[u].info)) {
		case FAIOT_CACHE_READ:
			fellow_cache_read_complete(fc,
			    faio_info_ptr(status[u].info),
			    status[u].result);
			break;
		case FAIOT_CACHE_WRITE:
			fellow_cache_async_write_complete(fc,
			    faio_info_ptr(status[u].info),
			    status[u].result);
			break;
		default:
			WRONG("faio_info_type in fellow_cache");
		}
	}
	//lint --e{454} mutex locked but not unlocked
	AZ(pthread_mutex_lock(&fc->async_mtx));
}

static inline struct fellow_disk_seg *
fellow_cache_seg_io_check(const struct fellow_cache *fc,
    struct fellow_cache_seg *fcs)
{
	struct fellow_disk_seg *fds;
	size_t asz;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fds = fcs->disk_seg;
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	asz = fellow_rndup(fc->ffd, fds->seg.size);

	assert(fcs->alloc.size == asz);
	AN(fds->seg.off);
	AN(fds->seg.size);
	assert(fds->seg.size >= fcs->u.fcs_len);
	assert(fds->seg.size <= asz);
	fcs->u.fcs_len = fds->seg.size;

	// O_DIRECT alignment on 4K
	AZ((uintptr_t) fcs->alloc.ptr & FELLOW_BLOCK_ALIGN);
	AZ((uintptr_t) fcs->alloc.size & FELLOW_BLOCK_ALIGN);
	AZ((uintptr_t)fds->seg.off & FELLOW_BLOCK_ALIGN);

	return (fds);
}

static void
fellow_cache_seg_sync_read(struct fellow_cache *fc,
    struct fellow_cache_seg * const *segs, unsigned n)
{
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;

	while (n > 0) {
		fcs = *segs;
		fds = fellow_cache_seg_io_check(fc, fcs);

		CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
		assert(FCOS_IS(fcs->state, READING));
		assert(fds->seg.off >= 0);
		fellow_cache_read_complete(fc, fcs,
		    fellow_io_pread_sync(fc->ffd,
			fcs->alloc.ptr, fcs->alloc.size, fds->seg.off));
		n--;
		segs++;
	}
}

/* no fcs locking required, it needs to be referenced and have the
 * reading/writing state, so can't go away
 */
static void
fellow_cache_seg_async_read(struct fellow_cache *fc,
    struct fellow_cache_seg * const *segs, unsigned n)
{
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;
	uint64_t info;
	int r;

	AZ(pthread_mutex_lock(&fc->async_mtx));
	while (n > 0) {
		fcs = *segs;
		info = faio_ptr_info(FAIOT_CACHE_READ, fcs);
		fds = fellow_cache_seg_io_check(fc, fcs);

		CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
		assert(FCOS_IS(fcs->state, READING));
		r = fellow_io_read_async_enq(fc->async_ioctx,
		    info, fcs->alloc.ptr, fcs->alloc.size, fds->seg.off);
		if (! r)
			break;
		n--;
		segs++;
	}

	if (fc->async_idle)
		AZ(pthread_cond_signal(&fc->async_cond));
	AZ(pthread_mutex_unlock(&fc->async_mtx));

	if (n == 0)
		return;

	// could not submit, fall back to sync
	DBG("ASYNC FULL; sync fallback %u", n);
	fellow_cache_seg_sync_read(fc, segs, n);
}

static void *
fellow_cache_async_thread(void *priv)
{
	struct fellow_cache *fc;
	unsigned n, entries;

	CAST_OBJ_NOTNULL(fc, priv, FELLOW_CACHE_MAGIC);

	AZ(pthread_mutex_lock(&fc->async_mtx));
	entries = fellow_io_entries(fc->async_ioctx);
	while (fc->running) {
		struct fellow_io_status status[entries];
		n = fellow_io_wait_completions(fc->async_ioctx,
		    status, entries, 1, fellow_cache_seg_async_compl_cb, fc);
		if (n == 0) {
			fc->async_idle = 1;
			AZ(pthread_cond_wait(&fc->async_cond, &fc->async_mtx));
			fc->async_idle = 0;
		}
	}
	AZ(pthread_mutex_unlock(&fc->async_mtx));
	return (NULL);
}

static inline void
fellow_cache_seg_evict_locked(
    struct fellow_cache_seg *fcs, struct buddy_ptr_extent *alloc)
{
	AZ(fcs->lcb_add);
	AZ(fcs->lcb_remove);
	AZ(fcs->refcnt);
	AZ(fcs->fcs_onlru);
	assert(fcs->state == FCS_INCORE);
	*alloc = fcs->alloc;
	fcs->alloc = buddy_ptr_extent_nil;
	fcs->u.fcs_len = 0;
	fcs->state = FCS_DISK;
}

static inline void
fellow_cache_lru_seg_evict_locked(
    struct fellow_cache_seg *fcs, struct buddy_ptr_extent *alloc,
    struct fellow_cache_lru *lru)
{
	AN(fcs->fcs_onlru);
	fcs->fcs_onlru = 0;
	AN(lru->n);
	lru->n--;
	VTAILQ_REMOVE(&lru->lru_head, fcs, lru_list);
	fellow_cache_seg_evict_locked(fcs, alloc);
}

/* evict all segments of an object */
void
fellow_cache_obj_slim(const struct fellow_cache *fc,
    struct fellow_cache_obj *fco)
{
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(fc->membuddy, BUDDY_RETURNS_MAX);
	struct buddy_ptr_extent alloc;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_lru *lru;
	struct fcscursor c;
	unsigned ref;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	fcsc_init(&c, fco->fcsl);

	lru = fco->lru;
	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);

	AZ(pthread_mutex_lock(&fco->mtx));

	// we wait for loading complete to be able to ignore pending
	fellow_cache_seglists_wait_locked(fco);

	/* anything to do at all? */
	while ((fcs = fcsc_next_ignore_pending(&c)) != NULL)
		if (fcs->alloc.ptr)
			break;

	if (fcs == NULL || fcs->alloc.ptr == NULL)
		goto out;

	fcsc_init(&c, fco->fcsl);
	AZ(pthread_mutex_lock(&lru->lru_mtx));
	while ((fcs = fcsc_next_ignore_pending(&c)) != NULL) {
		if (fcs->alloc.ptr == NULL)
			break;
		ref = 0;
		while (fcs->state == FCS_READING || fcs->state == FCS_WRITING) {
			AZ(pthread_mutex_unlock(&lru->lru_mtx));

			// NULL: can not be on lru
			(void) fellow_cache_seg_ref_locked(NULL, fcs);
			ref++;
			fellow_cache_seg_wait_locked(fcs);

			AZ(pthread_mutex_lock(&lru->lru_mtx));
		}
		if (fcs->state == FCS_INCORE &&
		    ref > 0 && fcs->refcnt == ref) {
			/* we own all refs, can not be on LRU,
			 * so no need to go through full deref
			 */
			fcs->refcnt = 0;
			fellow_cache_seg_evict_locked(fcs, &alloc);
			AN(buddy_return_ptr_extent(rets, &alloc));
			continue;
		}
		if (fcs->state == FCS_INCORE && fcs->refcnt == 0) {
			AZ(ref);
			// remove from LRU
			fellow_cache_lru_seg_evict_locked(fcs, &alloc, lru);
			AN(buddy_return_ptr_extent(rets, &alloc));
			continue;
		}

		AZ(fcs->fcs_onlru);
		if (ref)
			(void) fellow_cache_seg_deref_n_locked(NULL, fcs, ref);
	}
	AZ(pthread_mutex_unlock(&lru->lru_mtx));

  out:
	AZ(pthread_mutex_unlock(&fco->mtx));
	buddy_return(rets);
	}

static int
fellow_cache_lru_work(struct worker *wrk, struct fellow_cache_lru *lru)
{
	struct fellow_cache_seg *fcs, *fcss, *resume;
	struct fellow_cache_obj *fco;
	struct objcore *oc;
	struct buddy_ptr_extent alloc;
	int r;

	/*lint --e{454,455,456}
	 *
	 * flexelint does not understand the locking structure here:
	 *
	 * - it does not understand trylock
	 * - we leave the loop either with break
	 *   - mtx unlocked
	 *   - fcs != NULL
	 * - or foreach finishes, then fcs == NULL and the mtx
	 *   needs to be unlocked
	 *
	 * this is safe because the oc holds a ref on the fco, so
	 * the LRU state of the fco does not change
	 */

	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);
	resume = lru->resume;
	CHECK_OBJ(resume, FELLOW_CACHE_SEG_MAGIC);

	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(lru->fc->membuddy, BUDDY_RETURNS_MAX);

	alloc = buddy_ptr_extent_nil;
	oc = NULL;
	fco = NULL;
	AZ(pthread_mutex_lock(&lru->lru_mtx));
	if (resume->fcs_onlru) {
		VTAILQ_REMOVE(&lru->lru_head, resume, lru_list);
		resume->fcs_onlru = 0;
	}

	//lint -e{850} loop variable modified in body
	VTAILQ_FOREACH_SAFE(fcs, &lru->lru_head, lru_list, fcss) {
		assert(fcs != fcss);
		// no use trying the same object again and again
		if (fcs->fco == fco)
			continue;
		fco = fcs->fco;
		AN(fco);
		assert(fco->lru == lru);
		r = pthread_mutex_trylock(&fco->mtx);
		if (r != 0) {
			assert(r == EBUSY);
			continue;
		}
		if (fcs->state == FCS_INCORE) {
			while (1) {
				fellow_cache_lru_seg_evict_locked(fcs,
				    &alloc, lru);
				AN(buddy_return_ptr_extent(rets, &alloc));

				if (rets->size < lru->fc->membuddy->deficit &&
				    fcss != NULL &&
				    fcss->state == FCS_INCORE &&
				    fcss->fco == fco) {
					fcs = fcss;
					fcss = VTAILQ_NEXT(fcs, lru_list);
					continue;
				}
				break;
			}

			AZ(pthread_mutex_unlock(&fco->mtx));
			fco = NULL;
			if (rets->size < lru->fc->membuddy->deficit)
				continue;
			AZ(pthread_mutex_unlock(&lru->lru_mtx));
			break;
		}
		if (fcs->state == FCO_INCORE) {
			oc = fco->oc;
			CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
			assert(oc->stobj->priv == fco);
			AZ(fcs->fco_lru_mutate);
			fcs->fco_lru_mutate = 1;

			AN(lru->n);
			lru->n--;

			AZ(resume->fcs_onlru);
			VTAILQ_INSERT_AFTER(&lru->lru_head,
			    fcs, resume, lru_list);
			resume->fcs_onlru = 1;
			VTAILQ_REMOVE(&lru->lru_head, fcs, lru_list);
			AZ(pthread_mutex_unlock(&lru->lru_mtx));

			r = stvfe_mutate(wrk, lru, oc);
			if (r) {
				// mutate was successful
				AZ(fcs->fco_lru_mutate);
				AZ(pthread_mutex_unlock(&fco->mtx));
				break;
			}

			/* mutate has failed
			 *
			 * FCO will be put back on LRU below when we have the
			 * lru lock again
			 */

			AZ(pthread_mutex_lock(&lru->lru_mtx));

			AN(fcs->fcs_onlru);
			AN(fcs->fco_lru_mutate);
			fcs->fco_lru_mutate = 0;

			AN(resume->fcs_onlru);
			VTAILQ_INSERT_BEFORE(resume, fcs, lru_list);
			lru->n++;

			AZ(pthread_mutex_unlock(&fco->mtx));

			fcss = VTAILQ_NEXT(resume, lru_list);
			VTAILQ_REMOVE(&lru->lru_head, resume, lru_list);
			resume->fcs_onlru = 0;

			oc = NULL;
			continue;
		}
		AZ(pthread_mutex_unlock(&fco->mtx));
		WRONG("fcs state in lru");
	}

	/*
	 * for a break from the foreach, fcs != NULL
	 *
	 * so for fcs == NULL, we know we have iterated over
	 * all of the LRU
	 */
	if (fcs == NULL)
		AZ(pthread_mutex_unlock(&lru->lru_mtx));

	AZ(alloc.ptr);
	buddy_return(rets);

	if (fcs == NULL) {
		AZ(oc);
		return (0);
	}

	/*
	 * at this point, we know we terminated the foreach
	 * with a break. If we have an fco, we need to deref it.
	 */
	if (fco != NULL) {
		AN(oc);
		// we removed the oc's reference on fco in stvfe_mutate()
		fellow_cache_obj_deref(lru->fc, fco);
	}
	return (1);
}

static void
reserve_free(buddy_t *buddy, struct buddy_ptr_page *r, unsigned n)
{
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(buddy, BUDDY_RETURNS_MAX);

	if (n == 0)
		return;

	AN(r);

	while (n--)
		AN(buddy_return_ptr_page(rets, r++));

	buddy_return(rets);
}

static void
reserve_req(struct buddy_reqs *reqs, unsigned n, unsigned bits)
{
	if (n > BUDDY_REQS_MAX)
		n = BUDDY_REQS_MAX;
	while (n--)
		AN(buddy_req_page(reqs, bits, 0));
}

static void
reserve_fill(struct buddy_ptr_page *r, const struct buddy_reqs *reqs, uint8_t n)
{
	uint8_t u;

	for (u = 0; u < n; u++) {
		AZ(r->ptr);
		*r++ = buddy_get_ptr_page(reqs, u);
	}
}

static void *
fellow_cache_lru_thread(struct worker *wrk, void *priv)
{
	struct fellow_cache_lru *lru;
#ifdef LRU_NOISE
	struct vsl_log vsl;
#endif
	struct buddy_reqs *reqs;
	const struct fellow_cache *fc;
	struct fellow_cache_seg resume[1];
	buddy_t *buddy;
	struct buddy_ptr_page *r = NULL;
	unsigned i, filled = 0, nr = 0, rc, cb;
	uint8_t n;
	size_t sz;

	CAST_OBJ_NOTNULL(lru, priv, FELLOW_CACHE_LRU_MAGIC);
	fc = lru->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	buddy = fc->membuddy;
	CHECK_OBJ(buddy, BUDDY_MAGIC);

	AZ(lru->lru_thread);
	lru->lru_thread = pthread_self();

	// fellow_cache_lru_seg_evict_locked()
	assert_fcos_transition(FCS_INCORE, FCS_DISK);

	reqs = BUDDY_REQS_STK(buddy, BUDDY_REQS_MAX);
	BUDDY_REQS_PRI(reqs, FEP_RESERVE);

	INIT_OBJ(resume, FELLOW_CACHE_SEG_MAGIC);
	AZ(lru->resume);
	lru->resume = resume;

#ifdef LRU_NOISE
	AZ(wrk->vsl);
	wrk->vsl = &vsl;
	VSL_Setup(wrk->vsl, NULL, (size_t)0);
#endif

	while (fc->running) {
		cb = fc->tune->chunk_exponent;
		rc = fc->tune->mem_reserve_chunks;

		// reserve pointers
		while (nr != rc) {
			if (nr != 0)
				AN(r);
			reserve_free(buddy, r, filled);
			filled = 0;
			nr = rc;
#ifdef LRU_NOISE
			VSLb(wrk->vsl, SLT_Debug,
			    "sfedsk reserve config cb=%u nr=%u",
			    cb, nr);
#endif
			if (nr == 0)
				break;

			sz = nr * sizeof *r;
			r = realloc(r, sz);
			AN(r);
			memset(r, 0, sz);
			break;
		};

		// fill reserve
		n = 1;
		while (filled < nr && buddy->waiting == 0 && n > 0) {
			AN(r);
			reserve_req(reqs, nr - filled, cb);
			(void) BUDDYF(alloc_async)(reqs);

			while (buddy->waiting)
				if (! fellow_cache_lru_work(wrk, lru))
					break;

			n = BUDDYF(alloc_async_ready)(reqs);
			assert(filled + n <= nr);
			reserve_fill(r + filled, reqs, n);
			filled += n;
			BUDDYF(alloc_async_done)(reqs);
		}

#ifdef LRU_NOISE
		if (nr)
			VSLb(wrk->vsl, SLT_Debug,
			    "sfemem reserve fill: %u", filled);

		VSL_Flush(wrk->vsl, 0);
#endif
		stvfe_sumstat(wrk);
		buddy_wait_needspace(buddy);

		// drain reserve
		while (filled > 0 && buddy->waiting > 0) {
			i = --filled;

			AN(r);
			AN(r[i].ptr);
			BUDDYF(return1_ptr_page)(buddy, &r[i]);
		}

#ifdef LRU_NOISE
		if (nr)
			VSLb(wrk->vsl, SLT_Debug,
			    "sfemem reserve use : %u left", filled);
#endif

		while (buddy->waiting)
			if (! fellow_cache_lru_work(wrk, lru)) {
				/* give other threads a change to
				 * get the lru lock */
				(void)usleep(10*1000);
			}
	}
	AN(lru->resume);
	AZ(pthread_mutex_lock(&lru->lru_mtx));
	if (lru->resume->fcs_onlru) {
		VTAILQ_REMOVE(&lru->lru_head, lru->resume, lru_list);
		lru->resume->fcs_onlru = 0;
	}
	AZ(pthread_mutex_unlock(&lru->lru_mtx));
	lru->resume = NULL;
	reserve_free(buddy, r, filled);
	free(r);
	return (NULL);
}

/* returns if moved */
int
fellow_cache_obj_lru_touch(struct fellow_cache_obj *fco)
{
	struct buddy_ptr_page dowry = buddy_ptr_page_nil;
	struct fellow_cache_lru *lru;
	struct fellow_cache_seg *fcs;
	int r = 0;

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	lru = fco->lru;
	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);
	fcs = FCO_FCS(fco);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	if (! fcs->fcs_onlru)
		goto out;

	r = 1;
	// mutate should be failing, so it will do the lru move
	if (fcs->fco_lru_mutate)
		goto out;

	assert(fellow_cache_shouldlru(fcs->state, fco->oc, fcs->refcnt));

	r = pthread_mutex_trylock(&lru->lru_mtx);
	if (r != 0) {
		assert(r == EBUSY);
		return (0);
	}
	if (! fcs->fco_lru_mutate) {
		VTAILQ_REMOVE(&lru->lru_head, fcs, lru_list);
		VTAILQ_INSERT_TAIL(&lru->lru_head, fcs, lru_list);
	}
	//lint -e{455} flexelint does not grok trylock
	AZ(pthread_mutex_unlock(&lru->lru_mtx));

	r = 1;
  out:
	// deliberately unlocked, not critical if update is missed
	if (fco->ntouched++ > 3 && fco->fco_dowry.bits) {
		/* if the object has been touched 4 times and
		 * still has the dowry, it probably only receives
		 * HEAD requests, so we should use the memory for
		 * some better purpose
		 */
		AZ(pthread_mutex_lock(&fco->mtx));
		TAKE(dowry, fco->fco_dowry);
		AZ(pthread_mutex_unlock(&fco->mtx));
		if (dowry.bits) {
			CHECK_OBJ_NOTNULL(lru->fc, FELLOW_CACHE_MAGIC);
			buddy_return1_ptr_page(lru->fc->membuddy, &dowry);
		}
	}

	return (r);
}

// not unter logmtx
static void
fellow_cache_async_init(struct fellow_cache *fc, fellow_task_run_t taskrun)
{
	unsigned entries;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	entries = fellow_io_ring_size("fellow_cache_io_entries");
	DBG("io entries %u", entries);

	AZ(pthread_mutex_init(&fc->async_mtx, &fc_mtxattr_errorcheck));
	AZ(pthread_cond_init(&fc->async_cond, NULL));
	fc->async_ioctx = fellow_io_init(fellow_fd(fc->ffd),
	    entries, fc->membuddy->area, fc->membuddy->map->size,
	    taskrun);
	AN(fc->async_ioctx);
	AZ(pthread_create(&fc->async_thread, NULL,
		fellow_cache_async_thread, fc));
}

static void
fellow_cache_async_fini(struct fellow_cache *fc)
{
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	AZ(pthread_mutex_lock(&fc->async_mtx));
	AZ(pthread_cond_signal(&fc->async_cond));
	AZ(pthread_mutex_unlock(&fc->async_mtx));
	AZ(pthread_join(fc->async_thread, NULL));
	AZ(pthread_cond_destroy(&fc->async_cond));
	AZ(pthread_mutex_destroy(&fc->async_mtx));
}


/* .
 * .
 * .
 * ASYNC END
 * ============================================================
 */

static void
fellow_cache_seg_deref(struct fellow_cache_seg * const *segs, unsigned n)
{
	struct fellow_cache_seg *fcs;
	struct fellow_cache_obj *fco;

	CHECK_OBJ_NOTNULL(*segs, FELLOW_CACHE_SEG_MAGIC);
	AN(n);
	fco = (*segs)->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 64);

	AZ(pthread_mutex_lock(&fco->mtx));
	while (n--) {
		fcs = *segs++;
		assert(fcs->fco == fco);
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		(void) fellow_cache_seg_deref_locked(lcb, fcs);
	}
	fellow_cache_lru_chgbatch_apply(lcb);
	AZ(pthread_mutex_unlock(&fco->mtx));
}

struct fcoi_deref {
	unsigned		magic;
#define FCOI_DEREF_MAGIC	0x2a16ec74
	unsigned		n, max;
	struct fellow_cache_seg **segs;
	objiterate_f		*func;
	void			*priv;
	struct buddy_returns	*memret;
};

static inline void
fcoi_add(struct fcoi_deref *fcoid, struct fellow_cache_seg *fcs)
{

	CHECK_OBJ_NOTNULL(fcoid, FCOI_DEREF_MAGIC);
	AN(fcoid->segs);
	assert(fcoid->n < fcoid->max);

	fcoid->segs[fcoid->n++] = fcs;
}

/*
 * deref when we know a flush has just happened or not needed
 */
static inline void
fcoi_deref(struct fcoi_deref *fcoid)
{

	CHECK_OBJ_NOTNULL(fcoid, FCOI_DEREF_MAGIC);

	buddy_return(fcoid->memret);

	if (fcoid->n == 0)
		return;

	AN(fcoid->segs);
	assert(fcoid->n <= fcoid->max);

	fellow_cache_seg_deref(fcoid->segs, fcoid->n);
	fcoid->n = 0;
}

/*
 * when deref'ing from from _obj_iter(), we need to flush first.
 * this function wraps the deref'ing in a struct
 */
static int
fellow_cache_obj_iter_flush_deref(struct fcoi_deref *fcoid)
{
	int r;

	CHECK_OBJ_NOTNULL(fcoid, FCOI_DEREF_MAGIC);

	buddy_return(fcoid->memret);

	if (fcoid->n == 0)
		return (0);

	AN(fcoid->func);
	r = fcoid->func(fcoid->priv, OBJ_ITER_FLUSH, NULL, (size_t)0);

	fcoi_deref(fcoid);

	return (r);
}

static const char *
fellow_cache_seg_check(struct fellow_cache_seg *fcs)
{
	const struct fellow_disk_seg *fds;
	struct fellow_cache_obj *fco;
	enum fcos_state to = FCS_INCORE;
	const char *err = NULL;

	switch (fcs->state) {
	case FCS_READFAIL:
		return ("segment FCS_READFAIL");
	case FCS_BUSY:
	case FCS_WRITING:
	case FCS_INCORE:
		return (NULL);
	case FCS_CHECK:
		break;
	default:
		WRONG("segment state in _check");
	}

	fds = fcs->disk_seg;
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
#ifdef TODO_READ_ERR
	if ((fds->segnum % 16 == 0 && FC_INJ) ||
	    fhcmp(fds->fht, fds->fh, fcs->alloc.ptr, fcs->u.fcs_len)) {
		err = "segment checksum error";
		to = FCS_READFAIL;
	}
#endif

	fco = fcs->fco;
	AZ(pthread_mutex_lock(&fco->mtx));
	AN(fcs->refcnt);
	if (fcs->state == FCS_CHECK) {
		// re-check, could have raced
		fellow_cache_seg_transition_locked(NULL, fcs,
		    FCS_CHECK, to);
		AZ(pthread_cond_broadcast(&fco->cond));
	}
	AZ(pthread_mutex_unlock(&fco->mtx));

	return (err);
}


#define NREQS (newreqs ? 1 : 0)
#define OREQS (newreqs ? 0 : 1)

/* return 1 if allocation has been assigned */
static inline int
fellow_cache_obj_readahead_assign_or_request(
    const struct fellow_cache *fc,
    struct fellow_cache_seg *fcs,
    struct buddy_returns *rets,
    struct buddy_reqs reqs[2], unsigned newreqs)
{
	struct buddy_ptr_extent mem;
	size_t sz;
	int r;

	assert(fcs->state == FCS_DISK);
	AZ(fcs->alloc.ptr);

	sz = fellow_rndup(fc->ffd, fcs->disk_seg->seg.size);
	mem = buddy_get_next_ptr_extent(&reqs[OREQS]);
	while (mem.ptr != NULL && mem.size != sz) {
		DBG("%zu != %zu", mem.size, sz);
		AN(buddy_return_ptr_extent(rets, &mem));
		mem = buddy_get_next_ptr_extent(&reqs[OREQS]);
	}
	if (mem.ptr != NULL) {
		DBG("success %p", mem.ptr);
		fcs->alloc = mem;
		return (1);
	}
	DBG("fail %u", 0);
	r = buddy_req_extent(&reqs[NREQS], sz, 0);
	if (r == 0)
		assert(errno == ENOSPC);
	return (0);
}

#undef NREQS
#undef OREQS

#define NREQS (*newreqs ? 1 : 0)
#define OREQS (*newreqs ? 0 : 1)

static void
fellow_cache_obj_readahead(
    struct fellow_cache *fc,
    struct fcscursor *rac, struct fellow_cache_obj *fco,
    struct fellow_cache_seg *ra[], const unsigned mod /* length of ra */,
    unsigned *ranp, const unsigned ranto, unsigned need,
    struct fcoi_deref *fcoid,
    struct buddy_reqs reqs[2], unsigned *newreqs)
{
	struct fellow_cache_seg *fcs;
	struct fcscursor c;
	unsigned ran, ckp, u, n, needdisk, ion;
	struct buddy_ptr_page dowry;

	/* jump target for need == 1 and sync allocation */
  again:
	needdisk = 0;
	ion = 0;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	AN(rac);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	AN(ra);
	AN(mod);
	AN(ranp);
	ran = *ranp;
	assert(need <= 1);
	AN(fcoid);
	AN(reqs);
	AN(newreqs);
	assert(*newreqs <= 1);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 64);
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(fc->membuddy, BUDDY_RETURNS_MAX);

	assert(ranto >= ran);
	n = (ranto - ran) + 1;
	struct fellow_cache_seg *iosegs[n];
	memset(iosegs, 0, sizeof iosegs);

	c = *rac;

	// unlocked check
	if (! (ran < ranto &&
		(fcs = (need
		 ? fcsc_next_wait_locked(fco, &c)
		 : fcsc_next_ignore_pending(&c))) != NULL &&
		(need || FCOS(fcs->state) > FCOS_BUSY)))
		return;

	c = *rac;

	// update available allocations
	(void) buddy_alloc_async_ready(&reqs[OREQS]);

	BUDDY_REQS_PRI(&reqs[NREQS], FEP_MEM_ITER);

	ckp = ranto;
	AZ(pthread_mutex_lock(&fco->mtx));

	/* with need, we enter the loop also for FCOS_BUSY, but only once
	 *
	 * after the loop, needdisk signifies if we need the radisk
	 */
	while (ran < ranto &&
	    (fcs = (need
	     ? fcsc_next_wait_locked(fco, &c)
	     : fcsc_next_ignore_pending(&c))) != NULL &&
	    (need || FCOS(fcs->state) > FCOS_BUSY)) {
		if (need)
			assert(FCOS(fcs->state) >= FCOS_BUSY);
		else
			assert(FCOS(fcs->state) > FCOS_BUSY);
		assert(fcs->fco == fco);

		/* ref all incore-ish, remember _DISK */
		switch (fcs->state) {
		case FCS_READFAIL:
			goto err;
		case FCS_BUSY:
		case FCS_READING:
		case FCS_WRITING:
		case FCS_CHECK:
		case FCS_INCORE:
			goto ref;
		case FCS_DISK:
			break;
		default:
			WRONG("_readahead fcs->state");
		}

		assert(fcs->state == FCS_DISK);
		if (fellow_cache_obj_readahead_assign_or_request(fc, fcs,
			rets, reqs, *newreqs) == 0) {
			if (ran < ckp)
				ckp = ran;
			// end the loop to sync wait for single allocation
			if (need > needdisk) {
				needdisk = need;
				need = 0;
				// because sync wait, raise pri
				BUDDY_REQS_PRI(&reqs[NREQS], FEP_MEM_ITERPRI);
				break;
			}
			goto ref;
		}

		// ref for IO
		(void) fellow_cache_seg_ref_locked(NULL, fcs);
		fellow_cache_seg_transition_locked_notincore(fcs, FCS_READING);
		iosegs[ion++] = fcs;

	  ref:
		need = 0;

		/*
		 * for goto again, references are already taken
		 */

		if (ra[ran % mod] == fcs)
			AN(fcs->refcnt);
		else {
			(void) fellow_cache_seg_ref_locked(lcb, fcs);
			AZ(ra[ran % mod]);
			ra[ran % mod] = fcs;
		}
		ran++;
	}
  err:
	AN(FCO_REFCNT(fco));
	FCO_REFCNT(fco) += ion;
	fellow_cache_lru_chgbatch_apply(lcb);
	TAKE(dowry, fco->fco_dowry);
	AZ(pthread_mutex_unlock(&fco->mtx));

	if (ckp < ran)
		ran = ckp;

	if (ion)
		fellow_cache_seg_async_read(fc, iosegs, ion);

	// final assertions & advance read-ahead cursor
	for (u = *ranp; u < ran; u++) {
		fcs = fcsc_next(rac);
		assert(fcs == ra[u % mod]);
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		AN(fcs->refcnt);
	}
	*ranp = ran;

	(void) buddy_alloc_async(&reqs[NREQS]);
	if (dowry.bits)
		AN(buddy_return_ptr_page(rets, &dowry));
	buddy_alloc_async_done(&reqs[OREQS]);
	buddy_return(rets);
	uint8_t rdy = buddy_alloc_async_ready(&reqs[NREQS]);

	// swap OREQS <=> NREQS
	*newreqs = OREQS;

	if (fcs != NULL && fcs->state == FCS_READFAIL)
		return;

	// if allocation succeeded immediately, kick off I/O
	if (rdy)
		goto again;

	if (needdisk) {
		AZ(need);
		(void) fellow_cache_obj_iter_flush_deref(fcoid);
		AN(buddy_alloc_async_wait(&reqs[OREQS]));
		goto again;
	}
}

/*
 * ra[] is a ring of pointers to fcses which we (potentially)
 * read ahead.
 *
 * ra[n % mod] is the current fcs, anything after that are read
 * aheads.
 * ran the next element to be read ahead
 *
 * c is a cursor tracking the current fcs
 * rac is a cursor tracking the read ahead
 *
 */
struct fellow_cache_res
fellow_cache_obj_iter(struct fellow_cache *fc, struct fellow_cache_obj *fco,
    void *priv, objiterate_f func, int final)
{
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fc->tune, STVFE_TUNE_MAGIC);

	struct fellow_cache_res fcr;
	unsigned readahead = fc->tune->readahead;
	unsigned mod = readahead + 1;
	struct fellow_cache_seg *fcsnext, *fcs, *ra[mod], *deref[mod];
	struct fcscursor c, rac;
	unsigned need, n = 0, ran = 0, flags, flush;
	struct fcoi_deref fcoid[1];
	const char *err;
	ssize_t sz;
	int ret2;

	// 56 bytes per i_reqalloc + 176 bytes per reqs
	// 56 * 7 + 2 * 176 = 744
	struct buddy_reqs reqs[2] = {
		BUDDY_REQS_LIT(fc->membuddy, 3),
		BUDDY_REQS_LIT(fc->membuddy, 4)
	};
	unsigned newreqs = 0;

	// stack usage
	assert(readahead <= 31);

	if (fco->fcr.status != fcr_ok)
		return (fco->fcr);

	// fcr_ok is also returned if func() != 0
	memset(&fcr, 0, sizeof fcr);
	fcr.status = fcr_ok;

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	memset(ra, 0, sizeof ra);
	memset(deref, 0, sizeof deref);

	INIT_OBJ(fcoid, FCOI_DEREF_MAGIC);
	fcoid->max = mod;
	fcoid->segs = deref;
	fcoid->func = func;
	fcoid->priv = priv;
	fcoid->memret = BUDDY_RETURNS_STK(fc->membuddy, 1);

	fellow_cache_seglists_need(fc, fco);
	fcsc_init(&c, fco->fcsl);
	rac = c;

	flags = final ? OBJ_ITER_FLUSH : 0;
	flush = 0;
	while ((fcs = fcsc_next_wait(fco, &c)) != NULL) {
		/*
		 * fellow_stream_f/test_iter_f ensure
		 * that we do not read past the last busy segment
		 */
		assert(FCOS(fcs->state) >= FCOS_BUSY);
		assert(fcs->fco == fco);

		need = ran == n ? 1 : 0;

		assert(ran >= n);
		if (ran - n <= readahead / 2 + need) {
			DBG("(ran - n) %u <= %u + %u", ran - n,
			    readahead / 2, need);
			fellow_cache_obj_readahead(fc, &rac, fco, ra, mod, &ran,
			    n + readahead + need, need, fcoid,
			    reqs, &newreqs);
		}
		DBG("ran %u", ran);
		assert(ran > n);
		assert(ra[n % mod] == fcs);

		// aborted busy segment
		if (fcs->state == FCS_USABLE)
			break;

		if (fcs->state == FCS_READING) {
			/* before we wait, do some useful work
			 * and free memory
			 */
			fcr.r.integer =
			    fellow_cache_obj_iter_flush_deref(fcoid);
			if (fcr.r.integer)
				break;
			AZ(pthread_mutex_lock(&fcs->fco->mtx));
			while (fcs->state == FCS_READING)
				fellow_cache_seg_wait_locked(fcs);
			AZ(pthread_mutex_unlock(&fcs->fco->mtx));
		}

		err = fellow_cache_seg_check(fcs);
		if (err != NULL) {
			fcr = FCR_IOFAIL(err);
			break;
		}

		assert(fcs->state != FCS_READFAIL);

		/* peek at next segment for END
		 * flush if next segment not in ram or no deref space left
		 *
		 * for readahead == 0, this implies flushing always because
		 * derefn == mod - 1 == 0
		 */
		AZ(flags & OBJ_ITER_END);
		flush = 0;
		if (fcs->state == FCS_BUSY) {
			flush = OBJ_ITER_FLUSH;
		}
		else if (((fcsnext = fcsc_peek_wait(fco, &c)) == NULL) ||
		    fcsnext->state == FCS_USABLE) {
			flags |= OBJ_ITER_END;
			flush = OBJ_ITER_FLUSH;
		}
		else if (fcoid->n == fcoid->max - 1 ||
		    fcsnext->state != FCS_INCORE) {
			flush = OBJ_ITER_FLUSH;
		}

		assert(ra[n % mod] == fcs);
		ra[n % mod] = NULL;

		// for BUSY, we pass the maximum segment size
		// and our func-wrapper will look after the amount
		// to write
		if (fcs->state == FCS_BUSY)
			sz = (ssize_t)fcs->alloc.size;
		else
			sz = (ssize_t)fcs->disk_seg->seg.size;

		fcr.r.integer = func(priv, flags | flush, fcs->alloc.ptr, sz);

		if (final) {
			AZ(fcoid->n);
			AN(flags & OBJ_ITER_FLUSH);
			/* if the opportunistic free fails, the segment will
			 * get deleted when the object is */
			AZ(pthread_mutex_lock(&fcs->fco->mtx));
			if (fcs->refcnt == 1)
				fellow_cache_seg_free(fcoid->memret, fcs, 1);
			else {
				// NULL: refcount must be > 1, so cant be on LRU
				AN(fellow_cache_seg_deref_locked(NULL, fcs));
			}
			AZ(pthread_mutex_unlock(&fcs->fco->mtx));
		} else {
			fcoi_add(fcoid, fcs);
			if (flush)
				fcoi_deref(fcoid);
		}
		assert(fcr.status == fcr_ok);
		if (fcr.r.integer)
			break;
		n++;
	}

	if (fcr.status != fcr_ok || fcr.r.integer != 0)
		while (mod--) {
			if (ra[mod] != NULL)
				fellow_cache_seg_deref(&ra[mod], 1);
		}
	else
		while (mod--)
			AZ(ra[mod]);

	fcoi_deref(fcoid);

	buddy_alloc_async_done(&reqs[0]);
	buddy_alloc_async_done(&reqs[1]);


	if ((flags & OBJ_ITER_END) == 0 &&
	    (fcr.status == fcr_ok && fcr.r.integer == 0)) {
		ret2 = func(priv, OBJ_ITER_END, NULL, (size_t)0);
		if (fcr.status == fcr_ok && fcr.r.integer == 0)
			fcr.r.integer = ret2;
	}

	// if ok, wait for seglist loading to complete to see errors
	if (fcr.status == fcr_ok)
		fellow_cache_seglists_wait(fco);

	return (fellow_cache_obj_res(fc, fco, fcr));
}

/* Auxiliary attributes
 * ref is never returned and just ignored by
 * fellow_cache_seg_auxattr_free()
 *
 */
static void
fellow_cache_seg_auxattr_ref_in(
    struct fellow_cache *fc,
    struct fellow_cache_seg *fcs)
{
	struct fellow_cache_obj *fco;
	struct buddy_ptr_extent mem;
	unsigned io = 0;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	if (fcs->state == FCS_INCORE && fcs->refcnt >= 3)
		return;

	fco = fcs->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	/* racy memory allocation */
	mem = buddy_alloc1_ptr_extent_wait(fc->membuddy, FEP_META,
	    fellow_rndup(fc->ffd, fcs->disk_seg->seg.size), 0);
	AN(mem.ptr);

	AZ(pthread_mutex_lock(&fco->mtx));
	(void) fellow_cache_seg_ref_locked(lcb, fcs);

	while (fcs->state == FCS_BUSY || fcs->state == FCS_READING) {
		fellow_cache_lru_chgbatch_apply(lcb);
		fellow_cache_seg_wait_locked(fcs);
	}
	switch (fcs->state) {
	case FCS_BUSY:
	case FCS_READING:
		WRONG("auxattr ref_in: state can't be BUSY or READING");
		break;
	case FCS_READFAIL:
		// will fail in fellow_cache_seg_check()
		break;
	case FCS_WRITING:
	case FCS_CHECK:
	case FCS_INCORE:
		break;
	case FCS_DISK:
		// reference for io
		(void) fellow_cache_seg_ref_locked(lcb, fcs);

		fcs->alloc = mem;
		mem = buddy_ptr_extent_nil;

		fellow_cache_seg_transition_locked_notincore(fcs, FCS_READING);
		io = 1;
		break;
	default:
		WRONG("cache_seg_in state");
	}
	AN(FCO_REFCNT(fco));
	FCO_REFCNT(fco) += io;
	fellow_cache_lru_chgbatch_apply(lcb);
	AZ(pthread_mutex_unlock(&fco->mtx));

	if (mem.ptr)
		buddy_return1_ptr_extent(fc->membuddy, &mem);

	if (io)
		fellow_cache_seg_sync_read(fc, &fcs, 1);
}

static struct fellow_cache_res
fellow_busy_body_seg_next(struct fellow_busy *fbo)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_res fcr;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fcsl = fbo->body_seglist;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fdsl = fcsl->fdsl;
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

	assert(fdsl->nsegs <= fdsl->lsegs);
	if (fdsl->nsegs == fdsl->lsegs) {
		fcr = fellow_busy_body_seglist_alloc(fbo, fcsl);
		if (fcr.status != fcr_ok)
			return (fcr);
		CAST_OBJ_NOTNULL(fcsl, fcr.r.ptr, FELLOW_CACHE_SEGLIST_MAGIC);
		fbo->body_seglist = fcsl;
		fdsl = fcsl->fdsl;
		AZ(fdsl->nsegs);
		AN(fdsl->lsegs);
	}

	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	assert(fdsl->nsegs < fcsl->lsegs);

	fcs = &fcsl->segs[fdsl->nsegs];
	fbo->body_seg = fcs;
	assert(fcs->state == FCS_USABLE);
	assert(fcs->fco == fbo->fco);
	AZ(fcs->refcnt);
	return (FCR_OK(fcs));
}

/*
 * segment already has disk space allocated, allocate memory
 * half way between FCS_USABLE and FCS_BUSY, returns BUSY
 *
 * used for body and auxattr
 */
static size_t fellow_busy_seg_memalloc(struct fellow_busy *fbo,
    struct fellow_cache_seg *fcs, int8_t cram)
{
	struct fellow_disk_seg *fds;
	struct buddy_ptr_page mem;
	size_t sz;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fds = fcs->disk_seg;
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	assert (fcs->state == FCS_USABLE);
	AN(fds->seg.off);
	AN(fds->seg.size);

	AZ(fcs->alloc.ptr);
	AZ(fcs->alloc.size);

	if (FC_INJ)
		return (0);

	if (cram != 0) {
		cram = buddy_cramlimit_bits(fds->seg.size,
		    fbo->fc->tune->cram, MIN_FELLOW_BITS);
	}

	mem = buddy_get_next_ptr_page(
	    fbo_segmem_get(fbo->segmem, fbo->fc->tune));
	AN(mem.ptr);
	sz = (size_t)1 << mem.bits;
	if (sz >= fds->seg.size) {
		fcs->alloc.ptr = mem.ptr;
		fcs->alloc.size = sz;
		buddy_trim1_ptr_extent(fbo->fc->membuddy, &fcs->alloc,
		    fds->seg.size);
	}
	else {
		buddy_return1_ptr_page(fbo->fc->membuddy, &mem);
		fcs->alloc = buddy_alloc1_ptr_extent_wait(fbo->fc->membuddy,
		    FEP_SPCPRI, fds->seg.size, cram);
	}

	if (fcs->alloc.ptr == NULL)
		return (0);

	// cram needs to be chosen correctly
	assert(fcs->alloc.size >= MIN_FELLOW_BLOCK);
	assert((fcs->alloc.size & FELLOW_BLOCK_ALIGN) == 0);
	// membuddy min < dskbuddy min
	assert(fcs->alloc.size <= fds->seg.size);

	memset(fcs->alloc.ptr, 0, fcs->alloc.size);

	AZ(fcs->refcnt);
	fcs->refcnt = 1;
	fellow_cache_seg_transition_locked_notincore(fcs, FCS_BUSY);

	return (fcs->alloc.size);
}

/*
 * segment for auxattr
 */
static int
fellow_busy_seg_alloc(struct fellow_busy *fbo,
    struct fellow_cache_seg *fcs, size_t size)
{
	struct fellow_cache *fc;
	struct buddy_off_extent *fdr;
	struct fellow_disk_seg *fds;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	assert (fcs->state == FCS_USABLE);
	assert_cache_seg_consistency(fcs);

	fds = fcs->disk_seg;
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	fdr = fellow_busy_region_alloc(fbo, size, 0);
	if (fdr == NULL)
		return (0);
	fds->seg = *fdr;
	if (fellow_busy_seg_memalloc(fbo, fcs, 0) == 0)
		// we do not need to free because we use the busy region
		return (0);
	return (1);
}


// varnish-cache will ask for content-length if it is known.
// otherwise it will ask for cache_param->fetch_chunksize
// so we only need to limit that for mem
// for disk, we need to multiply by two each time we extend

struct fellow_cache_res
fellow_busy_obj_getspace(struct fellow_busy *fbo, size_t *sz, uint8_t **ptr)
{
	struct fellow_cache_seg *fcs, *unbusy = NULL;
	size_t max, spc, delta;
	struct fellow_cache_res fcr;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	AN(sz);
	assert(*sz > 0);
	AN(ptr);

	// #33 https://github.com/varnishcache/varnish-cache/pull/4013
	if (fbo->body_seglist == NULL)
		WRONG("_getspace() called after _trimstore()");

	CHECK_OBJ_NOTNULL(fbo->fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fbo->fc->tune, STVFE_TUNE_MAGIC);
	max = (size_t)1 << fbo->fc->tune->chunk_exponent;
	assert(max <= FIO_MAX);

	if (fbo->sz_returned >= fbo->fc->tune->objsize_max)
		return (FCR_ALLOCFAIL("objsize_max reached"));
	if (*sz + fbo->sz_returned > fbo->fc->tune->objsize_max)
		*sz = fbo->fc->tune->objsize_max - fbo->sz_returned;

	/*
	 * for content-length, varnish-cache will ask for less and less.
	 *
	 * if it keeps on asking for more, we
	 * increment each time we exceed the previous estimate
	 */

	if (fbo->sz_estimate == 0) {
		fbo->sz_estimate = *sz;
		fbo->sz_increment = *sz;
	} else if (*sz + fbo->sz_returned > fbo->sz_estimate) {
		fbo->grown++;

		delta = *sz + fbo->sz_returned - fbo->sz_estimate;

		if (fbo->sz_estimate > fbo->fc->tune->objsize_max) {
			fbo->sz_estimate = fbo->fc->tune->objsize_max;
			fbo->sz_increment = 0;
		} else if (fbo->sz_increment < delta) {
			fbo->sz_estimate += delta;
			fbo->sz_increment = delta;
		} else {
			fbo->sz_estimate += fbo->sz_increment;
			fbo->sz_increment <<= 1;
		}
	}

	fcs = fbo->body_seg;
	if (fcs != NULL && fcs->state == FCS_BUSY) {
		AN(fcs->alloc.size);
		if (fcs->u.fcs_len < fcs->alloc.size) {
			spc = fcs->alloc.size - fcs->u.fcs_len;
			assert(spc > 0);
			if (spc < *sz)
				*sz = spc;
			*ptr = (uint8_t *)fcs->alloc.ptr + fcs->u.fcs_len;
			return (FCR_OK(fbo));
		}

		assert(fcs->u.fcs_len == fcs->alloc.size);
		unbusy = fcs;
		fcs = NULL;
	}
	if (fcs == NULL) {
		fcr = fellow_busy_body_seg_next(fbo);
		if (fcr.status != fcr_ok)
			goto out;
		CAST_OBJ_NOTNULL(fcs, fcr.r.ptr, FELLOW_CACHE_SEG_MAGIC);
	}

	assert(fcs->state == FCS_USABLE);
	assert_cache_seg_consistency(fcs);

	spc = *sz;
	if (spc > max)
		spc = max;

	/* we set the state of the next segment to BUSY
	 * before we unbusy the previous to signal to
	 * _iter() that more data is to come
	 */

	spc = fellow_busy_body_seg_alloc(fbo, fcs->disk_seg, spc);
	if (spc == 0) {
		fcr = FCR_ALLOCFAIL("body disk seg alloc");
		goto out;
	}

	spc = fellow_busy_seg_memalloc(fbo, fcs, fbo->fc->tune->cram);
	if (spc == 0) {
		fellow_busy_body_seg_return(fbo, fcs->disk_seg);
		fcr = FCR_ALLOCFAIL("body memory seg alloc");
		goto out;
	}
	// reduce used length of region if fcs alloc crammed
	fellow_busy_body_seg_adjust(fbo, fcs);

	assert(fcs->state == FCS_BUSY);
	AN(fbo->body_seglist);
	AN(fbo->body_seglist->fdsl);
	fbo->body_seglist->fdsl->nsegs++;
	assert(fbo->body_seglist->fdsl->nsegs <=
	    fbo->body_seglist->fdsl->lsegs);

	*sz = spc;
	*ptr = fcs->alloc.ptr;

	fcr = FCR_OK(fbo);
  out:
	if (unbusy != NULL)
		fellow_cache_seg_unbusy(fbo, unbusy);

	return (fcr);
}

// caller commits used space
void
fellow_busy_obj_extend(struct fellow_busy *fbo, size_t l)
{
	struct fellow_cache_seg *fcs;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	assert(l > 0);

	fcs = fbo->body_seg;
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	assert(fcs->state == FCS_BUSY);
	fcs->u.fcs_len += l;
	fbo->sz_returned += l;
	assert(fcs->u.fcs_len <= fcs->alloc.size);
}

static void
fellow_busy_obj_trim_seglists(struct fellow_busy *fbo)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_obj *fco;
	struct buddy_returns *memret;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	AN(fbo->body_seglist);
	AN(fbo->body_seglist->fdsl);
	// only to be called if anything to trim at all
	AZ(fbo->body_seglist->fdsl->nsegs);

	// not for the first (embedded) seglist
	if (fbo->body_seglist == fco->fcsl)
		return;

	fcsl = fco->fcsl;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);

	// find the previous seglist
	while (fcsl->next != fbo->body_seglist) {
		fcsl = fcsl->next;
		CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	}
	assert(fcsl->next == fbo->body_seglist);
	fcsl->next = NULL;
	fdsl = fcsl->fdsl;
	fellow_busy_region_free(fbo, &fdsl->next);
	AZ(fdsl->next.off);
	AZ(fdsl->next.size);

	/* not holding fco->mtx because we trim surplus seglists
	 * which are not on the lru
	 */
	memret = BUDDY_RETURNS_STK(fbo->fc->membuddy, BUDDY_RETURNS_MAX);
	fellow_cache_seglist_free(memret, fbo->body_seglist);
	buddy_return(memret);
}

static inline int
fdr_contains(
    const struct buddy_off_extent *outer,
    const struct buddy_off_extent *inner)
{

	assert(outer->off > 0);
	assert(inner->off > 0);

	return (
	    inner->off >= outer->off &&
	    (typeof(inner->size))inner->off + inner->size <=
	    (typeof(outer->size))outer->off + outer->size);
}

/*
 * we need to write full disk blocks and if the membuddy minsize is less than
 * the dskbuddy minsize, we could trim to under the blocksize
 *
 * so we make this a two-step process: Determine surplus, trim disk allocation,
 * trim mem allocation if there is an FCS_BUSY
 */

void
fellow_busy_obj_trimstore(struct fellow_busy *fbo)
{
	struct fellow_body_region *fbr;
	struct buddy_off_extent *fdr;
	struct fellow_cache_seglist *fcsl;
	struct fellow_cache_seg *fcs;
	struct fellow_cache *fc;
	size_t nsz, base, surplus = 0;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fcsl = fbo->body_seglist;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fcs = fbo->body_seg;
#ifdef VC_4013_WORKAROUND
	if (fcs == NULL)
		return;
#endif
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	fbr = &fbo->body_region;
	fdr = fbr->reg;

	assert(fcs->state == FCS_USABLE || fcs->state == FCS_BUSY);
	assert_cache_seg_consistency(fcs);

	if (fcs->state == FCS_BUSY) {
		assert(fcs->u.fcs_len <= fcs->alloc.size);
		AN(fdr);

		/*
		 * fcs must be contained in the fdr, there can
		 * be a free region behind it
		 */

		assert(fcs->disk_seg->seg.off > 0);
		assert(fcs->disk_seg->seg.size > 0);
		assert(fdr_contains(fdr, &fcs->disk_seg->seg));
	} else {
		assert(fcs->state == FCS_USABLE);
		AZ(fcs->alloc.size);
		AZ(fcs->u.fcs_len);
	}

	if (fdr) {
		assert(fbr->len >= fcs->alloc.size);

		// fbr offset where fcs starts
		base = fbr->len - fcs->alloc.size;
		nsz = base + fcs->u.fcs_len;

		if (nsz == 0)
			fellow_busy_region_free(fbo, fdr);
		else {
			buddy_trim1_off_extent(fellow_dskbuddy(fbo->fc->ffd),
			    fdr, nsz);
			AZ(fdr->size & FELLOW_BLOCK_ALIGN);
			nsz = fdr->size;
			AN(nsz);
		}

		assert(fcs->alloc.size >= (nsz - base));
		surplus = fcs->alloc.size - (nsz - base);
	}

//	DBG("surplus %zu", surplus);

	if (fcs->state == FCS_BUSY) {
		assert(fcs->alloc.size >= surplus);
		nsz = fcs->alloc.size - surplus;

		buddy_trim1_ptr_extent(fc->membuddy, &fcs->alloc, nsz);

		// dsk/membuddy aligment must match
		assert(nsz == fcs->alloc.size);
		fcs->disk_seg->seg.size = fcs->alloc.size;
		assert(fcs->u.fcs_len <= fcs->alloc.size);
		if (fcs->u.fcs_len == 0) {
			fcs->alloc.ptr = NULL;
			AZ(fcs->disk_seg->seg.size);
			fcs->disk_seg->seg.off = 0;
			fellow_cache_seg_transition(fcs,
			    FCS_BUSY, FCS_USABLE);
			fcsl->fdsl->nsegs--;
			assert(fcs == &fcsl->segs[fcsl->fdsl->nsegs]);
		} else {
			fellow_cache_seg_unbusy(fbo, fcs);
		}
	} else {
		AZ(surplus);
		assert(fcs->state == FCS_USABLE);
	}

	if (fcsl->fdsl->nsegs == 0)
		fellow_busy_obj_trim_seglists(fbo);

	// invalidate
	fbo->body_seg = NULL;
	fbo->body_seglist = NULL;
}

#ifdef TEST_DRIVER
static int
fdr_compar(const void *aa, const void *bb)
{
	const struct buddy_off_extent *a = aa;
	const struct buddy_off_extent *b = bb;

	if (a->off < b->off)
		return (-1);
	if (a->off > b->off)
		return (1);
	if (a->size < b->size)
		return (-1);
	if (a->size > b->size)
		return (1);
	return (0);
}
#endif

/* NOT fco mtx */
static void
fellow_busy_log_submit(const struct fellow_busy *fbo)
{
	unsigned r = 0, ndle = DLE_N_REG(fbo->nregion) + 1;
	struct fellow_dle *e, dle_obj[1], dles[ndle];
	struct fellow_cache_obj *fco;
	const struct fellow_disk_obj *fdo;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	AN(fdo->fdo_flags & FDO_F_INLOG);

	fellow_dle_init(dle_obj, 1);
	stvfe_oc_dle_obj(fco->oc, dle_obj);

	fellow_dle_init(dles, ndle);
	if (fbo->nregion)
		r = fellow_dle_reg_fill(dles, ndle, fbo->region, fbo->nregion,
		    DLE_REG_ADD, dle_obj->u.obj.hash);

	assert(r < ndle);
	e = &dles[r++];
	e->type = DLEDSK(DLE_OBJ_ADD);
	memcpy(&e->u.obj, &dle_obj->u.obj, sizeof e->u.obj);
	e->u.obj.start = fbo->fco->fdb;

	assert(r <= ndle);
	fellow_log_dle_submit(fbo->fc->ffd, dles, r);

#ifdef TEST_DRIVER
	/*
	 * additional sanity check that we get back the same regions via
	 * fellow_obj_regions which we have created originally
	 */
	struct buddy_off_extent region[FCO_MAX_REGIONS];
	unsigned n;

	n = fellow_obj_regions(fbo->fc, fbo->fco, region);
	assert(n == fbo->nregion);
	qsort(((struct fellow_busy *)fbo)->region,
	    (size_t)n, sizeof(struct buddy_off_extent), fdr_compar);
	qsort(region,
	    (size_t)n, sizeof(struct buddy_off_extent), fdr_compar);
	for (r = 0; r < n; r++) {
		assert(region[r].off == fbo->region[r].off);
		assert(region[r].size == fbo->region[r].size);
	}
#endif
}

/*
 * write additional seglists
 * write object + embedded seglist
 * wait for all segments to be written (async)
 * - body
 * - aux
 *
 * XXX can save some io for inlog == 0
 */

void
fellow_busy_done(struct fellow_busy *fbo, struct objcore *oc, unsigned inlog)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_cache_obj *fco;
	struct fellow_disk_obj *fdo;
	struct fellow_cache *fc;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	if (fbo->segdowry.ptr)
		buddy_return1_ptr_page(fc->membuddy, &fbo->segdowry);
	if (fbo->segdskdowry.off >= 0) {
		buddy_return1_off_extent(fellow_dskbuddy(fc->ffd),
		    &fbo->segdskdowry);
	}
	BUDDY_POOL_FINI(fbo->segmem);
	buddy_alloc_wait_done(&fbo->bbrr.reqs);

	// nicer way?
	if (fbo->body_seg != NULL)
		fellow_busy_obj_trimstore(fbo);

	// the first disk seglist is always embedded
	fcsl = fco->fcsl;
	AN(fcsl);
	if (fcsl->next) {
		AN(fcsl->fdsl);
		fellow_cache_seglists_write(fbo,
		    fcsl->next, fcsl->fdsl->next);
	}

	fellow_disk_seglist_fini(fcsl->fdsl);

	AZ(fdo->fdo_flags & FDO_F_INLOG);
	AZ(fco->oc);
	// XXX still needed with logstate?
	if (inlog) {
		fdo->fdo_flags |= FDO_F_INLOG;
		fco->oc = oc;
		fdo->fdoa_present = oa2fdoa_present(oc->oa_present);
	}
	fellow_cache_obj_unbusy(fbo, inlog ? FCOL_WANTLOG : FCOL_NOLOG);
}

static void
fellow_busy_free(struct fellow_busy **fbop)
{
	struct fellow_busy *fbo;
	struct fellow_cache *fc;

	TAKE_OBJ_NOTNULL(fbo, fbop, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	buddy_return1_ptr_extent(fc->membuddy, &fbo->fbo_mem);
}

/*
 * data is written to the last cache_seglist
 * when done, nsegs is incremented
 *
 * to write segments:
 * - mem alloc space for fdsl
 * - mem alloc space for fcsl & point fcs'es to fds
 * - dsk alloc space for data (disk allocation)
 * - dsk alloc space for fdsl (n x fds)
 *
 * remaining space into first fds
 *
 * disk segment != disk allocation
 * disk segment = checksum, size to write/read
 * disk allocation -> alloc'ed in log
 *
 * rules for writing
 * - always from start to end
 * - if size is known, make one disk allocation
 * - if size is unknown, next alloc must be current + 1
 *
 *
 * the trouble with disk vs. mem segments
 *
 * the log assumes we got max FCO_MAX_REGIONS = 220 regions per obj
 * our memory segments must not be larger than 1/16th - 1/8 of mem
 *
 * so: alloc one big chunk of disk, chop into smaller, individually
 * checksummed segments
 *
 * XXX CHANGE:
 * for chunked: next pow2 with every chunk, so we never get above ~50
 * disk segments
 *
 * Ballpark
 * - disk/mem = 10k
 * min:
 * - mem =  1GB -> max seg 128MB
 * - dsk = 10TB -> max obj 1.28TB
 * max:
 * - max seg because I/O 1<<30 = 1 GB
 * - dsk = 16EB -> max obj 1<<61 2EB
 * - region sz -> 2EB / 220 = 9PB (16PB/8PB)
 * - seg/obj -> 1<<31 = 2g
 * - sizeof(fds) = 56
 * - size of all segments in seglists: 112GB
 *
 * TODO:
 * - region != segment
 * - fbo.io independent of FCO_MAX_REGIONS
 * - introduce max object size depending on dsk size
 * - change region allocation such that max object will always fit
 */

// XXX rearrange seglists ?

static void
fellow_cache_obj_fini(const struct fellow_cache_obj *fco)
{
	struct fellow_disk_seg	*fds;
	struct fellow_disk_obj	*fdo;

	fds = FCO_FCS(fco)->disk_seg;
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	assert(fdo == (void *)FCO_FCS(fco)->alloc.ptr);

	fh(fds->fht, fds->fh, (char *)fdo + FELLOW_DISK_OBJ_CHK_START,
	    FELLOW_DISK_OBJ_CHK_LEN(fdo));
}

static struct fellow_cache_res
fellow_cache_obj_prepread(struct fellow_cache *fc, fellow_disk_block fdba,
    unsigned crit)
{
	struct fellow_cache_res fcr;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_obj *fco;
	struct buddy_ptr_page dowry = buddy_ptr_page_nil;
	size_t sz;

	sz = fdb_size(fdba);
	if (! PAOK(sz))
		return (FCR_IOFAIL("bad size"));

	fcr = fellow_cache_obj_new(fc, sz, FDO_MAX_EMBED_SEGS, NULL,
	    crit ? NULL : &dowry,
	    crit ? FEP_OHLCK : FEP_GET);
	if (fcr.status != fcr_ok)
		return (fcr);

	CAST_OBJ_NOTNULL(fco, fcr.r.ptr, FELLOW_CACHE_OBJ_MAGIC);
	fco->fdb = fdba;
	fco->fco_dowry = dowry;

	assert(fco->logstate == FCOL_DUNNO);
	fco->logstate = FCOL_INLOG;

	fcs = FCO_FCS(fco);
	CHECK_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);

	// we prepare the state the object will have
	// when inserted
	fcs->refcnt = 1;
	fcs->fco_infdb = 1;
	fellow_cache_seg_transition_locked_notincore(fcs, FCO_READING);

	return (fcr);
}

// undo fellow_cache_obj_prepread()
static void
fellow_cache_obj_redundant(const struct fellow_cache *fc,
    struct fellow_cache_obj **fcop)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;

	TAKE_OBJ_NOTNULL(fco, fcop, FELLOW_CACHE_OBJ_MAGIC);
	fcs = FCO_FCS(fco);
	CHECK_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);

	assert(fcs->refcnt == 1);
	assert(fcs->fco_infdb);
	assert(fcs->state == FCO_READING);

	fcs->refcnt = 0;
	fcs->fco_infdb = 0;
	fellow_cache_seg_transition_locked_notincore(fcs, FCO_REDUNDANT);

	/* holding mutex not necessary here, but this call site is the exception
	 * and the other call side requires it
	 */

	AZ(pthread_mutex_lock(&fco->mtx));
	fellow_cache_obj_free(fc, &fco);
}

/*
 * if returns 0, fellow_cache_obj_free() must be called under the lock
 */
static unsigned
fellow_cache_obj_deref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache *fc, struct fellow_cache_obj *fco)
{
	struct fellow_cache_seg *fcs;
	unsigned last, refcnt;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	fcs = FCO_FCS(fco);
	AN(fcs->refcnt);

	assert_cache_seg_consistency(fcs);

	last = fcs->refcnt == 1 && fcs->fco_infdb;
	refcnt = fellow_cache_seg_deref_locked(lcb, fcs);
	if (last) {
		fcs->fco_infdb = 0;
		AZ(refcnt);
		/* REF_FDB_REMOVE */
		AZ(pthread_mutex_lock(&fc->fdb_mtx));
		(void) VRBT_REMOVE(fellow_cache_fdb_head, &fc->fdb_head, fco);
		AN(*fc->g_mem_obj);
		(*fc->g_mem_obj)--;
		AZ(pthread_mutex_unlock(&fc->fdb_mtx));
	}
	return (refcnt);
}

void
fellow_cache_obj_deref(struct fellow_cache *fc, struct fellow_cache_obj *fco)
{
	unsigned refcount;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	AZ(pthread_mutex_lock(&fco->mtx));
	refcount = fellow_cache_obj_deref_locked(lcb, fc, fco);
	fellow_cache_lru_chgbatch_apply(lcb);
	DBG("fco %p refcount %u", fco, refcount);

	if (refcount == 0)
		fellow_cache_obj_free(fc, &fco);
	else
		AZ(pthread_mutex_unlock(&fco->mtx));
}

/*
 * return with an additional reference
 *
 * if an ocp argument is present and this is the first reference, it gets taken
 *
 */
struct fellow_cache_res
fellow_cache_obj_get(struct fellow_cache *fc,
struct objcore **ocp, uintptr_t priv2, unsigned crit)
{
	struct fellow_cache_res fcr;
	struct fellow_cache_obj *fco, *nfco;
	struct fellow_cache_seg *fcs;
	struct fellow_disk_obj *fdo;
	struct fellow_disk_seglist *fdsl;
	struct buddy_ptr_extent fcsl_mem = buddy_ptr_extent_nil;
	struct buddy_ptr_page dowry = buddy_ptr_page_nil;
	fellow_disk_block fdba;
	unsigned oref = 0;
	const char *err;
	char *ptr;
	size_t spc, sz, nsz;
	int r;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	fdba.fdb = priv2;

	struct buddy_returns *rets = BUDDY_RETURNS_STK(fc->membuddy, 2);

	/*
	 * init a new object, then insert it atomically
	 *
	 * for a race, insert might result in an existing object, in which case
	 * we wasted some work and free the extra object again
	 *
	 */
	fcr =  fellow_cache_obj_prepread(fc, fdba, crit);
	if (fcr.status != fcr_ok) {
		fellow_cache_res_check(fc, fcr);
		return (fcr);
	}
	CAST_OBJ_NOTNULL(nfco, fcr.r.ptr, FELLOW_CACHE_OBJ_MAGIC);

  again:
	AZ(oref);
	//lint --e{456} flexelint does not grok trylock
	AZ(pthread_mutex_lock(&fc->fdb_mtx));
	fco = VRBT_INSERT(fellow_cache_fdb_head, &fc->fdb_head, nfco);
	if (fco != NULL) {
		// the fco might be about to be destroyed see REF_FDB_REMOVE
		r = pthread_mutex_trylock(&fco->mtx);
		if (r != 0) {
			assert(r == EBUSY);
		} else {
			if (FCO_REFCNT(fco))
				oref = ++FCO_REFCNT(fco);
			AZ(pthread_mutex_unlock(&fco->mtx));
		}
	}
	else if (ocp) {
		AZ(nfco->oc);
		TAKE_OBJ_NOTNULL(nfco->oc, ocp, OBJCORE_MAGIC);
		(*fc->g_mem_obj)++;
	}
	AZ(pthread_mutex_unlock(&fc->fdb_mtx));

	if (fco != NULL && oref == 0) {
		/* we did not manage to grab a reference on the
		 * object
		 */
		goto again;
	}

	if (fco != NULL) {
		// entries on the fdb need at least one reference
		AN(oref);
		// clean up over-allocated new fco
		fellow_cache_obj_redundant(fc, &nfco);

		if (FCO_STATE(fco) == FCO_READING) {
			AZ(pthread_mutex_lock(&fco->mtx));
			while (FCO_STATE(fco) == FCO_READING)
				fellow_cache_seg_wait_locked(FCO_FCS(fco));
			AZ(pthread_mutex_unlock(&fco->mtx));
		}

		switch (FCO_STATE(fco)) {
		case FCO_WRITING:
		case FCO_INCORE:
			break;
		case FCO_READFAIL:
			fellow_cache_obj_deref(fc, fco);
			(void) fellow_cache_obj_res(fc, fco,
			    FCR_IOFAIL("hit READFAIL object"));
			break;
		case FCO_EVICT:
			// race, retry
			fellow_cache_obj_deref(fc, fco);
			return (fellow_cache_obj_get(fc, ocp, priv2, 0));
		default:
			WRONG("fco state");
		}

		return (fco->fcr);
	}

	TAKEZN(fco, nfco);
	assert(fco->fdb.fdb == fdba.fdb);
	fcr = FCR_OK(fco);

	/* not using fellow_cache_seg_read_sync()
	 * because our disk size is smaller than mem size in fcs
	 */
	fcs = &fco->fdo_fcs;

	if (FC_INJ || fellow_io_pread_sync(fc->ffd, fcs->alloc.ptr,
		fdb_size(fdba), fdb_off(fdba)) != (int32_t)fdb_size(fdba)) {
		err = FC_ERRSTR("fdo read error");
		goto err;
	}

	fdo = fellow_disk_obj(fcs);
	AN(fdo);
	err = fellow_disk_obj_check(fdo, fdba);
	if (err != NULL)
		goto err;

	fellow_disk_obj_compat(fdo);
	// |= is because of oa_inlog_bit
	if (fco->oc)
		fco->oc->oa_present |= fdoa2oa_present(fdo->fdoa_present);

	/* the check of the fdo-embedded fdsl needs to happen before trim,
	 * because both trim and relocation change data after the used part of
	 * the fdsl
	 */
	fdsl = fellow_disk_obj_fdsl(fdo);
	err = fellow_disk_seglist_check(fdsl);
	if (err != NULL)
		goto err;

	ptr = (void *)(fco + 1);
	nsz = sizeof *fco;
	spc = fco->fco_mem.size - nsz;
	sz = fellow_cache_seglist_size(fdsl->nsegs);

	assert(PAOK(ptr));

	// embed fcsl if possible
	if (spc >= sz) {
		fco->fcsl = fellow_cache_seglist_init((void *)ptr, sz, fco);
		AN(fco->fcsl);
		ptr += sz;
		nsz += sz;
		spc -= sz;
	}
	else {
		// dup fellow_cache_seglists_load()
		fcsl_mem = buddy_alloc1_ptr_extent_wait(fc->membuddy, FEP_META,
		    SEGLIST_SIZE(fco->fcsl, fdsl->nsegs), 0);
		if (FC_INJ || fcsl_mem.ptr == NULL) {
			err = FC_ERRSTR("first disk seglist fcsl alloc failed");
			goto err;
		}
		fco->fcsl = fellow_cache_seglist_init(fcsl_mem.ptr,
		    fcsl_mem.size - sizeof *fco->fcsl, fco);
		AN(fco->fcsl);
		fco->fcsl->fcsl_sz = fcsl_mem.size;
	}
	assert(PAOK(ptr));

	fdsl->lsegs = fdsl->nsegs;
	sz = fellow_disk_obj_size(fdo, fdsl);

	if (spc >= sz) {
		memcpy(ptr, fdo, sz);
		fdo = fcs->u.fco_fdo = (void *)ptr;

		AN(buddy_return_ptr_extent(rets, &fcs->alloc));
		nsz += sz;
	}
	else
		fdo = fellow_disk_obj_trim(fc, fcs);

	buddy_trim1_ptr_extent(fc->membuddy, &fco->fco_mem, nsz);

	fdsl = fellow_disk_obj_fdsl(fdo);
	assert(PAOK(fdsl));
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	if (fdsl->nsegs == 0)
		TAKE(dowry, fco->fco_dowry);

	fellow_cache_seglist_associate(fco->fcsl, fdsl, FCS_DISK);
	if (fdsl->next.size > 0) {
		fco->fcsl->next = fcsl_pending;
		fco->seglstate = SEGL_NEED;
	}
	else {
		AZ(fco->fcsl->next);
		fco->seglstate = SEGL_DONE;
	}

#define FDO_AUXATTR(U, l)						\
	fellow_cache_seg_associate(&fco->aa_##l##_seg,			\
	    &fdo->aa_##l##_seg, fdo->aa_##l##_seg.seg.size == 0 ?	\
	FCS_USABLE : FCS_DISK);
#include "tbl/fellow_obj_attr.h"

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	AZ(pthread_mutex_lock(&fco->mtx));
	assert(fcs->state == FCO_READING);
	if (fcs->refcnt > 1)
		AZ(pthread_cond_broadcast(&fco->cond));
	assert_cache_seg_consistency(fcs);
	AN(fcs->fco_infdb);
	fcs->disk_seg = &fdo->fdo_fds;
	fellow_cache_seg_transition_locked(lcb, fcs, fcs->state, FCO_INCORE);
	if (fco->oc) {
		AZ(fco->oc->stobj->priv);
		fco->oc->stobj->priv = fco;
	}
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(fcs);
	AZ(pthread_mutex_unlock(&fco->mtx));

	if (dowry.bits)
		AN(buddy_return_ptr_page(rets, &dowry));
	buddy_return(rets);
	return (fellow_cache_obj_res(fc, fco, FCR_OK(fco)));
  err:
	if (ocp) {
		AZ(*ocp);
		TAKE_OBJ_NOTNULL(*ocp, &fco->oc, OBJCORE_MAGIC);
	}

	/* we are in the process of transitioning to FCO_READING,
	 * fcs is not yet consistent. Just set the failed state,
	 * _evict() will signal any waiters
	 */
	fellow_cache_seg_transition_locked_notincore(FCO_FCS(fco), FCO_READFAIL);
	fellow_cache_obj_deref(fc, fco);

	if (fcsl_mem.ptr)
		AN(buddy_return_ptr_extent(rets, &fcsl_mem));
	buddy_return(rets);
	if (strstr(err, "alloc"))
		fcr = FCR_ALLOCERR(err);
	else
		fcr = FCR_IOERR(err);
	return (fellow_cache_obj_res(fc, fco, fcr));
}

static void
fellow_disk_obj_delete_submit(struct fellow_fd *ffd,
    fellow_disk_block start, const uint8_t hash[DIGEST_LEN],
    const struct buddy_off_extent region[FCO_MAX_REGIONS], unsigned n)
{
	unsigned r, ndle = DLE_N_REG(n) + 1;
	struct fellow_dle *e, dles[ndle];

	fellow_dle_init(dles, ndle);
	if (n)
		r = fellow_dle_reg_fill(dles, ndle, region, n,
		    DLE_REG_DEL_ALLOCED, hash);
	else
		r = 0;

	assert(r < ndle);
	e = &dles[r++];
	e->type = DLEDSK(DLE_OBJ_DEL_ALLOCED);
	memcpy(e->u.obj.hash, hash, (size_t)DIGEST_LEN);
	e->u.obj.start = start;

	assert(r <= ndle);
	fellow_log_dle_submit(ffd, dles, r);
}

/*
 * for drain, wait until object is written before mutating
 */
void
fellow_cache_obj_wait_written(struct fellow_cache_obj *fco)
{
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	AZ(pthread_mutex_lock(&fco->mtx));
	/* we must not free the object's disk space while it is still writing */
	while (FCO_STATE(fco) == FCO_WRITING)
		fellow_cache_seg_wait_locked(FCO_FCS(fco));
	AZ(pthread_mutex_unlock(&fco->mtx));
}

static void
fellow_disk_obj_delete_thin(struct fellow_fd *ffd,
    fellow_disk_block start, const uint8_t hash[DIGEST_LEN])
{
	static const char ndle = 1;
	struct fellow_dle e[ndle];

	fellow_dle_init(e, ndle);
	e->type = DLEDSK(DLE_OBJ_DEL_THIN);
	memcpy(e->u.obj.hash, hash, (size_t)DIGEST_LEN);
	e->u.obj.start = start;

	fellow_log_dle_submit(ffd, e, ndle);
}

/* evict, then delete from log (this order is important to avoid
 * fdb reuse)
 *
 * will not be called if the object is still in use,
 * so we can delete all segments immediately (via the log)
 *
 * loses one reference
 */
void
fellow_cache_obj_delete(struct fellow_cache *fc,
    struct fellow_cache_obj *fco, const uint8_t hash[DIGEST_LEN])
{
	const struct fellow_disk_obj *fdo;
	/* one additional region for the object itself */
	struct buddy_off_extent region[FCO_MAX_REGIONS + 1] = {{0}};
	struct buddy_returns *rets;
	struct buddy *dskbuddy;
	struct stvfe_tune *tune;
	enum fcos_state state;
	enum fcol_state logstate;
	fellow_disk_block fdba;
	unsigned u, n;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	dskbuddy = fellow_dskbuddy(fc->ffd);
	rets = BUDDY_RETURNS_STK(dskbuddy, BUDDY_RETURNS_MAX);

	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fco, 1);

	fdba = fco->fdb;

	n = fellow_obj_regions(fc, fco, region);
	assert(n <= FCO_MAX_REGIONS);

	AZ(pthread_mutex_lock(&fco->mtx));
	/* we must not free the object's disk space while it is still writing */
	while (FCO_STATE(fco) == FCO_WRITING)
		fellow_cache_seg_wait_locked(FCO_FCS(fco));
	/* now the only other reference can be held by
	 * fellow_cache_seglists_load()
	 */
	assert(FCO_REFCNT(fco) <= 2);
	while (FCO_REFCNT(fco) > 1)
		AZ(pthread_cond_wait(&fco->cond, &fco->mtx));

	switch (fco->logstate) {
	case FCOL_DUNNO:
		fco->logstate = FCOL_TOOLATE;
		break;
	case FCOL_WANTLOG:
		// SYNC WITH fellow_cache_async_write_complete()
		// see comment there
		WRONG("fellow_cache_obj_delete FCOL_WANTLOG - can't race");
		break;
	case FCOL_NOLOG:
		break;
	case FCOL_TOOLATE:
		WRONG("fellow_cache_obj_delete FCOL_TOOLATE");
	case FCOL_INLOG:
		fco->logstate = FCOL_DELETED;
		stvfe_oc_log_removed(fco->oc);
		break;
	case FCOL_DELETED:
		WRONG("fellow_cache_obj_delete FCOL_DELETED");
	default:
		WRONG("fellow_cache_obj_delete logstate");
	}

	logstate = fco->logstate;
	state = FCO_STATE(fco);

	// must have been the last reference
	AZ(fellow_cache_obj_deref_locked(lcb, fc, fco));
	fellow_cache_lru_chgbatch_apply(lcb);
	fellow_cache_obj_free(fc, &fco);

	/* UNLOCKED because these states are all final */
	switch (logstate) {
	case FCOL_NOLOG:
	case FCOL_TOOLATE:
		break;
	case FCOL_DELETED:
		/* if read failed, we do not have all regions, but the log might
		 * still work */

		if (state == FCO_READFAIL)
			fellow_disk_obj_delete_thin(fc->ffd, fdba, hash);
		else {
			fellow_disk_obj_delete_submit(fc->ffd, fdba, hash,
			    region, n);
		}
		return;
	default:
		WRONG("fellow_cache_obj_delete logstate UNLOCKED");
	}

	/*
	 * NOTE: FOR FCO_READFAIL, we are likely to leak disk space
	 *
	 * When fellow_obj_regions() fails reading a segment list, we miss
	 * segments and, consequently, regions which the object occupies.
	 */

	/* XXX no async IO here yet: We need to free the dskbuddy space after
	 * the discard is complete. To do that, we would need to store the
	 * off/size of the discard in an additional structure of the io ring.
	 *
	 * we can not just hold the cache async io mtx because it would delay
	 * readahead
	 *
	 */

	assert(n < FCO_MAX_REGIONS + 1);
	region[n].off = fdb_off(fdba);
	region[n].size = fdb_size(fdba);
	n++;

	tune = fc->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	fellow_io_regions_discard(fc->ffd, NULL, region, n,
	    tune->discard_immediate, 1);
	for (u = 0; u < n; u++)
		AN(buddy_return_off_extent(rets, &region[u]));
	buddy_return(rets);
}

struct fellow_cache_res
fellow_cache_obj_getattr(struct fellow_cache *fc,
    struct fellow_cache_obj *fco,
    enum obj_attr attr, size_t *len)
{
	struct fellow_cache_res fcr;
	struct fellow_disk_obj *fdo;
	const char *err;

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	AN(len);

	fcr = fco->fcr;
	if (fcr.status != fcr_ok)
		return (fcr);

	/* Auxiliary attributes:
	 *
	 * ref is never returned and just ignored by
	 * fellow_cache_seg_auxattr_free()
	 */

	switch (attr) {
#define FDO_FIXATTR(U, l, s, vs)					\
	case OA_##U:							\
		*len = vs;						\
		fcr = FCR_OK(fdo->fa_##l);				\
		break;
#define FDO_VARATTR(U, l)						\
	case OA_##U:							\
		if (fdo->va_##l.aoff == 0) {				\
			fcr = FCR_OK(NULL);				\
			break;						\
		}							\
		*len = fdo->va_##l.alen;				\
		fcr = FCR_OK((uint8_t *)fdo + fdo->va_##l.aoff);	\
		break;
#define FDO_AUXATTR(U, l)						\
	case OA_##U: {							\
		struct fellow_cache_seg *fcs = &fco->aa_##l##_seg;	\
		if (fcs->disk_seg->seg.size == 0) {			\
			fcr = FCR_OK(NULL);				\
			break;						\
		}							\
		fellow_cache_seg_auxattr_ref_in(fc, fcs);		\
		err = fellow_cache_seg_check(fcs);			\
		if (err != NULL) {					\
			fcr = FCR_IOFAIL(err);				\
			break;						\
		}							\
		*len = fcs->u.fcs_len;					\
		fcr = FCR_OK(fcs->alloc.ptr);				\
		break;							\
	}
#include "tbl/fellow_obj_attr.h"
	default:
		WRONG("Unsupported OBJ_ATTR");
	}
	return (fellow_cache_obj_res(fc, fco, fcr));
}

void *
fellow_busy_setattr(struct fellow_busy *fbo, enum obj_attr attr,
    size_t len, const void *ptr)
{
	struct fellow_cache_obj *fco;
	struct fellow_disk_obj *fdo;
	struct fellow_cache *fc;
	void *retval = NULL;
	size_t sz;
	ssize_t diff;
	uint8_t *p;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	assert(fbo->va_data);
	assert(fbo->va_data >= fdo->va_data);
	assert(fbo->va_data <= fdo->va_data + fdo->va_data_len);
	assert(fbo->va_data_len <= fdo->va_data_len);

	switch (attr) {
		/* Fixed size attributes */
#define FDO_FIXATTR(U, l, s, vs)					\
	case OA_##U:							\
		assert(len <= sizeof fdo->fa_##l);			\
		retval = fdo->fa_##l;					\
		break;
#include "tbl/fellow_obj_attr.h"

		/* Variable size attributes */
#define FDO_VARATTR(U, l)						\
	case OA_##U:							\
		p = (uint8_t *)fdo;					\
		if (fdo->va_##l.alen > 0) {				\
			AN(fdo->va_##l.aoff);				\
			assert(len == fdo->va_##l.alen);		\
			retval = p + fdo->va_##l.aoff;			\
		} else if (len > 0) {					\
			assert(len <= UINT32_MAX);			\
			sz = fbo->va_data_len + len;			\
			assert(sz <= fdo->va_data_len);			\
			retval = fbo->va_data;				\
			diff = fbo->va_data - p;			\
			assert(diff > 0);				\
			assert(diff < UINT32_MAX);			\
			fdo->va_##l.aoff = (uint32_t)diff;		\
			fdo->va_##l.alen = (uint32_t)len;		\
			fbo->va_data += len;				\
		}							\
		break;
#include "tbl/fellow_obj_attr.h"

		/* Auxiliary attributes

		 * XXX: We only support writing once, this
		 * is different to varnish-cache
		 */
#define FDO_AUXATTR(U, l)						\
	case OA_##U: {							\
		struct fellow_cache_seg *fcs = &fco->aa_##l##_seg;	\
		if (len == 0)						\
			break;						\
		if (! fellow_busy_seg_alloc(fbo, fcs, len))		\
			break;						\
		assert(fcs->alloc.size >= len);				\
		AZ(fcs->u.fcs_len);						\
		fcs->u.fcs_len = len;						\
		retval = fcs->alloc.ptr;				\
		memcpy(retval, ptr, len);				\
		fellow_cache_seg_unbusy(fbo, fcs);			\
		return (retval);					\
	}
#include "tbl/fellow_obj_attr.h"

	default:
		WRONG("Unsupported OBJ_ATTR");
		break;
	}

	// !! early return for AUXATTR above !
	if (retval != NULL && ptr != NULL)
		memcpy(retval, ptr, len);
	return (retval);
}

static void
fellow_cache_assert_disk_fmt(void)
{
	//lint --e{506} constant value boolean

	assert(sizeof(uintptr_t) == sizeof (fellow_disk_block));

#define S struct fellow_disk_seg
	assert(offsetof(S, segnum) == 4);
	assert(offsetof(S, seg) == 8);
	assert(offsetof(S, fh) == 24);
	assert(sizeof(S) == 24 + 32);
	assert(sizeof(S) == 7 * 8);
#undef S

#define S struct fellow_disk_seglist
	assert(offsetof(S, version) == 4);
	assert(offsetof(S, fht) == 7);
	assert(offsetof(S, fh) == 8);
	assert(offsetof(S, next) == 40);
	assert(offsetof(S, nsegs) == 56);
	assert(offsetof(S, lsegs) == 58);
	assert(offsetof(S, segs) == 64);
	assert(sizeof(S) == 64);
#undef S

#define S struct fellow_disk_obj
	assert(offsetof(S, version) == 4);
	assert(offsetof(S, fdo_fds) == 8);
	assert(offsetof(S, va_data_len) == 64);

	assert(offsetof(S, fa_flags) == 70);
	assert(offsetof(S, fa_len) == 9 * 8);
	assert(offsetof(S, fa_vxid) == 10 * 8);
	assert(offsetof(S, fa_lastmodified) == 11 * 8);
	assert(offsetof(S, fa_gzipbits) == 12 * 8);
	assert(offsetof(S, fa_reserve) == 128);

	assert(offsetof(S, va_vary) == 192);
	assert(offsetof(S, va_headers) == 200);
	assert(offsetof(S, va_reserve) == 208);

	assert(offsetof(S, aa_esidata_seg) == 240);
	assert(offsetof(S, va_data) == 296);
	assert(offsetof(S, va_data) == 37 * 8);
#undef S

}

struct fellow_cache *
fellow_cache_init(struct fellow_fd *ffd, buddy_t *membuddy,
    struct stvfe_tune *tune, fellow_task_run_t taskrun, uint64_t *g_mem_obj)
{
	struct fellow_cache *fc;

	AN(ffd);
	AN(membuddy);

	fellow_cache_assert_disk_fmt();

	ALLOC_OBJ(fc, FELLOW_CACHE_MAGIC);
	AN(fc);

	fc->ffd = ffd;
	fc->membuddy = membuddy;
	fc->tune = tune;
	fc->taskrun = taskrun;
	fc->running = 1;

	fellow_cache_lrus_init(fc->lrus);

	AZ(pthread_mutex_init(&fc->fdb_mtx, &fc_mtxattr_errorcheck));
	VRBT_INIT(&fc->fdb_head);
	fc->g_mem_obj = g_mem_obj;
	AZ(*g_mem_obj);

	fellow_cache_async_init(fc, taskrun);

	return (fc);
}

/*
 * the LRU thread is started by the storage layer after loading
 */
void
fellow_cache_fini(struct fellow_cache **fcp)
{
	struct fellow_cache *fc;
	int i;

	TAKE_OBJ_NOTNULL(fc, fcp, FELLOW_CACHE_MAGIC);

	fc->running = 0;
	for (i = 0; i < 5; i++) {
		buddy_wait_kick(fc->membuddy);
		(void) usleep(10*1000);
	}
	fellow_cache_async_fini(fc);
	fellow_cache_lrus_fini(fc->lrus);

	assert(VRBT_EMPTY(&fc->fdb_head));
	AZ(*fc->g_mem_obj);

	AZ(pthread_mutex_destroy(&fc->fdb_mtx));

	FREE_OBJ(fc);
}

#ifdef TEST_DRIVER
#include "vsha256.h"
#include "fellow_testenv.h"

void
stvfe_sumstat(struct worker *wrk)
{
	(void) wrk;
}

int
stvfe_mutate(struct worker *wrk, struct fellow_cache_lru *lru,
    struct objcore *oc)
{
	WRONG("fellow_cache_test objects to not have an objcore");
	return (0);
}

#ifdef DEBUG
const char * const filename = "/tmp/fellowfile.cachetest";
#else
const char * const filename = "/tmp/fellowfile.cachetest.ndebug";
#endif

static int v_matchproto_(fellow_resurrect_f)
resurrect_keep(void *priv, const struct fellow_dle *e)
{

	(void) priv;
	(void) e;

	return (1);
}

static int v_matchproto_(fellow_resurrect_f)
resurrect_discard(void *priv, const struct fellow_dle *e)
{

	(void) priv;
	(void) e;

	return (0);
}

static int
iter_sha256(void *priv, unsigned flush, const void *ptr, ssize_t l)
{
	VSHA256_CTX *sha256ctx = priv;

	(void) flush;
	VSHA256_Update(sha256ctx, ptr, l);
	return (0);
}

static const char * const oatest[OA__MAX] = {
	[OA_LEN] = "lenlenle",
	[OA_VXID] = "vxidvxid",
	[OA_FLAGS] = "f",
	[OA_GZIPBITS] = "gzipbitsgzipbitsgzipbitsgzipbits",
	[OA_LASTMODIFIED] = "lastmodi",
	[OA_VARY] = "vary vary vary varyvaryvary vary varyvaryvary",
	[OA_HEADERS] = "headers hdrs hea",
	[OA_ESIDATA] = "This is OA_ESIDATA <EOS>"
};

void t_getattr(struct fellow_cache *fc, struct fellow_cache_obj *fco)
{
	struct fellow_cache_res fcr;
	unsigned u;
	size_t sz;

	for (u = 0; u < OA__MAX; u++) {
		fcr = fellow_cache_obj_getattr(fc, fco, u, &sz);
		assert(fcr.status == fcr_ok);
		AN(fcr.r.ptr);
		assert(sz == strlen(oatest[u]));
		AZ(strncmp(fcr.r.ptr, oatest[u], sz));
	}
}

/* mimic sfemem_bocdone() without fellow_storage */

int
stvfe_oc_inlog(struct objcore *oc)
{
	(void)oc;
	return (1);
}

void
stvfe_oc_log_removed(struct objcore *oc)
{
	(void)oc;
	return;
}

void
stvfe_oc_log_submitted(struct objcore *oc)
{
	(void)oc;
	return;
}

void
stvfe_oc_dle_obj(struct objcore *oc, struct fellow_dle *e)
{
	AN(oc);	// actually the hash
	AN(e);
	memcpy(e->u.obj.hash, oc, DIGEST_LEN);
	e->u.obj.ttl = 1;
}

static void
test_bocdone(struct fellow_busy *fbo, const uint8_t *hash, unsigned inlog)
{
	fellow_busy_done(fbo, TRUST_ME(hash), inlog);
}

enum incache_e {
	notincache = 0,
	incache = 1
};

/*
 * ======================================================================
 * test fellow cache object creation / unbusy with error injection
 * during write
 */
static void
test_fellow_cache_unbusy_inject(struct fellow_cache *fc)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_res fcr;
	struct fellow_busy *fbo;
	uint8_t hash[DIGEST_LEN];
	uintptr_t priv2;
	int injcount = -1;
	size_t sz = 1234;
	char *ptr;

	fc_inj_reset();
	while (injcount) {
		DBG("injcount=%d", injcount);
		if (injcount > 0)
			fc_inj_set(injcount);
		fcr = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234);
		if (fcr.status != fcr_ok) {
			DBG("fbo_alloc %d err %s", injcount, fcr.r.err);
			goto next;
		}

		fbo = fcr.r.ptr;
		CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);

		if (fcr.status == fcr_ok)
			fellow_busy_obj_extend(fbo, sz);
		else
			DBG("getspace inj %d err %s", injcount, fcr.r.err);

		fellow_busy_obj_trimstore(fbo);

		test_bocdone(fbo, TRUST_ME(hash), 1);
		fellow_cache_obj_deref(fc, fco);

	  next:
		if (injcount < 0)
			injcount = fc_inj_count();
		else
			injcount--;
	}
	fc_inj_reset();
}

/*
 * ======================================================================
 * test fellow cache with error injection at every possible point
 */

static struct fellow_cache_obj *
test_fellow_cache_obj_get(struct fellow_cache *fc, uintptr_t priv2,
    enum incache_e expect)
{
	struct fellow_cache_res fcr;
	struct fellow_cache_obj *fco;
	struct objcore *oc1, *toc, ocmem[1];
	int injcount;

	DBG("expect %s", expect == incache ? "incache" : "notincache");

	// baseline, no injection
	fc_inj_reset();
	INIT_OBJ(ocmem, OBJCORE_MAGIC);
	toc = ocmem;
	fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
	if (fcr.status != fcr_ok) {
		AN(toc);
		return (NULL);
	}
	CAST_OBJ_NOTNULL(fco, fcr.r.ptr, FELLOW_CACHE_OBJ_MAGIC);
	DBG("fco1 %p ref %u", fco, FCO_REFCNT(fco));
	if (expect == incache)
		AN(toc);
	else
		AZ(toc);
	oc1 = toc;

	injcount = fc_inj_count();
	DBG("injcount %d", injcount);
	if (injcount == 0)
		return (fco);

	fellow_cache_obj_deref(fc, fco);

	/* dumb wait until seglist reads are complete */
	while (FCO_REFCNT(fco) > 1)
		usleep(1000);

	AN(injcount);
	while (injcount) {
		fc_inj_set(injcount);
		INIT_OBJ(ocmem, OBJCORE_MAGIC);
		toc = ocmem;
		fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
		AN(toc);
		assert(fcr.status != fcr_ok);
		DBG("inj %d err %s", injcount, fcr.r.err);
		injcount--;
	}

	DBG("fco1e %p ref %u", fco, FCO_REFCNT(fco));

	fc_inj_reset();
	INIT_OBJ(ocmem, OBJCORE_MAGIC);
	toc = ocmem;
	fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
	DBG("fco2 %p ref %u", fco, FCO_REFCNT(fco));
	assert(toc == oc1);
	CAST_OBJ_NOTNULL(fco, fcr.r.ptr, FELLOW_CACHE_OBJ_MAGIC);
	return (fco);
}

static void test_fellow_cache_obj_iter_final(
    struct fellow_cache *fc, struct fellow_cache_obj **fcop,
    const unsigned char h1[SHA256_LEN], int final)
{
	struct fellow_cache_res fcr;
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	unsigned char h2[SHA256_LEN];
	struct objcore *toc, ocmem[1];
	VSHA256_CTX sha256ctx;
	uintptr_t priv2;
	unsigned u, refcnt;
	struct fcscursor c;
	int injcount;

	AN(fcop);
	AN(*fcop);

	// baseline, no injection
	fc_inj_reset();
	VSHA256_Init(&sha256ctx);
	fcr = fellow_cache_obj_iter(fc, *fcop, &sha256ctx, iter_sha256, final);
	assert(fcr.status == fcr_ok);
	AZ(fcr.r.integer);
	VSHA256_Final(h2, &sha256ctx);
	AZ(memcmp(h1, h2, sizeof *h1));

	fellow_cache_seglists_need(fc, *fcop);
	fcsc_init(&c, (*fcop)->fcsl);
	if ((fcs = fcsc_next_wait(*fcop, &c)) != NULL) {
		while (fcs->state == FCS_READING || fcs->state == FCS_WRITING)
			usleep(100);
		if (fcs->state != FCS_DISK) {
			assert(fcs->state == FCS_INCORE);
			AZ(fcs->refcnt);
			AN(fcs->fcs_onlru);
		}
	} else
		WRONG("no seg");
	fellow_cache_obj_slim(fc, *fcop);

	// remember object and deref
	priv2 = (*fcop)->fdb.fdb;
	refcnt = FCO_REFCNT(*fcop);
	for (u = 0; u < refcnt; u++)
		(void) fellow_cache_obj_deref(fc, *fcop);
	*fcop = NULL;

	fc_inj_reset();
	injcount = -1;
	AN(injcount);
	while (injcount) {
		DBG("injcount %d", injcount);

		fc_inj_set(0);
		INIT_OBJ(ocmem, OBJCORE_MAGIC);
		toc = ocmem;
		fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
		assert(fcr.status == fcr_ok);
		fco = fcr.r.ptr;

		fc_inj_set(injcount);
		VSHA256_Init(&sha256ctx);
		fcr = fellow_cache_obj_iter(fc, fco, &sha256ctx,
		    iter_sha256, 0);

		if (fcr.status == fcr_ok) {
			assert(injcount == -1);
			injcount = fc_inj_count();
		} else
			injcount--;

		/* dumb wait until writes are complete */
		while (FCO_REFCNT(fco) > 1)
			usleep(1000);
		assert(FCO_REFCNT(fco) == 1);
		fellow_cache_obj_deref(fc, fco);
	}

	// get back the original number of references
	for (u = 0; u < refcnt; u++) {
		INIT_OBJ(ocmem, OBJCORE_MAGIC);
		toc = ocmem;
		fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
		assert(fcr.status == fcr_ok);
		assert((toc == NULL) == (u == 0));
		fco = fcr.r.ptr;
	}
	*fcop = fco;
}

static void test_fellow_cache_obj_iter(
    struct fellow_cache *fc, struct fellow_cache_obj **fcop,
    const unsigned char h1[SHA256_LEN])
{
	int final;

	AN(fcop);
	AN(*fcop);

	for (final = 0; final < 2; final++)
		test_fellow_cache_obj_iter_final(fc, fcop, h1, final);
}

/* simulate v-c ObjExtend/ObjWaitExtend */
struct test_iter_wait_s {
	unsigned		magic;
#define TEST_ITER_WAIT_MAGIC	0x9b229927
	unsigned		done;
	unsigned		flush;
	ssize_t			r, w;
	pthread_mutex_t	mtx;
	pthread_cond_t		cond;
	// iter func
	void			*priv;
	objiterate_f		*func;
};

static void
init_test_iter_wait(struct test_iter_wait_s *tiw)
{
	INIT_OBJ(tiw, TEST_ITER_WAIT_MAGIC);
	AZ(pthread_mutex_init(&tiw->mtx, NULL));
	AZ(pthread_cond_init(&tiw->cond, NULL));
}

static void
fini_test_iter_wait(struct test_iter_wait_s *tiw)
{
	CHECK_OBJ_NOTNULL(tiw, TEST_ITER_WAIT_MAGIC);
	AZ(pthread_mutex_destroy(&tiw->mtx));
	AZ(pthread_cond_destroy(&tiw->cond));
}

static void
test_iter_extend(struct test_iter_wait_s *tiw, ssize_t sz)
{
	CHECK_OBJ_NOTNULL(tiw, TEST_ITER_WAIT_MAGIC);
	AZ(pthread_mutex_lock(&tiw->mtx));
	tiw->w += sz;
	AZ(pthread_cond_broadcast(&tiw->cond));
	AZ(pthread_mutex_unlock(&tiw->mtx));
}

static void
test_iter_wait(struct test_iter_wait_s *tiw)
{
	CHECK_OBJ_NOTNULL(tiw, TEST_ITER_WAIT_MAGIC);
	AZ(pthread_mutex_lock(&tiw->mtx));
	assert(tiw->r <= tiw->w);
	while (! tiw->done && tiw->r == tiw->w)
		AZ(pthread_cond_wait(&tiw->cond, &tiw->mtx));
	AZ(pthread_mutex_unlock(&tiw->mtx));
}

// analogous to fellow_stream_f in fellow_storage.c
static int
test_iter_f(void *priv, unsigned flush, const void *ptr, ssize_t len)
{
	struct test_iter_wait_s *tiw;
	const char *p;
	ssize_t l;
	int r = 0;

	CAST_OBJ_NOTNULL(tiw, priv, TEST_ITER_WAIT_MAGIC);

	DBG("r=%zd w=%zd (%zd) done=%u flush=%u ptr=%p len=%zd",
	    tiw->r, tiw->w, tiw->w - tiw->r, tiw->done, flush, ptr, len);

	if (ptr == NULL || len == 0)
		return (tiw->func(tiw->priv, flush, ptr, len));

	// can only see OBJ_ITER_END once
	if (flush & OBJ_ITER_END) {
		AZ(tiw->flush);
		tiw->flush = flush;
	}


	p = ptr;
	assert(tiw->w >= tiw->r);
	l = vmin_t(ssize_t, len, tiw->w - tiw->r);

	if (flush & OBJ_ITER_END)
		assert(l <= len);

	do {
		r = tiw->func(tiw->priv, flush, p, l);
		if (r)
			return (r);

		assert(len >= l);
		tiw->r += l;
		len -= l;
		p += l;

		test_iter_wait(tiw);
		assert(tiw->w >= tiw->r);
		l = vmin_t(ssize_t, len, tiw->w - tiw->r);
	} while (len > 0);

	AZ(len);
	return (r);
}

/* to pass arguments to the iter thread */
struct test_iter_thread_s {
	unsigned		magic;
#define TEST_ITER_THREAD_MAGIC	0xcbcd1884
	struct fellow_cache	*fc;
	struct fellow_cache_obj	*fco;
	void			*priv;
	objiterate_f		*func;
	int			final;
	// result
	struct fellow_cache_res	res;
	// the thread itself
	pthread_t		thr;
};

/* iterate in a separate thread */
static void *
test_fellow_cache_obj_iter_thread_f(void *p)
{
	struct test_iter_thread_s *args;
	struct test_iter_wait_s *tiw;

	CAST_OBJ_NOTNULL(args, p, TEST_ITER_THREAD_MAGIC);

	// analogous to sfemem_iterator():
	// have some data ready for obj_iter
	CAST_OBJ_NOTNULL(tiw, args->priv, TEST_ITER_WAIT_MAGIC);
	test_iter_wait(tiw);

	args->res = fellow_cache_obj_iter(args->fc, args->fco,
	    args->priv, args->func, args->final);
	return (NULL);
}

#define DBGSZ(x) \
	DBG(#x "\t%3zu", sizeof(struct x))

static unsigned
lru_find(struct fellow_cache_seg *fcs)
{
	struct fellow_cache_seg *needle;
	struct fellow_cache_lru *lru;

	lru = fcs->fco->lru;
	VTAILQ_FOREACH(needle, &lru->lru_head, lru_list)
	    if (needle == fcs)
		    return (1);
	return (0);
}

#define LCBMAX 16
static void
t_1lcb(struct fellow_cache_seg *fcs,
    uint8_t n, uint8_t i, uint8_t j, uint8_t k)
{
	uint8_t u,v;
	struct fellow_lru_chgbatch lcb[1] =
	    FELLOW_LRU_CHGBATCH_INIT(lcb, fcs->fco, LCBMAX);
	assert(n <= LCBMAX);

	for (u = 1; u < n + 2; u++) {	// length of remove
		AZ(lcb->n_rem);
		lcb->l_rem = u;

#define chg(from, to)							\
		DBG("%x->%x", from, to);				\
		for (v = 0; v < n; v++) {				\
		    fellow_cache_lru_chg(lcb, &fcs[v],			\
		    (int)(!!(to & 1<<v)) - (int)(!!(from & 1<<v)));	\
		}
#define apply(pos, to)							\
		if (k & (1<<pos)) {					\
			fellow_cache_lru_chgbatch_apply(lcb);		\
			for (v = 0; v < n; v++) {			\
				assert(!!(to & 1<<v) ==			\
				    lru_find(&fcs[v]));		\
			}						\
		}

		chg(0, i);
		apply(0, i);
		chg(i, j);
		apply(1, j);
		chg(j, k);
		apply(2, k);
		chg(k, i);
		apply(3, i);
		chg(i, 0);
		fellow_cache_lru_chgbatch_apply(lcb);
	}
}

static void
t_lcb(struct fellow_cache *fc)
{
	const uint8_t nfcs = 4;
	uint8_t i,j,k;
	struct fellow_cache_obj fco[1];
	struct fellow_cache_seg fcs[nfcs];

	INIT_OBJ(fco, FELLOW_CACHE_OBJ_MAGIC);
	fco->lru = fellow_cache_lru_new(fc);

	for (i = 0; i < nfcs; i++) {
		INIT_OBJ(&fcs[i], FELLOW_CACHE_SEG_MAGIC);
		fcs[i].fco = fco;
	}

	for (i = 0; i < 1<<nfcs; i++)
		for (j = 0; j < 1<<nfcs; j++)
			for (k = 0; k < 1<<nfcs; k++)
				t_1lcb(fcs, nfcs, i, j, k);
	DBG("done %s","---");
}

// test concurrent iter while filling object
static void
t_busyobj(unsigned chksum, struct fellow_cache *fc)
{
	unsigned char h1[SHA256_LEN], h2[SHA256_LEN];
	struct fellow_cache_obj *fco;
	uint8_t hash[DIGEST_LEN];
	struct fellow_busy *fbo;
	VSHA256_CTX sha256ctx;
	uintptr_t priv2;
	unsigned u;
	char *ptr;
	size_t sz;

	VSHA256_Init(&sha256ctx);
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	// --- set up thread to iterate while we create the busy obj
	VSHA256_CTX thr_sha256;
	VSHA256_Init(&thr_sha256);

	struct test_iter_wait_s tiw[1];
	init_test_iter_wait(tiw);
	tiw->priv = &thr_sha256;
	tiw->func = iter_sha256;

	struct test_iter_thread_s iter_thr[1];
	INIT_OBJ(iter_thr, TEST_ITER_THREAD_MAGIC);
	iter_thr->fc = fc;
	iter_thr->fco = fco;
	iter_thr->priv = tiw;
	iter_thr->func = test_iter_f;

	AZ(pthread_create(&iter_thr->thr, NULL,
		test_fellow_cache_obj_iter_thread_f, iter_thr));
	DBG("concurrent test_iter thread %p", (void *)iter_thr->thr);

	u = fco->fcsl->fdsl->lsegs + 72 * 2;
	XXX_LIMIT_LDSEGS = 1;
	while (u-- > 0) {
		void *p;
		sz = 1234;

		AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
		memset(ptr, 0, sz);
		p = ptr;
		(void) snprintf(ptr, sz, "We got %zu bytes at %p", sz, p);
		if (u > 0 && sz > 1234)
			sz = 1234;
		VSHA256_Update(&sha256ctx, ptr, sz);
		fellow_busy_obj_extend(fbo, sz);
		test_iter_extend(tiw, sz);
		if (u & 1)
			usleep(100);
	}
	XXX_LIMIT_LDSEGS = 0;
	VSHA256_Final(h1, &sha256ctx);
	// test trim of a 0-sized busy fcs
	AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
	fellow_busy_obj_trimstore(fbo);
	tiw->done = 1;
	test_iter_extend(tiw, 0);
	// if this fails, u is too low
	AN(fco->fcsl->next);
	// fixattr always return a pointer
	for (u = OA_VARY; u < OA__MAX; u++)
		AZ(fellow_cache_obj_getattr(fc, fco, u, &sz).r.ptr);
	for (u = 0; u < OA__MAX; u++)
		AN(fellow_busy_setattr(fbo, u, strlen(oatest[u]), oatest[u]));
	t_getattr(fc, fco);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	priv2 = fco->fdb.fdb;

	// check thread status
	DBG("concurrent test_iter thread join %p", (void *)iter_thr->thr);
	AZ(pthread_join(iter_thr->thr, NULL));
	VSHA256_Final(h2, &thr_sha256);
	AZ(memcmp(h1, h2, sizeof *h1));
	DBG("concurrent test_iter hash ok %02x%02x%02x%02x...",
	    h1[0], h1[1], h1[2], h1[3]);
	fini_test_iter_wait(tiw);

	/* dumb wait until writes are complete */
	while (FCO_REFCNT(fco) > 1)
		usleep(1000);
	//fellow_cache_obj_deref(fc, fco);
	fellow_cache_obj_delete(fc, fco, hash);
}

static void
t_cache(const char *fn, unsigned chksum)
{
	size_t sz;
	struct fellow_fd *ffd;
	buddy_t mb, *membuddy = &mb;
	struct fellow_cache *fc;
	struct fellow_cache_obj *fco;
	struct fellow_busy *fbo;
	uintptr_t priv2;
	void *resur_priv = NULL;
	unsigned u;
	uint8_t hash[DIGEST_LEN];
	VSHA256_CTX sha256ctx;
	unsigned char h1[SHA256_LEN];
	struct stvfe_tune tune[1];
	const size_t memsz = 10 * 1024 * 1024;
	const size_t dsksz = 100 * 1024 * 1024;
	const size_t objsize_hint = 4 * 1024;
	struct fellow_cache_res fcr;
	int injcount;
	char *ptr;
	uint64_t g_mem_obj = 0;

	DBGSZ(fellow_disk_seg);
	DBGSZ(fellow_disk_seglist);
	DBG("FELLOW_DISK_SEGLIST_MAX_SEGS %u", FELLOW_DISK_SEGLIST_MAX_SEGS);
	assert(FELLOW_DISK_SEGLIST_MAX_SEGS <= UINT16_MAX);
	DBGSZ(fellow_disk_obj);

	DBGSZ(fellow_cache_seg);
	DBGSZ(fellow_cache_seglist);
	DBGSZ(fellow_cache_obj);

	// canary so size increase does not happen unnoticed
	sz  = sizeof(struct fellow_cache_obj);
	assert(sz <= 360);

	AZ(stvfe_tune_init(tune, memsz, dsksz, objsize_hint));
	tune->hash_obj = chksum;
	// for injection tests
	tune->ioerr_obj = 1;
	tune->ioerr_log = 1;
	tune->allocerr_obj = 1;
	tune->allocerr_log = 1;
	// for coverage - hit cramming paths
	tune->cram = -64;
	AZ(stvfe_tune_check(tune));

	buddy_init(membuddy, MIN_BUDDY_BITS, memsz,
	    BUDDYF(mmap), NULL, NULL, NULL);
	ffd = fellow_log_init(fn, dsksz, objsize_hint,
	    NULL, NULL, membuddy, tune, fellow_simple_task_run, NULL);
	XXXAN(ffd);
	fellow_log_open(ffd, resurrect_discard, &resur_priv);

	fc = fellow_cache_init(ffd, membuddy, tune, fellow_simple_task_run,
	    &g_mem_obj);

	t_lcb(fc);

	// === empty obj, loop sizes
	for (sz = 0; sz < 4096 ; sz += 8) {
		fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, sz).r.ptr;
		CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
		test_bocdone(fbo, TRUST_ME(hash), 1);
		fellow_cache_obj_deref(fc, fco);
	}

	// === max out region alloc
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	u = 0;
	while (fbo->nregion < FCO_MAX_REGIONS - 1) {
		struct fellow_cache_seglist *fcsl;
		struct fellow_body_region *fbr;
		struct buddy_off_extent *fdr;
		struct fellow_cache_seg *fcs;

		/* allocate segments until seglist is full
		 *
		 * skip body region alloc heuristic in
		 * fellow_busy_body_seg_alloc() by making sure we always have an
		 * empty region
		 */
		fcsl = fbo->body_seglist;
		fbr = &fbo->body_region;
		while (fbo->nregion < FCO_MAX_REGIONS - 1 &&
		    fcsl->fdsl->nsegs < fcsl->fdsl->lsegs) {
			fdr = fbr->reg;
			if (fdr == NULL || fbr->len == fdr->size) {
				memset(fbr, 0, sizeof *fbr);
				fdr = fellow_busy_region_alloc(fbo, 1234,
				    INT8_MAX);
				fbr->reg = fdr;
				AZ(fbr->len);
			}
			AN(fdr);
			assert(fbr->len < fdr->size);
			assert(fcsl->segs[fcsl->fdsl->nsegs].state ==
			    FCS_USABLE);
			fcs = &fcsl->segs[fcsl->fdsl->nsegs++];
			AN(fellow_busy_body_seg_alloc(fbo, fcs->disk_seg,
				SIZE_MAX >> 1));
			// pretend - not actually written
			fcs->state = FCS_DISK;
			fbo->body_seg = fcs;
		}

		if (fbo->nregion == FCO_MAX_REGIONS - 1)
			break;

		fcsl = fbo->body_seglist;
		fcr = fellow_disk_seglist_alloc(fbo, fcsl, 1, FH_SHA256);
		assert(fcr.status == fcr_ok);
		CAST_OBJ_NOTNULL(fcsl, fcr.r.ptr, FELLOW_CACHE_SEGLIST_MAGIC);
		u++;

		fbo->body_seglist = fcsl;
		assert(fcsl->fdsl->lsegs >= 3);
		if (FCO_MAX_REGIONS - fbo->nregion <= 4)
			fcsl->fdsl->lsegs = 3;
		else
			fcsl->fdsl->lsegs = 1;
	}
	DBG("seglists created %u", u);

	for (u = 0; u < OA__MAX; u++) {
		AN(fellow_busy_setattr(fbo, u, strlen(oatest[u]), oatest[u]));
	}
	t_getattr(fc, fco);

	/*
	 * #39
	 * test fellow_busy_obj_trimstore() returning
	 * the second last segment (ESI attr are last)
	 */
	AN(fbo->body_seg);
	assert(fbo->body_seg->state == FCS_DISK);
	fbo->body_seg->state = FCS_USABLE;
	AN(fellow_busy_seg_memalloc(fbo, fbo->body_seg, 0));

	assert(fbo->nregion == FCO_MAX_REGIONS);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_delete(fc, fco, hash);

	// === alloc space, dont use
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	{
		sz = dsksz;
		AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
	}
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_deref(fc, fco);

	// === alloc space, trim
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	{
		sz = 1234;
		AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
	}
	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	(void)fellow_cache_obj_lru_touch(fco);
	fellow_cache_obj_deref(fc, fco);

	// === alloc space error inject
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc_inj_reset();
	injcount = -1;
	while (injcount) {
		DBG("injcount=%d", injcount);
		if (injcount > 0)
			fc_inj_set(injcount);

		sz = 1234;
		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);
		if (fcr.status == fcr_ok)
			fellow_busy_obj_extend(fbo, sz);
		else
			assert(fcr.status == fcr_allocerr);
		if (injcount < 0)
			injcount = fc_inj_count();
		else
			injcount--;
	}

	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_deref(fc, fco);

	// === hit objsize_max
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	do {
		sz = 1234;
		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);
		if (fcr.status == fcr_ok) {
			fellow_busy_obj_extend(fbo, sz);
			continue;
		}
		assert(fcr.status == fcr_allocerr);
		AZ(strcmp(fcr.r.err, "objsize_max reached"));
	} while (fcr.status == fcr_ok);

	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_deref(fc, fco);

	// === hit objsize_max with big chunks
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	do {
		sz = dsksz;
		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);
		if (fcr.status == fcr_ok) {
			fellow_busy_obj_extend(fbo, sz);
			continue;
		}
		assert(fcr.status == fcr_allocerr);
		AZ(strcmp(fcr.r.err, "objsize_max reached"));
	} while (fcr.status == fcr_ok);

	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_deref(fc, fco);

	// === hit objsize_max under memory pressure
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	do {
		if (fbo->nregion < FCO_MAX_REGIONS - FCO_REGIONS_RESERVE)
			FC_INJ_SZLIM_SET(4096);

		sz = 1234;
		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);
		if (fcr.status == fcr_ok) {
			fellow_busy_obj_extend(fbo, sz);
			continue;
		}
		assert(fcr.status == fcr_allocerr);
		AZ(strcmp(fcr.r.err, "objsize_max reached"));
		for (u = 0; u < OA__MAX; u++) {
			AN(fellow_busy_setattr(fbo, u,
				strlen(oatest[u]), oatest[u]));
		}
		t_getattr(fc, fco);
	} while (fcr.status == fcr_ok);

	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_deref(fc, fco);

	test_fellow_cache_unbusy_inject(fc);

	for (u = 0; u < 100; u++)
		t_busyobj(chksum, fc);

	// === alloc, then evict
	VSHA256_Init(&sha256ctx);
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	u = fco->fcsl->fdsl->lsegs + 72 * 2;
	while (u-- > 0) {
		void *p;
		sz = 1234;

		AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
		memset(ptr, 0, sz);
		p = ptr;
		(void) snprintf(ptr, sz, "We got %zu bytes at %p", sz, p);
		if (sz > 1234)
			sz = 1234;
		VSHA256_Update(&sha256ctx, ptr, sz);
		fellow_busy_obj_extend(fbo, sz);
	}
	VSHA256_Final(h1, &sha256ctx);
	fellow_busy_obj_trimstore(fbo);
	// if this fails, u is too low
	AN(fco->fcsl->next);
	// fixattr always return a pointer
	for (u = OA_VARY; u < OA__MAX; u++)
		AZ(fellow_cache_obj_getattr(fc, fco, u, &sz).r.ptr);
	for (u = 0; u < OA__MAX; u++)
		AN(fellow_busy_setattr(fbo, u, strlen(oatest[u]), oatest[u]));
	t_getattr(fc, fco);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	priv2 = fco->fdb.fdb;

	/* dumb wait until writes are complete */
	while (FCO_REFCNT(fco) > 1)
		usleep(1000);
	fellow_cache_obj_deref(fc, fco);

	/* get back object without and with close/open */
	for (u = 0; u < 2; u++) {
		fco = test_fellow_cache_obj_get(fc, priv2, notincache);
		assert_cache_seg_consistency(FCO_FCS(fco));
		DBG("fco1 %p ref %u", fco, FCO_REFCNT(fco));

		// second access does not assign the oc
		AN(fco == test_fellow_cache_obj_get(fc, priv2, incache));
		assert_cache_seg_consistency(FCO_FCS(fco));
		DBG("fco2 %p ref %u", fco, FCO_REFCNT(fco));

		// get attrs and compare
		t_getattr(fc, fco);

		// get segments and compare
		test_fellow_cache_obj_iter(fc, &fco, h1);

		assert_cache_seg_consistency(FCO_FCS(fco));

		fellow_cache_obj_deref(fc, fco);

		DBG("fco3 %p ref %u", fco, FCO_REFCNT(fco));
		if (u == 0)
			fellow_cache_obj_deref(fc, fco);
		else
			fellow_cache_obj_delete(fc, fco, hash);


		fellow_cache_fini(&fc);
		AZ(fc);
		fellow_log_close(&ffd);
		BWIT_ISEMPTY(membuddy->witness);
		buddy_fini(&membuddy, BUDDYF(unmap), NULL, NULL, NULL);
		AZ(ffd);

		if (u == 1)
			break;

		membuddy = &mb;
		buddy_init(membuddy, MIN_BUDDY_BITS, 10 * 1024 * 1024,
		    BUDDYF(mmap), NULL, NULL, NULL);
		ffd = fellow_log_init(fn, dsksz, objsize_hint,
		    NULL, NULL, membuddy, tune, fellow_simple_task_run, NULL);
		XXXAN(ffd);
		fellow_log_open(ffd, resurrect_keep, &resur_priv);

		AZ(g_mem_obj);
		fc = fellow_cache_init(ffd, membuddy, tune,
		    fellow_simple_task_run, &g_mem_obj);
	}
}

static void
t_tailq_concat(void)
{
	struct fellow_cache_seg	fcs[1];
	struct fellow_cache_lru_head	h1[1], h2[2];

	INIT_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);
	VTAILQ_INIT(h1);
	VTAILQ_INIT(h2);
	VTAILQ_INSERT_TAIL(h2, fcs, lru_list);
	VTAILQ_CONCAT(h1, h2, lru_list);
	assert(VTAILQ_FIRST(h1) == fcs);
	AZ(VTAILQ_NEXT(fcs, lru_list));
}

/*
 * if this fails, the strategy in fellow_busy_body_seglist_alloc() needs to be
 * adjusted
 */
static void
t_seglist_sizes(void)
{
	struct fellow_disk_seglist *fdsl;
	unsigned u, fcslsegs, fdslsegs, fdslcap;
	uint16_t strategy;
	size_t fdslsz;

	for (u = 12; u < 30; u++) {
		fcslsegs = fellow_cache_seglist_fit((size_t)1 << u);
		fdslsegs = fellow_disk_seglist_fit((size_t)1 << u);
		assert(fcslsegs <= fdslsegs);
		fdslsz = rup_min(SEGLIST_SIZE(fdsl, fcslsegs),
		    MIN_FELLOW_BITS);
		fdslcap = fellow_disk_seglist_fit(fdslsz);
		strategy = fellow_busy_body_seglist_size_strategy(fcslsegs + 1, 30);
		fprintf(stderr, "seglist 1<<%2u fcsl %6u (strat %6u) fdsl %6u "
		    "-> fdsl_size %7zu (%3zu blocks) cap %6u waste %6u\n",
		    u, fcslsegs, strategy, fdslsegs,
		    fdslsz, fdslsz >> MIN_FELLOW_BITS,
		    fdslcap, fdslcap - fcslsegs);
		// special case because limit in *seglist_fit
		if (strategy == FELLOW_DISK_SEGLIST_MAX_SEGS)
			assert(fcslsegs == 65535);
		else
			assert(strategy == fcslsegs);
		if (fcslsegs >= FELLOW_DISK_SEGLIST_MAX_SEGS ||
		    fdslsegs >= FELLOW_DISK_SEGLIST_MAX_SEGS)
			break;
	}
}

int
main(int argc, char **argv)
{
	unsigned l, hash;
	const char *fn = filename;

	if (argc > 1)
		fn = argv[1];

	DBG("fellow_busy %zu", sizeof(struct fellow_busy));
	assert(sizeof(struct fellow_busy) <= 2 * MIN_FELLOW_BLOCK);

	t_seglist_sizes();

	t_tailq_concat();

#ifdef HAVE_XXHASH_H
	l = FH_LIM;
#else
	l = FH_SHA256 + 1;
#endif

	for (hash = FH_SHA256; hash < l; hash++) {
		if (fh_name[hash])
			t_cache(fn, hash);
	}
	printf("OK\n");
	return (0);
}
#endif
