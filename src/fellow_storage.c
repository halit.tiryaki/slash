/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>	// vsl.h needs
#include <fcntl.h>	// open in tryopen
#include <sys/file.h>	// flock in tryopen
#include <sys/stat.h>	// stat in tryopen

#include "cache/cache_varnishd.h"

#include "cache/cache_obj.h"
#include "cache/cache_objhead.h"

#include "storage/storage.h"

#include "vnum.h"

// mapping
#include "common/heritage.h"
#include "common/vsmw.h"

#include "VSC_main.h"
#include "VSC_fellow.h"

#include "vtim.h"
#include "vapi/vsl.h"

#include "buddy.h"
#include "fellow_storage.h"
#include "fellow_cache.h"
#include "fellow_io.h"	// XXX disk region
#include "fellow_log_storage.h"
#include "fellow_cache_storage.h"
#include "fellow_pri.h"
#include "fellow_tune.h"
#include "pow2_units.h"
#include "foreign/vend.h"

#include "fellow_stash.h"

// XXX include cleanup
size_t fellow_rndup(const struct fellow_fd *ffd, size_t sz);

static void
mutate_to_sfeexp(struct objcore *oc, vxid_t xid);
static vtim_real
sfe_ban_earliest_time(const uint8_t *banspec, unsigned len);
static vtim_real
sfe_ban_latest_time(const uint8_t *banspec, unsigned len);
static void
stvfe_oc_dle_submit(struct fellow_fd *ffd, struct objcore *oc,
    enum dle_type type);
static inline int stvfe_oc_inlog(struct objcore *oc);

/* Compat ------------------------------------------------------------
 */

// varnish-cache pre 7f28888779fd14f99eb34e50f6fb07ea6bbff999
#ifndef NO_VXID
#define NO_VXID (0U)
#endif

/* Error messages ----------------------------------------------------
 *
 * for argv configuration, we ultimately call ARGV_ERR, which calls exit()
 *
 * to also support vmod configuration (addition of stevedores from vcl),
 * we can not use that, so have a static buffer
 */

static char errbuf[1024] = "";
#define STVERR(...) do {					\
		bprintf(errbuf, __VA_ARGS__);			\
		return (errbuf);				\
} while (0)

const char *
sfe_error(void)
{
	return (errbuf);
}

/* Shutdown support --------------------------------------------------
 *
 * to remove a stevedore, we need to delete (or migrate) all its objects from
 * varnish-cache, but we do not want to remove them from storage.  To
 * differentiate between the two cases, we use a magic marker in the strangelove
 * worker attribute.
 *
 * because that is also used for the lru_limit, we need to ensure that we never
 * use our special value as the limit.
 */

enum stve_strangelove {
	STVELOVE_NUKE = INT_MAX -2,
	STVELOVE_IS_DRAIN,
	STVELOVE_SIGNAL_DRAIN
};

/* Wait table --------------------------------------------------------
 *
 * We got a challenge in stvfe_dskoc_fco: while we can solve concurrent inserts
 * via the fdb index, we want already the memory allocation to be limited to
 * only one thread, otherwise a lot of threads waiting for an _obj_get to
 * complete will allocate fcos just to throw them away a moment later.
 *
 * We would like a "waitinglist" mechanism where only one thread goes to
 * allocate the object and the others hang on to the waitinglist, but before we
 * have allocated memory, we do not have the memory for this mechanism. Catch22.
 * Our way out is a per-stvfe wait table with mtx/cond pairs, indexed via a
 * hash. There will be spurious wakeups, but those are deemed better than
 * useless allocations.
 */

struct stvfe_wait_entry {
	uint64_t		priv;
	pthread_mutex_t		mtx;
	pthread_cond_t		cond;
};

struct stvfe_wait {
	unsigned		magic;
#define STVFE_WAIT_MAGIC	0x664ec959
	uint8_t		bits;
	// (1 << bits) entries
	struct stvfe_wait_entry	e[];
};

static void
stvfe_wait_fini(struct stvfe_wait **swp) {
	struct stvfe_wait *sw;
	size_t i, l;

	TAKE_OBJ_NOTNULL(sw, swp, STVFE_WAIT_MAGIC);
	AN(sw->bits);
	l = 1;
	l <<= sw->bits;
	for (i = 0; i < l; i++) {
		AZ(pthread_mutex_destroy(&sw->e[i].mtx));
		AZ(pthread_cond_destroy(&sw->e[i].cond));
	}
	FREE_OBJ(sw);
}

static struct stvfe_wait *
stvfe_wait_new(uint8_t pow2) {
	struct stvfe_wait *sw;
	size_t sz, i, l;

	AN(pow2);
	sz = sizeof(*sw) + (sizeof(struct stvfe_wait_entry) << pow2);
	sw = malloc(sz);
	AN(sw);
	memset(sw, 0, sz);
	sw->magic = STVFE_WAIT_MAGIC;
	sw->bits = pow2;

	l = 1;
	l <<= sw->bits;
	for (i = 0; i < l; i++) {
		AZ(pthread_mutex_init(&sw->e[i].mtx, NULL));
		AZ(pthread_cond_init(&sw->e[i].cond, NULL));
	}
	return (sw);
}

static void
stvfe_wait_tune(struct stvfe_wait **swp, uint8_t pow2)
{
	struct stvfe_wait_entry *e;
	struct stvfe_wait *sw;
	size_t i, l;

	AN(swp);
	sw = *swp;

	CHECK_OBJ_NOTNULL(sw, STVFE_WAIT_MAGIC);
	if (pow2 == sw->bits)
		return;
	*swp = stvfe_wait_new(pow2);
	// cool old pointer
	(void) usleep(10 * 1000);

	l = 1;
	l <<= sw->bits;
	for (i = 0; i < l; i++) {
		e = &sw->e[i];
		AZ(pthread_mutex_lock(&e->mtx));
		while (e->priv != 0)
			AZ(pthread_cond_wait(&e->cond, &e->mtx));
		AZ(pthread_mutex_unlock(&e->mtx));
	}
	stvfe_wait_fini(&sw);
	AZ(sw);
}

static struct stvfe_wait_entry *
stvfe_wait(struct stvfe_wait *sw, uint64_t priv)
{
	struct stvfe_wait_entry *e, *r = NULL;
	size_t i;

	CHECK_OBJ_NOTNULL(sw, STVFE_WAIT_MAGIC);
	AN(priv);
	i = fib(priv, sw->bits);
	e = &sw->e[i];

	AZ(pthread_mutex_lock(&e->mtx));
	while (e->priv != priv && e->priv != 0)
		AZ(pthread_cond_wait(&e->cond, &e->mtx));
	if (e->priv != priv) {
		AZ(e->priv);
		e->priv = priv;
		r = e;
	}
	else
		AZ(pthread_cond_wait(&e->cond, &e->mtx));
	AZ(pthread_mutex_unlock(&e->mtx));
	return (r);
}

static void
stvfe_wait_signal(struct stvfe_wait_entry *e, uint64_t priv)
{

	AN(e);
	AZ(pthread_mutex_lock(&e->mtx));
	assert(e->priv == priv);
	e->priv = 0;
	AZ(pthread_cond_broadcast(&e->cond));
	AZ(pthread_mutex_unlock(&e->mtx));
}

/* Stevedore ---------------------------------------------------------*/

enum stvfe_scope {
	STVFE_INVAL = 0,
	STVFE_GLOBAL,
	STVFE_VCL_DISCARD,	// discard all objects
	STVFE_VCL_EMPTY	// fail unless storage is empty
};

BUDDY_REQS(ban_reqs_s, DLE_BAN_REG_NREGION);

struct stvfe {
	unsigned		magic;
#define STVFE_MAGIC		0x26172c91
	unsigned		shutdown:1;	// once
	unsigned		banexport:2;
	unsigned		ban_space_state:2;
	enum stvfe_scope	scope;

	struct stevedore	*memstv;
	struct stevedore	*dskstv;
	struct fellow_cache	*fc;
	struct fellow_fd	*ffd;
	struct VSC_fellow	*stats;
	struct vsc_seg		*vsc_seg;
	struct stvfe_wait	*wait;

	buddy_t		my_membuddy;
	buddy_t		*membuddy;

	pthread_t			dsk_lru_thread;

	uintptr_t			oev;

	struct stvfe_tune	tune;
	const char		*path;
	const struct stvfe	*inherit;
	unsigned		ref;

	// since last export
	struct {
		unsigned	n_new, n_drop;
		size_t		l_new, l_drop;
	} banstat;
	struct buddy_off_extent	ban_space[DLE_BAN_REG_NREGION];
	/* async reservation for ban export disk space */
	struct ban_reqs_s	ban_reqs[1];
	VTAILQ_ENTRY(stvfe)	list;
};

/* global instances only, unlocked */
VTAILQ_HEAD(stvfe_head_s, stvfe);
static struct stvfe_head_s stvfe_head = VTAILQ_HEAD_INITIALIZER(stvfe_head);

// lookup by memstv name
static struct stvfe *
stvfe_lookup(const char *ident)
{
	const struct stevedore *stv;
	struct stvfe *stvfe;

	AN(ident);
	VTAILQ_FOREACH(stvfe, &stvfe_head, list) {
		CHECK_OBJ_NOTNULL(stvfe, STVFE_MAGIC);
		stv = stvfe->memstv;
		CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
		AN(stv->ident);
		if (! strcmp(ident, stv->ident))
			return (stvfe);
	}
	return (NULL);
}

#define OA_INLOG OA__MAX

static void sfe_ban_space_return(struct stvfe *stvfe);

static inline const struct stevedore *
oc_stv(const struct worker *wrk, const struct objcore *oc)
{
	const struct stevedore *stv;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	stv = oc->stobj->stevedore;
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	return (stv);
}

static inline const struct stvfe *
stv_stvfe(const struct stevedore *stv)
{
	struct stvfe *stvfe;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);
	return (stvfe);
}

/* ----------------------------------------------------------------------
 * memory vs. disk objects
 *
 * we use different objcores for memory vs. disk objects with different
 * stevedores to shorten call paths.
 *
 * only a memoc can be busy
 *
 * priv2 always is the fellow disk block (fdb, offset + size)
 *
 * memoc:
 * priv always points to fellow_cache_obj
 *
 * dskoc:
 * priv normally does not point to fellow_cache_obj, except while getting it
 * (racy)
 *
 * loaded objects start off as dskoc
 * memocs mutate to dskocs for memory LRU
 */

#define assert_memstv(stvfe, stv) do {		\
	assert(stv != NULL);			\
	assert(stv != stvfe->dskstv);		\
	assert(stv == stvfe->memstv);		\
	} while (0)

static inline struct fellow_cache_obj *
stvfe_memoc_fco(const struct stevedore *stv, const struct stvfe *stvfe,
    const struct objcore *oc)
{
	void *priv;

	CHECK_OBJ_NOTNULL(stvfe, STVFE_MAGIC);
	assert_memstv(stvfe, stv);
	AN(oc->stobj->priv2);
	// CST implies a memory barrier
	priv = __atomic_load_n(/*lint --e(605)*/ &oc->stobj->priv,
	    __ATOMIC_SEQ_CST);
	AN(priv);
	return (priv);
}

/* cookie to handle case with/without reference to fco */
struct fcoc {
	struct fellow_cache_obj *fco;
	struct fellow_cache_obj *fcoref;
};

static inline void
fcoc_fini(struct fellow_cache *fc, struct fcoc *fcoc) {
	AN(fcoc->fco);
	if (fcoc->fcoref)
		fellow_cache_obj_deref(fc, fcoc->fcoref);
	memset(fcoc, 0, sizeof *fcoc);
}

static struct fcoc
stvfe_dskoc_fco(struct worker *wrk,
    const struct stevedore *stv, const struct stvfe *stvfe,
    struct objcore *oc, unsigned crit)
{
	struct stvfe_wait_entry *we = NULL;
	struct fellow_cache_res fcr;
	struct objcore *refoc;
	struct fcoc fcoc = {0};
	uint64_t *counter;
	void *priv;

	CHECK_OBJ_NOTNULL(stvfe, STVFE_MAGIC);
	AN(oc->stobj->priv2);

	counter = &stvfe->stats->c_dsk_obj_get_present;

  again:
	// CST implies a memory barrier
	priv = __atomic_load_n(&oc->stobj->priv, __ATOMIC_SEQ_CST);

	if (priv != NULL) {
		fcoc.fco = priv;
		(*counter)++;
		return (fcoc);
	}

	if (! crit) {
		we = stvfe_wait(stvfe->wait, oc->stobj->priv2);
		if (we == NULL ) {
			counter = &stvfe->stats->c_dsk_obj_get_coalesce;
			goto again;
		}
	}

	/* we race to get the fco and, if we win the race,
	 * point the oc to the fco
	 *
	 * XXX/TODO: since introduction of stvfe_wait, there
	 * should be no more race in the normal case, but
	 * only if the object gets removed immediately
	 */

	refoc = oc;

	fcr = fellow_cache_obj_get(stvfe->fc, &refoc, oc->stobj->priv2, crit);
	if (fcr.status != fcr_ok) {
		VSLb(wrk->vsl, SLT_Error, "%s %s: %s",
		    stv->name, stv->ident,
		    fcr.r.err ? fcr.r.err : "Unknown error");
		HSH_Fail(oc);
		AZ(oc->flags & OC_F_BUSY);
		HSH_Kill(oc);

		AZ(fcoc.fco);
		stvfe->stats->c_dsk_obj_get_fail++;
		goto out;
	}

	fcoc.fco = fcr.r.ptr;

	if (refoc != NULL) {
		/* lost race */
		assert(refoc == oc);
		fcoc.fcoref = fcoc.fco;
		stvfe->stats->c_dsk_obj_get_coalesce++;
		goto out;
	}

	assert(fcoc.fco == oc->stobj->priv);
	assert(stv == stvfe->dskstv);
	oc->stobj->stevedore = stvfe->memstv;

	wrk->stats->n_vampireobject--;
	wrk->stats->n_object++;

	stvfe->stats->c_dsk_obj_get++;

  out:
	if (we)
		stvfe_wait_signal(we, oc->stobj->priv2);
	return (fcoc);
}


/* ----------------------------------------------------------------------
 * error handling
 */

static void
stvfe_fcr_handle(struct worker *wrk, struct objcore *oc,
    const struct stevedore *stv, const struct stvfe *stvfe,
    struct fellow_cache_res fcr)
{

	if (fcr.status == fcr_ok)
		return;

	stvfe->stats->c_mem_obj_fail++;

	VSLb(wrk->vsl, SLT_Error, "%s %s %s: %s",
	    stv->name, stv->ident,
	    fellow_cache_res_s[fcr.status],
	    fcr.r.err ? fcr.r.err : "Unknown error");
	HSH_Fail(oc);
	if (!(/*lint --e(641)*/ oc->flags & OC_F_BUSY))
		HSH_Kill(oc);
}

/* handle fellow_cache_res for iterators */
static int
stvfe_fcr_handle_iter(struct worker *wrk, struct objcore *oc,
    const struct stevedore *stv, const struct stvfe *stvfe,
    struct fellow_cache_res fcr)
{

	if (fcr.status == fcr_ok)
		return (fcr.r.integer);

	stvfe_fcr_handle(wrk, oc, stv, stvfe, fcr);

	return (-1);
}

/* ----------------------------------------------------------------------
 * obj_methods
 */

static void
sfemem_bocfini(struct boc *boc, struct objcore *oc, unsigned inlog)
{
	struct fellow_busy *fbo;

	CHECK_OBJ_NOTNULL(boc, BOC_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	fbo = boc->stevedore_priv;
	AN(fbo);
	boc->stevedore_priv = NULL;

	fellow_busy_done(fbo, oc, inlog);
}

/* mem free */

/*
 * called in two cases:
 * - oc->boc == NULL: cache object on LRU freed
 * - oc->boc != NULL: cache object replaced for backend error
 */
static void
sfemem_free(struct worker *wrk, struct objcore *memoc)
{
	const struct stevedore *stv = oc_stv(wrk, memoc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fellow_cache_obj *fco = stvfe_memoc_fco(stv, stvfe, memoc);
	const struct objhead *oh = memoc->objhead;

	CHECK_OBJ_NOTNULL(oh, OBJHEAD_MAGIC);
	assert_memstv(stvfe, stv);

	AN(stv->lru);
	// ref fellow_busy_done()
	if (memoc->boc != NULL)
		sfemem_bocfini(memoc->boc, memoc, 0);
	else
		LRU_Remove(memoc);

	if (stvfe->shutdown && wrk->strangelove == (int)STVELOVE_IS_DRAIN) {
		fellow_cache_obj_wait_written(fco);
		fellow_cache_obj_deref(stvfe->fc, fco);
	} else {
		// fellow_cache_obj_delete() implies deref
		fellow_cache_obj_delete(stvfe->fc, fco, oh->digest);
		stvfe->stats->c_mem_obj_free++;
	}
}

static void v_matchproto_(objslim_f)
sfemem_objslim(struct worker *wrk, struct objcore *memoc)
{
	const struct stevedore *stv = oc_stv(wrk, memoc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	vxid_t xid;

	/* for drain, mutate to exp object */
	if (stvfe->shutdown && wrk->strangelove == (int)STVELOVE_IS_DRAIN) {
		xid = ObjGetXID(wrk, memoc);

		sfemem_free(wrk, memoc);
		mutate_to_sfeexp(memoc, xid);
		return;
	}

	/*
	 * for calls from HSH_Cancel, we could write a new disk object with no
	 * body. For now, just free any body segments cached in memory
	 */

	struct fellow_cache_obj *fco = stvfe_memoc_fco(stv, stvfe, memoc);
	fellow_cache_obj_slim(stvfe->fc, fco);
}

static void v_matchproto_(objfree_f)
sfemem_objfree(struct worker *wrk, struct objcore *memoc)
{

	sfemem_free(wrk, memoc);
	memset(memoc->stobj, 0, sizeof memoc->stobj);
	wrk->stats->n_object--;
}

/*
 * dsk free
 */

static void v_matchproto_(objfree_f)
sfedsk_objfree(struct worker *wrk, struct objcore *dskoc)
{
	const struct stevedore *stv = oc_stv(wrk, dskoc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fcoc fcoc;
	struct objhead *oh = dskoc->objhead;

	CHECK_OBJ_NOTNULL(oh, OBJHEAD_MAGIC);

	AN(stv->lru);
	AZ(dskoc->boc);
	LRU_Remove(dskoc);

	AN(stvfe_oc_inlog(dskoc));

	if (stvfe->membuddy->waiting) {
		/* prio #1: memory shortage. get rid of the object
		 *	    without needing more memory */
		stvfe_oc_dle_submit(stvfe->ffd, dskoc, DLE_OBJ_DEL_THIN);
		stvfe_oc_log_removed(dskoc);
		wrk->stats->n_vampireobject--;
		stvfe->stats->c_dsk_obj_free_thin++;
	}
	else if (
	    (fcoc = stvfe_dskoc_fco(wrk, stv, stvfe, dskoc, 1)).fco != NULL) {
		/* attempt to read to free space */
		fellow_cache_obj_delete(stvfe->fc, fcoc.fco, oh->digest);
		fcoc_fini(stvfe->fc, &fcoc);
		wrk->stats->n_object--;
		stvfe->stats->c_dsk_obj_free_get++;
	}
	else {
		/* if that failed, delete at least */
		stvfe_oc_dle_submit(stvfe->ffd, dskoc, DLE_OBJ_DEL_THIN);
		stvfe_oc_log_removed(dskoc);
		wrk->stats->n_vampireobject--;
		stvfe->stats->c_dsk_obj_free_thin++;
	}
	memset(dskoc->stobj, 0, sizeof dskoc->stobj);
}

static void v_matchproto_(objslim_f)
sfedsk_objslim(struct worker *wrk, struct objcore *dskoc)
{
	const struct stevedore *stv = oc_stv(wrk, dskoc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fcoc fcoc;

	/* for drain, mutate to exp object */
	if (stvfe->shutdown && wrk->strangelove == (int)STVELOVE_IS_DRAIN) {
		LRU_Remove(dskoc);
		mutate_to_sfeexp(dskoc, (struct vxids){42});
		return;
	}

	/* NOOP for calls from HSH_Cancel() */
	//lint -e{641,655}
	if (dskoc->flags & (OC_F_PRIVATE | OC_F_HFM | OC_F_HFP))
		return;

	/* turn into memoc for proper delete during LRU */
	fcoc = stvfe_dskoc_fco(wrk, stv, stvfe, dskoc, 1);
	fcoc_fini(stvfe->fc, &fcoc);

	stvfe->stats->c_dsk_obj_free_get++;
	// avoid double accounting
	stvfe->stats->c_mem_obj_free--;
}

/*
 * our iterator at the cache layer should not need to know about varnish-cache
 * specifics, so we encapsulate the busy object / streaming coordination in an
 * iterator function.
 *
 * the basic assumption at the cache layer is that there always exists a segment
 * if there is any data to be iterated over, so we always call ObjWaitExtend()
 * via fellow_stream_wait() before returning.
 *
 *
 * NOTE: for a tune event, this might still race (by design)
 */

struct fellow_stream {
	unsigned magic;
#define FELLOW_STREAM_MAGIC	0x374d85b8
	enum boc_state_e state;

	struct worker *wrk;
	struct objcore *oc;
	void *priv;
	objiterate_f *func;
	struct boc *boc;

	ssize_t available;
	ssize_t written;
	ssize_t checkpoint;	// up to this segment
};

static enum boc_state_e
fellow_stream_wait(struct fellow_stream *fs)
{
	enum boc_state_e state;
	uint64_t l;

	CHECK_OBJ_NOTNULL(fs, FELLOW_STREAM_MAGIC);
	do {
		l = ObjWaitExtend(fs->wrk, fs->oc, (uint64_t)fs->available);
		state = fs->boc->state;

		if (state == BOS_FINISHED || state == BOS_FAILED)
			break;
		assert(state == BOS_STREAM);
	} while (l == (uint64_t)fs->available);
	fs->state = state;
	fs->available = (ssize_t)l;
	return (state);
}

/* called on each (potentially busy) segment's data */
static int v_matchproto_(objiterate_f)
fellow_stream_f(void *priv, unsigned flush, const void *ptr, ssize_t len)
{
	struct fellow_stream *fs;
	const char *p;
	ssize_t l;
	int r;

	CAST_OBJ_NOTNULL(fs, priv, FELLOW_STREAM_MAGIC);

	if (fs->state == BOS_FAILED)
		return (-1);

	assert((fs->state == BOS_FINISHED && fs->available >= fs->written) ||
	    (fs->state == BOS_STREAM && fs->available > fs->written));
	assert(fs->checkpoint == fs->written);

	if (ptr == NULL || len == 0)
		return (fs->func(fs->priv, flush, ptr, len));

	if (flush & OBJ_ITER_END) {
		for (r = 0; fs->state != BOS_FINISHED && r < 2; r++)
			if (fellow_stream_wait(fs) == BOS_FAILED)
				return (-1);
		assert(fs->state == BOS_FINISHED);
	}

	p = ptr;
	assert(fs->available >= fs->written);
	l = vmin_t(ssize_t, len, fs->available - fs->written);

	do {
		r = fs->func(fs->priv, flush, p, l);
		if (r)
			return (r);

		assert(len >= l);
		fs->written += l;
		len -= l;
		p += l;

		if (fellow_stream_wait(fs) == BOS_FAILED)
			return (-1);

		assert(fs->available >= fs->written);
		l = vmin_t(ssize_t, len, fs->available - fs->written);
	} while (l > 0);

	fs->checkpoint = fs->written;
	return (r);
}

#ifdef VC_4013_WORKAROUND
/* to avoid the inefficient allocation if we copy a fellow object for 304, we
 * store the length remaining from OA_LEN and use it instead */
static pthread_key_t vc4013_key;
static void __attribute__((constructor))
init_vc4013(void)
{
	PTOK(pthread_key_create(&vc4013_key, NULL));
}

struct vc4013 {
	unsigned	magic;
#define VC4013_MAGIC	0xaf1f05b9
	struct boc	*boc;
	// not yet allocated
	size_t		l;
	// current allocation
	uint8_t		*p;
	size_t		pl;
};

// do do {} while() because the local variable needs to be in scope
#define VC4013_INIT(wrk, oc)				\
	struct vc4013 hack[1];				\
	INIT_OBJ(hack, VC4013_MAGIC);			\
	hack->l = ObjGetLen(wrk, oc);			\
	assert(hack->l > 0);				\
	PTOK(pthread_setspecific(vc4013_key, hack));
#define VC4013_FINI() PTOK(pthread_setspecific(vc4013_key, NULL))
#else
#define VC4013_INIT(wrk, oc) (void)0
#define VC4013_FINI() (void)0
#endif


static int v_matchproto_(objiterator_f)
sfemem_iterator(struct worker *wrk, struct objcore *oc,
    void *priv, objiterate_f *func, int final)
{
	const struct stevedore *stv = oc_stv(wrk, oc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fellow_cache_obj *fco = stvfe_memoc_fco(stv, stvfe, oc);
	struct fellow_cache_res fcr;
	struct fellow_stream fs;
	struct boc *boc;
	int ret;

	boc = HSH_RefBoc(oc);

	if (boc == NULL) {
		VC4013_INIT(wrk, oc);
		fcr = fellow_cache_obj_iter(stvfe->fc, fco, priv, func, final);
		VC4013_FINI();
		return (stvfe_fcr_handle_iter(wrk, oc, stv, stvfe, fcr));
	}

	INIT_OBJ(&fs, FELLOW_STREAM_MAGIC);
	fs.wrk = wrk;
	fs.oc = oc;
	fs.priv = priv;
	fs.func = func;
	fs.boc = boc;

	// flush header if waiting for data
	if (fs.boc->BOC_FETCHED_SO_FAR == 0) {
		ret = func(priv, OBJ_ITER_FLUSH, NULL, (ssize_t)0);
		if (ret)
			goto out;
	}

	// have some data ready for obj_iter
	if (fellow_stream_wait(&fs) == BOS_FAILED) {
		ret = -1;
		goto out;
	}

	fcr = fellow_cache_obj_iter(stvfe->fc, fco,
	    &fs, fellow_stream_f, final);
	ret = stvfe_fcr_handle_iter(wrk, oc, stv, stvfe, fcr);

  out:
	HSH_DerefBoc(wrk, oc);
	return (ret);
}

static int v_matchproto_(objiterator_f)
sfedsk_iterator(struct worker *wrk, struct objcore *dskoc,
    void *priv, objiterate_f *func, int final)
{
	const struct stevedore *stv = oc_stv(wrk, dskoc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fcoc fcoc = stvfe_dskoc_fco(wrk, stv, stvfe, dskoc, 0);
	struct fellow_cache_res fcr;

	if (fcoc.fco == NULL)
		return (-1);

	AZ(HSH_RefBoc(dskoc));

	VC4013_INIT(wrk, dskoc);
	fcr = fellow_cache_obj_iter(stvfe->fc, fcoc.fco, priv, func, final);
	VC4013_FINI();
	fcoc_fini(stvfe->fc, &fcoc);
	return (stvfe_fcr_handle_iter(wrk, dskoc, stv, stvfe, fcr));
}

static inline struct fellow_busy *
sfe_fbo(struct objcore *oc)
{
	struct boc *boc;

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	boc = oc->boc;
	CHECK_OBJ_NOTNULL(boc, BOC_MAGIC);
	return (boc->stevedore_priv);
}

static int v_matchproto_(objgetspace_f)
sfemem_getspace(struct worker *wrk, struct objcore *oc, ssize_t *ssz,
    uint8_t **ptr)
{
	struct fellow_cache_res fcr;
	size_t sz;

	(void) wrk;
	AN(ssz);
	AN(ptr);
	assert(*ssz > 0);
	sz = (size_t)*ssz;

#ifdef VC_4013_WORKAROUND
	struct vc4013 *hack;
	struct boc *boc;

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	boc = oc->boc;
	CHECK_OBJ_NOTNULL(boc, BOC_MAGIC);

	hack = pthread_getspecific(vc4013_key);
	if (hack != NULL && (hack->boc == NULL || hack->boc == boc)) {
		CHECK_OBJ(hack, VC4013_MAGIC);
		hack->boc = boc;
		fcr.status = fcr_ok;
		if (hack->pl == 0) {
			hack->p = NULL;
			AN(hack->l);
			hack->pl = hack->l;
			fcr = fellow_busy_obj_getspace(sfe_fbo(oc),
			    &hack->pl, &hack->p);
			if (fcr.status != fcr_ok)
				goto out;
			if (hack->pl < hack->l)
				hack->l -= hack->pl;
			else
				hack->l = 0;
		}
		AN(hack->pl);
		AN(hack->p);
		*ptr = hack->p;
		sz = vmin(hack->pl, sz);
		hack->p += sz;
		hack->pl -= sz;
		goto out;
	}
#endif

	fcr = fellow_busy_obj_getspace(sfe_fbo(oc), &sz, ptr);
#ifdef VC_4013_WORKAROUND
  out:
#endif
	if (fcr.status != fcr_ok) {
		/*
		 * XXX can not fail the oc here:
		 * Assert error in vbf_fetch_thread(), cache/cache_fetch.c line 1124:
		 *   Condition((oc->flags & OC_F_FAILED) == 0) not true.
		 */
		//XXX oc_fcr_handle(wrk, oc, fcr);
		VSLb(wrk->vsl, SLT_Error, "fellow %s: %s",
		    fellow_cache_res_s[fcr.status],
		    fcr.r.err ? fcr.r.err : "Unknown error");
		*ssz = -1;
		return (0);
	}
	*ssz = (ssize_t)sz;
	return (1);
}

static void v_matchproto_(objextend_f)
sfemem_extend(struct worker *wrk, struct objcore *oc, ssize_t l)
{

	(void) wrk;
	assert(l > 0);

	fellow_busy_obj_extend(sfe_fbo(oc), (size_t)l);
}

#ifndef VC_4013_WORKAROUND
static void v_matchproto_(objtrimstore_f)
sfemem_trimstore(struct worker *wrk, struct objcore *oc)
{

	(void) wrk;

	fellow_busy_obj_trimstore(sfe_fbo(oc));
}
#endif

static const uint16_t oa_inlog_bit = 1 << (int)OA_INLOG;

/* if the oc is in the log */
static inline int
stvfe_oc_inlog(struct objcore *oc)
{
	const struct stevedore *stv;
	const struct stvfe *stvfe;

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	stv = oc->stobj->stevedore;
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	assert(stv == stvfe->memstv || stv == stvfe->dskstv);

	return (!!(oc->oa_present & oa_inlog_bit));
}

/* marks the oc as submitted to the log */
void
stvfe_oc_log_removed(struct objcore *oc)
{
	const struct stevedore *stv;
	const struct stvfe *stvfe;

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	stv = oc->stobj->stevedore;
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	assert(stv == stvfe->memstv || stv == stvfe->dskstv);

	AN(oc->oa_present & oa_inlog_bit);
	oc->oa_present &= ~oa_inlog_bit;
}

/* marks the oc as submitted to the log */
void
stvfe_oc_log_submitted(struct objcore *oc)
{
	const struct stevedore *stv;
	const struct stvfe *stvfe;

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	stv = oc->stobj->stevedore;
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	assert(stv == stvfe->memstv || stv == stvfe->dskstv);

	AZ(oc->oa_present & oa_inlog_bit);
	oc->oa_present |= oa_inlog_bit;
}

/* fills only the u.obj sub-struct */
void
stvfe_oc_dle_obj(struct objcore *oc, struct fellow_dle *e)
{
	struct objhead *oh;

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	oh = oc->objhead;
	CHECK_OBJ_NOTNULL(oh, OBJHEAD_MAGIC);

	memcpy(e->u.obj.hash, oh->digest, (size_t)DIGEST_LEN);
	EXP_COPY(&e->u.obj, oc);
	e->u.obj.ban = BAN_Time(oc->ban);
}

static void
stvfe_oc_dle_submit(struct fellow_fd *ffd, struct objcore *oc,
    enum dle_type type)
{
	static const char ndle = 1;
	struct fellow_dle dle[ndle];

	AN(stvfe_oc_inlog(oc));
	fellow_dle_init(dle, ndle);
	stvfe_oc_dle_obj(oc, dle);
	dle->type = DLEDSK(type);
	dle->u.obj.start.fdb = oc->stobj->priv2;

	fellow_log_dle_submit(ffd, dle, ndle);
}

static void v_matchproto_(objbocdone_f)
sfemem_bocdone(struct worker *wrk, struct objcore *oc, struct boc *boc)
{
	unsigned inlog;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	CHECK_OBJ_NOTNULL(boc, BOC_MAGIC);

	if (oc->boc == NULL)
		LRU_Add(oc, VTIM_real());

	inlog = ! (/*lint --e(641,655)*/oc->flags &
	    (OC_F_PRIVATE|OC_F_DYING));

	sfemem_bocfini(boc, oc, inlog);
}

static const void * v_matchproto_(objgetattr_f)
sfemem_getattr(struct worker *wrk, struct objcore *memoc, enum obj_attr attr,
   ssize_t *slen)
{
	const struct stevedore *stv = oc_stv(wrk, memoc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fellow_cache_obj *fco = stvfe_memoc_fco(stv, stvfe, memoc);
	struct fellow_cache_res fcr;
	size_t len;

	assert_memstv(stvfe, stv);

	fcr = fellow_cache_obj_getattr(stvfe->fc, fco, attr, &len);
	stvfe_fcr_handle(wrk, memoc, stv, stvfe, fcr);

	if (slen)
		*slen = (ssize_t)len;

	if (attr == OA_VARY)
		(void) fellow_cache_obj_lru_touch(fco);

	return (fcr.status == fcr_ok ? fcr.r.ptr : NULL);
}

static const void * v_matchproto_(objgetattr_f)
sfedsk_getattr_tomem(struct worker *wrk, struct objcore *dskoc,
    enum obj_attr attr, ssize_t *slen)
{
	const struct stevedore *stv = oc_stv(wrk, dskoc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fcoc fcoc = stvfe_dskoc_fco(wrk, stv, stvfe, dskoc,
	    attr == OA_VARY ? 1 : 0);
	struct fellow_cache_res fcr;
	size_t len;

	if (fcoc.fco == NULL)
		return (NULL);

	fcr = fellow_cache_obj_getattr(stvfe->fc, fcoc.fco, attr, &len);
	stvfe_fcr_handle(wrk, dskoc, stv, stvfe, fcr);
	fcoc_fini(stvfe->fc, &fcoc);
	if (slen)
		*slen = (ssize_t)len;
	return (fcr.status == fcr_ok ? fcr.r.ptr : NULL);
}

/* we do not want to pull in the fco just to get the xid, just return 0 */
static const void * v_matchproto_(objgetattr_f)
sfedsk_getattr(struct worker *wrk, struct objcore *dskoc, enum obj_attr attr,
   ssize_t *slen)
{
	static const struct vxids zero = {0};

	if (attr != OA_VXID)
		return (sfedsk_getattr_tomem(wrk, dskoc, attr, slen));

	AN(wrk);
	CHECK_OBJ_NOTNULL(dskoc, OBJCORE_MAGIC);
	if (slen)
		*slen = sizeof zero;
	return (&zero);
}

static void * v_matchproto_(objsetattr_f)
sfemem_setattr(struct worker *wrk, struct objcore *memoc, enum obj_attr attr,
    ssize_t len, const void *ptr)
{
	const struct stevedore *stv = oc_stv(wrk, memoc);
	const struct stvfe *stvfe = stv_stvfe(stv);

	assert_memstv(stvfe, stv);
	assert(len >= 0);

#ifdef VC_4013_WORKAROUND
	if (attr == OA_LEN) {
		VSLb(wrk->vsl, SLT_Debug, "OA_LEN trim oc %p", memoc);
		fellow_busy_obj_trimstore(sfe_fbo(memoc));
	}
#endif

	return (fellow_busy_setattr(sfe_fbo(memoc), attr, (size_t)len, ptr));
}

static void v_matchproto_(objtouch_f)
sfemem_touch(struct worker *wrk, struct objcore *memoc, vtim_real now)
{
	const struct stevedore *stv = oc_stv(wrk, memoc);
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fellow_cache_obj *fco = stvfe_memoc_fco(stv, stvfe, memoc);

	assert_memstv(stvfe, stv);

	if (/*lint --e(641)*/ memoc->flags & OC_F_PRIVATE ||
	    isnan(memoc->last_lru))
		return;
	if (now - memoc->last_lru < cache_param->lru_interval)
		return;

	/* we lock at the fellow_cache layer, so this is racy with respect to
	 * last_lru. Does it matter?
	 */

	(void) fellow_cache_obj_lru_touch(fco);
	LRU_Touch(wrk, memoc, now);
}

static const struct obj_methods sfemem_methods = {
	.objfree	= sfemem_objfree,
	.objiterator	= sfemem_iterator,
	.objgetspace	= sfemem_getspace,
	.objextend	= sfemem_extend,
	.objgetattr	= sfemem_getattr,
	.objsetattr	= sfemem_setattr,
	// optional
#ifndef VC_4013_WORKAROUND
	.objtrimstore	= sfemem_trimstore,
#else
	.objtrimstore	= NULL,
#endif
	.objbocdone	= sfemem_bocdone,
	.objslim	= sfemem_objslim,
	.objtouch	= sfemem_touch,
	.objsetstate	= NULL
};

static const struct obj_methods sfedsk_methods = {
	.objfree	= sfedsk_objfree,
	.objiterator	= sfedsk_iterator,
// dsk object never has a busy objcore
	.objgetspace	= NULL,
	.objextend	= NULL,
	.objgetattr	= sfedsk_getattr,
	.objsetattr	= NULL,
	// optional
	.objtrimstore	= NULL,
	.objbocdone	= NULL,
	.objslim	= sfedsk_objslim,
	.objtouch	= NULL,
	.objsetstate	= NULL
};

/*lint -e{785}*/
static const struct stevedore sfedsk_stevedore = {
	.magic		=	STEVEDORE_MAGIC,
	.name		=	"fellow disk (internal)",
	.methods	=	&sfedsk_methods,
	// we never hand out internal stevedores to VCL
	.var_free_space =	NULL,
	.var_used_space =	NULL,
	.var_happy	=	NULL,
};

/* ----------------------------------------------------------------------
 * exp stevedore
 *
 * for expiry and draining, we do all the work in the slim method,
 * but lru and exp still want to access the XID. So we put that into
 * the stobj
 */

static void v_matchproto_(objfree_f)
sfeexp_objfree(struct worker *wrk, struct objcore *oc);
static const void * v_matchproto_(objgetattr_f)
sfeexp_getattr(struct worker *wrk, struct objcore *oc, enum obj_attr attr,
    ssize_t *slen);

static const struct obj_methods sfeexp_methods = {
	.objfree	= sfeexp_objfree,
	.objiterator	= NULL,
	.objgetspace	= NULL,
	.objextend	= NULL,
	.objgetattr	= sfeexp_getattr,
	.objsetattr	= NULL,
	// optional
	.objtrimstore	= NULL,
	.objbocdone	= NULL,
	.objslim	= NULL,
	.objtouch	= NULL,
	.objsetstate	= NULL
};

/*lint -e{785}*/
static const struct stevedore sfeexp_stevedore = {
	.magic		=	STEVEDORE_MAGIC,
	.name		=	"fellow expiry (internal)",
	.methods	=	&sfeexp_methods,
	// we never hand out internal stevedores to VCL
	.var_free_space =	NULL,
	.var_used_space =	NULL,
	.var_happy	=	NULL,
};

static void v_matchproto_(objfree_f)
sfeexp_objfree(struct worker *wrk, struct objcore *oc)
{

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	assert(oc->stobj->stevedore == &sfeexp_stevedore);
	memset(oc->stobj, 0, sizeof oc->stobj);
	wrk->stats->n_object--;
}

#define OA_VXID_LEN 8

static const void * v_matchproto_(objgetattr_f)
sfeexp_getattr(struct worker *wrk, struct objcore *oc, enum obj_attr attr,
   ssize_t *slen)
{

	AN(wrk);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	assert(oc->stobj->stevedore == &sfeexp_stevedore);
	assert(attr == OA_VXID);
	if (slen)
		*slen = OA_VXID_LEN;
	return (&oc->stobj->priv2);
}

static void
mutate_to_sfeexp(struct objcore *oc, vxid_t xid)
{

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	memset(oc->stobj, 0, sizeof oc->stobj);
	oc->stobj->stevedore = &sfeexp_stevedore;
	//lint -e{506}
	assert(sizeof xid == OA_VXID_LEN);
	memcpy(&oc->stobj->priv2, &xid, (size_t)OA_VXID_LEN);
}

/* ----------------------------------------------------------------------
 * stevedore
 */

// XXX compat with mgmt use
#ifndef ARGV_ERR
#include <stdio.h>
#define ARGV_ERR(...)							\
	do {								\
		fprintf(stderr, "Error: " __VA_ARGS__);			\
		return;						\
	} while(0)
#endif

static void *
sfe_mapper(size_t *sz, void *priv)
{
	struct stevedore *stv;

	CAST_OBJ_NOTNULL(stv, priv, STEVEDORE_MAGIC);

	fprintf(stderr, "fellow: metadata (bitmap) memory: %zu bytes\n",
	    *sz);
	return (VSMW_Allocf(heritage.proc_vsmw, NULL,
		VSM_CLASS_SLASH, *sz, "fellow.%s", stv->vclname));
}

static void
sfe_umapper(void **ptr, size_t sz, void **priv)
{
	(void) sz;
	(void) priv;

	VSMW_Free(heritage.proc_vsmw, ptr);
}

static void
sfe_diag(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	VSLv(SLT_Storage, NO_VXID, fmt, ap);
	va_end(ap);
}

#define SFE_DIAG(name, fmt, ...) \
	sfe_diag("fellow %s: " fmt, name, __VA_ARGS__)

static int
sfe_taskrun(fellow_task_func_t func, void *priv, fellow_task_privstate *state)
{
	struct pool_task *task;

	AN(func);
	AN(state);

	//lint -e{506}
	assert(sizeof *state >= sizeof *task);
	task = TRUST_ME(state);
	task->func = func;
	task->priv = priv;
	AZ(Pool_Task_Any(task, TASK_QUEUE_BO));
	return (0);
}

static const char *
sfe_tryopen(const char *path)
{
	struct stat st;
	int fd;

	if (stat(path, &st) != 0 &&
	    ! fellow_sane_file_path(path)) {
		STVERR("%s does not exist (stat failed),"
		    " but the path suggests that it should be an "
		    "existing block device. Do you have a typo?\n", path);
	}

	fd = open(path, O_RDWR | O_CREAT, 0700);
	if (fd < 0)
		STVERR("open(%s) failed: %s (%d)",
		    path, strerror(errno), errno);
	if (flock(fd, LOCK_EX | LOCK_NB)) {
		assert(errno == EWOULDBLOCK);
		STVERR("flock(%s) failed: %s (%d)"
		    " - a fellow file can only be used once",
		    path, strerror(errno), errno);
	}
	AZ(flock(fd, LOCK_UN));
	AZ(close(fd));
	return (NULL);
}

static const char *
sfe_init(struct stevedore *memstv, enum stvfe_scope scope,
    const char *filename, size_t dsksz, size_t memsz, size_t objsize_hint,
    const struct stvfe *inherit)
{
	struct stevedore *dskstv;
	struct stvfe *stvfe;
	struct vsb *vsb;
	struct stvfe_tune tune;
	size_t min;
	const char *err;

	AN(memstv);
	assert(scope > STVFE_INVAL);
	AN(filename);
	AN(dsksz);
	if (memsz == 0)
		memsz = 1;
	AN(objsize_hint);

	err = sfe_tryopen(filename);
	if (err != NULL)
		return (err);

	min = fellow_minsize();
	if (dsksz < min) {
		fprintf(stderr, "fellow: Need at least %zu disk bytes, "
		    "raising", min);
		dsksz = min;
	}

	min = MEMBUDDY_MINSIZE(dsksz, objsize_hint);
	if (memsz < min) {
		fprintf(stderr,
		    "fellow: Need at least %zu bytes of memory for\n"
		    "fellow: %zu objects * %zu bytes = %zu bytes on disk,"
		    " raising\n",
		    min, (dsksz / objsize_hint), objsize_hint, dsksz);
		memsz = min;
	}

	err = stvfe_tune_init(&tune, memsz, dsksz, objsize_hint);
	if (err != NULL)
		STVERR("fellow tune error: %s", err);

	ALLOC_OBJ(stvfe, STVFE_MAGIC);
	XXXAN(stvfe);

	ALLOC_OBJ(dskstv, STEVEDORE_MAGIC);
	XXXAN(dskstv);

	stvfe->wait = stvfe_wait_new(tune.wait_table_exponent);
	XXXAN(stvfe->wait);

	stvfe->scope = scope;
	stvfe->tune = tune;
	stvfe->path = filename;
	stvfe->inherit = inherit;

	memcpy(dskstv, &sfedsk_stevedore, sizeof *dskstv);
	dskstv->priv = stvfe;
	vsb = VSB_new_auto();
	AN(vsb);
	(void) VSB_printf(vsb, "%s.dsk", memstv->ident);
	AZ(VSB_finish(vsb));
	dskstv->ident = strdup(VSB_data(vsb));
	// XXX vclname is storage.%s for module but just ident for vmod
	dskstv->vclname = dskstv->ident;
	VSB_destroy(&vsb);

	memstv->priv = stvfe;

	stvfe->memstv = memstv;
	stvfe->dskstv = dskstv;

	if (stvfe->scope == STVFE_GLOBAL)
		VTAILQ_INSERT_TAIL(&stvfe_head, stvfe, list);

	return (NULL);
}

/* get a copy of the current tuning settings */
void
sfe_tune_get(const struct stevedore *stv, struct stvfe_tune *tune)
{
	const struct stvfe *stvfe = stv_stvfe(stv);

	AN(tune);
	*tune = stvfe->tune;
}

/* apply tuning settings and return NULL or
 * return error string if validation failed
 */
const char *
sfe_tune_apply(const struct stevedore *stv, const struct stvfe_tune *tuna)
{
	struct stvfe *stvfe;
	struct stvfe_tune tune;
	const char *err;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	tune = *tuna;
	err = stvfe_tune_check(&tune);
	if (err != NULL)
		return (err);
	stvfe->tune = tune;
	fellow_log_discardctl(stvfe->ffd, tune.discard_immediate);
	stvfe_wait_tune(&stvfe->wait, tune.wait_table_exponent);
	return (NULL);
}

#define fellow_szarg(str, name)						\
	do {								\
	err = VNUM_2bytes((str), &name, (uintmax_t)0);			\
	if (err != NULL)						\
		ARGV_ERR("(-sfellow) %s \"%s\": %s\n", #name, (str), err); \
} while(0)

static void
sfe_cfg(struct stevedore *parent, int ac, char * const *av)
{
	size_t l, dsksz = 0, memsz = 0, objsize_hint = 0;
	const char *err, *inherit;
	struct stvfe *stvfe = NULL;

	if (ac != 4 || *av[0] == '\0' || *av[1] == '\0' || *av[2] == '\0' ||
	    *av[3] == '\0')
		ARGV_ERR("(-sfellow) need 4 arguments: "
		    "path,dsksz,memsz[=storage],objsize_hint\n");
	fellow_szarg(av[1], dsksz);

	inherit = strchr(av[2], '=');
	if (inherit == NULL)
		fellow_szarg(av[2], memsz);
	else {
		char buf[64];

		assert(inherit >= av[2]);
		l = (size_t)(inherit - av[2]);
		assert(l < sizeof buf);
		if (l > 0)
			(void) strncpy(buf, av[2], l);
		buf[l] = '\0';
		fellow_szarg(buf, memsz);

		inherit++;
		stvfe = stvfe_lookup(inherit);
		if (stvfe == NULL)
			ARGV_ERR("(-sfellow) shared storage "
			    "\"%s\" not found", inherit);
		stvfe->ref++;
	}
	fellow_szarg(av[3], objsize_hint);

	err = sfe_init(parent, STVFE_GLOBAL, av[0], dsksz, memsz,
	    objsize_hint, stvfe);
	if (err)
		ARGV_ERR("(-sfellow) %s\n", err);
}

struct sfedsk_reserve {
	struct buddy_off_page	*arr;
	// init, sizing
	buddy_t		*buddy;
	struct buddy_reqs	*reqs;
	unsigned		nr;
	unsigned		bits;
	unsigned		filled;
};

static inline void
sfedsk_reserve_init(struct sfedsk_reserve *r, buddy_t *buddy,
    struct buddy_reqs *reqs)
{
	memset(r, 0, sizeof *r);
	r->buddy = buddy;
	r->reqs = reqs;
}

/*
 * simply retrurn all of the reserve.
 *
 * we _could_ return it only partially when bits do not change but the number of
 * pages is reduced
 */

static inline void
sfedsk_reserve_free(struct sfedsk_reserve *r)
{
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(r->buddy, BUDDY_RETURNS_MAX);
	unsigned u;

	if (r->filled == 0)
		return;

	for (u = 0; u < r->filled; u++)
		AN(buddy_return_off_page(rets, &r->arr[u]));
	buddy_return(rets);
	r->filled = 0;
}

static inline void
sfedsk_reserve_resize(struct sfedsk_reserve *r, unsigned n, unsigned bits)
{
	size_t sz;

	if (r->nr == 0)
		AZ(r->arr);
	else
		AN(r->arr);

	if (r->nr == n && r->bits == bits)
		return;

	sfedsk_reserve_free(r);

	if (n == 0) {
		r->nr = 0;
		free(r->arr);
		r->arr = NULL;
		return;
	}

	sz = n * sizeof *r->arr;
	r->arr = realloc(r->arr, sz);
	XXXAN(r->arr);
	memset(r->arr, 0, sz);

	r->bits = bits;
	r->nr = n;
}

static inline void
sfedsk_reserve_release(struct sfedsk_reserve *r)
{
	unsigned u;

	if (r->filled == 0)
		return;

	u = --r->filled;
	buddy_return1_off_page(r->buddy, &r->arr[u]);
}

static unsigned
sfedsk_nuke(struct worker *wrk, struct fellow_fd *ffd,
    const struct stevedore *stv, unsigned n)
{

	n *= FELLOW_DISK_LOG_BLOCK_ENTRIES;
	wrk->strangelove = (int)STVELOVE_NUKE;
	while (n--) {
		if (LRU_NukeOne(wrk, stv->lru))
			continue;
		wrk->strangelove++;
		break;
	}
	fellow_logwatcher_kick_locked(ffd);

	return ((unsigned)((int)STVELOVE_NUKE - wrk->strangelove));
}

static void
sfedsk_reserve_req(struct sfedsk_reserve *r)
{
	unsigned n = r->nr;

	if (n > BUDDY_REQS_MAX)
		n = BUDDY_REQS_MAX;
	while (n--)
		AN(buddy_req_page(r->reqs, r->bits, 0));
	(void) buddy_alloc_async(r->reqs);
}

static void
sfedsk_reserve_fill(struct sfedsk_reserve *r)
{
	struct buddy_off_page *arr;
	uint8_t u;
	unsigned n = r->nr - r->filled;

	u = buddy_alloc_async_ready(r->reqs);
	if (u < n)
		n = u;

	arr = r->arr + r->filled;

	for (u = 0; u < n; u++) {
		AZ(arr->bits);
		*arr++ = buddy_get_off_page(r->reqs, u);
	}
	r->filled += n;

	buddy_alloc_async_done(r->reqs);
}

#define LOG(wrk, ...)			\
	VSLb((wrk)->vsl, SLT_ExpKill, "LRU fellow disk " __VA_ARGS__)

static void *
sfedsk_lru_thread(struct worker *wrk, void *arg)
{
	const struct stevedore *stv;
	struct vsl_log vsl;

	struct stvfe *stvfe;
	buddy_t *buddy;

	struct sfedsk_reserve r[1];
	unsigned nuked, n;

	struct buddy_reqs *reqs;

	CAST_OBJ_NOTNULL(stv, arg, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);
	buddy = fellow_dskbuddy(stvfe->ffd);
	CHECK_OBJ(buddy, BUDDY_MAGIC);

	reqs = BUDDY_REQS_STK(buddy, BUDDY_REQS_MAX);
	BUDDY_REQS_PRI(reqs, FEP_RESERVE);
	sfedsk_reserve_init(r, buddy, reqs);

	AZ(wrk->vsl);
	wrk->vsl = &vsl;
	VSL_Setup(wrk->vsl, NULL, (size_t)0);

	buddy_wait_needspace(buddy);

	while (!stvfe->shutdown) {
		sfedsk_reserve_resize(r,
		    stvfe->tune.dsk_reserve_chunks,
		    stvfe->tune.chunk_exponent);

		if (r->filled < r->nr) {
			LOG(wrk, "reserve need %u * %s",
			    r->nr - r->filled,
			    pow2_units[stvfe->tune.chunk_exponent]);
		} else {
			LOG(wrk, "lru wakeup reserve full");
		}

		nuked = 0;
		n = 0;

		sfedsk_reserve_release(r);
		nuked += sfedsk_nuke(wrk, stvfe->ffd, stv, ++n);

		sfedsk_reserve_req(r);
		while (buddy->waiting) {
			sfedsk_reserve_release(r);
			nuked += sfedsk_nuke(wrk, stvfe->ffd, stv, ++n);
		}
		sfedsk_reserve_fill(r);

		if (nuked != UINT_MAX)
			LOG(wrk, "%u rounds nuked %u", n, nuked);

		if (r->filled < r->nr && nuked == 0) {
			LOG(wrk, "reserve fail");
			VSL_Flush(wrk->vsl, 0);
			buddy_wait_fail(buddy);
		}

		Pool_Sumstat(wrk);
		VSL_Flush(wrk->vsl, 0);

		buddy_wait_needspace(buddy);
	}
	sfedsk_reserve_free(r);
	return (NULL);
}

/*
 * remove all varnish objects, but keep disk objects
 */

static void
sfedsk_drain(struct worker *wrk, const struct stevedore *stv)
{
	uintmax_t n;
	int r = 0;

	do {
		n = 0;
		wrk->strangelove = (int)STVELOVE_SIGNAL_DRAIN;
		while (LRU_NukeOne(wrk, stv->lru)) {
			n++;
			wrk->strangelove = (int)STVELOVE_SIGNAL_DRAIN;
		}
		VSLb(wrk->vsl, SLT_Debug, "check %d drain %lu", r, n);
		if (n == 0)
			(void) usleep(100 * 1000);
	} while (r++ < 10);
	VSLb(wrk->vsl, SLT_Debug, "drain done");
}

void
stvfe_sumstat(struct worker *wrk)
{
	Pool_Sumstat(wrk);
}

/*
 * Like HSH_Snipe, but turns the oc into a dskoc under the objhead lock to avoid
 * races.
 *
 * right after we release the oh mtx, another request could come in to stage
 * the object into ram again. So we need to ensure that before we unlock,
 * the object is removed from the fdb,
 * which fellow_cache_obj_evict_mutate does.
 *
 * Returns if successful
 *
 * fco->mtx held!
 */
int
stvfe_mutate(struct worker *wrk, struct fellow_cache_lru *lru,
    struct objcore *oc)
{
	const struct stevedore *stv;
	const struct stvfe *stvfe;
	struct objhead *oh;
	int retval = 0;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	oh = oc->objhead;
	CHECK_OBJ_NOTNULL(oh, OBJHEAD_MAGIC);

	if (oc->refcnt == 1 &&
	    !(oc->flags & OC_F_DYING) &&
	    !Lck_Trylock(&oh->mtx)) {
		if (oc->refcnt != 1 || (oc->flags & OC_F_DYING))
			goto unlock;

		stv = oc_stv(wrk, oc);
		stvfe = stv_stvfe(stv);

		AZ(retval);
		if (stv != stvfe->memstv)
			goto unlock;

		assert_memstv(stvfe, stv);
		AN(oc->stobj->priv);
		AN(oc->stobj->priv2);

		fellow_cache_obj_evict_mutate(lru, oc->stobj->priv);

		oc->stobj->priv = NULL;
		oc->stobj->stevedore = stvfe->dskstv;
		retval = 1;
		stvfe->stats->c_mem_obj_mutate++;
		wrk->stats->n_vampireobject++;
		wrk->stats->n_object--;

	  unlock:
		Lck_Unlock(&oh->mtx);
	}

	return (retval);
}

struct sfe_ban_t {
	vtim_real earliest, latest;
};

#include "fellow_storage_deref.h"

/*
 * notes on time keeping during resurrection:
 *
 * For our statistics (t1, td), it would be correct to use vtim_mono, but
 * because varnish-cache uses vtim_real for object expiry, we need vtim_real
 * anyway and can save some clock queries.
 *
 * For expiry check, we offset the actual time by a margin dt_exp (consider it a
 * reasonable minimum cache load time) to not spend^Wwaste time on objects which
 * are just about to expire
 */

struct sfe_resurrect_priv {
	unsigned			magic;
#define SFE_RESURRECT_PRIV_MAGIC	0x6a3591fd
	unsigned			has_bans:1;
	/* our current time, updated every dn_t_now_upd objcts */
	vtim_real			t_now;
	/* offset to t_now for expiry check */
	vtim_dur			dt_exp;
	/* when we last reported progress */
	vtim_real			t_reported;
	/* time of first resurrect call */
	vtim_real			t1;
	struct worker			*wrk;
	struct stvfe			*stvfe;
	struct ban			*dummy_ban;
	struct festash_top		fet[1];
	unsigned			n_exp;
	unsigned			n_resurrected;
	unsigned			n_t_now_upd;
	unsigned			dn_t_now_upd;
	struct sfe_ban_t		ban_exp_t;

	struct sfe_deref		deref;
};

/*
 * assertions and check object expiry
 */
static inline int
sfe_check(struct sfe_resurrect_priv *sfer, const struct fellow_dle *e)
{
	CHECK_OBJ_NOTNULL(sfer, SFE_RESURRECT_PRIV_MAGIC);
	CHECK_OBJ_NOTNULL(e, FELLOW_DLE_MAGIC);
	assert(e->version == 1);
	assert(e->type == DLEDSK(DLE_OBJ_ADD) ||
	    e->type == DLEDSK(DLE_OBJ_CHG));

	if (sfer->t1 == 0.0)
		sfer->t1 = sfer->t_now;

	if (EXP_WHEN(&e->u.obj) < sfer->t_now + sfer->dt_exp) {
		sfer->n_exp++;
		return (0);
	}
	return (1);
}

/*
 * maintain stats for kept objects
 */
static inline int
sfe_keep(struct sfe_resurrect_priv *sfer)
{
	sfer->n_resurrected++;
	if (sfer->n_resurrected % 100 == 0 &&
	    sfer->t_now > sfer->t_reported + 1) {
		sfer->t_reported = sfer->t_now;
		SFE_DIAG(sfer->stvfe->memstv->ident,
		    "resurrected %u ", sfer->n_resurrected);
		Pool_Sumstat(sfer->wrk);
		fellow_fd_update_space_stats(sfer->stvfe->ffd);
	}
	return (1);
}

/*
 * delete everything
 *
 * used for STVFE_VCL_DISCARD
 */
static int
sfe_discard(void *priv, const struct fellow_dle *e)
{
	struct sfe_resurrect_priv *sfer;

	CAST_OBJ_NOTNULL(sfer, priv, SFE_RESURRECT_PRIV_MAGIC);
	(void) e;

	if (sfer->t1 == 0.0)
		sfer->t1 = sfer->t_now;

	sfer->n_exp++;
	return (0);
}

/*
 * just check if the object is still alive, and, if so, let the fellow keep it.
 *
 * used for STVFE_VCL_EMPTY
 */
static int
sfe_alive(void *priv, const struct fellow_dle *e)
{
	struct sfe_resurrect_priv *sfer;

	CAST_OBJ_NOTNULL(sfer, priv, SFE_RESURRECT_PRIV_MAGIC);

	return (sfe_check(sfer, e));
}

static struct sfe_ban_t
sfe_ban_reg(struct sfe_resurrect_priv *sfer, const struct fellow_dle *e);
static struct sfe_ban_t
sfe_ban_imm(struct sfe_resurrect_priv *sfer, const struct fellow_dle *e);

static inline struct objcore *
sfe_objnew(struct sfe_resurrect_priv *sfer, struct worker *wrk,
    const struct fellow_dle *e, struct ban *ban)
{
	struct objcore *oc;

	oc = ObjNew(wrk);
	AN(oc);
	oc->stobj->stevedore = sfer->stvfe->dskstv;
	AZ(oc->stobj->priv);
	oc->stobj->priv2 = e->u.obj.start.fdb;
	stvfe_oc_log_submitted(oc);
	EXP_COPY(oc, &e->u.obj);
	// #17 clear sign bit of t_origin, see sfe_resurrect()
	oc->t_origin = copysign(oc->t_origin, 1.0);
	oc->refcnt++;
	HSH_Insert(wrk, e->u.obj.hash, oc, ban);
	AN(oc->ban);
	return (oc);
}

static int
sfe_resurrect_ban(struct sfe_resurrect_priv *sfer, const struct fellow_dle *e)
{
	struct sfe_ban_t ban_t;

	assert(DLE_TYPE(e->type) == DLE_T_BAN_REG ||
	       DLE_TYPE(e->type) == DLE_T_BAN_IMM);

	sfer->has_bans = 1;
	switch (DLE_TYPE(e->type)) {
	case DLE_T_BAN_REG:
		ban_t = sfe_ban_reg(sfer, e);
		break;
	case DLE_T_BAN_IMM:
		ban_t = sfe_ban_imm(sfer, e);
		break;
	default:
		WRONG("dle type for ban resurrect");
	}
	AN(ban_t.earliest);
	AN(ban_t.latest);
	switch (DLE_OP(e->type)) {
	case DLE_OP_ADD:
		// single ban
		festash_work_one(sfer->fet, ban_t.latest);
		break;
	case DLE_OP_CHG:
		// ban export
		AZ(sfer->ban_exp_t.earliest);
		AZ(sfer->ban_exp_t.latest);
		sfer->ban_exp_t = ban_t;
		festash_top_work(sfer->fet, sfer->has_bans);
		break;
	default:
		WRONG("dle op for ban resurrect");
	}
	//fprintf(stdout, "loaded ban %f\n", ban_t);
	return (0);
}

/*
 * create vampire objects for objects in storage
 *
 * used for STVFE_GLOBAL
 */
static int
sfe_resurrect(void *priv, const struct fellow_dle *e)
{
	struct sfe_resurrect_priv *sfer;
	struct objcore *oc;
	struct ban *ban;
	vtim_real t, ban_t;
	unsigned n;

	CAST_OBJ_NOTNULL(sfer, priv, SFE_RESURRECT_PRIV_MAGIC);

	n = sfer->n_exp + sfer->n_resurrected;
	if (n >= sfer->n_t_now_upd || sfer->t_now == 0.0) {
		sfer->t_now = VTIM_real();
		sfer->n_t_now_upd = n + sfer->dn_t_now_upd;
	}

	if (DLE_TYPE(e->type) == DLE_T_BAN_REG ||
	    DLE_TYPE(e->type) == DLE_T_BAN_IMM)
		return (sfe_resurrect_ban(sfer, e));

	if (! sfe_check(sfer, e))
		return (0);

	/*
	 * #17: clear sign bit of ban and t_origin to _possibly_ use these in
	 * the future and retain backwards compatibility from this point onwards
	 */
	ban_t = copysign(e->u.obj.ban, 1.0);

	/*
	 * note on earliest ban export time: There are plenty of oppurtunities
	 * in varnish-cache for an export to happen before all objects have been
	 * notified of a ban change - in particular a BANCHG happens after the
	 * ban change has been completes.
	 *
	 * So if we have an earliest ban export time and the object is from
	 * before, we can be certain that it has been tested to at least the
	 * earliest ban we know.
	 *
	 * Unless, of course, we have a bug such that objects deleted by bans
	 * are not logged and wrongly get revived through this mechanism.
	 */

	t = ban_t;
	if (t < sfer->ban_exp_t.earliest)
		t = sfer->ban_exp_t.earliest;

	ban = BAN_FindBan(t);
	if (ban == NULL)
		ban = sfer->dummy_ban;
	AN(ban);

	sfer->wrk->stats->n_vampireobject++;

	oc = sfe_objnew(sfer, sfer->wrk, e, ban);

	if (ban == sfer->dummy_ban)
		festash_insert(sfer->fet, oc, ban_t);
	else
		sfe_deref_post(&sfer->deref, oc);

	return (sfe_keep(sfer));
}

#ifdef DEBUG
extern size_t membuddy_low;
#endif

static void v_matchproto_(task_func_t)
sfe_open_task(struct worker *wrk, void *priv)
{
	struct sfe_resurrect_priv *sfer;
	fellow_resurrect_f *func;
	const char *name;
	struct stvfe *stvfe;
	vtim_real t, t0;

	CAST_OBJ_NOTNULL(sfer, priv, SFE_RESURRECT_PRIV_MAGIC);
	sfer->wrk = wrk;

	stvfe = sfer->stvfe;
	CHECK_OBJ_NOTNULL(stvfe, STVFE_MAGIC);
	CHECK_OBJ_NOTNULL(stvfe->memstv, STEVEDORE_MAGIC);
	name = stvfe->memstv->ident;

	switch (stvfe->scope) {
	case STVFE_GLOBAL:
		BAN_Hold();
		func = sfe_resurrect;
		break;
	case STVFE_VCL_DISCARD:
		func = sfe_discard;
		break;
	case STVFE_VCL_EMPTY:
		func = sfe_alive;
		break;
	default:
		WRONG("stvfe->scope");
	}

	t0 = VTIM_real();
	SFE_DIAG(name, "%s", "loading...");
	fellow_log_set_diag(stvfe->ffd, sfe_diag);

	sfe_deref_init(&sfer->deref);

	fellow_log_open(stvfe->ffd, func, sfer);

	sfe_deref_fini(&sfer->deref);

	festash_top_work(sfer->fet, sfer->has_bans);
	t = VTIM_real();
	if (sfer->t1 > 0.0)
		SFE_DIAG(name, "system init until cache load "
		    "t1 = %f", sfer->t1 - t0);
	else
		sfer->t1 = t0;
	SFE_DIAG(name, "%u resurrected in %fs (%f/s), "
	    "%u already expired",
	    sfer->n_resurrected, (t - sfer->t1),
	    (double)(sfer->n_resurrected + sfer->n_exp) / (t - sfer->t1),
	    sfer->n_exp);
#ifdef DEBUG
	size_t sz;
	sz = buddy_size(stvfe->membuddy);
	SFE_DIAG(name, "membuddy usage during load %zu/%zu = %f%%",
	    sz - membuddy_low, sz,
	    (((double)sz - (double)membuddy_low) / (double)sz) * 100);
	membuddy_low = 0;
#endif

	if (stvfe->scope == STVFE_GLOBAL)
		BAN_Release();
}

static struct ban *
dummy_ban(void)
{
	struct ban *ban;
	struct objcore oc[1];

	INIT_OBJ(oc, OBJCORE_MAGIC);
	oc->objhead = (void *)1;	// hack for BAN_NewObjCore()
	BAN_NewObjCore(oc);
	ban = oc->ban;
	AN(ban);
	BAN_DestroyObj(oc);
	return (ban);
}

enum sfe_open_worker_state {
	SOW_INIT = 0,
	SOW_STARTING,
	SOW_RUNNING,
	SOW_STOPPING,
	SOW_DONE
};

struct sfe_open_worker_priv {
	unsigned			magic;
#define SFE_OPEN_WORKER_PRIV		0xe4711115
	enum sfe_open_worker_state	state;

	pthread_mutex_t			mtx;
	pthread_cond_t			cond;
	struct worker			*wrk;

	pthread_t			thr;
};

static void *
sfe_open_worker_thread(struct worker *wrk, void *priv)
{
	struct sfe_open_worker_priv *sow;

	CAST_OBJ_NOTNULL(sow, priv, SFE_OPEN_WORKER_PRIV);
	AZ(pthread_mutex_lock(&sow->mtx));
	AZ(sow->wrk);
	sow->wrk = wrk;
	assert(sow->state == SOW_STARTING);
	sow->state = SOW_RUNNING;
	AZ(pthread_cond_signal(&sow->cond));
	while (sow->state < SOW_STOPPING)
		AZ(pthread_cond_wait(&sow->cond, &sow->mtx));
	AZ(sow->wrk);
	sow->state = SOW_DONE;
	AZ(pthread_mutex_unlock(&sow->mtx));
	return (NULL);
}

static struct worker *
sfe_open_worker_init(struct sfe_open_worker_priv *sow)
{

	INIT_OBJ(sow, SFE_OPEN_WORKER_PRIV);
	AZ(pthread_mutex_init(&sow->mtx, NULL));
	AZ(pthread_cond_init(&sow->cond, NULL));

	AZ(pthread_mutex_lock(&sow->mtx));
	assert(sow->state == SOW_INIT);
	sow->state = SOW_STARTING;
	WRK_BgThread(&sow->thr, "sfe-open-worker",
	    sfe_open_worker_thread, sow);
	while (sow->state < SOW_RUNNING)
		AZ(pthread_cond_wait(&sow->cond, &sow->mtx));
	AN(sow->wrk);
	//lint -e{454} mutex
	return (sow->wrk);
}

static void
sfe_open_worker_fini(struct sfe_open_worker_priv *sow)
{
	void *r;

	CHECK_OBJ_NOTNULL(sow, SFE_OPEN_WORKER_PRIV);

	sow->wrk = NULL;

	assert(sow->state == SOW_RUNNING);
	sow->state = SOW_STOPPING;

	AZ(pthread_cond_signal(&sow->cond));
	//lint -e{455} mutex
	AZ(pthread_mutex_unlock(&sow->mtx));

	AZ(pthread_join(sow->thr, &r));
	AZ(r);

	FINI_OBJ(sow);
}

/*
 * notes on OEV_EXPIRE:
 *
 * right after expiring, the object may stop referencing its ban, and we hit
 * objects with no ban.  log a CHG with zero ttl so the object will not be be
 * resurrected
 *
 * XXX UPDATE / WITH_OEV_EXPIRES
 *
 * this is racy. We have recently started to accept objects with too old a ban,
 * because not even varnish-cache guarantees proper ordering, and it seems we
 * should just avoid it...
 *
 * the issue with OEV_EXPIRES is:
 *
 * - to make sure we never write the CHG before the ADD, we need to inspect the
 *   fco log_written flag
 * - but during deletion we can't, because we race for it with deletion.
 *
 * this is this previous comment:
 *
 * we also see this event on a memstv object with priv == NULL, presumably from
 * a concurrent eviction.
 */

static void v_matchproto_(obj_event_f)
sfe_oc_event(struct worker *wrk, void *priv, struct objcore *oc, unsigned ev)
{
	const struct stevedore *stv;
	struct stvfe *stvfe;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	stv = oc->stobj->stevedore;
	CAST_OBJ_NOTNULL(stvfe, priv, STVFE_MAGIC);

	if (stv != stvfe->memstv && stv != stvfe->dskstv)
		return;

	AZ(/*lint --e(641)*/oc->flags & OC_F_PRIVATE);

	AZ(stvfe->shutdown);
	AN(oc->stobj->priv2);

	switch (ev) {
	case OEV_BANCHG:
	case OEV_TTLCHG:
		break;
#ifdef WITH_OEV_EXPIRE
	case OEV_EXPIRE:
		EXP_ZERO(oc);
		break;
#endif
	default:
		WRONG("oev");
	}
	if (stvfe_oc_inlog(oc))
		stvfe_oc_dle_submit(stvfe->ffd, oc, DLE_OBJ_CHG);
}

static void
sfe_register_events(void *priv)
{
	struct stvfe *stvfe;

	CAST_OBJ_NOTNULL(stvfe, priv, STVFE_MAGIC);
#ifdef WITH_OEV_EXPIRE
	stvfe->oev = ObjSubscribeEvents(sfe_oc_event, stvfe,
	    OEV_BANCHG|OEV_TTLCHG|OEV_EXPIRE);
#else
	stvfe->oev = ObjSubscribeEvents(sfe_oc_event, stvfe,
	    OEV_BANCHG|OEV_TTLCHG);
#endif
	AN(stvfe->oev);
}

/*
 * open the storage and return error information or NULL
 * if successful
 */
static const char *
sfe_open_scope(struct stevedore *stv)
{
	buddy_t *buddyp;
	struct stvfe *stvfe;
	struct fellow_fd *ffd;
	struct fellow_cache *fc = NULL;
	struct sfe_resurrect_priv sfer[1];
	struct sfe_open_worker_priv sow[1];
	const char *err = NULL;

	ASSERT_CLI();
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);
	assert(stvfe->scope > STVFE_INVAL);
	AN(stvfe->dskstv);
	AN(stvfe->memstv);

	stvfe->stats = VSC_fellow_New(NULL, &stvfe->vsc_seg,
	    stvfe->memstv->ident);

	if (stvfe->inherit != NULL) {
		CHECK_OBJ(stvfe->inherit, STVFE_MAGIC);
		AN(stvfe->inherit->membuddy);
		stvfe->membuddy = stvfe->inherit->membuddy;
	} else {
		buddy_init(&stvfe->my_membuddy, MIN_BUDDY_BITS,
		    stvfe->tune.memsz, BUDDYF(mmap), NULL,
		    sfe_mapper, stvfe->memstv);
		stvfe->membuddy = &stvfe->my_membuddy;
	}
	ffd = fellow_log_init(stvfe->path,
	    stvfe->tune.dsksz, stvfe->tune.objsize_hint,
	    sfe_mapper, stvfe->dskstv,
	    stvfe->membuddy, &stvfe->tune, sfe_taskrun, stvfe->stats);
	if (ffd == NULL) {
		err = "fellow_log_init() failed, see previous message";
		goto err;
	}

	fc = fellow_cache_init(ffd, stvfe->membuddy, &stvfe->tune,
	    sfe_taskrun, &stvfe->stats->g_mem_obj);
	if (fc == NULL) {
		/* should never happen, unless we turn assertions into
		 * errors */
		err = "fellow_cache_init() failed";
		goto err;
	}

	stvfe->fc = fc;
	stvfe->ffd = ffd;

	stvfe->dskstv->lru = LRU_Alloc();
	stvfe->memstv->lru = stvfe->dskstv->lru;

	INIT_OBJ(sfer, SFE_RESURRECT_PRIV_MAGIC);
	sfer->dt_exp = 60;		// XXX config ?
	sfer->dn_t_now_upd = 1000;	// XXX config ?
	sfer->stvfe = stvfe;
	sfer->dummy_ban = dummy_ban();

	AN(sfe_open_worker_init(sow));
	festash_top_init(sfer->fet, stvfe->membuddy, sfer->dummy_ban, sow->wrk,
	    sfer->t_now, &sfer->ban_exp_t.earliest);

	/*
	 * In the varnish-cache persistent storage, bans are handled separate to
	 * objects and ban reloading happens before object loading.
	 *
	 * Consequently, in varnish-cache, BAN_Reload is limited to the CLI
	 * thread.
	 *
	 * In slash/fellow, however, we combine objects and bans into a single
	 * log and thus need a worker for vampire creation, but also the CLI
	 * thread for ban manipulation.
	 *
	 * Rather than juggling between two threads, we create a pro-forma
	 * bgthread and steal it's worker struct for use in the CLI thread.
	 *
	 * Note: Once we have seen a full ban export, we could load remaining
	 * objects in a bgthread, if we wanted.
	 */

	fellow_log_register_open_cb(ffd, sfe_register_events, stvfe);
	sfe_open_task(sow->wrk, sfer);
	sfe_open_worker_fini(sow);
	festash_top_fini(sfer->fet);

	buddyp = fellow_dskbuddy(stvfe->ffd);
	BUDDY_REQS_INIT(stvfe->ban_reqs, buddyp);


	if (stvfe->scope == STVFE_VCL_EMPTY && sfer->n_resurrected > 0) {
		err = "Storage not empty and delete=false";
		goto err;
	}

	WRK_BgThread(&stvfe->dsk_lru_thread, "sfe-dsk-lru",
	    sfedsk_lru_thread, stvfe->dskstv);

	stvfe->ref++;

	return (err);

  err:
	if (stvfe->dskstv->lru) {
		assert(stvfe->dskstv->lru == stvfe->memstv->lru);
		stvfe->memstv->lru = NULL;
		LRU_Free(&stvfe->dskstv->lru);
		AZ(stvfe->dskstv->lru);
	}

	if (fc != NULL)
		fellow_cache_fini(&fc);
	AZ(fc);

	if (ffd != NULL)
		fellow_log_close(&ffd);
	AZ(ffd);

	if (stvfe->membuddy == &stvfe->my_membuddy)
		buddy_fini(&stvfe->membuddy, BUDDYF(unmap), NULL,
		    sfe_umapper, NULL);
	else
		stvfe->membuddy = NULL;
	AZ(stvfe->membuddy);

	return (err);
}

static void v_matchproto_(storage_open_f)
sfe_open(struct stevedore *stv)
{
	const char *err;

	err = sfe_open_scope(stv);
	if (err == NULL)
		return;
	SFE_DIAG(stv->ident, "Could not open: %s", err);
	fprintf(stderr, "Fatal: fellow %s: Could not open: %s",
	    stv->ident, err);
	exit (4);
}

static void *
sfe_drain_thread(struct worker *wrk, void *arg)
{
	struct stvfe *stvfe;
	struct vsl_log vsl;

	CAST_OBJ_NOTNULL(stvfe, arg, STVFE_MAGIC);

	AZ(wrk->vsl);
	wrk->vsl = &vsl;
	VSL_Setup(wrk->vsl, NULL, (size_t)0);

	sfedsk_drain(wrk, stvfe->dskstv);

	VSL_Flush(wrk->vsl, 0);
	return (NULL);
}

static void
sfe_close_warn(struct stvfe *stvfe)
{
	pthread_t thr;
	void *r;

	if (stvfe->scope == STVFE_GLOBAL)
		VTAILQ_REMOVE(&stvfe_head, stvfe, list);

	if (stvfe->oev)
		ObjUnsubscribeEvents(&stvfe->oev);
	fellow_log_flush(stvfe->ffd);
	stvfe->shutdown = 1;
	buddy_wait_kick(fellow_dskbuddy(stvfe->ffd));
	AZ(pthread_join(stvfe->dsk_lru_thread, &r));
	AZ(r);

	WRK_BgThread(&thr, "sfe-drain", sfe_drain_thread, stvfe);
	AZ(pthread_join(thr, &r));
	AZ(r);
}

static void
sfe_close_deref(struct stvfe *stvfe)
{
	CHECK_OBJ_NOTNULL(stvfe, STVFE_MAGIC);

	AN(stvfe->ref);
	if (--stvfe->ref)
		return;

	if (stvfe->membuddy == &stvfe->my_membuddy)
		buddy_fini(&stvfe->membuddy, BUDDYF(unmap), NULL,
		    sfe_umapper, NULL);
	else
		stvfe->membuddy = NULL;
	AZ(stvfe->membuddy);
}

static void
sfe_close_real(struct stvfe *stvfe)
{
	CHECK_OBJ_NOTNULL(stvfe, STVFE_MAGIC);

	stvfe->memstv->lru = NULL;
	LRU_Free(&stvfe->dskstv->lru);
	AZ(stvfe->dskstv->lru);

	sfe_ban_space_return(stvfe);
	buddy_alloc_async_done(&stvfe->ban_reqs->reqs);

	fellow_cache_fini(&stvfe->fc);
	AZ(stvfe->fc);

	fellow_log_close(&stvfe->ffd);
	AZ(stvfe->ffd);

	VSC_fellow_Destroy(&stvfe->vsc_seg);

	//deliberate un-const
	if (stvfe->inherit != NULL)
		sfe_close_deref((struct stvfe *)stvfe->inherit);
	sfe_close_deref(stvfe);
}

static void v_matchproto_(storage_close_f)
sfe_close(const struct stevedore *stv, int warn)
{
	struct stvfe *stvfe;
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	switch (warn) {
	case 1:
		sfe_close_warn(stvfe);
		break;
	case 0:
		sfe_close_real(stvfe);
		break;
	default:
		WRONG("sfe_close warn value");
	}
}

static int v_matchproto_(storage_allocobj_f)
sfe_allocobj(struct worker *wrk, const struct stevedore *stv,
    struct objcore *oc, unsigned wsl)
{
	const struct stvfe *stvfe = stv_stvfe(stv);
	struct fellow_cache_obj *fco;
	struct fellow_cache_res fcr;
	struct objhead *oh;
	struct boc *boc;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	AN(stv->methods);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	boc = oc->boc;
	CHECK_OBJ_NOTNULL(boc, BOC_MAGIC);
	oh = oc->objhead;
	CHECK_OBJ_NOTNULL(oh, OBJHEAD_MAGIC);

	AZ(oc->stobj->stevedore);
	AZ(oc->stobj->priv);
	AZ(oc->stobj->priv2);

	if (stvfe->shutdown) {
		VSLb(wrk->vsl, SLT_Error, "%s %s: shutting down",
		    stv->name, stv->ident);
		return (0);
	}

	stvfe->stats->c_allocobj++;
	fcr = fellow_busy_obj_alloc(stvfe->fc,
	    &fco, &oc->stobj->priv2,
	    wsl);
	if (fcr.status != fcr_ok) {
		stvfe->stats->c_allocobj_fail++;
		VSLb(wrk->vsl, SLT_Error, "%s %s: %s",
		    stv->name, stv->ident,
		    fcr.r.err ? fcr.r.err : "Unknown error");
		return (0);
	}

	// counter maintained in STV_NewObject()
	//wrk->stats->n_object++;

	oc->stobj->stevedore = stv;
	boc->stevedore_priv = fcr.r.ptr;
	AN(boc->stevedore_priv);
	AN(fco);
	oc->stobj->priv = fco;
	return (1);
}

static void
sfe_ban_space_return(struct stvfe *stvfe)
{
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(stvfe->ban_reqs->reqs.buddy, DLE_BAN_REG_NREGION);
	unsigned u;

	for (u = 0; u < DLE_BAN_REG_NREGION; u++) {
		if (stvfe->ban_space[u].size == 0)
			continue;
		AN(buddy_return_off_extent(rets, &stvfe->ban_space[u]));
	}
	buddy_return(rets);
}

/* stvfe->ban_space_state flag:
 *
 * 0 none / consumed
 * 1 space requested
 * 2 space available
 *
 * banexport flag:
 * 0 not needed
 * 1 needed
 * 2 requested
 *
 * all happening under ban mutex except for the actual export, so no
 * locking
 */

static int
sfe_banexport_space(struct stvfe *stvfe)
{
	size_t n, q, l = VSC_C_main->bans_persisted_bytes -
	    VSC_C_main->bans_persisted_fragmentation;
	uint8_t u, ready;

	if (l <= FELLOW_DLE_BAN_IMM_MAX)
		return (1);

	while (1) {
		switch (stvfe->ban_space_state) {
		case 2:
			/* check if we have sufficient pre-alloc'ed space */
			for (u = 0, q = 0; u < DLE_BAN_REG_NREGION; u++)
				q += stvfe->ban_space[u].size;

			if (q >= l)
				return (1);

			/* not enough space, return any pre-alloced space */
			sfe_ban_space_return(stvfe);
			stvfe->ban_space_state = 0;
			break;
		case 1:
			/* check if an allocation is ready */
			ready = buddy_alloc_async_ready(&stvfe->ban_reqs->reqs);
			if (ready == 0)
				return (0);

			/* XXX: we ignore the case where we only have some of
			 * the request ready
			 */

			assert(ready <= DLE_BAN_REG_NREGION);
			for (u = 0, q = 0; u < ready; u++)
				stvfe->ban_space[u] =
				    buddy_get_off_extent(&stvfe->ban_reqs->reqs, u);
			buddy_alloc_async_done(&stvfe->ban_reqs->reqs);
			stvfe->ban_space_state = 2;
			break;
		case 0:
			/* issue an async allocation for next time +10% */
			if (l < UINT_MAX / 10) {
				l *= 11;
				l /= 10;
			}
			q = l / DLE_BAN_REG_NREGION;
			n = 0;

			while (l > 0) {
				q = fellow_rndup(stvfe->ffd, q);
				q = (size_t)1 << log2up(q);
				if (q > l || (n == DLE_BAN_REG_NREGION - 1))
					q = l;
				AN(buddy_req_extent(&stvfe->ban_reqs->reqs, q, 0));
				n++;
				l -= q;
			}
			assert(n <= DLE_BAN_REG_NREGION);
			stvfe->ban_space_state = 1;
			if (buddy_alloc_async(&stvfe->ban_reqs->reqs) == n)
				break;
			return (0);
		default:
			WRONG("banexport state in sfe_banexport_space()");
		}
	}
}

/* decision heuristic for when we want a banexport
 *
 * we export when we dropped more bans than left over
 */
#ifdef DEBUG
static inline void
log_banexport(const struct stvfe *stvfe, size_t explen, int r)
{
	fprintf(stderr, "%s\texplen %zu [nl]_drop %u %zu [nl]_new %u %zu\n",
	    r ? "BANEXP" : "no exp", explen,
	    stvfe->banstat.n_drop, stvfe->banstat.l_drop,
	    stvfe->banstat.n_new, stvfe->banstat.l_new);
}
#else
#define log_banexport(x, y, z) (void)0
#endif

static inline int
want_banexport_new(const struct stvfe *stvfe)
{
	size_t explen = VSC_C_main->bans_persisted_bytes -
	    VSC_C_main->bans_persisted_fragmentation;
	/*
	 * factor for how many log blocks we think we need to save
	 * to justify the extra overhead for an export block
	 */
	const unsigned new_factor = 25;
	int r;

	/* new export fits in an IMM */
	if (explen <=  FELLOW_DLE_BAN_IMM_MAX)
		r = 1;
	else
	/* we guess the export is more efficient log entries
	 * (overhead is mostly for rewriting)
	 */
	if (stvfe->banstat.n_new > FELLOW_DISK_LOG_BLOCK_ENTRIES * new_factor ||
	    stvfe->banstat.l_new > FELLOW_DLE_BAN_IMM_MAX * new_factor)
		r = 2;
	else
		r = 0;

	log_banexport(stvfe, explen, r);

	return (r);
}

static inline int
want_banexport_drop(const struct stvfe *stvfe)
{
	size_t explen = VSC_C_main->bans_persisted_bytes -
	    VSC_C_main->bans_persisted_fragmentation;
	int r;

	/* new export fits in an IMM */
	if (explen <=  FELLOW_DLE_BAN_IMM_MAX)
		r = 1;
	else
	/* ban list has shrunk by at least one logblk */
	if (stvfe->banstat.n_drop > FELLOW_DISK_LOG_BLOCK_ENTRIES ||
	    stvfe->banstat.l_drop > FELLOW_DLE_BAN_IMM_MAX)
		r = 2;
	else
		r = 0;

	log_banexport(stvfe, explen, r);

	return (r);
}

static int
sfe_baninfo(const struct stevedore *stv, enum baninfo event,
	    const uint8_t *ban, unsigned len)
{
	struct stvfe *stvfe;
	vtim_real t;
	int r;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	switch (event) {
	case BI_NEW:
		stvfe->banstat.n_new++;
		stvfe->banstat.l_new += len;

		t = sfe_ban_latest_time(ban, len);
		AN(fellow_log_ban(stvfe->ffd, DLE_OP_ADD, ban, len, t, NULL));
		if (stvfe->banexport == 0 && want_banexport_new(stvfe))
			stvfe->banexport = 1;
		if (stvfe->banexport == 1 && sfe_banexport_space(stvfe)) {
			stvfe->banexport = 2;
			return (1);
		}
		break;
	case BI_DROP:
		stvfe->banstat.n_drop++;
		stvfe->banstat.l_drop += len;

		r = 0;
		if (stvfe->banexport == 0)
			r = want_banexport_drop(stvfe);

		if (r) {
			/* We only write a new region export when we have new
			 * bans, but if we can put the export into an IMM, we
			 * do so immediately
			 */
			stvfe->banexport = 1;
			if (r == 1 && sfe_banexport_space(stvfe)) {
				stvfe->banexport = 2;
				return (1);
			}
		}
		break;
	default:
		WRONG("baninfo event");
	}

	return (0);
}

static void
sfe_banexport(const struct stevedore *stv, const uint8_t *bans, unsigned len)
{
	struct buddy_off_extent *space = NULL;
	struct stvfe *stvfe;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	if (len > FELLOW_DLE_BAN_IMM_MAX && stvfe->ban_space_state == 2)
		space = stvfe->ban_space;

	if (fellow_log_ban(stvfe->ffd, DLE_OP_CHG, bans, len,
	    sfe_ban_latest_time(bans, len), space)) {
		stvfe->banexport = 0;
		memset(&stvfe->banstat, 0, sizeof stvfe->banstat);
	} else {
		stvfe->banexport = 1;
	}
}

static struct sfe_ban_t
sfe_ban_reg(struct sfe_resurrect_priv *sfer, const struct fellow_dle *e)
{
	struct buddy_ptr_extent mem;
	struct sfe_ban_t t;

	CHECK_OBJ_NOTNULL(sfer, SFE_RESURRECT_PRIV_MAGIC);
	CHECK_OBJ_NOTNULL(e, FELLOW_DLE_MAGIC);

	// log code ensures this
	assert(e->u.ban_reg.ban_time > sfer->ban_exp_t.latest);

	/*
	 * XXX could be async: perform async read,
	 * load thread check bans to add
	 */
	mem = fellow_log_read_ban_reg(sfer->stvfe->ffd, e);
	XXXAN(mem.ptr);

	t.earliest = sfe_ban_earliest_time(mem.ptr, e->u.ban_reg.len);
	t.latest = e->u.ban_reg.ban_time;

	BAN_Reload(mem.ptr, e->u.ban_reg.len);
	buddy_return1_ptr_extent(sfer->stvfe->membuddy, &mem);
	AZ(mem.ptr);

	return (t);
}

static struct sfe_ban_t
sfe_ban_imm(struct sfe_resurrect_priv *sfer, const struct fellow_dle *e)
{
	unsigned l, ll, len;
	const uint8_t *ban;
	struct sfe_ban_t t;
	int8_t cont, c;

	CHECK_OBJ_NOTNULL(sfer, SFE_RESURRECT_PRIV_MAGIC);
	CHECK_OBJ_NOTNULL(e, FELLOW_DLE_MAGIC);

	assert(DLE_TYPE(e->type) == DLE_T_BAN_IMM);
	cont = c = e->u.ban_im0.cont;
	assert(cont >= 0);

	AZ(e->u.ban_im0._pad);
	ll = len = e->u.ban_im0.len;
	AN(len);
	assert(len <= FELLOW_DLE_BAN_IMM_MAX);
	ban = e->u.ban_im0.ban;
	l = DLE_BAN_IM0_LEN;

	t.earliest = sfe_ban_earliest_time(ban, l);
	t.latest = e->u.ban_im0.ban_time;

	assert(t.latest > sfer->ban_exp_t.latest);

	uint8_t *b, bans[len];
	b = bans;
	while (ll > 0) {
		if (cont) {
			if (c == 0)
				assert(e->u.ban_imm.cont == 0 - cont);
			else
				assert(e->u.ban_imm.cont == c);
			c--;
		}

		if (ll < l)
			l = ll;
		memcpy(b, ban, (size_t)l);
		b += l;
		ll -= l;
		if (ll == 0)
			break;

		e++;
		l = DLE_BAN_IMM_LEN;
		ban = e->u.ban_imm.ban;
	}
	assert(e->u.ban_imm.cont == 0 - cont);

	BAN_Reload(bans, len);
	return (t);
}

#ifdef TODO
void
sfe_panic_st(struct vsb *vsb, const char *hd, const struct sfe *sfe)
{
	VSB_printf(vsb, "%s = %p {emb=%u, ptr=%p, len=%zu, space=%zu},\n",
	hd, sfe, sfe->embedded, (void *)sfe->ptr, sfe->len, sfe->space);
}

void
sfe_panic(struct vsb *vsb, const struct objcore *oc)
{
	struct obu *o;
	struct sfe *st;

	VSB_printf(vsb, "Buddy = %p,\n", oc->stobj->priv);
	if (oc->stobj->priv == NULL)
		return;
	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);

	sfe_panic_st(vsb, "Obj", o->objstore);

#define OBJ_FIXATTR(U, l, sz) \
	VSB_printf(vsb, "%s = ", #U); \
	VSB_quote(vsb, (const void*)o->fa_##l, sz, VSB_QUOTE_HEX); \
	VSB_printf(vsb, ",\n");

#define OBJ_VARATTR(U, l) \
	VSB_printf(vsb, "%s = {len=%u, ptr=%p},\n", \
	    #U, o->va_##l##_len, o->va_##l);

#define OBJ_AUXATTR(U, l)						\
	do {								\
		if (o->aa_##l != NULL) sfe_panic_st(vsb, #U, o->aa_##l);\
	} while(0);

#include "tbl/obj_attr.h"

	VTAILQ_FOREACH(st, &o->list, list) {
		sfe_panic_st(vsb, "Body", st);
	}
}
#endif

static VCL_BYTES v_matchproto_(stv_var_free_space)
sfe_free_space(const struct stevedore *stv)
{
	struct stvfe *stvfe;

	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	return ((VCL_BYTES)stvfe->stats->g_dsk_space);
}

static VCL_BYTES v_matchproto_(stv_var_used_space)
sfe_used_space(const struct stevedore *stv)
{
	struct stvfe *stvfe;

	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	return ((VCL_BYTES)stvfe->stats->g_dsk_bytes);
}

static void * v_matchproto_(storage_allocbuf_t)
sfe_allocbuf(struct worker *wrk, const struct stevedore *stv, size_t size,
    uintptr_t *ppriv)
{
	struct stvfe *stvfe;
	void *p;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);
	AN(ppriv);

	p = buddy_malloc_wait(stvfe->membuddy, size, FEP_GET);
	if (p == NULL)
		return (NULL);
	*ppriv = (uintptr_t)p;
	return (p);
}

static void v_matchproto_(storage_freebuf_t)
sfe_freebuf(struct worker *wrk, const struct stevedore *stv, uintptr_t priv)
{
	struct stvfe *stvfe;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	buddy_free(stvfe->membuddy, (void *)priv);
}

/* the stevedore facing varnish is always the
 * memory stevedore
 *
 * the disk stevedore only internal, it can not be opened etc
 */
/*lint -e{785}*/
static const struct stevedore sfemem_stevedore = {
	.magic		=	STEVEDORE_MAGIC,
	.name		=	"fellow",
	.init		=	sfe_cfg,
	.open		=	sfe_open,
	.close		=	sfe_close,
	.allocobj	=	sfe_allocobj,
	.baninfo        =	sfe_baninfo,
	.banexport      =	sfe_banexport,

//	.panic		=	sfe_panic,
	.methods	=	&sfemem_methods,
	.var_free_space =	sfe_free_space,
	.var_used_space =	sfe_used_space,
	.allocbuf	=	sfe_allocbuf,
	.freebuf	=	sfe_freebuf,
};

static void __attribute__((constructor))
fellow_stv_register(void)
{
	STV_Register(&sfemem_stevedore, NULL);
}

/*
 * stevedore configuration from VCL
 */
struct stevedore *
sfe_new(const char *name, const char *filename, size_t dsksz, size_t memsz,
    size_t objsize_hint, unsigned discard)
{
	struct stevedore *stv;
	char *id;

	stv = malloc(sizeof *stv);
	AN(stv);

	*stv = sfemem_stevedore;
	AN(stv->name);

	id = strdup(name);
	AN(id);
	stv->ident = id;
	stv->vclname = id;

	if (sfe_init(stv, discard ? STVFE_VCL_DISCARD : STVFE_VCL_EMPTY,
	    filename, dsksz, memsz, objsize_hint, NULL) ||
	    sfe_open_scope(stv)) {
		free(id);
		free(stv);
		return (NULL);
	}

	AN(stv->allocobj);
	AN(stv->methods);

	return (stv);
}

static struct stevedore *original_transient = NULL;

void
sfe_as_transient(struct stevedore *stv)
{
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);

	if (original_transient == NULL)
		original_transient = stv_transient;

	stv_transient = stv;
}

int
sfe_is(VCL_STEVEDORE stv)
{
	struct stvfe *stvfe;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	stvfe = stv->priv;
	return (stvfe != NULL && stvfe->magic == STVFE_MAGIC);
}

void
sfe_fini(struct stevedore **stvp)
{
	struct stevedore *stv;
	struct stvfe *stvfe;

	TAKE_OBJ_NOTNULL(stv, stvp, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvfe, stv->priv, STVFE_MAGIC);

	if (stv_transient == stv)
		stv_transient = original_transient;

	sfe_close_warn(stvfe);
	sfe_close_real(stvfe);
	stv->priv = NULL;

	stvfe_wait_fini(&stvfe->wait);
	AZ(stvfe->wait);
	FREE_OBJ(stvfe->dskstv);
	FREE_OBJ(stvfe);
}

/* XXX uses inside knowledge of varnishd */
//lint -save -e506, -e835
#include "foreign/ban.h"
//lint -restore

/*
 * ban exports are from old to new
 */
static vtim_real
sfe_ban_earliest_time(const uint8_t *banspec, unsigned len)
{
	AN(banspec);
	assert(len >= 16);	// HEAD_LEN
	return (ban_time(banspec));
}

static vtim_real
sfe_ban_latest_time(const uint8_t *banspec, unsigned len)
{
	vtim_real t, max = 0;
	unsigned l;

	AN(banspec);
	assert(len >= 16);	// HEAD_LEN
	while (len) {
		t = ban_time(banspec);
		if (t > max)
			max = t;

		l = ban_len(banspec);
		assert(l <= len);
		len -= l;
		banspec += l;
	}

	AN(max);
	return (max);
}
