.. _slash-counters(7):

==============
slash-counters
==============

------------------------------------------------------------
Varnish counter fields exposed by the SLASH/ storage engines
------------------------------------------------------------

:Manual section: 7

.. include:: counters.rst
