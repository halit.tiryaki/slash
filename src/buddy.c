/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#ifdef HAVE_LINUX_MMAN_H
#include <linux/mman.h>
#endif
#include <pthread.h>
#include <errno.h>

// vanish
#include <vdef.h>
#include <vas.h>
#include <miniobj.h>
#include <vtree.h>
// varnish copied
#include "foreign/vmb.h"

#include "debug.h"
#include "buddy.h"

/*
 * ============================================================
 * buddy2 interface
 */

static void __attribute__((constructor))
assert_buddy_types(void)
{
	buddyoff_t t;
	intptr_t i;

	i = INTPTR_MAX;
	t = i;
	assert(i == t);

	i = BUDDY_OFF_NIL;
	t = i;
	assert(i == t);
}

#ifdef FREEPAGE_WHEN
#include <vtree.h>
struct freepage;
VRBT_HEAD(freepage_head, freepage);

struct freemap_extra {
	unsigned		magic;
#define FREEMAP_EXTRA_MAGIC	0x1e9f70ab
	unsigned		page_bits;
	struct freepage_head	freepage_head;
	unsigned char		*base;
};

struct freepage_arg {
	unsigned	deadline;
	size_t		page;
	double		when;
};
typedef struct freepage_arg freepage_t;

struct freepage {
	unsigned		magic;
#define FREEPAGE_MAGIC		0x753ced00
	unsigned		page_bits;
	struct freepage_arg	fpa;
	VRBT_ENTRY(freepage)	entry;
};

struct bitf;

// freepage to page
#define frp(p) (p).page
// page to freepage with when = 0, deadline = 0
#define pfr(p) (struct freepage_arg){.page = (p), .when = 0, .deadline = 0}
// page to freepage with args
#define pfra(p,w,d) (struct freepage_arg)		\
	{.page = (p), .when = (w), .deadline = (d)}
// modified page
#define mfr(p, m) (struct freepage_arg)				\
	{.page = (p).page m, .when = (p).when, .deadline = (p).deadline}
#else
typedef size_t freepage_t;
#define frp(p) p
#define pfr(p) p
#define pfra(p,w,d) p
#define mfr(p, m) (p m)
#endif

#if defined(FREEPAGE_WHEN) && defined(DEBUG)
#define CHECK_FREEPAGE(f) check_freepage(f)
#else
#define CHECK_FREEPAGE(f) (void)0
#endif

/************************************************************
 * util
 */

void *
BUDDYF(mmap)(size_t *sz, void *priv)
{
	int i, flags;
	size_t rsz;
	void *base = MAP_FAILED;
	const struct {
		int		   flags;
		unsigned	   pow2;
		const char * const str;
	} flagdef[] = {
		/* always added: MAP_PRIVATE | MAP_ANONYMOUS */
#define MMAP_FLAGS(x, b) {.flags = (x), .pow2 = b, .str = #x},
#include "tbl/mmap_flags.h"
	};

	(void) priv;

	i = 0;
	do {
		flags = MAP_PRIVATE | MAP_ANONYMOUS | flagdef[i].flags;
		rsz = *sz;
		if (flagdef[i].pow2) {
			if ((size_t)1 << flagdef[i].pow2 > rsz)
				continue;
			rsz = rup_min(rsz, flagdef[i].pow2);
			assert(rsz >= *sz);
		}
		base = mmap(NULL, rsz, PROT_READ|PROT_WRITE,
		    flags, -1, (off_t)0);
		if (base == MAP_FAILED)
			continue;
		*sz = rsz;
		fprintf(stderr, "mmap(%zu, %s) succeeded\n",
		    *sz, flagdef[i].str);
		DBG("base = %p, sz = %zd, flags = %p (try %d)",
		    base, *sz, (void *)(uintptr_t)flags, i);
		break;
	} while(flagdef[i++].flags != 0);

	assert(base != MAP_FAILED);
	return (base);
}

void
BUDDYF(unmap)(void **map, size_t sz, void **priv)
{
	void *p = *map;
	*map = NULL;

	(void) priv;

	AZ(munmap(p, sz));
}


/*
 * number of 1<<bits sized pages to fit
 *
 * round up, then ensure there exists a buddy page, except for the top page
 */
static inline size_t
npages(size_t size, unsigned bits)
{
	size_t s = (size_t)1 << bits;

	if (s >= size)
		return (1);
	s = (size + s - 1) / s;
	if (s & 1)
		return (s + 1);
	return (s);
}

/*
 * limit a cram value such that an allocation returned by buddy can
 * not be smaller than 1<<bits
 */
int8_t
BUDDYF(cramlimit_bits)(size_t sz, int8_t cram, int8_t bits)
{
	int8_t up;

	if (cram == 0)
		return (cram);
	up = (int8_t)(uint8_t)log2up(sz);
	assert(up >= bits);
	up -= bits;
	if (abs(cram) <= up)
		return (cram);
	return (cram < 0 ? 0 - up : up);
}


/************************************************************
 * init free map
 *
 * 1-bits are free pages
 */

static size_t
freemap_sz(unsigned min, size_t size)
{
	unsigned max, u;
	size_t s, sz = 0;

	max = log2up(size);

	// why bother to find a closed form
	for (u = min; u <= max; u++)
		sz += bitf_sz(npages(size, u), BITF_INDEX);

	// space for the freemap array
	assert(max > min);
	u = max - min;
	u++;
	s = sizeof(struct bitf *) * u;
	sz += s;
	// struct
	sz += sizeof(struct slashmap);
#ifdef FREEPAGE_WHEN
	// n x struct freemap_extra
	sz += sizeof(struct freemap_extra) * u;
#endif

	return (sz);
}

struct fptr {
	char	*p;
	size_t	sz;
};

static inline void *
fptr_get(struct fptr *f, size_t sz)
{
	void *p;

	assert(f->p != NULL);
	assert(f->sz >= sz);
	p = f->p;
	f->sz -= sz;
	f->p += sz;
	return (p);
}

static struct slashmap *
freemap_init(unsigned char *base, unsigned min, size_t size, mapper *mapf,
    void *priv)
{
	struct slashmap *r;
	struct bitf **fp, *bitf;
	unsigned max, u;
	size_t s, sz;
	struct fptr f[1];

	(void)base;
	assert(min >= MIN_BUDDY_BITS);
	max = log2up(size);
	assert(max > min);

	sz = freemap_sz(min, size);
	f->sz = sz;

	if (mapf)
		f->p = mapf(&sz, priv);
	else
		f->p = BUDDYF(mmap)(&sz, NULL);
	AN(f->p);
	assert(sz >= f->sz);

	r = fptr_get(f, sizeof *r);
	memset(r, 0, sizeof *r);
	VWMB();
	r->self = r;
	r->size = size;
	r->mmap_size = sz;
	r->min = min;
	r->max = max;

	fp = r->freemap;
	// space for the freemap array
	u = max - min;
	u++;
	sz = sizeof bitf * u;
	(void) fptr_get(f, sz);

#ifdef FREEPAGE_WHEN
	// extra structs
	struct freemap_extra *extra = fptr_get(f, sizeof *extra * u);
#endif

	for (u = min; u <= max; u++) {
		s = npages(size, u);
		sz = bitf_sz(s, BITF_INDEX);
		DBG("%zu pages of size %u", s, 1 << u);
		bitf = bitf_init(fptr_get(f, sz), s, sz, BITF_INDEX);
		*fp++ = bitf;
#ifdef FREEPAGE_WHEN
		AN(base);
		INIT_OBJ(extra, FREEMAP_EXTRA_MAGIC);
		VRBT_INIT(&extra->freepage_head);
		extra->page_bits = u;
		extra->base = base;
		bitf->extra = extra++;
#endif
	}
	AZ(f->sz);

	// sanity check
	for (u = min; u <= max; u++) {
		s = npages(size, u);
		bitf = *freemapsl(r, u);
		assert(bitf_nbits(bitf) == s);
		assert(bitf_nset(bitf) == 0);
#ifdef FREEPAGE_WHEN
		CAST_OBJ_NOTNULL(extra, bitf->extra, FREEMAP_EXTRA_MAGIC);
		assert(extra->page_bits == u);
		assert(extra->base == base);
#endif
	}
	VWMB();
	r->magic = BUDDY_MAP_MAGIC;

	return (r);
}

static size_t
freemap_space(const struct slashmap *map)
{
	const struct bitf *f;
	size_t nset;
	unsigned u;
	size_t b, r = 0;

	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);
	for (u = map->min; u <= map->max;  u++) {
		f = *freemapsr(map, u);
		nset = bitf_nset(f);
		if (nset == 0)
			continue;
		b = (size_t)1 << u;
		r += b * nset;
		DBG("%20zu: %zu/%zu pages free", b, nset, bitf_nbits(f));
	}
	return (r);
}

/************************************************************
 * expire time based free pages
 *
 */

#ifdef FREEPAGE_WHEN
static inline int
freepage_cmp(const struct freepage *m1, const struct freepage *m2)
{
	if (m1->fpa.when < m2->fpa.when)
		return (-1);
	if (m1->fpa.when > m2->fpa.when)
		return (1);
	// need to make the sort key unique
	if (m1->fpa.page < m2->fpa.page)
		return (-1);
	if (m1->fpa.page > m2->fpa.page)
		return (1);
	return (0);
}

VRBT_GENERATE_INSERT_COLOR(freepage_head, freepage, entry, static)
//VRBT_GENERATE_FIND(freepage_head, freepage, entry, freepage_cmp, static)
#ifdef VRBT_GENERATE_INSERT_FINISH
VRBT_GENERATE_INSERT_FINISH(freepage_head, freepage, entry, static)
#endif
VRBT_GENERATE_INSERT(freepage_head, freepage, entry, freepage_cmp, static)
VRBT_GENERATE_REMOVE_COLOR(freepage_head, freepage, entry, static)
VRBT_GENERATE_REMOVE(freepage_head, freepage, entry, static)
VRBT_GENERATE_NEXT(freepage_head, freepage, entry, static)
#if defined(FREEPAGE_WHEN) && defined(DEBUG)
VRBT_GENERATE_MINMAX(freepage_head, freepage, entry, static)
#endif

// same as name##_VRBT_NFIND()
// but also returns 2nd best element
static struct freepage *
freepage_head_VRBT_2NFIND(const struct freepage_head *head,
    const struct freepage *elm)
{
	struct freepage *tmp = VRBT_ROOT(head);
	struct freepage *res = NULL;
	int comp;
	while (tmp) {
		res = tmp;
		comp = freepage_cmp(elm, tmp);
		if (comp < 0) {
			// elm->fpa.when < tmp->fpa.when
			tmp = VRBT_LEFT(tmp, entry);
		}
		else if (comp > 0)
			// elm->fpa.when > tmp->fpa.when
			tmp = VRBT_RIGHT(tmp, entry);
		else
			return (tmp);
	}
	return (res);
}
#define VRBT_2NFIND(name, x, y) name##_VRBT_2NFIND(x, y)

#if defined(FREEPAGE_WHEN) && defined(DEBUG)
static inline void
check_freepage(const struct bitf *f)
{
	struct freepage *fp;
	struct freemap_extra *extra;
	size_t buddy;
	int i = 0;
	double when = 0;

	CAST_OBJ_NOTNULL(extra, f->extra, FREEMAP_EXTRA_MAGIC);
	VRBT_FOREACH(fp, freepage_head, &extra->freepage_head) {
		CHECK_OBJ(fp, FREEPAGE_MAGIC);
		AN(bitf_get(f, fp->fpa.page));
		buddy = fp->fpa.page ^ (size_t)1;
		if (buddy < bitf_nbits(f))
			AZ(bitf_get(f, buddy));
		assert (fp->fpa.when >= when);
		when = fp->fpa.when;
		i++;
	}
	assert (i == bitf_nset(f));
}
#endif

static inline struct freepage *
freepage(const struct bitf *f, size_t page)
{
	struct freemap_extra *extra;

	CAST_OBJ_NOTNULL(extra, f->extra, FREEMAP_EXTRA_MAGIC);
	return ((void *)(extra->base + (page << extra->page_bits)));
}

static inline void
freepage_add(struct bitf *f, freepage_t page)
{
	struct freemap_extra *extra;
	struct freepage *fp = freepage(f, page.page);

	CAST_OBJ_NOTNULL(extra, f->extra, FREEMAP_EXTRA_MAGIC);
	INIT_OBJ(fp, FREEPAGE_MAGIC);
	fp->page_bits = extra->page_bits;
	fp->fpa = page;
	VRBT_INSERT(freepage_head, &extra->freepage_head, fp);
}

static inline void
freepage_remove(struct bitf *f, size_t page)
{
	struct freemap_extra *extra;
	struct freepage *fp = freepage(f, page);

	CAST_OBJ_NOTNULL(extra, f->extra, FREEMAP_EXTRA_MAGIC);
	CHECK_OBJ(fp, FREEPAGE_MAGIC);
	assert(fp->page_bits == extra->page_bits);
	assert(fp->fpa.page == page);
	VRBT_REMOVE(freepage_head, &extra->freepage_head, fp);
}
#else
#define freepage_add(f, p) (void)0
#define freepage_remove(f, p) (void)0
#endif

/************************************************************
 * page ops
 *
 */

/* basically just wrapping bitf_* */
static inline unsigned
page_free(struct bitf *f, freepage_t page)
{
	CHECK_FREEPAGE(f);
	unsigned r = bitf_set(f, frp(page));
	if (r)
		freepage_add(f, page);
	CHECK_FREEPAGE(f);
	return (r);
}

static inline unsigned
page_take(struct bitf *f, size_t page)
{
	CHECK_FREEPAGE(f);

	unsigned r = bitf_clr(f, page);

	if (r)
		freepage_remove(f, page);

	CHECK_FREEPAGE(f);
	return (r);
}

//lint -e{818} ff pointer to const: I do not see, how
static size_t
page_split(struct bitf **ff, freepage_t page, unsigned levels)
{
	struct bitf *f = *ff;

	AN(page_take(f, frp(page)));

	while (levels-- > 0) {
		ff--;
		f = *ff;
		frp(page) <<= 1;
		AN(page_free(f, mfr(page, | 1)));
	}

	return (frp(page));
}

#ifdef FREEPAGE_WHEN
// split to take a specific page
static void
page_splitto(struct bitf **ff, freepage_t page)
{
	struct bitf *f = *ff;

	if (page_take(f, frp(page)))
		return;

	page_splitto(ff + 1, mfr(page, >> 1));
	AN(page_free(f, mfr(page, ^ 1)));
}
# else
/*
 * split to take a specific page
 * simpler version which violates free->take order
 * (thus does not work with FREEPAGE_WHEN)
 */
static void
page_splitto(struct bitf * const *ff, freepage_t page)
{
	struct bitf *f = *ff;

	while (! page_take(f, frp(page))) {
		AN(page_free(f, mfr(page, ^ 1)));
		frp(page) >>= 1;
		f = *(++ff);
	}
}
#endif

//lint -e{818} ff pointer to const: I do not see, how
static void
page_merge(struct bitf **ff, size_t page)
{
	struct bitf *f = *ff;
	size_t buddy = page ^ (size_t)1;
#ifdef FREEPAGE_WHEN
	struct freepage *fp;
	double d, latest = 0;
	unsigned deadline = 1;
#endif
	assert(! bitf_get(f, page));

	/* if it is the root or if there is no buddy to merge, we just free the
	 * single page
	 */

	if (bitf_nbits(f) == 1 || buddy >= bitf_nbits(f) ||
	    ! bitf_get(f, buddy)) {
		AN(page_free(f, pfr(page)));
		return;
	}

	/*
	 * we clear and all buddies found on the way, then set the final merge
	 * page
	 */
	while (bitf_nbits(f) > 1 && buddy < bitf_nbits(f) &&
	    page_take(f, buddy)) {
#ifdef FREEPAGE_WHEN
		fp = freepage(f, buddy);
		CHECK_OBJ(fp, FREEPAGE_MAGIC);
		d = fp->fpa.when;
		if (d > latest)
			latest = d;
		deadline &= fp->fpa.deadline;
#endif
		f = *++ff;
		page >>= 1;
		buddy = page ^ (size_t)1;
	}
	AN(page_free(f, pfra(page, latest, 0)));
}

/* fit an oversized page down such that the sum of all page allocations
 * represents a contiguous region of (at least) the given size
 *
 * bits of the size indicate the allocation
 * 1: both buddies taken, continue right
 * 0: right buddy free, continue left
 *
 * must only be called if there is anything to fit (not for pow2)
 */
//lint -e{818} ff pointer to const: I do not see, how
static void
page_fit(struct bitf **ff, size_t size, freepage_t page, const unsigned bit)
{
	struct bitf *f = *ff;
	size_t mask;

	DBG("size %zd", size);

	mask = (size_t)1 << bit;

	DBG(" bit %u", bit);
	DBG("mask %p", (void *)mask);
	while (size & (mask - 1)) {
		mask >>= 1;
		frp(page) <<= 1;
		ff--;
		f = *ff;
		DBG(" &   %p", (void *)(size & mask));
		if (size & mask)
			frp(page) |= 1;
		else
			AN(page_free(f, mfr(page, | 1)));
	}
	// follows from not pow2 requirement
	assert(frp(page) & 1);
	AN(page_free(f, page));
}

static inline void
page_free_fitted_mask(struct bitf **ff, size_t size, size_t page,
    size_t mask)
{

	while (size & (mask - 1)) {
		mask >>= 1;
		if (size & mask) {
			DBG("unfit  %p", (void *)(size & mask));
			AZ(page & 1);
			page_merge(ff, page);
			page |= 1;
		}
		page <<= 1;
		ff--;
	}
}

/* free a fitted allocation
 * the bit argument is the highest bit set
 */
static inline void
page_free_fitted(struct bitf **ff, size_t size, size_t page,
    const unsigned bit)
{

	page_free_fitted_mask(ff, size, page, (size_t)1 << (bit + 1));
}

/*
 * similar to page_fit:
 *
 * fit the area such that the given size is marked free
 *
 * enables use of sizes smaller than a pow2
 *
 * ff points to the highest bitf (which needs to contain 1 bit, and clr)
 */
static void
area_fit(struct bitf * const *ff, size_t size, unsigned min)
{
	struct bitf *f = *ff;
	unsigned bit;
	size_t page, mask;

	DBG("area fit size %zd", size);

	assert(bitf_nbits(f) == 1);
	assert(bitf_nset(f) == 0);

	// caller needs to ensure this
	AZ(size & (((size_t)1 << min) - 1));

	bit = log2up(size);
	if (size == (size_t)1 << bit) {
		AN(page_free(f, pfr(0UL)));
		return;
	}

	page = 0;
	mask = (size_t)1 << bit;

	DBG("afit  bit %u", bit);
	DBG("afit mask %p", (void *)mask);
	while (size & (mask - 1)) {
		mask >>= 1;
		page <<= 1;
		ff--;
		f = *ff;
		DBG("afit  &   %p", (void *)(size & mask));
		if (size & mask) {
			AN(page_free(f, pfr(page)));
			page |= 1;
		}
	}
}

/************************************************************
 * page alloc/free
 *
 */

#ifdef FREEPAGE_WHEN
/* helper for next function
 *
 * use page with closest when
 * if deadlined, use only smaller when
 */

static inline const struct freepage *
page_choose(const struct bitf *f, double when)
{
	struct freemap_extra *extra;
	struct freepage *fp;
	const struct freepage needle = {
		.magic = FREEPAGE_MAGIC, .fpa.when = when};

	CAST_OBJ_NOTNULL(extra, f->extra, FREEMAP_EXTRA_MAGIC);
	fp = VRBT_2NFIND(freepage_head, &extra->freepage_head, &needle);
	AN(fp);
	DBG("page_choose sz %2u: want %f found %f",
	    extra->page_bits, when, fp->fpa.when);
	// do not use deadlined fp unless we are earlier
	while (fp && fp->fpa.deadline && when > fp->fpa.when)
		fp = VRBT_NEXT(freepage_head, entry, fp);
	if (fp)
		DBG("page_choose      : want %f took %f", when, fp->fpa.when);
	else
		DBG("page_choose      : want %f NULL", when);
	return (fp);
}
#endif

#ifdef FREEPAGE_WHEN
#define PAGE_CHOOSE(f, page)						\
	if (bitf_nset(f) == 0)						\
		continue;						\
	const struct freepage *fp = page_choose(f, when);		\
	if (fp == NULL)						\
		continue;						\
	page = fp->fpa
#else
#define PAGE_CHOOSE(f, page)						\
	if (bitf_nset(f) == 0)						\
		continue;						\
	page = bitf_ffs(f);						\
	AN(page);							\
	page--
#endif

/* returns bits alloced or 0 if failed
 * under buddy map_mtx
 *
 * abs(crama): number of bits the allocation can be smaller
 * positive: prefer smaller allocation over split
 * negative: split, only return smaller allocation if full
 */

static inline unsigned
page_alloc_locked(struct slashmap *map, const unsigned bits,
    int crama, size_t *pagep, double when)
{
	struct bitf **ffstart, **ff, *f;
	unsigned cram, u, l;
	freepage_t page;

	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);
	AN(pagep);
	(void)when;

	ffstart = freemapsl(map, bits);

	cram = (unsigned)abs(crama);
	assert(cram <= bits);	// buddy_req_x abslimit()

	l = bits - cram;
	if (l < map->min)
		l = map->min;

	// exact size or smaller available if crama > 0?
	if (crama > 0) {
		ff = ffstart;
		for (u = bits; u >= l; u--, ff--) {
			f = *ff;
			PAGE_CHOOSE(f, page);
			AN(page_take(f, frp(page)));
			*pagep = frp(page);
			return (u);
		}
	}

	// split
	ff = ffstart;
	l = map->max - bits;
	for (u = 0; u <= l; u++, ff++) {
		f = *ff;
		PAGE_CHOOSE(f, page);
		*pagep = page_split(ff, pfra(frp(page), when, 0), u);
		return (bits);
	}

	if (! cram)
		return (0);

	// split failed, fall back to smaller if cram
	ff = ffstart;
	l = bits - cram;
	if (l < map->min)
		l = map->min;
	for (u = bits; u >= l; u--, ff--) {
		f = *ff;
		PAGE_CHOOSE(f, page);
		AN(page_take(f, frp(page)));
		*pagep = frp(page);
		return (u);
	}

	return (0);
}

/* under buddy map_mtx */
static inline void
page_free_locked(struct slashmap *map, unsigned bits, size_t page)
{
	struct bitf **ff;

	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);
	ff = freemapsl(map, bits);
	page_merge(ff, page);
}

/************************************************************
 * wait interface
 *
 */

#if defined(DEBUG) || defined(EXTRA_ASSERTIONS)
static void
assert_waitq(buddy_t *buddy)
{
	unsigned pri;

	if (buddy->waiting == 0)
		AZ(buddy->wait_pri);

	for (pri = BUDDY_WAIT_MAXPRI;
	     pri > buddy->wait_pri;
	     pri--)
		assert(VTAILQ_EMPTY(&buddy->reqs_head[pri]));

	assert(pri == buddy->wait_pri);
	assert(VTAILQ_EMPTY(&buddy->reqs_head[pri]) ==
	    ! buddy->waiting);
}
#define ASSERT_WAITQ(buddy) assert_waitq(buddy)
#else
#define ASSERT_WAITQ(buddy) (void)0
#endif

static inline unsigned
buddy_reqs_work_list_locked(struct buddy_reqs_head *head);

// under buddy mtx
static void
buddy_wait_work(buddy_t *buddy)
{
	struct buddy_reqs_head *head;
	unsigned pri;

	ASSERT_WAITQ(buddy);

	AN(buddy->waiting);
	AN(buddy->deficit);
	AZ(buddy->wait_working);
	buddy->wait_working = 1;
	pri = buddy->wait_pri;
	do {
		assert(pri <= BUDDY_WAIT_MAXPRI);
		head = &buddy->reqs_head[pri];
		buddy->waiting -= buddy_reqs_work_list_locked(head);
	} while (pri-- > 0 && VTAILQ_EMPTY(head));

	buddy->wait_pri = pri + 1;
	assert(buddy->wait_pri <= BUDDY_WAIT_MAXPRI);
	buddy->wait_working = 0;

	ASSERT_WAITQ(buddy);
}

// external: wait until space is needed (LRU)
void
BUDDYF(wait_needspace)(buddy_t *buddy)
{

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	AZ(pthread_mutex_lock(&buddy->map_mtx));
	if (buddy->waiting == 0)
		AZ(pthread_cond_wait(&buddy->wait_kick_cond,
		    &buddy->map_mtx));
	AZ(pthread_mutex_unlock(&buddy->map_mtx));
}
// external: kick wait_needspace for termination
void
BUDDYF(wait_kick)(buddy_t *buddy)
{

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	AZ(pthread_mutex_lock(&buddy->map_mtx));
	AZ(pthread_cond_broadcast(&buddy->wait_kick_cond));
	AZ(pthread_mutex_unlock(&buddy->map_mtx));
}

static struct buddy_reqs *buddy_reqs_takefirst(buddy_t *buddy);

// external: failed to make any more space, fail a
// waiting request
void
BUDDYF(wait_fail)(buddy_t *buddy)
{
	struct buddy_reqs *reqs;
	struct i_wait *w;

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	AZ(pthread_mutex_lock(&buddy->map_mtx));
	reqs = buddy_reqs_takefirst(buddy);
	/* potentially the next request can be fulfilled */
	if (buddy->waiting)
		buddy_wait_work(buddy);

	if (reqs == NULL) {
		AZ(pthread_mutex_unlock(&buddy->map_mtx));
		return;
	}

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);

	w = &reqs->i_wait;
	AZ(pthread_mutex_lock(&w->wait_mtx));
	AZ(pthread_mutex_unlock(&buddy->map_mtx));
	assert(w->state == IW_WAITING);
	w->state = IW_SIGNALLED;
	AZ(pthread_cond_signal(&w->wait_cond));
	AZ(pthread_mutex_unlock(&w->wait_mtx));
}

/************************************************************
 * buddy malloc()
 */

struct buddy_minfo {
	unsigned		magic;
#define BUDDY_MINFO_MAGIC	0xb396f867
	VRBT_ENTRY(buddy_minfo) entry;
	struct buddy_ptr_page	mi;	// self
	struct buddy_ptr_extent r;
};

static unsigned buddy_minfo_bits = 0;

static inline int
minfo_cmp(const struct buddy_minfo *m1, const struct buddy_minfo *m2)
{
	if (m1->r.ptr < m2->r.ptr)
		return (-1);
	if (m1->r.ptr > m2->r.ptr)
		return (1);
	return (0);
}

static inline unsigned
minfo_bits(const struct slashmap *map)
{
	return (map->min > buddy_minfo_bits ? map->min : buddy_minfo_bits);
}

VRBT_GENERATE_INSERT_COLOR(buddy_minfo_head, buddy_minfo, entry, static)
VRBT_GENERATE_FIND(buddy_minfo_head, buddy_minfo, entry, minfo_cmp, static)
#ifdef VRBT_GENERATE_INSERT_FINISH
VRBT_GENERATE_INSERT_FINISH(buddy_minfo_head, buddy_minfo, entry, static)
#endif
VRBT_GENERATE_INSERT(buddy_minfo_head, buddy_minfo, entry, minfo_cmp, static)
VRBT_GENERATE_REMOVE_COLOR(buddy_minfo_head, buddy_minfo, entry, static)
VRBT_GENERATE_REMOVE(buddy_minfo_head, buddy_minfo, entry, static)
//VRBT_GENERATE_MINMAX(buddy_minfo_head, buddy_minfo, entry, static)
//VRBT_GENERATE_NEXT(buddy_minfo_head, buddy_minfo, entry, static)

#define MALLOC_DECL buddy_t *buddy, size_t size
#define MALLOC_DECL_WHEN MALLOC_DECL, double when
#define MALLOC_ARGS buddy, size
#define MALLOC_ARGS_WHEN MALLOC_ARGS, when

static inline void *
_buddy_malloc(MALLOC_DECL_WHEN)
{
	struct buddy_reqs *reqs = BUDDY_REQS_STK(buddy, 2);
	struct buddy_minfo *minfo, *ominfo;
	struct buddy_ptr_extent r;
	struct buddy_ptr_page mi;
	int done;

	(void)when;

#ifdef FREEPAGE_WHEN
	done = buddywhen_req_extent(reqs, size, 0, when);
	done += buddywhen_req_page(reqs, minfo_bits(buddy->map), 0, when);
#else
	done = buddy_req_extent(reqs, size, 0);
	done += buddy_req_page(reqs, minfo_bits(buddy->map), 0);
#endif

	if (done != 2)
		return (NULL);

	done = BUDDYF(alloc)(reqs);

	if (done != 2) {
		BUDDYF(alloc_async_done)(reqs);
		errno = ENOMEM;
		return (NULL);
	}

	r = buddy_get_ptr_extent(reqs, 0);
	mi = buddy_get_ptr_page(reqs, 1);

	minfo = mi.ptr;
	INIT_OBJ(minfo, BUDDY_MINFO_MAGIC);
	minfo->mi = mi;
	minfo->r = r;
	AZ(pthread_mutex_lock(&buddy->minfo_mtx));
	ominfo = VRBT_INSERT(buddy_minfo_head, &buddy->minfo_head, minfo);
	AZ(pthread_mutex_unlock(&buddy->minfo_mtx));
	AZ(ominfo);
	return (r.ptr);
}

// XXX remove code duplication
static inline void *
_buddy_malloc_wait(MALLOC_DECL, unsigned pri, double when)
{
	struct buddy_reqs *reqs = BUDDY_REQS_STK(buddy, 2);
	struct buddy_minfo *minfo, *ominfo;
	struct buddy_ptr_extent r;
	struct buddy_ptr_page mi;
	int done;

	(void)pri;
	(void)when;

#ifdef FREEPAGE_WHEN
	done = buddywhen_req_extent(reqs, size, 0, when);
	done += buddywhen_req_page(reqs, minfo_bits(buddy->map), 0, when);
#else
	done = buddy_req_extent(reqs, size, 0);
	done += buddy_req_page(reqs, minfo_bits(buddy->map), 0);
#endif

	if (done != 2)
		return (NULL);

	done = BUDDYF(alloc_wait)(reqs);

	if (done != 2) {
		BUDDYF(alloc_wait_done)(reqs);
		errno = ENOMEM;
		return (NULL);
	}

	r = buddy_get_ptr_extent(reqs, 0);
	mi = buddy_get_ptr_page(reqs, 1);

	BUDDYF(alloc_wait_done)(reqs);

	minfo = mi.ptr;
	INIT_OBJ(minfo, BUDDY_MINFO_MAGIC);
	minfo->mi = mi;
	minfo->r = r;
	AZ(pthread_mutex_lock(&buddy->minfo_mtx));
	ominfo = VRBT_INSERT(buddy_minfo_head, &buddy->minfo_head, minfo);
	AZ(pthread_mutex_unlock(&buddy->minfo_mtx));
	AZ(ominfo);
	return (r.ptr);
}

#ifdef FREEPAGE_WHEN
void *
buddywhen_malloc(MALLOC_DECL_WHEN)
{

	return (_buddy_malloc(MALLOC_ARGS_WHEN));
}
void *
buddywhen_malloc_wait(MALLOC_DECL, unsigned pri, double when)
{

	return (_buddy_malloc_wait(MALLOC_ARGS, pri, when));
}
#else
void *
buddy_malloc(MALLOC_DECL)
{
	const double when = 0;

	return (_buddy_malloc(MALLOC_ARGS_WHEN));
}
void *
buddy_malloc_wait(MALLOC_DECL, unsigned pri)
{
	const double when = 0;

	return (_buddy_malloc_wait(MALLOC_ARGS, pri, when));
}
#endif

void
BUDDYF(free)(buddy_t *buddy, void *ptr)
{
	struct buddy_minfo needle, *minfo, *rminfo;
	struct buddy_returns *rets = BUDDY_RETURNS_STK(buddy, 2);

	if (ptr == NULL)
		return;

	INIT_OBJ(&needle, BUDDY_MINFO_MAGIC);
	needle.r.ptr = ptr;

	AZ(pthread_mutex_lock(&buddy->minfo_mtx));
	minfo = VRBT_FIND(buddy_minfo_head, &buddy->minfo_head, &needle);
	rminfo = VRBT_REMOVE(buddy_minfo_head, &buddy->minfo_head, minfo);
	assert (minfo == rminfo);
	AZ(pthread_mutex_unlock(&buddy->minfo_mtx));

	CHECK_OBJ_NOTNULL(minfo, BUDDY_MINFO_MAGIC);
	assert(minfo->mi.ptr == minfo);
	assert(minfo->r.ptr == ptr);

	AN(BUDDYF(return_ptr_extent)(rets, &minfo->r));
	AN(BUDDYF(return_ptr_page)(rets, &minfo->mi));
	BUDDYF(return)(rets);
}

/*
 * take an allocation of a certain size
 *
 * caller asserts that it is free
 *
 * size has to be rounded appropriately
 */

#ifdef FREEPAGE_WHEN
void
BUDDYF(take_off_extent)(buddy_t *buddy, const struct buddy_off_extent *rs,
    unsigned n, double when)
#else
void
BUDDYF(take_off_extent)(buddy_t *buddy, const struct buddy_off_extent *rs,
    unsigned n)
#endif
{
	const struct buddy_off_extent *r;
	struct slashmap *map;
	struct bitf **ff, **ffa[n];
	freepage_t page, pages[n];
	unsigned u, bits;
	size_t size, b, bs[n];

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	map = buddy->map;
	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);

	if (n == 0)
		return;

	for (u = 0, r = rs; u < n; u++, r++) {
		assert(r->size == rup_min(r->size, map->min));
		bits = log2down(r->size);
		assert(bits < UINT8_MAX);
		ffa[u] = freemapsl(map, bits);
		bs[u] = (size_t)1 << bits;
		pages[u] = pfra(ipageof(buddy, r->off, (uint8_t)bits), when, 0);
	}

	AZ(pthread_mutex_lock(&buddy->map_mtx));
	for (u = 0, r = rs; u < n; u++, r++) {
		//lint --e{771}
		size = r->size;
		ff = ffa[u];
		page = pages[u];
		b = bs[u];
		BWIT_TAKE(buddy->witness, r->off, size);

		while (size != 0) {
			if (size & b) {
				page_splitto(ff, page);
				frp(page)++;
				size &= ~b;
			}
			b >>= 1;
			frp(page) <<= 1;
			ff--;
		}
	}
	AZ(pthread_mutex_unlock(&buddy->map_mtx));
}

/*
 * trim: reduce an allocation
 *
 * a lower size implies that some leftmost bit changes from one to zero.
 *
 * the old allocation can be              v---- changing bit
 * a) just that page              (e.g. ..1000)
 * b) the page plus fitted alloc  (e.g. ..1011)
 *
 * likewise, the new allocation can be
 * A) nothing                     (e.g. ..0000)
 * B) fitted alloc                (e.g. ..0010)
 *
 * b) is an allocation of a page plus its buddy. For this we need to free
 *    (unfit) the buddy's additional allocation, which will potentially free the
 *    buddy. For this case, there exists a buddy, so the page address is even.
 *
 * a) the old allocation is just the page, so the page number can be even or
 *    uneven.
 *
 * Effectively, a trim is:
 *
 * - if a) a free (unfit) below the buddy
 * - if A) freeing our page
 * - if B) fitting the new allocation under our page
 *
 */

void
BUDDYF(trim1_off_extent)(buddy_t *buddy, struct buddy_off_extent *r, size_t nsz)
{
	struct slashmap *map;
	unsigned bits;
	struct bitf **ff;
	size_t fitmask, xosz, page;
	off_t off;

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	map = buddy->map;
	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);
	AN(r);
	assert(r->off >= 0);

	if (nsz == 0) {
		BUDDYF(return1_off_extent)(buddy, r);
		return;
	}

	nsz = rup_min(nsz, map->min);
	if (r->size == nsz)
		return;

	assert(r->size > nsz);

	xosz = r->size ^ nsz;
	bits = log2down(xosz);

	// basically ipageof() without the lower bits zero assert
	off = r->off + (buddyoff_t)nsz;
	assert(off < (buddyoff_t)map->size);
	page = (size_t)off >> bits;

	ff = freemapsl(map, bits);

	DBG("%020zx r->off", r->off);
	DBG("%020zx ptr page=%zu", off, page);
	DBG("%020zx osz  %zu bits %u", r->size, r->size, log2down(r->size));
	DBG("%020zx nsz  %zu bits %u", nsz, nsz, log2down(nsz));
	DBG("%020zx xosz %zu bits %u", xosz, xosz, bits);

	fitmask = ((size_t)1 << bits) - 1;

	AZ(pthread_mutex_lock(&buddy->map_mtx));
	if (r->size & fitmask) {
		// if a) a free (unfit) below the buddy
		AZ(page & 1);
		page_free_fitted(ff - 1, r->size, (page | 1) << 1, bits - 1);
	}
	if (nsz & fitmask) {
		// if B) fitting the new allocation under our page
		page_fit(ff, nsz, pfr(page), bits); // XXX FREEPAGE_WHEN?
	} else {
		// if A) freeing our page
		page_merge(ff, page);
	}
	BWIT_RELEASE(buddy->witness, r->off, r->size);
	BWIT_TAKE(buddy->witness, r->off, nsz);
	if (buddy->waiting)
		buddy_wait_work(buddy);
	AZ(pthread_mutex_unlock(&buddy->map_mtx));

	r->size = nsz;
}

/************************************************************
 * init/fini
 */
void
BUDDYF(init_)(unsigned witness, buddy_t *buddy, unsigned min, size_t size,
    mapper *area_mapper, void *area_mapper_priv,
    mapper *freemap_mapper, void *freemap_mapper_priv)
{
	unsigned u, max;

#ifdef FREEPAGE_WHEN
	assert(sizeof(struct freepage) < (size_t)1 << min);
#endif

	assert(witness == HAS_WITNESS);

	/*
	 * clear low bits of size, we can not use space
	 * smaller than 1 << min
	 */

	size = rdown_min(size, min);

	INIT_OBJ(buddy, BUDDY_MAGIC);

	if (area_mapper != NULL) {
		buddy->area = area_mapper(&size, area_mapper_priv);
		XXXAN(buddy->area);
		buddy->end = buddy->area + size;
	} else {
		AZ(buddy->area);
		AZ(buddy->end);
	}

	max = log2up(size);

	buddy->map = freemap_init(buddy->area, min, size,
	    freemap_mapper, freemap_mapper_priv);

	AZ(pthread_mutex_init(&buddy->map_mtx, NULL));
	area_fit(freemapsl(buddy->map, max), size, min);

	buddy_minfo_bits = log2up(sizeof(struct buddy_minfo));

	AZ(pthread_mutex_init(&buddy->minfo_mtx, NULL));
	VRBT_INIT(&buddy->minfo_head);

	AZ(buddy->waiting);
	AZ(pthread_cond_init(&buddy->wait_kick_cond, NULL));

	for (u = 0; u <= BUDDY_WAIT_MAXPRI; u++)
		VTAILQ_INIT(&buddy->reqs_head[u]);
}

void
BUDDYF(fini)(buddy_t **buddyp,
    umapper *area_umapper, void **area_mapper_priv,
    umapper *freemap_umapper, void **freemap_mapper_priv)
{
	struct slashmap *map;
	buddy_t *buddy;
	unsigned u;

	TAKE_OBJ_NOTNULL(buddy, buddyp, BUDDY_MAGIC);
	TAKE_OBJ_NOTNULL(map, &buddy->map, BUDDY_MAP_MAGIC);

	AZ(buddy->waiting);
	for (u = 0; u <= BUDDY_WAIT_MAXPRI; u++)
		AZ(VTAILQ_FIRST(&buddy->reqs_head[u]));
	AZ(pthread_cond_destroy(&buddy->wait_kick_cond));
	assert(freemap_space(map) == map->size);
	assert(VRBT_EMPTY(&buddy->minfo_head));
	AZ(pthread_mutex_destroy(&buddy->minfo_mtx));

	if (area_umapper != NULL) {
		area_umapper((void **)&buddy->area, map->size,
		    area_mapper_priv);
		buddy->area = NULL;
	}
	AZ(buddy->area);

	if (freemap_umapper)
		freemap_umapper((void **)&map,
		    map->mmap_size, freemap_mapper_priv);
	else
		AZ(munmap((void *)map, map->mmap_size));
	AZ(pthread_mutex_destroy(&buddy->map_mtx));
	memset(buddy, 0, sizeof *buddy);
}

/************************************************************
 * meta
 */

size_t
BUDDYF(size)(const buddy_t *buddy)
{
	return (buddy->map->size);
}

size_t
BUDDYF(space)(buddy_t *buddy, int exact)
{
	size_t sz;

	if (! exact)
		return (freemap_space(buddy->map));

	AZ(pthread_mutex_lock(&buddy->map_mtx));
	sz = freemap_space(buddy->map);
	AZ(pthread_mutex_unlock(&buddy->map_mtx));
	return (sz);
}

/*
 * ============================================================
 * buddy2 interface
 */

/*
 * ------------------------------------------------------------
 * buddy2 interface
 *
 * alloc/fini functions called by BUDDYF(alloc)
 */

/* opportunistically grow a crammed allocation
 *
 * with our current allocation scheme, this is unlikely to hit
 * because
 * - any non-page-sized allocation needs to start at the next
 *   bigger page
 * - we take pages from left to right
 *
 * we should consider using the right allocation when splitting
 * should we also seach from the right?
 *
 */

static inline void
buddy_extent_grow(struct slashmap *map, struct i_req_extent *r)
{
#if 1
	(void) map;
	r->rup = (size_t)1 << r->bits;
#else
	struct bitf **ff, *f;
	unsigned u;
	size_t sz, page;

	sz = (size_t)1 << r->bits;
	page = r->page;

	u = r->bits - 1;
	if (page & 1 || u < map->min) {
		r->rup = sz;
		return;
	}

	ff = freemapsl(map, u);
	page <<= 1;

	while (sz < r->rup && u >= map->min) {
		f = *ff;
		if (page < bitf_nbits(f) && page_take(f, page)) {
			// XXX would need to hit here to be useful
			sz |= (size_t)1 << u;
			page |= 1;
		}
		page <<= 1;
		u--;
		ff--;
	}
	r->rup = sz;
#endif
}

//i_emalloc
static inline int
buddy_extent_alloc(struct slashmap *map, struct i_req_extent *r)
{
	unsigned b;
#ifdef FREEPAGE_WHEN
	double when = r->when;
#else
	const double when = 0;
#endif

	b = page_alloc_locked(map, r->bits, r->cram, &r->page, when);
	if (b == 0)
		return (0);
	assert(b < UINT8_MAX);

	if (b < r->bits) {
		// crammed
		r->bits = (uint8_t)b;
		buddy_extent_grow(map, r);
	} else {
		assert(b == r->bits);

		if (r->ff != NULL) {
			page_fit(r->ff, r->rup, pfra(r->page, when, 1),
			    r->bits);
		}
	}

	BWIT_TAKE(r->witness, r->page << r->bits, r->rup);
	return (1);
}

//r_v_emalloc
static inline void
buddy_extent_fini(struct i_reqalloc *ra)
{
	const struct i_req_extent *r;
	struct buddy_off_extent *extent;

	r = &ra->i_req.extent;
	extent = &ra->off_alloc.extent;

	extent->off = (buddyoff_t)(r->page << r->bits);
	extent->size = r->rup;
}

//i_balloc
static inline int
buddy_page_alloc(struct slashmap *map, struct i_req_page *r)
{
	unsigned bits;
#ifdef FREEPAGE_WHEN
	double when = r->when;
#else
	const double when = 0;
#endif

	bits = page_alloc_locked(map, r->bits, r->cram, &r->page, when);

	if (bits == 0)
		return (0);

	assert(bits < UINT8_MAX);
	BWIT_TAKE(r->witness, r->page << bits, (size_t)1 << bits);

	r->bits = (uint8_t)bits;

	return (1);
}

//r_v_balloc
static inline void
buddy_page_fini(struct i_reqalloc *ra)
{
	const struct i_req_page *r;
	struct buddy_off_page *page;

	r = &ra->i_req.page;
	page = &ra->off_alloc.page;

	page->off = (buddyoff_t)(r->page << r->bits);
	page->bits = r->bits;
	page->magic = BUDDY_PAGE_MAGIC;
}

/*
 * ------------------------------------------------------------
 * buddy2 interface
 *
 * alloc and fini dispatch
 */

/* under buddy->map_mtx and, for waiting requests, w->wait_mtx */
static uint8_t
buddy_reqs_alloc_locked(struct buddy_reqs *reqs)
{
	struct i_reqalloc *ra;
	uint8_t u;
	int ret = 1;

	for (u = reqs->i_wait.alloced; ret && u < reqs->n; u++) {
		ra = &reqs->i_reqalloc[u];
		CHECK_OBJ(ra, I_REQALLOC_MAGIC);
		switch(ra->type) {
		case BUDDY_T_EXTENT:
			ret = buddy_extent_alloc(reqs->map, &ra->i_req.extent);
			break;
		case BUDDY_T_PAGE:
			ret = buddy_page_alloc(reqs->map, &ra->i_req.page);
			break;
		default:
			WRONG("reqalloc type");
		}
	}

	assert(u > 0);
	u -= ret ? 0 : 1;
	VWMB();
	reqs->i_wait.alloced = u;
	return (u);
}

static inline void
buddy_reqs_fini(struct buddy_reqs *reqs, const uint8_t * const alloced)
{
	struct i_reqalloc *ra;
	struct i_wait *w;
	uint8_t u;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	w = &reqs->i_wait;
	CHECK_OBJ(w, I_WAIT_MAGIC);

	if (*alloced == w->finid)
		return;

	VRMB();
	assert(alloced == &w->alloced || *alloced == w->alloced);
	for (u = w->finid; u < *alloced; u++) {
		ra = &reqs->i_reqalloc[u];
		CHECK_OBJ(ra, I_REQALLOC_MAGIC);
		switch(ra->type) {
		case BUDDY_T_EXTENT:
			buddy_extent_fini(ra);
			break;
		case BUDDY_T_PAGE:
			buddy_page_fini(ra);
			break;
		default:
			WRONG("reqalloc type (2)");
		}
	}

	w->finid = u;
}

/*
 * ------------------------------------------------------------
 * buddy2 interface
 *
 * wait
 */

// under buddy mtx
static inline void
buddy_reqs_enqueue(struct buddy_reqs *reqs)
{
	struct i_wait *w;
	buddy_t *buddy;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	w = &reqs->i_wait;
	CHECK_OBJ(w, I_WAIT_MAGIC);
	buddy = reqs->buddy;

	ASSERT_WAITQ(buddy);

	assert(w->state == IW_ARMED);
	w->state = IW_WAITING;
	assert(reqs->pri <= BUDDY_WAIT_MAXPRI);
	w->pri = reqs->pri;
	VTAILQ_INSERT_TAIL(&buddy->reqs_head[w->pri], reqs, i_wait.list);
	buddy->waiting++;
	buddy->deficit += reqs->sz;
	if (w->pri > buddy->wait_pri)
		buddy->wait_pri = w->pri;
	AZ(pthread_cond_broadcast(&buddy->wait_kick_cond));
	ASSERT_WAITQ(buddy);
}

/*
 * fini or cancel the wait component of buddy requests only if it was used,
 * that is, only if the state is IW_WAITING or IW_SIGNALLED
 *
 * we rely on IW_WAITING (or IW_SIGNALLED, consequently) from
 * buddy_reqs_enqueue() to be visible here outside the lock.
 *
 * our lock order is buddy->map_mtx > w->wait_mtx from
 * buddy_reqs_work_list_locked
 */

static inline void
buddy_reqs_wait_fini_unlock(struct i_wait *w)
{
	int err;

	assert(w->state == IW_SIGNALLED);
	w->state = IW_INVAL;
	//lint -e{455} not locked mutex unlocked
	AZ(pthread_mutex_unlock(&w->wait_mtx));

	AZ(pthread_cond_destroy(&w->wait_cond));
	err = pthread_mutex_destroy(&w->wait_mtx);
	if (err == 0)
		return;

	assert(err == EBUSY);
	AZ(pthread_mutex_lock(&w->wait_mtx));
	AZ(pthread_mutex_unlock(&w->wait_mtx));
	AZ(pthread_mutex_destroy(&w->wait_mtx));
}

/*
 * to be called if an i_wait with prio pri is removed
 */

static inline void
buddy_new_wait_pri(buddy_t *buddy, unsigned pri)
{

	if (buddy->waiting == 0)
		buddy->wait_pri = 0;
	else if (pri == buddy->wait_pri) {
		while (VTAILQ_EMPTY(&buddy->reqs_head[pri])) {
			if (pri == 0)
				break;
			pri--;
		}
		buddy->wait_pri = pri;
		buddy_wait_work(buddy);
	}
}

static inline void
buddy_reqs_wait_cancel(struct buddy_reqs *reqs)
{
	struct buddy_reqs_head *head;
	struct i_wait *w;
	buddy_t *buddy;
	unsigned pri;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	w = &reqs->i_wait;
	CHECK_OBJ_NOTNULL(w, I_WAIT_MAGIC);

	if (w->state == IW_ARMED)
		w->state = IW_INVAL;
	if (w->state == IW_INVAL)
		return;

	if (w->state == IW_WAITING) {
		/* remove from wait list, w->state could be outdated ! */
		pri = w->pri;
		buddy = reqs->buddy;
		head = &buddy->reqs_head[pri];

		AZ(pthread_mutex_lock(&reqs->buddy->map_mtx));
		AZ(pthread_mutex_lock(&w->wait_mtx));

		ASSERT_WAITQ(buddy);

		if (w->state == IW_WAITING) {
			AN(buddy->waiting);
			buddy->waiting--;
			assert(buddy->deficit >= reqs->sz);
			buddy->deficit -= reqs->sz;

			VTAILQ_REMOVE(head, reqs, i_wait.list);
			w->state = IW_SIGNALLED;
		}

		buddy_new_wait_pri(buddy, pri);

		ASSERT_WAITQ(buddy);

		AZ(pthread_mutex_unlock(&reqs->buddy->map_mtx));
	} else {
		AZ(pthread_mutex_lock(&w->wait_mtx));
	}
	buddy_reqs_wait_fini_unlock(w);
	//lint -e{454} locked mutex not unlocked
}

/*
 * if necessary, adjust the priority of a waiting allocation
 * to that set in the reqs
 */
static inline void
buddy_reqs_wait_prio_adjust(struct buddy_reqs *reqs)
{
	struct buddy_reqs_head *ohead, *nhead;
	enum i_wait_state state;
	struct i_wait *w;
	buddy_t *buddy;
	unsigned opri;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	w = &reqs->i_wait;
	CHECK_OBJ_NOTNULL(w, I_WAIT_MAGIC);

	// unlocked checks for noop
	if (w->state != IW_WAITING ||
	    w->pri == reqs->pri)
		return;

	buddy = reqs->buddy;
	ohead = &buddy->reqs_head[w->pri];
	nhead = &buddy->reqs_head[reqs->pri];
	opri = w->pri;

	AZ(pthread_mutex_lock(&reqs->buddy->map_mtx));
	AZ(pthread_mutex_lock(&w->wait_mtx));

	state = w->state;
	if (state == IW_WAITING) {
		AN(buddy->waiting);
		/* _INSERT_HEAD? _INSERT_TAIL?
		 *
		 * _HEAD sounds right, because the request either cam from a
		 * higher priority, or it was promoted because it is urgent
		 */
		VTAILQ_REMOVE(ohead, reqs, i_wait.list);
		VTAILQ_INSERT_HEAD(nhead, reqs, i_wait.list);
		w->pri = reqs->pri;
	}
	AZ(pthread_mutex_unlock(&w->wait_mtx));

	if (state == IW_WAITING) {
		if (reqs->pri >= buddy->wait_pri) {
			buddy->wait_pri = reqs->pri;
			buddy_wait_work(buddy);
		}
		else
			buddy_new_wait_pri(buddy, opri);
	}

	AZ(pthread_mutex_unlock(&reqs->buddy->map_mtx));
}

static uint8_t
buddy_reqs_wait_allocated(struct i_wait *w)
{
	uint8_t alloced;

	CHECK_OBJ_NOTNULL(w, I_WAIT_MAGIC);

	if (w->state == IW_ARMED)
		w->state = IW_INVAL;
	if (w->state == IW_INVAL)
		return (w->alloced);

	AZ(pthread_mutex_lock(&w->wait_mtx));
	while (w->state == IW_WAITING)
		AZ(pthread_cond_wait(&w->wait_cond, &w->wait_mtx));
	alloced = w->alloced;
	assert(w->state == IW_SIGNALLED);
	buddy_reqs_wait_fini_unlock(w);
	//lint -e{454} locked mutex not unlocked
	return (alloced);
}

// under buddy mtx
static struct buddy_reqs *
buddy_reqs_takefirst(buddy_t *buddy)
{
	struct buddy_reqs_head *head;
	struct buddy_reqs *reqs;
	unsigned pri;

	pri = buddy->wait_pri;
	do {
		assert(pri <= BUDDY_WAIT_MAXPRI);
		head = &buddy->reqs_head[pri];
		reqs = VTAILQ_FIRST(head);
		if (reqs == NULL)
			continue;
		AN(buddy->waiting);
		buddy->waiting--;
		VTAILQ_REMOVE(head, reqs, i_wait.list);
		assert(buddy->deficit >= reqs->sz);
		buddy->deficit -= reqs->sz;
		return (reqs);
	} while (pri-- > 0);

	buddy->wait_pri = 0;
	//lint -e{438} last value of pri not used
	return (NULL);
}

// under buddy mtx
static inline unsigned
buddy_reqs_work_list_locked(struct buddy_reqs_head *head)
{
	struct buddy_reqs *reqs;
	struct i_wait *w;
	uint8_t n, u;
	unsigned r = 0;

	while ((reqs = VTAILQ_FIRST(head)) != NULL) {
		CHECK_OBJ(reqs, BUDDY_REQS_MAGIC);
		w = &reqs->i_wait;

		AZ(pthread_mutex_lock(&w->wait_mtx));
		assert(w->state == IW_WAITING);
		n = reqs->n;
		u = buddy_reqs_alloc_locked(reqs);
		assert (u <= n);
		if (u == n) {
			VTAILQ_REMOVE(head, reqs, i_wait.list);
			assert(reqs->buddy->deficit >= reqs->sz);
			reqs->buddy->deficit -= reqs->sz;

			w->state = IW_SIGNALLED;
			AZ(pthread_cond_signal(&w->wait_cond));
			r++;
		}
		AZ(pthread_mutex_unlock(&w->wait_mtx));
		if (u < n)
			break;
	}

	return (r);
}

static inline void
buddy_reqs_clear(struct buddy_reqs *reqs)
{

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	memset(reqs->i_reqalloc, 0, reqs->n * sizeof *reqs->i_reqalloc);
	AN(reqs->space);
	reqs->n = 0;
	AN(reqs->buddy);
	AN(reqs->map);
	assert(reqs->i_wait.state == IW_INVAL);
	reqs->i_wait = BUDDY_REQS_I_WAIT_INITIALIZER;
}

/*
 * try to allocate any still unallocated requests
 *
 * the caller needs to free partial reservation it does not want to keep
 */

uint8_t
BUDDYF(alloc)(struct buddy_reqs *reqs)
{
	uint8_t u;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	if (reqs->n == 0)
		return (0);
	assert(reqs->i_wait.finid < reqs->n);
	AZ(pthread_mutex_lock(&reqs->buddy->map_mtx));
	u = buddy_reqs_alloc_locked(reqs);
	AZ(pthread_mutex_unlock(&reqs->buddy->map_mtx));

	buddy_reqs_fini(reqs, &reqs->i_wait.alloced);

	return (u);
}

#define NEED_WAIT(buddy, pri)					\
	((buddy)->waiting && (buddy)->wait_pri >= (pri))

/* return whatever immediately allocatable, enqueue for more */
uint8_t
BUDDYF(alloc_async)(struct buddy_reqs *reqs)
{
	struct i_wait *w;
	buddy_t *buddy;
	uint8_t u;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	w = &reqs->i_wait;
	CHECK_OBJ(w, I_WAIT_MAGIC);

	if (reqs->n == 0)
		return (0);

	buddy = reqs->buddy;

	assert(w->alloced < reqs->n);
	assert(w->finid < reqs->n);

	AZ(pthread_mutex_lock(&reqs->buddy->map_mtx));
	do {
		if (! NEED_WAIT(buddy, reqs->pri)) {
			u = buddy_reqs_alloc_locked(reqs);
			if (u == reqs->n)
				break;
		}
		buddy_reqs_enqueue(reqs);
	} while (0);
	AZ(pthread_mutex_unlock(&reqs->buddy->map_mtx));

	return (BUDDYF(alloc_async_ready)(reqs));
}

/* prepare ready allocations for use */
uint8_t
BUDDYF(alloc_async_ready)(struct buddy_reqs *reqs)
{
	struct i_wait *w;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	w = &reqs->i_wait;
	CHECK_OBJ(w, I_WAIT_MAGIC);

	if (reqs->n == 0)
		return (0);

	buddy_reqs_wait_prio_adjust(reqs);
	buddy_reqs_fini(reqs, &w->alloced);

	return (w->finid);
}

/* return when allocations done or failed definitively */
uint8_t
BUDDYF(alloc_async_wait)(struct buddy_reqs *reqs)
{
	struct i_wait *w;
	uint8_t alloced;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	w = &reqs->i_wait;
	CHECK_OBJ(w, I_WAIT_MAGIC);

	if (reqs->n == 0)
		return (0);

	buddy_reqs_wait_prio_adjust(reqs);
	alloced = buddy_reqs_wait_allocated(w);
	buddy_reqs_fini(reqs, &alloced);

	return (w->finid);
}

/* waiting allocation, previous two functions in one */
uint8_t
BUDDYF(alloc_wait)(struct buddy_reqs *reqs)
{
	uint8_t n;

	n = BUDDYF(alloc_async)(reqs);
	if (n == reqs->n)
		return (n);
	return (BUDDYF(alloc_async_wait)(reqs));
}

/*
 * return any allocations not taken and finish off an async
 * allocation
 */
void
BUDDYF(alloc_async_done)(struct buddy_reqs *reqs)
{
	buddy_t *buddy = reqs->buddy;
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(buddy, BUDDY_RETURNS_MAX);
	struct i_reqalloc *ra;
	unsigned u;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);

	buddy_reqs_wait_cancel(reqs);
	buddy_reqs_fini(reqs, &reqs->i_wait.alloced);

	for (u = 0; u < reqs->i_wait.finid; u++) {
		ra = &reqs->i_reqalloc[u];
		CHECK_OBJ(ra, I_REQALLOC_MAGIC);
		switch(ra->type) {
		case BUDDY_T_EXTENT:
			if (ra->off_alloc.extent.off < 0)
				continue;
			AN(BUDDYF(return_off_extent)
			    (rets, &ra->off_alloc.extent));
			break;
		case BUDDY_T_PAGE:
			if (ra->off_alloc.page.off < 0)
				continue;
			AN(BUDDYF(return_off_page)
			    (rets, &ra->off_alloc.page));
			break;
		default:
			WRONG("reqalloc type");
		}
	}
	BUDDYF(return)(rets);
	buddy_reqs_clear(reqs);
}

void
BUDDYF(return)(struct buddy_returns *rets)
{
	struct i_return *a;
	struct slashmap *map;
	buddy_t *buddy;
	uint8_t u;

	CHECK_OBJ_NOTNULL(rets, BUDDY_RETURNS_MAGIC);
	buddy = rets->buddy;
	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	map = buddy->map;
	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);

	if (rets->n == 0) {
		AZ(rets->size);
		return;
	}

	for (u = 0, a = rets->i_return; u < rets->n; u++, a++) {
		CHECK_OBJ(a, I_RETURN_MAGIC);
		AN(a->bits);
		assert(a->off >= 0);
		AN(a->size);
		if (a->ff == NULL)
			assert(a->size == (size_t)1 << a->bits);
	}

	AZ(pthread_mutex_lock(&buddy->map_mtx));
	for (u = 0, a = rets->i_return; u < rets->n; u++, a++) {
		if (a->ff != NULL)
			page_free_fitted(a->ff, a->size, a->page, a->bits);
		else
			page_free_locked(map, a->bits, a->page);
		BWIT_RELEASE(buddy->witness, a->off, a->size);
	}
	if (buddy->waiting)
		buddy_wait_work(buddy);
	AZ(pthread_mutex_unlock(&buddy->map_mtx));

	memset(rets->i_return, 0, rets->n * sizeof *rets->i_return);
	rets->n = 0;
	rets->size = 0;
}

/*
 * buddy2 interface
 * ============================================================
 */


#ifdef TEST_DRIVER

#include <stdlib.h>

/************************************************************
 * tests
 */

static void
t_cramlimit(void)
{
	const int8_t min = 12;
	int8_t cram, t, bits, l;
	size_t sz;

	for (cram = -10; cram <= 10; cram++) {
		for (bits = min; bits < 20; bits++) {
			// valid cram is bits - min
			sz = (size_t)1 << bits;
			t = BUDDYF(cramlimit_bits)(sz, cram, min);
			l = bits - min;
			assert(abs(t) <= l);
			assert(t == 0 || (t < 0) == (cram < 0));

			// test round up from pow2 + 1
			sz++;
			l++;
			t = BUDDYF(cramlimit_bits)(sz, cram, min);
			assert(abs(t) <= l);
			assert(t == 0 || (t < 0) == (cram < 0));

			// test round up from (pow2 + 1) - 1
			sz = (size_t)1 << (bits + 1);
			sz--;
			t = BUDDYF(cramlimit_bits)(sz, cram, min);
			assert(abs(t) <= l);
			assert(t == 0 || (t < 0) == (cram < 0));
		}
	}
}

int t[] = {
	-1,
	255,
	254,
	249,
	248,
	247,
	246,
	245,
	244,
	243,
	242,
	241,
	240,
	239,
	232,
	231,
	224,
	216,
	130,
	129,
	128,
	127,
	126,
	66,
	65,
	64,
	63,
	1,
	0,
	-1};

#define NBITS 8192 << 12

static void
t_bitf(void)
{
	size_t sz = bitf_sz(NBITS, BITF_INDEX);
	void *mem;
	struct bitf *bitf;
	int shift, i, last, find, *check, *tt = t;
	unsigned nset = 0;

	mem = malloc(sz);
	assert(mem != NULL);
	bitf = bitf_init(mem, NBITS, sz, BITF_INDEX);

	assert(! bitf_get(bitf, 0));
	assert(bitf_nset(bitf) == 0);
	assert(bitf_set(bitf, 0));
	assert(bitf_nset(bitf) == 1);

	for (i = 1; i < NBITS; i++)
		assert(bitf_get(bitf, i) == 0);
	assert(bitf_clr(bitf, 0));
	for (i = 0; i < NBITS; i++)
		assert(bitf_get(bitf, i) == 0);

	for (shift = 0; shift < 12; shift++) {
		bitf = bitf_init(mem, NBITS, sz, BITF_INDEX);
		nset = 0;
		tt = t + 1;
		while (*tt != -1) {
			assert(! bitf_get(bitf, *tt));
			assert(bitf_nset(bitf) == nset);

			// set bit and check it is set
			assert(bitf_set(bitf, *tt));
			nset++;
			assert(bitf_nset(bitf) == nset);

			// setting again is noop
			assert(! bitf_set(bitf, *tt));
			assert(bitf_nset(bitf) == nset);

			// we find our new bit first
			assert(bitf_get(bitf, *tt));
			assert(bitf_ffs(bitf) == *tt + 1);

			// clear
			assert(bitf_clr(bitf, *tt));
			nset--;
			assert(bitf_nset(bitf) == nset);

			// clear again is noop
			assert(! bitf_clr(bitf, *tt));
			assert(bitf_nset(bitf) == nset);

			// we now find previous bit set first
			assert(bitf_ffs(bitf) == tt[-1] + 1);

			// set again
			assert(bitf_set(bitf, *tt));
			nset++;
			assert(bitf_nset(bitf) == nset);

			check = tt;
			last = find = 0;
			while (check > t) {
				find = bitf_ffs_from(bitf, find);
				assert(find != 0);
				find--;
				assert(find == *check);
				for (i = last; i < find; i++) {
					assert(bitf_get(bitf, i) == 0);
					assert(bitf_set(bitf, i) == 1);
					assert(bitf_clr(bitf, i) == 1);
				}
				last = find + 1;
				find++;
				check--;
			}
			tt++;
		}
		tt = t + 1;
		while (*tt != -1) {
			*tt <<= 1;
			tt++;
		}
	}
	free(mem);
}

#define ASSERT_NBITS_SZ_NOIDX(sz) do {					\
	size_t bits = sz_nbits(sz, BITF_NOIDX);				\
	assert(bitf_sz(bits, BITF_NOIDX) <= sz);			\
									\
	bits += wbits;							\
	assert(bitf_sz(bits, BITF_NOIDX) > sz);			\
	} while(0)

static void
t_sz_nbits(void)
{
	size_t u, v, sz;

	for (u = 6; u < 32; u++) {
		ASSERT_NBITS_SZ_NOIDX((size_t)1 << u);
		(void) sz_nbits((size_t)1 << u, BITF_INDEX);
	}

	for (sz = 64; sz < 128; sz++) {
		ASSERT_NBITS_SZ_NOIDX(sz);
		(void) sz_nbits(sz, BITF_INDEX);
	}

	for (u = 7; u < 61; u++) {
		for (v = ((size_t)1 << u) - 32;
		     v < ((size_t)1 << u) + 128;
		     v += 8) {
			ASSERT_NBITS_SZ_NOIDX(v);
			(void) sz_nbits(v, BITF_INDEX);
		}
	}
}

static size_t
malloc_sz(struct slashmap *map, size_t size)
{
	const size_t msz = sizeof(struct buddy_minfo);
	size_t moff, rup;

	rup = rup_min(size, map->min);
	moff = PRNDUP(size);

	if (rup - moff < msz)
		rup += rup_min(msz, map->min);

	return (rup);
}

/* wrap free to the same sig as efree */
static void *
wrap_malloc(buddy_t *buddy, size_t *sizep)
{
	static void *r;

#ifdef FREEPAGE_WHEN
	r = buddywhen_malloc(buddy, *sizep, drand48());
#else
	r = buddy_malloc(buddy, *sizep);
#endif
	*sizep = malloc_sz(buddy->map, *sizep);

	return (r);
}

static void
wrap_free(buddy_t *buddy, void *ptr, size_t size)
{
	(void) size;

	BUDDYF(free)(buddy, ptr);
}

// test page_take() by efree, page_take, efree
static void
wrap_return1_take(buddy_t *buddy, void *ptr, size_t size)
{
	struct buddy_ptr_extent nil = buddy_ptr_extent_nil;
	struct buddy_ptr_extent e = BUDDY_PTR_EXTENT(ptr, size);

	BUDDYF(return1_ptr_extent)(buddy, &e);
	assert(e.ptr == nil.ptr);
	assert(e.size == nil.size);
	DBG("take %p %zu", ptr, size);
	e = BUDDY_PTR_EXTENT(ptr, size);
#ifdef FREEPAGE_WHEN
	BUDDYF(take_ptr_extent)(buddy, &e, 1, drand48());
#else
	BUDDYF(take_ptr_extent)(buddy, &e, 1);
#endif
	BUDDYF(return1_ptr_extent)(buddy, &e);
	assert(e.ptr == nil.ptr);
	assert(e.size == nil.size);
}

// test efree_trim() by efree_trim, efree
static void
wrap_return1_etrim(buddy_t *buddy, void *ptr, size_t size)
{
	struct buddy_ptr_extent e = BUDDY_PTR_EXTENT(ptr, size);
	size_t sz;

	sz = size / 2;
	BUDDYF(trim1_ptr_extent)(buddy, &e, sz);
	BUDDYF(return1_ptr_extent)(buddy, &e);
}

static void
wrap_return1(buddy_t *buddy, void *ptr, size_t size)
{
	struct buddy_ptr_extent e = BUDDY_PTR_EXTENT(ptr, size);

	BUDDYF(return1_ptr_extent)(buddy, &e);
}

static struct buddy_ptr_page
wrap_alloc1_ptr_page(buddy_t *buddy, unsigned bits, int cram)
{
#ifdef FREEPAGE_WHEN
	return (buddywhen_alloc1_ptr_page(buddy, bits, cram, drand48()));
#else
	return (buddy_alloc1_ptr_page(buddy, bits, cram));
#endif
}

static int
wrap_req_page(struct buddy_reqs *reqs, unsigned bits, int8_t cram)
{
#ifdef FREEPAGE_WHEN
	return (buddywhen_req_page(reqs, bits, cram, drand48()));
#else
	return (buddy_req_page(reqs, bits, cram));
#endif
}

static void *
wrap_alloc1_ptr_extent(buddy_t *buddy, size_t *sizep)
{
	struct buddy_ptr_extent r;

#ifdef FREEPAGE_WHEN
	r = buddywhen_alloc1_ptr_extent(buddy, *sizep, 0, drand48());
#else
	r = buddy_alloc1_ptr_extent(buddy, *sizep, 0);
#endif

	if (r.ptr != NULL)
		assert(r.size == BUDDYF(rndup)(buddy, *sizep));
	*sizep = r.size;
	return (r.ptr);
}

static void *
wrap_alloc1_ptr_extent_wait(buddy_t *buddy, size_t *sizep)
{
	struct buddy_ptr_extent r;

#ifdef FREEPAGE_WHEN
	r = buddywhen_alloc1_ptr_extent_wait(buddy, 0, *sizep, 0, drand48());
#else
	r = buddy_alloc1_ptr_extent_wait(buddy, 0, *sizep, 0);
#endif

	if (r.ptr != NULL)
		assert(r.size == BUDDYF(rndup)(buddy, *sizep));
	*sizep = r.size;
	return (r.ptr);
}

#define CRAM_LEVELS					\
	CRAM(0)					\
	CRAM(1)					\
	CRAM(2)					\
	CRAM(3)					\
	CRAM(4)					\
	CRAM(5)					\
	CRAM(6)					\
	CRAM(7)					\
	CRAM(8)					\
	CRAM(9)					\
	CRAM(10)

#ifdef FREEPAGE_WHEN
#define CRAM(cram)							\
static void *								\
wrap_alloc1_ptr_extent_cram ## cram(buddy_t *buddy, size_t *sizep)	\
{									\
	struct buddy_ptr_extent r;					\
	r = buddywhen_alloc1_ptr_extent(buddy, *sizep, cram, drand48()); \
	*sizep = r.size;						\
	return (r.ptr);							\
}									\
static void *								\
wrap_alloc1_ptr_extent_cram_neg ## cram(buddy_t *buddy, size_t *sizep)	\
{									\
	struct buddy_ptr_extent r;					\
	r = buddywhen_alloc1_ptr_extent(buddy, *sizep, 0 - cram, drand48()); \
	*sizep = r.size;						\
	return (r.ptr);							\
}
CRAM_LEVELS
#else
#define CRAM(cram)							\
static void *								\
wrap_alloc1_ptr_extent_cram ## cram(buddy_t *buddy, size_t *sizep)	\
{									\
	struct buddy_ptr_extent r;					\
	r = buddy_alloc1_ptr_extent(buddy, *sizep, cram);		\
	*sizep = r.size;						\
	return (r.ptr);							\
}									\
static void *								\
wrap_alloc1_ptr_extent_cram_neg ## cram(buddy_t *buddy, size_t *sizep)	\
{									\
	struct buddy_ptr_extent r;					\
	r = buddy_alloc1_ptr_extent(buddy, *sizep, 0 - cram);		\
	*sizep = r.size;						\
	return (r.ptr);							\
}
CRAM_LEVELS
#endif
#undef CRAM

// return sum of sizes from t_next_freepage
static size_t
t_next_freepage(const struct slashmap *map)
{
	struct mappage mp, mpr;
	size_t sum = 0, cache[map->max + 1];
	unsigned u;

	for (u = 0; u <= map->max; u++)
		cache[u] = SIZE_MAX;

	mp.page = 0;
	mp.level = map->max;
	while (1) {
		mpr = next_freepage(map, mp, cache);
		DBG("next freepage page=%zu, level=%d", mp.page, mp.level);
		if (mpr.level < 0)
			break;
		assert(mpr.page << mpr.level >= mp.page << mp.level);
		sum += (size_t)1 << mpr.level;
		DBG("sum %zu", sum);
		mpr.page++;
		mp = mpr;
	}
	return (sum);
}

static void
t_assert_free(const struct slashmap *map, size_t free)
{
	assert(freemap_space(map) == free);
	assert(t_next_freepage(map) == free);
}

typedef void *malloc_fp(buddy_t *, size_t *);
typedef void free_fp(buddy_t *, void *, size_t);

static void
t_mallocfree(buddy_t *buddy, malloc_fp *f_malloc, free_fp *f_free,
    size_t free, size_t sizes[], int returns_usable)
{
	int max = 521; // arbitrary, larger than sizes array * shiftmax
	int i, j, shift;
	void *p[max], *t;
	size_t s, usable[max], gross[max];
	int shiftmax = 4;
	const struct slashmap *map = buddy->map;

	s = SIZE_MAX;
	AZ(f_malloc(buddy, &s));

	for (i = 0; sizes[i] != 0; i++) {
		usable[i] = gross[i]  = sizes[i];
		t = f_malloc(buddy, &gross[i]);
		AN(t);
		free -= gross[i];
		t_assert_free(map, free);

		f_free(buddy, t, gross[i]);
		free += gross[i];
		t_assert_free(map, free);
	}

	for (shift = 0, j = 0; shift < shiftmax; shift++) {
		for (i = 0; sizes[i] != 0; i++, j++) {
			assert(j < max);
			s = sizes[i];
			s <<= shift;
			usable[j] = gross[j] = s;
			p[j] = f_malloc(buddy, &gross[j]);
			AN(p[j]);
			free -= gross[j];
			t_assert_free(map, free);
			if (returns_usable)
				usable[j] = gross[j];
		}
	}
	for (shift = 0, j = 0; shift < shiftmax; shift++) {
		for (i = 0; sizes[i] != 0; i++, j++) {
			memset(p[j], usable[j], usable[j]);
		}
	}
	for (shift = 0, j = 0; shift < shiftmax; shift++) {
		for (i = 0; sizes[i] != 0; i++, j++) {
			f_free(buddy, p[j], gross[j]);
			free += gross[j];
			t_assert_free(map, free);
		}
	}
}

struct freethread_arg {
	buddy_t *buddy;
	free_fp *f_free;
	size_t *gross;
	void **p;
	int jsnap, jmax;
};

static void *
freethread(void *aa)
{
	struct freethread_arg *a = aa;
	int j;

	// first reverse from snap
	j = a->jsnap - 1;
	while (j >= 0) {
		a->f_free(a->buddy, a->p[j], a->gross[j]);
		usleep(drand48() * 1000);
		j--;
	}
	// then up from snap to make all allocs succeed
	j = a->jsnap;
	while (j < a->jmax) {
		a->f_free(a->buddy, a->p[j], a->gross[j]);
		j++;
	}

	free(a->gross);
	free(a->p);
	free(aa);
	return (NULL);
}

static inline void *
t_buddy_eatall(buddy_t *buddy, unsigned *bits)
{
	struct buddy_ptr_page p;

#ifdef FREEPAGE_WHEN
	p = buddywhen_alloc1_ptr_page(buddy, *bits, 0 - (int)*bits, 0);
#else
	p = buddy_alloc1_ptr_page(buddy, *bits, 0 - (int)*bits);
#endif

	*bits = p.bits;
	return (p.ptr);
}

static void
t_wait(buddy_t *buddy, malloc_fp *f_malloc, free_fp *f_free,
    size_t ofree, size_t sizes[], int returns_usable)
{
	int max = 521; // arbitrary, larger than sizes array * shiftmax
	int jsnap, i, j, shift;
	void *p[max];
	size_t free, s, usable[max], gross[max];
	int shiftmax = 4;
	const struct slashmap *map = buddy->map;
	unsigned b;
	struct freethread_arg *farg;
	pthread_t thread;

	s = SIZE_MAX;
	free = ofree;

	for (shift = 0, j = 0; shift < shiftmax; shift++) {
		for (i = 0; sizes[i] != 0; i++, j++) {
			assert(j < max);
			s = sizes[i];
			s <<= shift;
			usable[j] = gross[j] = s;
			p[j] = f_malloc(buddy, &gross[j]);
			AN(p[j]);
			free -= gross[j];
			t_assert_free(map, free);
			if (returns_usable)
				usable[j] = gross[j];
		}
	}
	for (shift = 0, j = 0; shift < shiftmax; shift++) {
		for (i = 0; sizes[i] != 0; i++, j++) {
			memset(p[j], usable[j], usable[j]);
		}
	}
	jsnap = j;

	(void) b;

	// alloc remaining storage
	do {
		assert(j < max);
		b = map->max;
		p[j] = t_buddy_eatall(buddy, &b);
		if (b == 0) {
			AZ(p[j]);
			AZ(free);
			break;
		}
		AN(p[j]);
		s = (size_t)1 << b;
		usable[j] = gross[j] = s;
		free -= gross[j];
		t_assert_free(map, free);
		j++;
	} while (free > 0);

	AZ(freemap_space(map));

	farg = malloc(sizeof *farg);
	AN(farg);
	farg->buddy = buddy;
	farg->f_free = f_free;
	farg->gross = calloc(j, sizeof *gross);
	memcpy(farg->gross, gross, j * sizeof *gross);
	farg->p = calloc(j, sizeof *p);
	memcpy(farg->p, p, j * sizeof *p);
	farg->jsnap = jsnap;
	farg->jmax = j;

	AZ(pthread_create(&thread, NULL, freethread, farg));

	free = ofree;

	/* reduce shiftmax to make the test work in light of
	 * fragmentation issues
	 */

	shiftmax--;

	/* while the freethread is running, we alloc in original order, which is
	 * reverse from free order.  we can only check the free size after the
	 * freethread has terinated, until then, free is not the actual free
	 * size
	 */

	for (shift = 0, j = 0; shift < shiftmax; shift++) {
		for (i = 0; sizes[i] != 0; i++, j++) {
			assert(j < max);
			s = sizes[i];
			s <<= shift;
			usable[j] = gross[j] = s;
			p[j] = f_malloc(buddy, &gross[j]);
			AN(p[j]);
			free -= gross[j];
			if (returns_usable)
				usable[j] = gross[j];
		}
	}
	for (shift = 0, j = 0; shift < shiftmax; shift++) {
		for (i = 0; sizes[i] != 0; i++, j++) {
			memset(p[j], usable[j], usable[j]);
		}
	}
	AZ(pthread_join(thread, NULL));
	for (shift = 0, j = 0; shift < shiftmax; shift++) {
		for (i = 0; sizes[i] != 0; i++, j++) {
			f_free(buddy, p[j], gross[j]);
			free += gross[j];
			t_assert_free(map, free);
		}
	}
}

static void
t_async(buddy_t *buddy)
{
	struct buddy_reqs *reqs = BUDDY_REQS_STK(buddy, 1);
	unsigned u;

	// query async which has not been started
	u = BUDDYF(alloc_async_ready)(reqs);
	AZ(u);

	BUDDYF(alloc_async_done)(reqs);
}

static void
t_prio(buddy_t *buddy)
{
	struct buddy_reqs *reqs_block = BUDDY_REQS_STK(buddy, 1);
	struct buddy_reqs *reqs_raise = BUDDY_REQS_STK(buddy, 1);
	struct buddy_ptr_page hog[2];

	// allocate (more than) half the buddy plus a bit
	hog[0] = wrap_alloc1_ptr_page(buddy, log2up(buddy->map->size) - 1, 0);
	AN(hog[0].ptr);
	hog[1] = wrap_alloc1_ptr_page(buddy, MIN_BUDDY_BITS, 0);
	AN(hog[1].ptr);

	// try to allocate another half at prio one, this must block
	AN(wrap_req_page(reqs_block, log2up(buddy->map->size) - 1, 0));
	BUDDY_REQS_PRI(reqs_block, 1);
	AZ(BUDDYF(alloc_async)(reqs_block));

	// try to allocate min bits at prio 0, this must also block
	AN(wrap_req_page(reqs_raise, MIN_BUDDY_BITS, 0));
	BUDDY_REQS_PRI(reqs_raise, 0);
	AZ(BUDDYF(alloc_async)(reqs_raise));

	// raise the priority and wait, this should return.
	// note: because of _INSERT_HEAD in the wait, prio 1
	// is sufficient
	BUDDY_REQS_PRI(reqs_raise, 1);
	AN(BUDDYF(alloc_async_wait)(reqs_raise));

	// no need to get the actual allocations, the fact that we got
	// here tests that the prio raise has been successful

	BUDDYF(alloc_async_done)(reqs_raise);
	BUDDYF(alloc_async_done)(reqs_block);
	BUDDYF(return1_ptr_page)(buddy, &hog[0]);
	BUDDYF(return1_ptr_page)(buddy, &hog[1]);
}

static void
t_page(buddy_t *buddy, size_t free)
{
	const struct slashmap *map;
	struct buddy_ptr_page page[512], t;
	int i;

	unsigned bits[] = {
		6,
		8,
		7,
		10,
		11,
		12,
		13,
		14,
		8,
		8,
		0
	};

	map = buddy->map;

	for (i = 0; bits[i] != 0; i++) {
		free -= (size_t)1 << bits[i];
		page[i] = wrap_alloc1_ptr_page(buddy, bits[i], 0);
		t_assert_free(map, free);
	}

	if (free) {
		t = wrap_alloc1_ptr_page(buddy, log2up(free + 1), 0);
		AZ(t.ptr);
	}

	// try 7 bits with cram
	if (free) {
		t = wrap_alloc1_ptr_page(buddy, 7, 7);
		AN(t.ptr);
		if (free < 128)
			assert(t.bits == 6);
		BUDDYF(return1_ptr_page)(buddy, &t);
	}

	for (i = 0; bits[i] != 0; i++) {
		BUDDYF(return1_ptr_page)(buddy, &page[i]);
		free += (size_t)1 << bits[i];
		t_assert_free(map, free);
	}
}

static void
t_buddy(size_t free)
{
	buddy_t bspc;
	buddy_t *buddy = &bspc;
	const struct slashmap *map;
	size_t sizes[] = {
		1,
		64,
		64,
		257,
		64,
		129,
		65,
		127,
		128,
		191,
		192,
		193,
		0
	};

	BUDDYF(init)(buddy, MIN_BUDDY_BITS, free,
	    BUDDYF(mmap), NULL,
	    BUDDYF(mmap), NULL);
	map = buddy->map;
	printf("==== size %zu -> %zu ====\n", free, map->size);
	free = rdown_min(free, MIN_BUDDY_BITS);
	assert(free == map->size);
	t_assert_free(map, free);

	t_async(buddy);
	assert(free == map->size);

	t_prio(buddy);
	assert(free == map->size);

	t_page(buddy, free);
	assert(free == map->size);

	srand48(0);
	fprintf(stderr, "=== malloc ===\n");
	t_mallocfree(buddy, wrap_malloc, wrap_free, free, sizes, 0);
	fprintf(stderr, "=== alloc1/return1 ===\n");
	t_mallocfree(buddy,
	    wrap_alloc1_ptr_extent,
	    wrap_return1,
	    free, sizes, 1);
	fprintf(stderr, "=== alloc1 w/take ===\n");
	t_mallocfree(buddy,
	    wrap_alloc1_ptr_extent,
	    wrap_return1_take,
	    free, sizes, 1);
	fprintf(stderr, "=== alloc1 w/trim ===\n");
	t_mallocfree(buddy,
	    wrap_alloc1_ptr_extent,
	    wrap_return1_etrim,
	    free, sizes, 1);
#define CRAM(cram)							\
	fprintf(stderr, "=== alloc1 cram " #cram " ===\n");		\
	t_mallocfree(buddy,						\
	    wrap_alloc1_ptr_extent_cram ## cram,			\
	    wrap_return1,						\
	    free, sizes, 1);						\
	fprintf(stderr, "=== alloc1 -" #cram " ===\n");			\
	t_mallocfree(buddy,						\
	    wrap_alloc1_ptr_extent_cram_neg ## cram,			\
	    wrap_return1,						\
	    free, sizes, 1);
	CRAM_LEVELS
#undef CRAM

	fprintf(stderr, "=== emalloc wait  ===\n");
	t_wait(buddy,
	    wrap_alloc1_ptr_extent_wait,
	    wrap_return1,
	    free, sizes, 1);

	freemap_space(map);
	BUDDYF(fini)(&buddy, BUDDYF(unmap), NULL, NULL, NULL);
}

static void
t_log2(size_t min, size_t max)
{
	size_t s;

	assert(min > 0);
	for (s = min; s <= max; s++) {
		assert(log2down(s) == log2up(s + 1) - 1);
	}
	fprintf(stderr, "log2 %zu..%zu OK\n", min, max);
}

BUDDY_POOL(t_pool_s, 4);

static void
t_pool_fill(struct buddy_reqs *reqs, const void *priv)
{
	int i;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);

	(void)priv;

	BUDDY_REQS_PRI(reqs, 3);	// 3 is arbitrary
#ifdef FREEPAGE_WHEN
	for (i = 0; i < reqs->space; i++)
		AN(buddywhen_req_page(reqs, 8, 0, 42.0));
#else
	for (i = 0; i < reqs->space; i++)
		AN(buddy_req_page(reqs, 8, 0));
#endif
}

// defines t_pool_s_get()
BUDDY_POOL_GET_FUNC(t_pool_s, static)
// defines t_pool_s_avail()
BUDDY_POOL_AVAIL_FUNC(t_pool_s, static)

/*
 * get allocations from a pool and return them
 */
static void
t_pool(void)
{
	unsigned u;
	buddy_t bspc;
	buddy_t *buddy = &bspc;
	struct t_pool_s pool[1];
	struct buddy_reqs *reqs;
	struct buddy_returns *rets;
	struct buddy_ptr_page alloc;

	BUDDYF(init)(buddy, MIN_BUDDY_BITS, 1 << 14,
	    BUDDYF(mmap), NULL,
	    BUDDYF(mmap), NULL);
	rets = BUDDY_RETURNS_STK(buddy, 8);

	BUDDY_POOL_INIT(pool, buddy, 0, t_pool_fill, NULL);

	for (u = 0; u < 42; u++) {
		reqs = t_pool_s_get(pool, NULL);
		AN(t_pool_s_avail(pool));
		alloc = buddy_get_next_ptr_page(reqs);
		assert(alloc.ptr > 0);
		do {
			BUDDYF(return_ptr_page)(rets, &alloc);
			alloc = buddy_get_next_ptr_page(reqs);
		} while (alloc.ptr > 0);
	}
	BUDDYF(return)(rets);
	BUDDY_POOL_FINI(pool);

	BUDDYF(fini)(&buddy, BUDDYF(unmap), NULL, NULL, NULL);
}

int main(void)
{
	t_pool();
	t_cramlimit();
	t_log2(1, (size_t)1 << 30);
	t_log2(SIZE_MAX - 5, SIZE_MAX - 1);
	assert(log2up(SIZE_MAX) == sizeof(size_t) * 8);
	assert(log2down(SIZE_MAX) == sizeof(size_t) * 8 - 1);
	t_bitf();
	t_sz_nbits();
	t_buddy(1 << 15);
	t_buddy((1 << 15) - 1);
	t_buddy((1 << 15) + 63);
	t_buddy((1 << 15) + 128);
	t_buddy((1 << 15) + 196);
	t_buddy((1 << 17) + 9763);
	printf("OK\n");
}
#endif
