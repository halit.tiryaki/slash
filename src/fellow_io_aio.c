/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <limits.h>	// UINT_MAX

#include <sys/types.h>
#include <sys/asynch.h>

// varnish
#include "miniobj.h"
#include "vdef.h"
#include "vas.h"
#include "vqueue.h"

#include "fellow_io_backend.h"
#include "fellow_io_ioctl.h"
#include "bitf.h"

#define ERR(x) do {							\
		int _e = (x);						\
		fprintf(stderr, "%s: %s (%d)\n",			\
		    __func__, strerror(_e), _e);			\
	} while(0)

struct fellow_io_aio_ctx;

struct fellow_aio_result {
	aio_result_t			aio;
	uint64_t			info;
	struct fellow_io_aio_ctx	*ctx;
	VTAILQ_ENTRY(fellow_aio_result)	list;
};

struct fellow_io_aio_ctx {
	unsigned			magic;
#define FELLOW_IO_AIO_CTX_MAGIC		0x7f985631
	unsigned			entries;
	unsigned			outstanding;
	int				fd;
	/* cond to signal enqueue to ready by other thread */
	pthread_cond_t			cond;
	VTAILQ_HEAD(,fellow_aio_result)	ready;
};

static void
test_task(struct worker *wrk, void *priv)
{
	int *answer = priv;

	(void) wrk;
	AN(answer);
	*answer = 42;
}

/* d-oh the AIO interface seems to be from the time before threads...
 *
 * there are no aio handles, aio seems to be per process, so we can not have
 * separate instances, but we need to return results for the right handle only.
 */

struct fellow_io_aio {
	unsigned			magic;
#define FELLOW_IO_AIO_MAGIC		0x59b58221
	unsigned			refcnt;
	// set bit == free slot
	struct bitf			*bitf;
	struct fellow_io_ioctl		*ioctl;
	unsigned			inflight;
	unsigned			waiting;
	struct fellow_aio_result	results[];
};

pthread_mutex_t the_mtx[1] = {PTHREAD_MUTEX_INITIALIZER};
struct fellow_io_aio *the_aio = NULL;

fellow_ioctx_t *
fellow_io_init(int fd, unsigned entries, void *base, size_t len,
    fellow_task_run_t taskrun)
{
	fellow_task_privstate taskstate;
	struct fellow_io_aio_ctx *ctx;
	struct fellow_io_aio *aio;
	unsigned u;
	int answer;
	size_t sz;
	void *p;

	ALLOC_OBJ(ctx, FELLOW_IO_AIO_CTX_MAGIC);
	ctx->fd = fd;
	ctx->entries = entries;
	AZ(pthread_cond_init(&ctx->cond, NULL));
	VTAILQ_INIT(&ctx->ready);

#ifdef TODO
	aio->ioctl = fellow_io_ioctl_init(fd, taskrun, entries);
	AN(aio->ioctl);
#endif

	AZ(pthread_mutex_lock(the_mtx));
	if (the_aio != NULL) {
		CHECK_OBJ(the_aio, FELLOW_IO_AIO_MAGIC);
		AN(the_aio->refcnt);
		the_aio->refcnt++;
		AZ(pthread_mutex_unlock(the_mtx));
		return(IOCTX(ctx));
	}

	AZ(taskrun(test_task, &answer, &taskstate));

	// this is shared across all instances, so blow it up
	entries <<= 2;
	sz = sizeof *aio + entries * sizeof *aio->results;
	aio = malloc(sz);
	AN(aio);
	memset(aio, 0, sz);
	aio->magic = FELLOW_IO_AIO_MAGIC;
	aio->refcnt = 1;

	sz = bitf_sz(entries, BITF_INDEX);
	p = malloc(sz);
	AN(p);
	aio->bitf = bitf_init(p, entries, sz, BITF_INDEX);
	AN(aio->bitf);
	for (u = 0; u < entries; u++)
		AN(bitf_set(aio->bitf, u));

	while (answer != 42)
		(void) usleep(1000);

	the_aio = aio;
	AZ(pthread_mutex_unlock(the_mtx));

	return (IOCTX(ctx));
}

void
fellow_io_fini(fellow_ioctx_t **ctxp)
{
	if (*ctxp == NULL)
		return;

	unsigned entries = fellow_io_entries(*ctxp);

	struct fellow_io_aio_ctx *ctx;
	struct fellow_io_status results[entries];

	TAKE_OBJ_NOTNULL(ctx, (void **)ctxp, FELLOW_IO_AIO_CTX_MAGIC);

	(void) fellow_io_wait_completions(IOCTX(ctx), results, entries,
	    UINT_MAX, NULL, NULL);

	AZ(pthread_cond_destroy(&ctx->cond));

#ifdef TODO
	fellow_io_ioctl_fini(&ctx->ioctl);
#endif

	AZ(pthread_mutex_lock(the_mtx));
	CHECK_OBJ_NOTNULL(the_aio, FELLOW_IO_AIO_MAGIC);
	if (--the_aio->refcnt) {
		AZ(pthread_mutex_unlock(the_mtx));
		return;
	}

	assert(bitf_nset(the_aio->bitf) == bitf_nbits(the_aio->bitf));
	free (the_aio->bitf);
	free (the_aio);
	the_aio = NULL;
	AZ(pthread_mutex_unlock(the_mtx));
}

unsigned
fellow_io_entries(fellow_ioctx_t *ctxp)
{
	struct fellow_io_aio_ctx *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_AIO_CTX_MAGIC);
	return (ctx->entries);
}

unsigned
fellow_io_outstanding(fellow_ioctx_t *ctxp)
{
	struct fellow_io_aio_ctx *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_AIO_CTX_MAGIC);
	return (ctx->outstanding);
}

void
fellow_io_submit(fellow_ioctx_t *ctxp)
{
	(void) ctxp;
	return;
}

int
fellow_io_read_async_enq(fellow_ioctx_t *ctxp,
    uint64_t info, void *buf, size_t bytes, off_t off)
{
	struct fellow_io_aio_ctx *ctx;
	struct fellow_aio_result *r;
	unsigned slot;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_AIO_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(the_aio, FELLOW_IO_AIO_MAGIC);

	AZ(pthread_mutex_lock(the_mtx));
	slot = bitf_ffs(the_aio->bitf);
	if (slot == 0) {
		AZ(pthread_mutex_unlock(the_mtx));
		return (0);
	}

	slot--;
	AN(bitf_clr(the_aio->bitf, slot));
	AZ(pthread_mutex_unlock(the_mtx));

	r = &the_aio->results[slot];
	memset(r, 0, sizeof *r);
	r->info = info;
	r->ctx = ctx;

	AZ(aioread(ctx->fd, (void *)buf, bytes, off, SEEK_SET, &r->aio));

	AZ(pthread_mutex_lock(the_mtx));
	ctx->outstanding++;
	the_aio->inflight++;
	AZ(pthread_mutex_unlock(the_mtx));

	return (1);
}

int
fellow_io_write_async_enq(fellow_ioctx_t *ctxp,
    uint64_t info, const void *buf, size_t bytes, off_t off)
{
	struct fellow_io_aio_ctx *ctx;
	struct fellow_aio_result *r;
	unsigned slot;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_AIO_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(the_aio, FELLOW_IO_AIO_MAGIC);

	AZ(pthread_mutex_lock(the_mtx));
	slot = bitf_ffs(the_aio->bitf);
	if (slot == 0) {
		AZ(pthread_mutex_unlock(the_mtx));
		return (0);
	}

	slot--;
	AN(bitf_clr(the_aio->bitf, slot));
	AZ(pthread_mutex_unlock(the_mtx));

	r = &the_aio->results[slot];
	memset(r, 0, sizeof *r);
	r->info = info;
	r->ctx = ctx;

	AZ(aiowrite(ctx->fd, (void *)buf, bytes, off, SEEK_SET, &r->aio));

	AZ(pthread_mutex_lock(the_mtx));
	ctx->outstanding++;
	the_aio->inflight++;
	AZ(pthread_mutex_unlock(the_mtx));

	return (1);
}

#define timeo_zero &((struct timeval){.tv_sec = 0,.tv_usec = 0})
#define aio_err ((aio_result_t *)-1)

unsigned
fellow_io_wait_completions(fellow_ioctx_t *ctxp,
    struct fellow_io_status *results, unsigned space, unsigned min,
    fellow_io_compl_cb cb, void *priv)
{
	struct fellow_io_aio_ctx *ctx;
	unsigned slot, ret = 0;
	aio_result_t *res;
	struct fellow_aio_result *ares, *aress;
	struct fellow_io_status *rstart;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_AIO_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(the_aio, FELLOW_IO_AIO_MAGIC);

	AN(results);
	AN(space);

	if (min > ctx->outstanding)
		min = ctx->outstanding;
	if (min > space)
		min = space;

	AZ(pthread_mutex_lock(the_mtx));

	while (min > ret || min == 0) {
		rstart = results;
		VTAILQ_FOREACH_SAFE(ares, &ctx->ready, list, aress) {
			if (space == 0)
				break;

			assert(ares->ctx == ctx);

			results->info = ares->info;
			results->result = ares->aio.aio_return;
			results->flags = 0;

			ret++;
			results++;
			space--;

			slot = ares - the_aio->results;
			AN(bitf_set(the_aio->bitf, slot));
			VTAILQ_REMOVE(&ctx->ready, ares, list);
		}

		if (cb && results > rstart) {
			AZ(pthread_mutex_unlock(the_mtx));
			cb(priv, rstart, results - rstart);
			if (min >= ret || min == 0)
				goto out;
			AZ(pthread_mutex_lock(the_mtx));
		}

		if (min == 0)
			break;

		while (min > ret && VTAILQ_EMPTY(&ctx->ready)) {
			/* because there are outstanding requests from this ctx,
			 * (min = ctx->outstanding) above, there must be used
			 * slots
			 */
			assert(bitf_nset(the_aio->bitf) <
			      bitf_nbits(the_aio->bitf));

			assert(the_aio->waiting <= the_aio->inflight);
			if (the_aio->waiting == the_aio->inflight) {
				/* other threads already issued all aios */
				AZ(pthread_cond_wait(&ctx->cond, the_mtx));
				assert(! VTAILQ_EMPTY(&ctx->ready));
				break;
			}

			/*
			 * check for immediate completions under the lock to
			 * reduce lock turnover
			 */
			res = aiowait(timeo_zero);
			assert(res != aio_err);

			if (res == NULL) {
				the_aio->waiting++;

				AZ(pthread_mutex_unlock(the_mtx));
				res = aiowait(NULL);
				assert(res != NULL);
				assert(res != aio_err);
				AZ(pthread_mutex_lock(the_mtx));

				the_aio->waiting--;
			}

			// XXX check aio_return? would need to save size
			AZ(res->aio_errno);
			ares = (struct fellow_aio_result *)res;

			AN(ares);
			CHECK_OBJ_NOTNULL(ares->ctx, FELLOW_IO_AIO_CTX_MAGIC);

			the_aio->inflight--;

			VTAILQ_INSERT_TAIL(&ares->ctx->ready, ares, list);
			if (ares->ctx != ctx)
				AZ(pthread_cond_signal(&ares->ctx->cond));
		}
	}
	AZ(pthread_mutex_unlock(the_mtx));

  out:

	assert(ctx->outstanding >= ret);
	ctx->outstanding -= ret;

	return (ret);
}
