/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <assert.h>
#include <string.h>

#ifdef HAVE_XXHASH_H
#define MIN_XXH_VERSION 703
#include <xxhash.h>
#if XXH_VERSION_NUMBER < MIN_XXH_VERSION
#error "xxhash version unsupported"
#endif
static void __attribute__((constructor))
fh_assert_xxhash_version(void)
{
	assert(XXH_versionNumber() >= MIN_XXH_VERSION);
}
#undef MIN_XXH_VERSION
#endif

#include "fellow_sha256.h"
#include "fellow_hash.h"

static const size_t fh_len[FH_LIM] = {
	[FH_NONE]   = 0,
#define FH(up, lo, len)				\
	[FH_ ## up] = len,
#include "tbl/fellow_hash.tbl.h"
};

const char * const fh_name[FH_LIM] = {
	[FH_NONE]   = "none",
#define FH(up, lo, len)				\
	[FH_ ## up] = #lo,
#include "tbl/fellow_hash.tbl.h"
};

static void __attribute__((constructor))
fh_assert_lengths(void)
{
	//lint -e{708}
	union fh fhh = {{0}};

	(void)fhh;
#define FH(up, lo, len)				\
	assert(fh_len[FH_ ## up] == sizeof fhh.lo);
#include "tbl/fellow_hash.tbl.h"
}

#define FH_H_SHA256(d, p, l)   sha256(d, p, l)
#define FH_H_XXH32(d, p, l)    do { (d) = XXH32(p, l, 0); } while(0)
#define FH_H_XXH3_64(d, p, l)  do { (d) = XXH3_64bits(p, l); } while(0)
#define FH_H_XXH3_128(d, p, l) do {				\
		XXH128_hash_t h = XXH3_128bits(p, l);		\
		memcpy(d, &h, sizeof d);			\
	} while (0);

void
fh(uint8_t fht, union fh *fhh, const void *p, size_t l)
{
	//lint -e{506} Constant value Boolean (assert)
	//lint -e{525} negative indentation
	switch (fht) {
#define FH(up, lo, len)				\
	case FH_ ## up:				\
		FH_H_ ## up(fhh->lo, p, l);		\
		break;
#include "tbl/fellow_hash.tbl.h"
	default:
		assert(0 && "wrong hash type");
	}
}

#if XXH_VERSION_NUMBER >= 800
static inline int
xxh3_128_cmp(const uint8_t v[16], const void *p, size_t l)
{
	XXH128_hash_t h = XXH3_128bits(p, l);
	return (memcmp(v, &h, sizeof h));
}
#endif

#define FH_C_SHA256(v, p, l)   sha256cmp(v, p, l)
#define FH_C_XXH32(v, p, l)    ((v) != XXH32(p, l, 0))
#define FH_C_XXH3_64(v, p, l)  ((v) != XXH3_64bits(p, l))
#define FH_C_XXH3_128(v, p, l) xxh3_128_cmp(v, p, l)

int
fhcmp(uint8_t fht, const union fh *fhh, const void *p, size_t l)
{
	//lint -e{506} Constant value Boolean (assert)
	//lint -e{525} negative indentation
	switch (fht) {
#define FH(up, lo, len)				\
	case FH_ ## up:				\
		return(FH_C_ ## up(fhh->lo, p, l));
#include "tbl/fellow_hash.tbl.h"
	default:
		assert(0 && "wrong hash type");
	}
	// SunOS gcc says "control reaches...", flint says not
	//lint -e{527}
	return (-1);
}
