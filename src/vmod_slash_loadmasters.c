/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <string.h>
#include <stdlib.h>
//#include <limits.h>

#include <cache/cache.h>
#include <cache/cache_objhead.h>	// struct objhead
#include <storage/storage.h>
#include <vcl.h>

#include "vcc_slash_if.h" //lint -e763

/*
 * LOADMASTER COMMON
 * =================
 */
struct loadmaster_common {
	unsigned		magic;
#define LOADMASTER_COMMON_MAGIC	0xfc021a98
	struct stevedore	stv[1];

	unsigned		l;
	unsigned		n;
	VCL_STEVEDORE		*s;
};

static void
lc_init(struct loadmaster_common *lc, const struct stevedore *parent,
    void *priv, const char *name)
{
	AZ(lc->magic);
	lc->magic = LOADMASTER_COMMON_MAGIC;
	*lc->stv = *parent;
	lc->stv->ident = name;
	lc->stv->vclname = name;
	lc->stv->priv = priv;
}

static void
lc_fini(struct loadmaster_common *lc)
{
	CHECK_OBJ_NOTNULL(lc, LOADMASTER_COMMON_MAGIC);
}

static void
lc_add(struct loadmaster_common *lc, VCL_STEVEDORE stv)
{
	CHECK_OBJ_NOTNULL(lc, LOADMASTER_COMMON_MAGIC);
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);

	if (lc->n >= lc->l) {
		lc->l += 16;
		lc->s = realloc(lc->s, lc->l * sizeof *lc->s);
	}
	lc->s[lc->n++] = stv;
}

#define lc_name(lc) (lc)->stv->ident

static int
lc_allocobj(struct worker *wrk, struct loadmaster_common *lc,
    struct objcore *oc, unsigned wsl, unsigned n)
{
	VCL_STEVEDORE s;
	unsigned u, l;
	int r;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(lc, LOADMASTER_COMMON_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	if (lc->n == 0) {
		VSLb(wrk->vsl, SLT_Storage,
		    "loadmaster %s: no storages configured",
		    lc_name(lc));
		return (0);
	}

	n %= lc->n;
	l = n + lc->n;
	for (u = n; u < l; u++) {
		s = lc->s[u % lc->n];
		CHECK_OBJ_NOTNULL(s, STEVEDORE_MAGIC);
		r = s->allocobj(wrk, s, oc, wsl);
		if (r)
			return (r);
	}
	VSLb(wrk->vsl, SLT_Storage, "loadmaster %s: all %u storages failed",
	    lc_name(lc), lc->n);
	return (0);
}
/*
 * LOADMASTER RR
 * =============
 */

struct VPFX(slash_loadmaster_rr) {
	unsigned			magic;
#define LOADMASTER_RR_MAGIC		0x0df99520
	unsigned			next;
	struct loadmaster_common	lc[1];
};

static int v_matchproto_(storage_allocobj_f)
lm_rr_allocobj(struct worker *wrk, const struct stevedore *stv,
    struct objcore *oc, unsigned wsl)
{
	struct VPFX(slash_loadmaster_rr) *lm;
	struct loadmaster_common *lc;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(lm, stv->priv, LOADMASTER_RR_MAGIC);
	lc = lm->lc;
	CHECK_OBJ_NOTNULL(lc, LOADMASTER_COMMON_MAGIC);

	return (lc_allocobj(wrk, lc, oc, wsl, lm->next++));
}

//lint -e{785}
static const struct stevedore lm_rr_stv = {
	.magic		=	STEVEDORE_MAGIC,
	.name		=	"loadmaster round-robin",
	.allocobj	=	lm_rr_allocobj,
};

/*
 * LOADMASTER HASH
 * =============
 */

struct VPFX(slash_loadmaster_hash) {
	unsigned			magic;
#define LOADMASTER_HASH_MAGIC		0x7731f94d
	struct loadmaster_common	lc[1];
};

static int v_matchproto_(storage_allocobj_f)
lm_hash_allocobj(struct worker *wrk, const struct stevedore *stv,
    struct objcore *oc, unsigned wsl)
{
	struct VPFX(slash_loadmaster_hash) *lm;
	struct loadmaster_common *lc;
	struct objhead *oh;
	uint32_t n, *p;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(lm, stv->priv, LOADMASTER_HASH_MAGIC);
	lc = lm->lc;
	CHECK_OBJ_NOTNULL(lc, LOADMASTER_COMMON_MAGIC);

	oh = oc->objhead;
	AN(oh);
	AN(oh->digest);
	p = (void *)oh->digest;
	n = *p;

	return (lc_allocobj(wrk, lc, oc, wsl, n));
}

//lint -e{785}
static const struct stevedore lm_hash_stv = {
	.magic		=	STEVEDORE_MAGIC,
	.name		=	"loadmaster round-robin",
	.allocobj	=	lm_hash_allocobj,
};

/*
 * LOADMASTER VMOD GLUE CODE
 * =========================
 */

#define LM_GLUE(ll, UU)						\
VCL_VOID								\
vmod_loadmaster_ ## ll ## __init(VRT_CTX,				\
    struct VPFX(slash_loadmaster_ ## ll) **lmp,			\
    const char *vcl_name)						\
{									\
	struct VPFX(slash_loadmaster_ ## ll) *lm;			\
									\
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);				\
	AN(lmp);							\
	AZ(*lmp);							\
									\
	ALLOC_OBJ(lm, LOADMASTER_ ## UU ## _MAGIC);			\
	AN(lm);								\
	lc_init(lm->lc, &lm_ ## ll ## _stv, lm, vcl_name);		\
									\
	*lmp = lm;							\
}									\
									\
VCL_VOID								\
vmod_loadmaster_ ## ll ## __fini(					\
    struct VPFX(slash_loadmaster_ ## ll) **lmp)			\
{									\
	struct VPFX(slash_loadmaster_ ## ll) *lm;			\
									\
	TAKE_OBJ_NOTNULL(lm, lmp, LOADMASTER_ ## UU ## _MAGIC);		\
	lc_fini(lm->lc);						\
									\
	FREE_OBJ(lm);							\
}									\
									\
VCL_VOID								\
vmod_loadmaster_ ## ll ## _add_storage(VRT_CTX,				\
    struct VPFX(slash_loadmaster_ ## ll) *lm, VCL_STEVEDORE stv)	\
{									\
	struct loadmaster_common *lc;					\
									\
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);				\
	CHECK_OBJ_NOTNULL(lm, LOADMASTER_ ## UU ## _MAGIC);		\
	lc = lm->lc;							\
	CHECK_OBJ_NOTNULL(lc, LOADMASTER_COMMON_MAGIC);			\
									\
	assert(ctx->method == VCL_MET_INIT);				\
									\
	if (stv == NULL) {						\
		VRT_fail(ctx, "%s.add_storage() can not be used "	\
		    "with None storage", lc_name(lc));			\
		return;							\
	}								\
									\
	lc_add(lc, stv);						\
}									\
									\
VCL_STEVEDORE								\
vmod_loadmaster_ ## ll ## _storage(VRT_CTX,				\
    struct VPFX(slash_loadmaster_ ## ll) *lm)				\
{									\
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);				\
	CHECK_OBJ_NOTNULL(lm, LOADMASTER_ ## UU ## _MAGIC);		\
	CHECK_OBJ_NOTNULL(lm->lc, LOADMASTER_COMMON_MAGIC);		\
									\
	return (lm->lc->stv);						\
}									\
// END LM_GLUE(ll, UU)

LM_GLUE(rr, RR)
LM_GLUE(hash, HASH)
#undef LM_GLUE
